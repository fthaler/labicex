#ifndef LABICEX_LATTICE_THERMAL_COMMON_H
#define LABICEX_LATTICE_THERMAL_COMMON_H

#include <cassert>

#include "base/inline_math.h"
#include "lattice/numerical_equilibrium.h"
#include "lattice/tags.h"

namespace labicex {

namespace lattice {

namespace thermal {

template <class Lattice>
class equilibrium;

template <class Lattice>
class conserved_moments {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::grad_moments grad_moments;
  static const int count = 2 + Lattice::d;

  LABICEX_COMMON_INLINE conserved_moments(const fvector& f)
      : density_(0), momentum_(rvector::Zero()), pressure_trace_(0) {
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      density_ += f(i);
      momentum_ += f(i) * Lattice::cf(i);
      pressure_trace_ += f(i) * Lattice::cf2(i);
    }
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(real density, const rvector& momentum,
                                          real pressure_trace)
      : density_(density),
        momentum_(momentum),
        pressure_trace_(pressure_trace) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(const primitive_variables& variables)
      : density_(variables.density()),
        momentum_(variables.density() * variables.velocity()),
        pressure_trace_(variables.density() *
                        (Lattice::d * variables.temperature() +
                         (variables.velocity() * variables.velocity()).sum())) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(const grad_moments& grad)
      : density_(grad.density()),
        momentum_(grad.momentum()),
        pressure_trace_(grad.pressure_trace()) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments() {}

  LABICEX_COMMON_INLINE const real& density() const { return density_; }
  LABICEX_COMMON_INLINE const rvector& momentum() const { return momentum_; }
  LABICEX_COMMON_INLINE const real& pressure_trace() const {
    return pressure_trace_;
  }
  LABICEX_COMMON_INLINE real& density() { return density_; }
  LABICEX_COMMON_INLINE rvector& momentum() { return momentum_; }
  LABICEX_COMMON_INLINE real& pressure_trace() { return pressure_trace_; }

 private:
  LABICEX_COMMON_INLINE conserved_moments(real density, const rvector& momentum)
      : density_(density), momentum_(momentum) {}

  real density_;
  rvector momentum_;
  real pressure_trace_;
};

template <class Lattice>
class primitive_variables {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::conserved_moments conserved_moments;
  static const int count = 2 + Lattice::d;

  LABICEX_COMMON_INLINE primitive_variables(
      real density, const rvector& velocity,
      real temperature = Lattice::reference_temperature())
      : density_(density), velocity_(velocity), temperature_(temperature) {
    assert(density_ > 0);
    assert(temperature_ > 0);
  }

  LABICEX_COMMON_INLINE primitive_variables(const conserved_moments& moments)
      : density_(moments.density()),
        velocity_(moments.momentum() / moments.density()),
        temperature_((moments.pressure_trace() -
                      (moments.momentum() * moments.momentum()).sum() /
                          moments.density()) /
                     (Lattice::d * moments.density())) {
    assert(density_ > 0);
    assert(temperature_ > 0);
  }

  LABICEX_COMMON_INLINE primitive_variables() {}

  LABICEX_COMMON_INLINE const real& density() const { return density_; }
  LABICEX_COMMON_INLINE const rvector& velocity() const { return velocity_; }
  LABICEX_COMMON_INLINE const real& temperature() const { return temperature_; }
  LABICEX_COMMON_INLINE real& density() { return density_; }
  LABICEX_COMMON_INLINE rvector& velocity() { return velocity_; }
  LABICEX_COMMON_INLINE real& temperature() { return temperature_; }

 private:
  real density_;
  rvector velocity_;
  real temperature_;
};

}  // thermal

}  // lattice

}  // common

#endif  // LABICEX_LATTICE_THERMAL_COMMON_H

#ifndef LABICEX_LATTICE_THERMAL_D2Q25_H
#define LABICEX_LATTICE_THERMAL_D2Q25_H

#include "lattice/standard/d2q25.h"
#include "lattice/thermal/common.h"

namespace labicex {

namespace lattice {

namespace thermal {

struct d2q25 : standard::d2q25 {
  typedef thermal::conserved_moments<d2q25> conserved_moments;
  typedef lattice::grad_moments<d2q25> grad_moments;
  typedef thermal::primitive_variables<d2q25> primitive_variables;
  typedef thermal::equilibrium<d2q25> equilibrium;
  typedef lattice::numerical_equilibrium<d2q25> numerical_equilibrium;
  typedef lattice::grad_approximation<d2q25> grad_approximation;
  typedef thermal_tag lattice_category;

  LABICEX_COMMON_INLINE static real w(int i, real temperature) {
    assert(i >= 0 && i < q);

    const real t = temperature;
    const real t2 = t * t;
    const real t3 = t2 * t;

    const real a = 9 - 10 * t + 3 * t2;
    const real b = -3 + t;
    const real c = 1 - 3 * t;

    const real w00 = 0.012345679012345678 * a * a;
    const real w01 =
        0.020833333333333332 * t * (27 - 39 * t + 19 * t2 - 3 * t3);
    const real w11 = 0.03515625 * b * b * t2;
    const real w03 =
        0.0007716049382716049 * t * (-9 + 37 * t - 33 * t2 + 9 * t3);
    const real w13 = -0.0013020833333333333 * t2 * (3 - 10 * t + 3 * t2);
    const real w33 = 0.000048225308641975306 * c * c * t2;

    if (i == 0) return w00;
    if (i <= 4) return w01;
    if (i <= 8) return w11;
    if (i <= 12) return w03;
    if (i <= 20) return w13;
    return w33;
  }

  LABICEX_COMMON_INLINE static real w(int i, const primitive_variables& pv) {
    return w(i, pv.temperature());
  }
};

template <>
struct equilibrium<d2q25> {
  typedef typename d2q25::conserved_moments conserved_moments;
  typedef typename d2q25::primitive_variables primitive_variables;
  typedef typename d2q25::grad_moments grad_moments;
  typedef typename d2q25::rvector rvector;
  typedef typename d2q25::rtensor rtensor;
  typedef typename d2q25::ivector ivector;
  typedef typename d2q25::fvector fvector;

  LABICEX_COMMON_INLINE static fvector get(const primitive_variables& pv) {
    fvector f;
    const real& rho = pv.density();
    const real& ux = pv.velocity()(0);
    const real& uy = pv.velocity()(1);
    const real& t = pv.temperature();

    const real x0 = 1.0 / t;
    const real x1 = ipow(ux, 2);
    const real x2 = ipow(uy, 2);
    const real x3 = ipow(t, 2);
    const real x4 = 3.0 * x3;
    const real x5 = -10.0 * t + x4 + 9.0;
    asm_barrier(f);
    f(0) = -0.0061728395061728395062 * rho * x0 * ipow(x5, 2) *
           (-2.0 * t + x1 + x2);
    const real x6 = 1.0 / x3;
    const real x7 = t - 3.0;
    const real x8 = 0.0034722222222222222222 * rho * x5 * x6 * x7;
    const real x9 = 3.0 * t;
    const real x10 = ipow(ux, 3);
    const real x11 = x10 * x9;
    const real x12 = ux * x3;
    const real x13 = 6.0 * x12;
    const real x14 = -x13;
    const real x15 = x2 * x9;
    const real x16 = ux * x15;
    const real x17 = x11 + x14 + x16;
    const real x18 = ipow(t, 3);
    const real x19 = 6.0 * x18;
    const real x20 = -x10;
    const real x21 = x1 * x9;
    const real x22 = -x1 * x4;
    const real x23 = -x2 * x4;
    asm_barrier(f);
    f(1) = -x8 * (x17 + x19 + x20 + x21 + x22 + x23);
    const real x24 = -x11;
    const real x25 = -x16;
    asm_barrier(f);
    f(2) = -x8 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25);
    const real x26 = ipow(uy, 3);
    const real x27 = -x26;
    const real x28 = x26 * x9;
    const real x29 = uy * x3;
    const real x30 = 6.0 * x29;
    const real x31 = -x30;
    const real x32 = uy * x21;
    const real x33 = x15 + x19 + x22 + x23;
    asm_barrier(f);
    f(3) = -x8 * (x27 + x28 + x31 + x32 + x33);
    const real x34 = -x28;
    const real x35 = -x32;
    asm_barrier(f);
    f(4) = -x8 * (x26 + x30 + x33 + x34 + x35);
    const real x36 = 0.005859375 * rho * x0 * ipow(x7, 2);
    const real x37 = uy * x1;
    const real x38 = 3.0 * x37;
    const real x39 = x15 + x27 + x28 + x31 + x32 - x38;
    const real x40 = t * ux * uy;
    const real x41 = 6.0 * x40;
    const real x42 = ux * x2;
    const real x43 = 3.0 * x42;
    const real x44 = -x43;
    asm_barrier(f);
    f(5) =
        x36 * (x11 + x14 + x16 + x19 + x20 + x21 + x22 + x23 + x39 + x41 + x44);
    const real x45 = -x41;
    asm_barrier(f);
    f(6) = x36 * (x11 + x14 + x15 + x16 + x19 + x20 + x21 + x22 + x23 + x26 +
                  x30 + x34 + x35 + x38 + x44 + x45);
    asm_barrier(f);
    f(7) =
        x36 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 + x39 + x43 + x45);
    asm_barrier(f);
    f(8) = x36 * (x10 + x13 + x15 + x19 + x21 + x22 + x23 + x24 + x25 + x26 +
                  x30 + x34 + x35 + x38 + x41 + x43);
    const real x46 = x9 - 1.0;
    const real x47 = 0.00038580246913580246914 * rho * x46 * x5 * x6;
    const real x48 = 2.0 * x18;
    const real x49 = 9.0 * x10;
    const real x50 = -x49;
    const real x51 = t * x1;
    const real x52 = 9.0 * x51;
    const real x53 = -x1 * x3;
    const real x54 = -x2 * x3;
    asm_barrier(f);
    f(9) = x47 * (x17 + x48 + x50 + x52 + x53 + x54);
    asm_barrier(f);
    f(10) = x47 * (x13 + x24 + x25 + x48 + x49 + x52 + x53 + x54);
    const real x55 = 9.0 * x26;
    const real x56 = -x55;
    const real x57 = t * x2;
    const real x58 = 9.0 * x57;
    const real x59 = x48 + x53 + x54 + x58;
    asm_barrier(f);
    f(11) = x47 * (x28 + x31 + x32 + x56 + x59);
    asm_barrier(f);
    f(12) = x47 * (x30 + x34 + x35 + x55 + x59);
    const real x60 = 0.00021701388888888888889 * rho * x0 * x46 * x7;
    const real x61 = 27.0 * x10;
    const real x62 = -x61;
    const real x63 = 18.0 * x40;
    const real x64 = t * x49;
    const real x65 = 18.0 * x12;
    const real x66 = -x65;
    const real x67 = 9.0 * x42;
    const real x68 = -x67;
    const real x69 = ux * x58;
    const real x70 = 27.0 * x51;
    const real x71 = 27.0 * x37;
    const real x72 = -x71;
    const real x73 = x15 + x19 + x22 + x23 + x27 + x28 + x31 + x32 + x70 + x72;
    asm_barrier(f);
    f(13) = -x60 * (x62 + x63 + x64 + x66 + x68 + x69 + x73);
    const real x74 = -x63;
    asm_barrier(f);
    f(14) = -x60 * (x15 + x19 + x22 + x23 + x26 + x30 + x34 + x35 + x62 + x64 +
                    x66 + x68 + x69 + x70 + x71 + x74);
    const real x75 = -x64;
    const real x76 = -x69;
    asm_barrier(f);
    f(15) = -x60 * (x61 + x65 + x67 + x73 + x74 + x75 + x76);
    asm_barrier(f);
    f(16) = -x60 * (x15 + x19 + x22 + x23 + x26 + x30 + x34 + x35 + x61 + x63 +
                    x65 + x67 + x70 + x71 + x75 + x76);
    const real x77 = 27.0 * x26;
    const real x78 = -x77;
    const real x79 = t * x55;
    const real x80 = 18.0 * x29;
    const real x81 = -x80;
    const real x82 = 9.0 * x37;
    const real x83 = -x82;
    const real x84 = uy * x52;
    const real x85 = 27.0 * x57;
    const real x86 = 27.0 * x42;
    const real x87 = -x86;
    const real x88 = x11 + x14 + x16 + x19 + x20 + x21 + x22 + x23 + x85 + x87;
    asm_barrier(f);
    f(17) = -x60 * (x63 + x78 + x79 + x81 + x83 + x84 + x88);
    const real x89 = -x79;
    const real x90 = -x84;
    asm_barrier(f);
    f(18) = -x60 * (x74 + x77 + x80 + x82 + x88 + x89 + x90);
    asm_barrier(f);
    f(19) = -x60 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 + x74 + x78 +
                    x79 + x81 + x83 + x84 + x85 + x86);
    asm_barrier(f);
    f(20) = -x60 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 + x63 + x77 +
                    x80 + x82 + x85 + x86 + x89 + x90);
    const real x91 = 0.000024112654320987654321 * rho * x0 * ipow(x46, 2);
    asm_barrier(f);
    f(21) = x91 * (x11 + x14 + x16 + x28 + x31 + x32 + x48 + x50 + x52 + x53 +
                   x54 + x56 + x58 + x63 + x72 + x87);
    asm_barrier(f);
    f(22) = x91 * (x11 + x14 + x16 + x30 + x34 + x35 + x48 + x50 + x52 + x53 +
                   x54 + x55 + x58 + x71 + x74 + x87);
    const real x92 = x13 + x24 + x25 + x48 + x49 + x52 + x53 + x54 + x58 + x86;
    asm_barrier(f);
    f(23) = x91 * (x28 + x31 + x32 + x56 + x72 + x74 + x92);
    asm_barrier(f);
    f(24) = x91 * (x30 + x34 + x35 + x55 + x63 + x71 + x92);
    asm_barrier(f);

#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(f);
    assert(abs(pveq.density() - pv.density()) < 1e-5);
    assert((pveq.velocity() - pv.velocity()).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return f;
  }

  LABICEX_COMMON_INLINE static grad_moments axpy(const primitive_variables& pv,
                                                 real alpha, fvector& f) {
    real feq_density = 0;
    rvector feq_momentum = rvector::Zero();
    rtensor feq_pressure = rtensor::Zero();
    const real& rho = pv.density();
    const real& ux = pv.velocity()(0);
    const real& uy = pv.velocity()(1);
    const real& t = pv.temperature();

    const real x0 = 1.0 / t;
    const real x1 = ipow(ux, 2);
    const real x2 = ipow(uy, 2);
    const real x3 = ipow(t, 2);
    const real x4 = 3.0 * x3;
    const real x5 = -10.0 * t + x4 + 9.0;
    asm_barrier(f);
    const real feq0 = -0.0061728395061728395062 * rho * x0 * ipow(x5, 2) *
                      (-2.0 * t + x1 + x2);
    f(0) += alpha * feq0;
    feq_density += feq0;
    feq_momentum += feq0 * d2q25::cf(0);
    feq_pressure(0, 0) += feq0 * d2q25::cf(0)(0) * d2q25::cf(0)(0);
    feq_pressure(0, 1) += feq0 * d2q25::cf(0)(0) * d2q25::cf(0)(1);
    feq_pressure(1, 1) += feq0 * d2q25::cf(0)(1) * d2q25::cf(0)(1);
    const real x6 = 1.0 / x3;
    const real x7 = t - 3.0;
    const real x8 = 0.0034722222222222222222 * rho * x5 * x6 * x7;
    const real x9 = 3.0 * t;
    const real x10 = ipow(ux, 3);
    const real x11 = x10 * x9;
    const real x12 = ux * x3;
    const real x13 = 6.0 * x12;
    const real x14 = -x13;
    const real x15 = x2 * x9;
    const real x16 = ux * x15;
    const real x17 = x11 + x14 + x16;
    const real x18 = ipow(t, 3);
    const real x19 = 6.0 * x18;
    const real x20 = -x10;
    const real x21 = x1 * x9;
    const real x22 = -x1 * x4;
    const real x23 = -x2 * x4;
    asm_barrier(f);
    const real feq1 = -x8 * (x17 + x19 + x20 + x21 + x22 + x23);
    f(1) += alpha * feq1;
    feq_density += feq1;
    feq_momentum += feq1 * d2q25::cf(1);
    feq_pressure(0, 0) += feq1 * d2q25::cf(1)(0) * d2q25::cf(1)(0);
    feq_pressure(0, 1) += feq1 * d2q25::cf(1)(0) * d2q25::cf(1)(1);
    feq_pressure(1, 1) += feq1 * d2q25::cf(1)(1) * d2q25::cf(1)(1);
    const real x24 = -x11;
    const real x25 = -x16;
    asm_barrier(f);
    const real feq2 = -x8 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25);
    f(2) += alpha * feq2;
    feq_density += feq2;
    feq_momentum += feq2 * d2q25::cf(2);
    feq_pressure(0, 0) += feq2 * d2q25::cf(2)(0) * d2q25::cf(2)(0);
    feq_pressure(0, 1) += feq2 * d2q25::cf(2)(0) * d2q25::cf(2)(1);
    feq_pressure(1, 1) += feq2 * d2q25::cf(2)(1) * d2q25::cf(2)(1);
    const real x26 = ipow(uy, 3);
    const real x27 = -x26;
    const real x28 = x26 * x9;
    const real x29 = uy * x3;
    const real x30 = 6.0 * x29;
    const real x31 = -x30;
    const real x32 = uy * x21;
    const real x33 = x15 + x19 + x22 + x23;
    asm_barrier(f);
    const real feq3 = -x8 * (x27 + x28 + x31 + x32 + x33);
    f(3) += alpha * feq3;
    feq_density += feq3;
    feq_momentum += feq3 * d2q25::cf(3);
    feq_pressure(0, 0) += feq3 * d2q25::cf(3)(0) * d2q25::cf(3)(0);
    feq_pressure(0, 1) += feq3 * d2q25::cf(3)(0) * d2q25::cf(3)(1);
    feq_pressure(1, 1) += feq3 * d2q25::cf(3)(1) * d2q25::cf(3)(1);
    const real x34 = -x28;
    const real x35 = -x32;
    asm_barrier(f);
    const real feq4 = -x8 * (x26 + x30 + x33 + x34 + x35);
    f(4) += alpha * feq4;
    feq_density += feq4;
    feq_momentum += feq4 * d2q25::cf(4);
    feq_pressure(0, 0) += feq4 * d2q25::cf(4)(0) * d2q25::cf(4)(0);
    feq_pressure(0, 1) += feq4 * d2q25::cf(4)(0) * d2q25::cf(4)(1);
    feq_pressure(1, 1) += feq4 * d2q25::cf(4)(1) * d2q25::cf(4)(1);
    const real x36 = 0.005859375 * rho * x0 * ipow(x7, 2);
    const real x37 = uy * x1;
    const real x38 = 3.0 * x37;
    const real x39 = x15 + x27 + x28 + x31 + x32 - x38;
    const real x40 = t * ux * uy;
    const real x41 = 6.0 * x40;
    const real x42 = ux * x2;
    const real x43 = 3.0 * x42;
    const real x44 = -x43;
    asm_barrier(f);
    const real feq5 =
        x36 * (x11 + x14 + x16 + x19 + x20 + x21 + x22 + x23 + x39 + x41 + x44);
    f(5) += alpha * feq5;
    feq_density += feq5;
    feq_momentum += feq5 * d2q25::cf(5);
    feq_pressure(0, 0) += feq5 * d2q25::cf(5)(0) * d2q25::cf(5)(0);
    feq_pressure(0, 1) += feq5 * d2q25::cf(5)(0) * d2q25::cf(5)(1);
    feq_pressure(1, 1) += feq5 * d2q25::cf(5)(1) * d2q25::cf(5)(1);
    const real x45 = -x41;
    asm_barrier(f);
    const real feq6 = x36 * (x11 + x14 + x15 + x16 + x19 + x20 + x21 + x22 +
                             x23 + x26 + x30 + x34 + x35 + x38 + x44 + x45);
    f(6) += alpha * feq6;
    feq_density += feq6;
    feq_momentum += feq6 * d2q25::cf(6);
    feq_pressure(0, 0) += feq6 * d2q25::cf(6)(0) * d2q25::cf(6)(0);
    feq_pressure(0, 1) += feq6 * d2q25::cf(6)(0) * d2q25::cf(6)(1);
    feq_pressure(1, 1) += feq6 * d2q25::cf(6)(1) * d2q25::cf(6)(1);
    asm_barrier(f);
    const real feq7 =
        x36 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 + x39 + x43 + x45);
    f(7) += alpha * feq7;
    feq_density += feq7;
    feq_momentum += feq7 * d2q25::cf(7);
    feq_pressure(0, 0) += feq7 * d2q25::cf(7)(0) * d2q25::cf(7)(0);
    feq_pressure(0, 1) += feq7 * d2q25::cf(7)(0) * d2q25::cf(7)(1);
    feq_pressure(1, 1) += feq7 * d2q25::cf(7)(1) * d2q25::cf(7)(1);
    asm_barrier(f);
    const real feq8 = x36 * (x10 + x13 + x15 + x19 + x21 + x22 + x23 + x24 +
                             x25 + x26 + x30 + x34 + x35 + x38 + x41 + x43);
    f(8) += alpha * feq8;
    feq_density += feq8;
    feq_momentum += feq8 * d2q25::cf(8);
    feq_pressure(0, 0) += feq8 * d2q25::cf(8)(0) * d2q25::cf(8)(0);
    feq_pressure(0, 1) += feq8 * d2q25::cf(8)(0) * d2q25::cf(8)(1);
    feq_pressure(1, 1) += feq8 * d2q25::cf(8)(1) * d2q25::cf(8)(1);
    const real x46 = x9 - 1.0;
    const real x47 = 0.00038580246913580246914 * rho * x46 * x5 * x6;
    const real x48 = 2.0 * x18;
    const real x49 = 9.0 * x10;
    const real x50 = -x49;
    const real x51 = t * x1;
    const real x52 = 9.0 * x51;
    const real x53 = -x1 * x3;
    const real x54 = -x2 * x3;
    asm_barrier(f);
    const real feq9 = x47 * (x17 + x48 + x50 + x52 + x53 + x54);
    f(9) += alpha * feq9;
    feq_density += feq9;
    feq_momentum += feq9 * d2q25::cf(9);
    feq_pressure(0, 0) += feq9 * d2q25::cf(9)(0) * d2q25::cf(9)(0);
    feq_pressure(0, 1) += feq9 * d2q25::cf(9)(0) * d2q25::cf(9)(1);
    feq_pressure(1, 1) += feq9 * d2q25::cf(9)(1) * d2q25::cf(9)(1);
    asm_barrier(f);
    const real feq10 = x47 * (x13 + x24 + x25 + x48 + x49 + x52 + x53 + x54);
    f(10) += alpha * feq10;
    feq_density += feq10;
    feq_momentum += feq10 * d2q25::cf(10);
    feq_pressure(0, 0) += feq10 * d2q25::cf(10)(0) * d2q25::cf(10)(0);
    feq_pressure(0, 1) += feq10 * d2q25::cf(10)(0) * d2q25::cf(10)(1);
    feq_pressure(1, 1) += feq10 * d2q25::cf(10)(1) * d2q25::cf(10)(1);
    const real x55 = 9.0 * x26;
    const real x56 = -x55;
    const real x57 = t * x2;
    const real x58 = 9.0 * x57;
    const real x59 = x48 + x53 + x54 + x58;
    asm_barrier(f);
    const real feq11 = x47 * (x28 + x31 + x32 + x56 + x59);
    f(11) += alpha * feq11;
    feq_density += feq11;
    feq_momentum += feq11 * d2q25::cf(11);
    feq_pressure(0, 0) += feq11 * d2q25::cf(11)(0) * d2q25::cf(11)(0);
    feq_pressure(0, 1) += feq11 * d2q25::cf(11)(0) * d2q25::cf(11)(1);
    feq_pressure(1, 1) += feq11 * d2q25::cf(11)(1) * d2q25::cf(11)(1);
    asm_barrier(f);
    const real feq12 = x47 * (x30 + x34 + x35 + x55 + x59);
    f(12) += alpha * feq12;
    feq_density += feq12;
    feq_momentum += feq12 * d2q25::cf(12);
    feq_pressure(0, 0) += feq12 * d2q25::cf(12)(0) * d2q25::cf(12)(0);
    feq_pressure(0, 1) += feq12 * d2q25::cf(12)(0) * d2q25::cf(12)(1);
    feq_pressure(1, 1) += feq12 * d2q25::cf(12)(1) * d2q25::cf(12)(1);
    const real x60 = 0.00021701388888888888889 * rho * x0 * x46 * x7;
    const real x61 = 27.0 * x10;
    const real x62 = -x61;
    const real x63 = 18.0 * x40;
    const real x64 = t * x49;
    const real x65 = 18.0 * x12;
    const real x66 = -x65;
    const real x67 = 9.0 * x42;
    const real x68 = -x67;
    const real x69 = ux * x58;
    const real x70 = 27.0 * x51;
    const real x71 = 27.0 * x37;
    const real x72 = -x71;
    const real x73 = x15 + x19 + x22 + x23 + x27 + x28 + x31 + x32 + x70 + x72;
    asm_barrier(f);
    const real feq13 = -x60 * (x62 + x63 + x64 + x66 + x68 + x69 + x73);
    f(13) += alpha * feq13;
    feq_density += feq13;
    feq_momentum += feq13 * d2q25::cf(13);
    feq_pressure(0, 0) += feq13 * d2q25::cf(13)(0) * d2q25::cf(13)(0);
    feq_pressure(0, 1) += feq13 * d2q25::cf(13)(0) * d2q25::cf(13)(1);
    feq_pressure(1, 1) += feq13 * d2q25::cf(13)(1) * d2q25::cf(13)(1);
    const real x74 = -x63;
    asm_barrier(f);
    const real feq14 = -x60 * (x15 + x19 + x22 + x23 + x26 + x30 + x34 + x35 +
                               x62 + x64 + x66 + x68 + x69 + x70 + x71 + x74);
    f(14) += alpha * feq14;
    feq_density += feq14;
    feq_momentum += feq14 * d2q25::cf(14);
    feq_pressure(0, 0) += feq14 * d2q25::cf(14)(0) * d2q25::cf(14)(0);
    feq_pressure(0, 1) += feq14 * d2q25::cf(14)(0) * d2q25::cf(14)(1);
    feq_pressure(1, 1) += feq14 * d2q25::cf(14)(1) * d2q25::cf(14)(1);
    const real x75 = -x64;
    const real x76 = -x69;
    asm_barrier(f);
    const real feq15 = -x60 * (x61 + x65 + x67 + x73 + x74 + x75 + x76);
    f(15) += alpha * feq15;
    feq_density += feq15;
    feq_momentum += feq15 * d2q25::cf(15);
    feq_pressure(0, 0) += feq15 * d2q25::cf(15)(0) * d2q25::cf(15)(0);
    feq_pressure(0, 1) += feq15 * d2q25::cf(15)(0) * d2q25::cf(15)(1);
    feq_pressure(1, 1) += feq15 * d2q25::cf(15)(1) * d2q25::cf(15)(1);
    asm_barrier(f);
    const real feq16 = -x60 * (x15 + x19 + x22 + x23 + x26 + x30 + x34 + x35 +
                               x61 + x63 + x65 + x67 + x70 + x71 + x75 + x76);
    f(16) += alpha * feq16;
    feq_density += feq16;
    feq_momentum += feq16 * d2q25::cf(16);
    feq_pressure(0, 0) += feq16 * d2q25::cf(16)(0) * d2q25::cf(16)(0);
    feq_pressure(0, 1) += feq16 * d2q25::cf(16)(0) * d2q25::cf(16)(1);
    feq_pressure(1, 1) += feq16 * d2q25::cf(16)(1) * d2q25::cf(16)(1);
    const real x77 = 27.0 * x26;
    const real x78 = -x77;
    const real x79 = t * x55;
    const real x80 = 18.0 * x29;
    const real x81 = -x80;
    const real x82 = 9.0 * x37;
    const real x83 = -x82;
    const real x84 = uy * x52;
    const real x85 = 27.0 * x57;
    const real x86 = 27.0 * x42;
    const real x87 = -x86;
    const real x88 = x11 + x14 + x16 + x19 + x20 + x21 + x22 + x23 + x85 + x87;
    asm_barrier(f);
    const real feq17 = -x60 * (x63 + x78 + x79 + x81 + x83 + x84 + x88);
    f(17) += alpha * feq17;
    feq_density += feq17;
    feq_momentum += feq17 * d2q25::cf(17);
    feq_pressure(0, 0) += feq17 * d2q25::cf(17)(0) * d2q25::cf(17)(0);
    feq_pressure(0, 1) += feq17 * d2q25::cf(17)(0) * d2q25::cf(17)(1);
    feq_pressure(1, 1) += feq17 * d2q25::cf(17)(1) * d2q25::cf(17)(1);
    const real x89 = -x79;
    const real x90 = -x84;
    asm_barrier(f);
    const real feq18 = -x60 * (x74 + x77 + x80 + x82 + x88 + x89 + x90);
    f(18) += alpha * feq18;
    feq_density += feq18;
    feq_momentum += feq18 * d2q25::cf(18);
    feq_pressure(0, 0) += feq18 * d2q25::cf(18)(0) * d2q25::cf(18)(0);
    feq_pressure(0, 1) += feq18 * d2q25::cf(18)(0) * d2q25::cf(18)(1);
    feq_pressure(1, 1) += feq18 * d2q25::cf(18)(1) * d2q25::cf(18)(1);
    asm_barrier(f);
    const real feq19 = -x60 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 +
                               x74 + x78 + x79 + x81 + x83 + x84 + x85 + x86);
    f(19) += alpha * feq19;
    feq_density += feq19;
    feq_momentum += feq19 * d2q25::cf(19);
    feq_pressure(0, 0) += feq19 * d2q25::cf(19)(0) * d2q25::cf(19)(0);
    feq_pressure(0, 1) += feq19 * d2q25::cf(19)(0) * d2q25::cf(19)(1);
    feq_pressure(1, 1) += feq19 * d2q25::cf(19)(1) * d2q25::cf(19)(1);
    asm_barrier(f);
    const real feq20 = -x60 * (x10 + x13 + x19 + x21 + x22 + x23 + x24 + x25 +
                               x63 + x77 + x80 + x82 + x85 + x86 + x89 + x90);
    f(20) += alpha * feq20;
    feq_density += feq20;
    feq_momentum += feq20 * d2q25::cf(20);
    feq_pressure(0, 0) += feq20 * d2q25::cf(20)(0) * d2q25::cf(20)(0);
    feq_pressure(0, 1) += feq20 * d2q25::cf(20)(0) * d2q25::cf(20)(1);
    feq_pressure(1, 1) += feq20 * d2q25::cf(20)(1) * d2q25::cf(20)(1);
    const real x91 = 0.000024112654320987654321 * rho * x0 * ipow(x46, 2);
    asm_barrier(f);
    const real feq21 = x91 * (x11 + x14 + x16 + x28 + x31 + x32 + x48 + x50 +
                              x52 + x53 + x54 + x56 + x58 + x63 + x72 + x87);
    f(21) += alpha * feq21;
    feq_density += feq21;
    feq_momentum += feq21 * d2q25::cf(21);
    feq_pressure(0, 0) += feq21 * d2q25::cf(21)(0) * d2q25::cf(21)(0);
    feq_pressure(0, 1) += feq21 * d2q25::cf(21)(0) * d2q25::cf(21)(1);
    feq_pressure(1, 1) += feq21 * d2q25::cf(21)(1) * d2q25::cf(21)(1);
    asm_barrier(f);
    const real feq22 = x91 * (x11 + x14 + x16 + x30 + x34 + x35 + x48 + x50 +
                              x52 + x53 + x54 + x55 + x58 + x71 + x74 + x87);
    f(22) += alpha * feq22;
    feq_density += feq22;
    feq_momentum += feq22 * d2q25::cf(22);
    feq_pressure(0, 0) += feq22 * d2q25::cf(22)(0) * d2q25::cf(22)(0);
    feq_pressure(0, 1) += feq22 * d2q25::cf(22)(0) * d2q25::cf(22)(1);
    feq_pressure(1, 1) += feq22 * d2q25::cf(22)(1) * d2q25::cf(22)(1);
    const real x92 = x13 + x24 + x25 + x48 + x49 + x52 + x53 + x54 + x58 + x86;
    asm_barrier(f);
    const real feq23 = x91 * (x28 + x31 + x32 + x56 + x72 + x74 + x92);
    f(23) += alpha * feq23;
    feq_density += feq23;
    feq_momentum += feq23 * d2q25::cf(23);
    feq_pressure(0, 0) += feq23 * d2q25::cf(23)(0) * d2q25::cf(23)(0);
    feq_pressure(0, 1) += feq23 * d2q25::cf(23)(0) * d2q25::cf(23)(1);
    feq_pressure(1, 1) += feq23 * d2q25::cf(23)(1) * d2q25::cf(23)(1);
    asm_barrier(f);
    const real feq24 = x91 * (x30 + x34 + x35 + x55 + x63 + x71 + x92);
    f(24) += alpha * feq24;
    feq_density += feq24;
    feq_momentum += feq24 * d2q25::cf(24);
    feq_pressure(0, 0) += feq24 * d2q25::cf(24)(0) * d2q25::cf(24)(0);
    feq_pressure(0, 1) += feq24 * d2q25::cf(24)(0) * d2q25::cf(24)(1);
    feq_pressure(1, 1) += feq24 * d2q25::cf(24)(1) * d2q25::cf(24)(1);
    asm_barrier(f);
    feq_pressure(1, 0) = feq_pressure(0, 1);

    const grad_moments feq_gm(feq_density, feq_momentum, feq_pressure);
#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq_gm);
    assert(abs(pveq.density() - pv.density()) < 1e-5);
    assert((pveq.velocity() - pv.velocity()).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq_gm;
  }

  LABICEX_COMMON_INLINE static fvector gravity_difference(
      const primitive_variables& pv, const real g) {
    fvector f;
    const real& rho = pv.density();
    const real& ux = pv.velocity()(0);
    const real& uy = pv.velocity()(1);
    const real& t = pv.temperature();

    const real x0 = 1.0 / t;
    const real x1 = 2.0 * uy;
    const real x2 = g - x1;
    const real x3 = ipow(t, 2);
    const real x4 = -10.0 * t + 3.0 * x3 + 9.0;
    asm_barrier(f);
    f(0) = -0.0061728395061728395062 * g * rho * x0 * x2 * ipow(x4, 2);
    const real x5 = t - 3.0;
    const real x6 = 0.010416666666666666667 * g * rho * x0 * x2 * x4 * x5;
    asm_barrier(f);
    f(1) = x6 * (t - ux);
    asm_barrier(f);
    f(2) = x6 * (t + ux);
    const real x7 = 1.0 / x3;
    const real x8 = 0.0034722222222222222222 * g * rho * x4 * x5 * x7;
    const real x9 = 3.0 * t;
    const real x10 = g * x9;
    const real x11 = -x10;
    const real x12 = t * uy;
    const real x13 = 6.0 * x12;
    const real x14 = 3.0 * g;
    const real x15 = x14 * x3;
    const real x16 = 6.0 * x3;
    const real x17 = uy * x16;
    const real x18 = -x17;
    const real x19 = uy * x14;
    const real x20 = ipow(g, 2);
    const real x21 = -x20;
    const real x22 = -x16;
    const real x23 = ipow(uy, 2);
    const real x24 = 3.0 * x23;
    const real x25 = -x24;
    const real x26 = 9.0 * g;
    const real x27 = t * x26;
    const real x28 = uy * x27;
    const real x29 = -x28;
    const real x30 = x20 * x9;
    const real x31 = ipow(ux, 2);
    const real x32 = x31 * x9;
    const real x33 = 9.0 * t * x23;
    const real x34 = x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33;
    asm_barrier(f);
    f(3) = x8 * (x11 + x13 + x15 + x18 + x34);
    const real x35 = -x13;
    const real x36 = -x15;
    asm_barrier(f);
    f(4) = -x8 * (x10 + x17 + x34 + x35 + x36);
    const real x37 = 0.005859375 * g * rho * x0 * ipow(x5, 2);
    const real x38 = t * ux;
    const real x39 = 6.0 * x38;
    const real x40 = 3.0 * x31;
    const real x41 = -x40;
    const real x42 = x39 + x41;
    const real x43 = 3.0 * ux;
    const real x44 = g * x43;
    const real x45 = ux * uy;
    const real x46 = 6.0 * x45;
    const real x47 = -x46;
    const real x48 = ux * x10;
    const real x49 = -x48;
    const real x50 = uy * x39;
    asm_barrier(f);
    f(5) = -x37 * (x11 + x13 + x15 + x18 + x19 + x21 + x22 + x25 + x29 + x30 +
                   x32 + x33 + x42 + x44 + x47 + x49 + x50);
    const real x51 = -x44;
    const real x52 = -x50;
    asm_barrier(f);
    f(6) = x37 * (x10 + x17 + x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33 +
                  x35 + x36 + x42 + x46 + x48 + x51 + x52);
    const real x53 = -x19;
    const real x54 = -x30;
    const real x55 = -x32;
    const real x56 = -x33;
    asm_barrier(f);
    f(7) = x37 * (x10 + x16 + x17 + x20 + x24 + x28 + x35 + x36 + x39 + x40 +
                  x44 + x47 + x49 + x50 + x53 + x54 + x55 + x56);
    asm_barrier(f);
    f(8) = -x37 * (x11 + x13 + x15 + x16 + x18 + x20 + x24 + x28 + x39 + x40 +
                   x46 + x48 + x51 + x52 + x53 + x54 + x55 + x56);
    const real x57 = x9 - 1.0;
    const real x58 = 0.00038580246913580246914 * g * rho * x0 * x2 * x4 * x57;
    asm_barrier(f);
    f(9) = -x58 * (t - x43);
    asm_barrier(f);
    f(10) = -x58 * (t + x43);
    const real x59 = 0.00038580246913580246914 * g * rho * x4 * x57 * x7;
    const real x60 = -x27;
    const real x61 = 18.0 * x12;
    const real x62 = g * x3;
    const real x63 = x1 * x3;
    const real x64 = -x63;
    const real x65 = 27.0 * g;
    const real x66 = uy * x65;
    const real x67 = 9.0 * x20;
    const real x68 = -x67;
    const real x69 = 27.0 * x23;
    const real x70 = -x69;
    const real x71 = x22 + x29 + x30 + x32 + x33 + x66 + x68 + x70;
    asm_barrier(f);
    f(11) = -x59 * (x60 + x61 + x62 + x64 + x71);
    const real x72 = -x61;
    const real x73 = -x62;
    asm_barrier(f);
    f(12) = x59 * (x27 + x63 + x71 + x72 + x73);
    const real x74 = 0.00021701388888888888889 * g * rho * x0 * x5 * x57;
    const real x75 = 18.0 * x38;
    const real x76 = 27.0 * x31;
    const real x77 = x75 - x76;
    const real x78 = ux * x26;
    const real x79 = 18.0 * x45;
    const real x80 = -x79;
    const real x81 = ux * x27;
    const real x82 = -x81;
    const real x83 = uy * x75;
    asm_barrier(f);
    f(13) = x74 * (x11 + x13 + x15 + x18 + x19 + x21 + x22 + x25 + x29 + x30 +
                   x32 + x33 + x77 + x78 + x80 + x82 + x83);
    const real x84 = -x78;
    const real x85 = -x83;
    asm_barrier(f);
    f(14) = -x74 * (x10 + x17 + x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33 +
                    x35 + x36 + x77 + x79 + x81 + x84 + x85);
    asm_barrier(f);
    f(15) = -x74 * (x10 + x16 + x17 + x20 + x24 + x28 + x35 + x36 + x53 + x54 +
                    x55 + x56 + x75 + x76 + x78 + x80 + x82 + x83);
    asm_barrier(f);
    f(16) = x74 * (x11 + x13 + x15 + x16 + x18 + x20 + x24 + x28 + x53 + x54 +
                   x55 + x56 + x75 + x76 + x79 + x81 + x84 + x85);
    const real x86 = 0.00065104166666666666667 * g * rho * x0 * x5 * x57;
    const real x87 = g * x38;
    const real x88 = -x87;
    const real x89 = x1 * x38;
    asm_barrier(f);
    f(17) = x86 * (x22 + x29 + x30 + x32 + x33 + x39 + x41 + x60 + x61 + x62 +
                   x64 + x66 + x68 + x70 + x78 + x80 + x88 + x89);
    const real x90 = -x89;
    asm_barrier(f);
    f(18) = -x86 * (x22 + x27 + x29 + x30 + x32 + x33 + x42 + x63 + x66 + x68 +
                    x70 + x72 + x73 + x79 + x84 + x87 + x90);
    const real x91 = -x66;
    const real x92 =
        x16 + x27 + x28 + x54 + x55 + x56 + x63 + x67 + x69 + x72 + x73 + x91;
    asm_barrier(f);
    f(19) = -x86 * (x39 + x40 + x78 + x80 + x88 + x89 + x92);
    asm_barrier(f);
    f(20) = x86 * (x16 + x28 + x39 + x40 + x54 + x55 + x56 + x60 + x61 + x62 +
                   x64 + x67 + x69 + x79 + x84 + x87 + x90 + x91);
    const real x93 = 0.000024112654320987654321 * g * rho * x0 * ipow(x57, 2);
    const real x94 = ux * x65;
    const real x95 = 54.0 * x45;
    const real x96 = -x95;
    asm_barrier(f);
    f(21) = -x93 * (x22 + x29 + x30 + x32 + x33 + x49 + x50 + x60 + x61 + x62 +
                    x64 + x66 + x68 + x70 + x77 + x94 + x96);
    const real x97 = -x94;
    asm_barrier(f);
    f(22) = x93 * (x22 + x27 + x29 + x30 + x32 + x33 + x48 + x52 + x63 + x66 +
                   x68 + x70 + x72 + x73 + x77 + x95 + x97);
    asm_barrier(f);
    f(23) = x93 * (x49 + x50 + x75 + x76 + x92 + x94 + x96);
    asm_barrier(f);
    f(24) = -x93 * (x16 + x28 + x48 + x52 + x54 + x55 + x56 + x60 + x61 + x62 +
                    x64 + x67 + x69 + x75 + x76 + x91 + x95 + x97);
    asm_barrier(f);

    return f;
  }

  LABICEX_COMMON static void gravity_update(const primitive_variables& pv,
                                            const real g, fvector& f) {
    const real& rho = pv.density();
    const real& ux = pv.velocity()(0);
    const real& uy = pv.velocity()(1);
    const real& t = pv.temperature();

    const real x0 = 1.0 / t;
    const real x1 = 2.0 * uy;
    const real x2 = g - x1;
    const real x3 = ipow(t, 2);
    const real x4 = -10.0 * t + 3.0 * x3 + 9.0;
    asm_barrier(f);
    f(0) += -0.0061728395061728395062 * g * rho * x0 * x2 * ipow(x4, 2);
    const real x5 = t - 3.0;
    const real x6 = 0.010416666666666666667 * g * rho * x0 * x2 * x4 * x5;
    asm_barrier(f);
    f(1) += x6 * (t - ux);
    asm_barrier(f);
    f(2) += x6 * (t + ux);
    const real x7 = 1.0 / x3;
    const real x8 = 0.0034722222222222222222 * g * rho * x4 * x5 * x7;
    const real x9 = 3.0 * t;
    const real x10 = g * x9;
    const real x11 = -x10;
    const real x12 = t * uy;
    const real x13 = 6.0 * x12;
    const real x14 = 3.0 * g;
    const real x15 = x14 * x3;
    const real x16 = 6.0 * x3;
    const real x17 = uy * x16;
    const real x18 = -x17;
    const real x19 = uy * x14;
    const real x20 = ipow(g, 2);
    const real x21 = -x20;
    const real x22 = -x16;
    const real x23 = ipow(uy, 2);
    const real x24 = 3.0 * x23;
    const real x25 = -x24;
    const real x26 = 9.0 * g;
    const real x27 = t * x26;
    const real x28 = uy * x27;
    const real x29 = -x28;
    const real x30 = x20 * x9;
    const real x31 = ipow(ux, 2);
    const real x32 = x31 * x9;
    const real x33 = 9.0 * t * x23;
    const real x34 = x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33;
    asm_barrier(f);
    f(3) += x8 * (x11 + x13 + x15 + x18 + x34);
    const real x35 = -x13;
    const real x36 = -x15;
    asm_barrier(f);
    f(4) += -x8 * (x10 + x17 + x34 + x35 + x36);
    const real x37 = 0.005859375 * g * rho * x0 * ipow(x5, 2);
    const real x38 = t * ux;
    const real x39 = 6.0 * x38;
    const real x40 = 3.0 * x31;
    const real x41 = -x40;
    const real x42 = x39 + x41;
    const real x43 = 3.0 * ux;
    const real x44 = g * x43;
    const real x45 = ux * uy;
    const real x46 = 6.0 * x45;
    const real x47 = -x46;
    const real x48 = ux * x10;
    const real x49 = -x48;
    const real x50 = uy * x39;
    asm_barrier(f);
    f(5) += -x37 * (x11 + x13 + x15 + x18 + x19 + x21 + x22 + x25 + x29 + x30 +
                    x32 + x33 + x42 + x44 + x47 + x49 + x50);
    const real x51 = -x44;
    const real x52 = -x50;
    asm_barrier(f);
    f(6) += x37 * (x10 + x17 + x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33 +
                   x35 + x36 + x42 + x46 + x48 + x51 + x52);
    const real x53 = -x19;
    const real x54 = -x30;
    const real x55 = -x32;
    const real x56 = -x33;
    asm_barrier(f);
    f(7) += x37 * (x10 + x16 + x17 + x20 + x24 + x28 + x35 + x36 + x39 + x40 +
                   x44 + x47 + x49 + x50 + x53 + x54 + x55 + x56);
    asm_barrier(f);
    f(8) += -x37 * (x11 + x13 + x15 + x16 + x18 + x20 + x24 + x28 + x39 + x40 +
                    x46 + x48 + x51 + x52 + x53 + x54 + x55 + x56);
    const real x57 = x9 - 1.0;
    const real x58 = 0.00038580246913580246914 * g * rho * x0 * x2 * x4 * x57;
    asm_barrier(f);
    f(9) += -x58 * (t - x43);
    asm_barrier(f);
    f(10) += -x58 * (t + x43);
    const real x59 = 0.00038580246913580246914 * g * rho * x4 * x57 * x7;
    const real x60 = -x27;
    const real x61 = 18.0 * x12;
    const real x62 = g * x3;
    const real x63 = x1 * x3;
    const real x64 = -x63;
    const real x65 = 27.0 * g;
    const real x66 = uy * x65;
    const real x67 = 9.0 * x20;
    const real x68 = -x67;
    const real x69 = 27.0 * x23;
    const real x70 = -x69;
    const real x71 = x22 + x29 + x30 + x32 + x33 + x66 + x68 + x70;
    asm_barrier(f);
    f(11) += -x59 * (x60 + x61 + x62 + x64 + x71);
    const real x72 = -x61;
    const real x73 = -x62;
    asm_barrier(f);
    f(12) += x59 * (x27 + x63 + x71 + x72 + x73);
    const real x74 = 0.00021701388888888888889 * g * rho * x0 * x5 * x57;
    const real x75 = 18.0 * x38;
    const real x76 = 27.0 * x31;
    const real x77 = x75 - x76;
    const real x78 = ux * x26;
    const real x79 = 18.0 * x45;
    const real x80 = -x79;
    const real x81 = ux * x27;
    const real x82 = -x81;
    const real x83 = uy * x75;
    asm_barrier(f);
    f(13) += x74 * (x11 + x13 + x15 + x18 + x19 + x21 + x22 + x25 + x29 + x30 +
                    x32 + x33 + x77 + x78 + x80 + x82 + x83);
    const real x84 = -x78;
    const real x85 = -x83;
    asm_barrier(f);
    f(14) += -x74 * (x10 + x17 + x19 + x21 + x22 + x25 + x29 + x30 + x32 + x33 +
                     x35 + x36 + x77 + x79 + x81 + x84 + x85);
    asm_barrier(f);
    f(15) += -x74 * (x10 + x16 + x17 + x20 + x24 + x28 + x35 + x36 + x53 + x54 +
                     x55 + x56 + x75 + x76 + x78 + x80 + x82 + x83);
    asm_barrier(f);
    f(16) += x74 * (x11 + x13 + x15 + x16 + x18 + x20 + x24 + x28 + x53 + x54 +
                    x55 + x56 + x75 + x76 + x79 + x81 + x84 + x85);
    const real x86 = 0.00065104166666666666667 * g * rho * x0 * x5 * x57;
    const real x87 = g * x38;
    const real x88 = -x87;
    const real x89 = x1 * x38;
    asm_barrier(f);
    f(17) += x86 * (x22 + x29 + x30 + x32 + x33 + x39 + x41 + x60 + x61 + x62 +
                    x64 + x66 + x68 + x70 + x78 + x80 + x88 + x89);
    const real x90 = -x89;
    asm_barrier(f);
    f(18) += -x86 * (x22 + x27 + x29 + x30 + x32 + x33 + x42 + x63 + x66 + x68 +
                     x70 + x72 + x73 + x79 + x84 + x87 + x90);
    const real x91 = -x66;
    const real x92 =
        x16 + x27 + x28 + x54 + x55 + x56 + x63 + x67 + x69 + x72 + x73 + x91;
    asm_barrier(f);
    f(19) += -x86 * (x39 + x40 + x78 + x80 + x88 + x89 + x92);
    asm_barrier(f);
    f(20) += x86 * (x16 + x28 + x39 + x40 + x54 + x55 + x56 + x60 + x61 + x62 +
                    x64 + x67 + x69 + x79 + x84 + x87 + x90 + x91);
    const real x93 = 0.000024112654320987654321 * g * rho * x0 * ipow(x57, 2);
    const real x94 = ux * x65;
    const real x95 = 54.0 * x45;
    const real x96 = -x95;
    asm_barrier(f);
    f(21) += -x93 * (x22 + x29 + x30 + x32 + x33 + x49 + x50 + x60 + x61 + x62 +
                     x64 + x66 + x68 + x70 + x77 + x94 + x96);
    const real x97 = -x94;
    asm_barrier(f);
    f(22) += x93 * (x22 + x27 + x29 + x30 + x32 + x33 + x48 + x52 + x63 + x66 +
                    x68 + x70 + x72 + x73 + x77 + x95 + x97);
    asm_barrier(f);
    f(23) += x93 * (x49 + x50 + x75 + x76 + x92 + x94 + x96);
    asm_barrier(f);
    f(24) += -x93 * (x16 + x28 + x48 + x52 + x54 + x55 + x56 + x60 + x61 + x62 +
                     x64 + x67 + x69 + x75 + x76 + x91 + x95 + x97);
    asm_barrier(f);
  }
};

}  // thermal

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_THERMAL_D2Q25_H

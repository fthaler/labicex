#ifndef LABICEX_LATTICE_THERMAL_D2Q9_H
#define LABICEX_LATTICE_THERMAL_D2Q9_H

#include "lattice/standard/d2q9.h"
#include "lattice/thermal/common.h"

namespace labicex {

namespace lattice {

namespace thermal {

struct d2q9 : standard::d2q9 {
  typedef thermal::conserved_moments<d2q9> conserved_moments;
  typedef lattice::grad_moments<d2q9> grad_moments;
  typedef thermal::primitive_variables<d2q9> primitive_variables;
  typedef thermal::equilibrium<d2q9> equilibrium;
  typedef lattice::numerical_equilibrium<d2q9> numerical_equilibrium;
  typedef lattice::grad_approximation<d2q9> grad_approximation;
  typedef thermal_tag lattice_category;

  LABICEX_COMMON_INLINE static real w(int i, real temperature) {
    assert(i >= 0 && i < q);
    const real a = (1 - temperature);
    const real a2 = a * a;
    if (i == 0) return a2;
    const real b = temperature / (2 * a);
    if (i <= 4) return a2 * b;
    return a2 * b * b;
  }

  LABICEX_COMMON_INLINE static real w(int i, const primitive_variables& pv) {
    return w(i, pv.temperature());
  }
};

template <>
struct equilibrium<d2q9> {
  typedef typename d2q9::conserved_moments conserved_moments;
  typedef typename d2q9::grad_moments grad_moments;
  typedef typename d2q9::primitive_variables primitive_variables;
  typedef typename d2q9::rvector rvector;
  typedef typename d2q9::rtensor rtensor;
  typedef typename d2q9::ivector ivector;
  typedef typename d2q9::fvector fvector;

  LABICEX_COMMON static fvector get(const primitive_variables& pv) {
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    const real& temperature = pv.temperature();
    const rvector velocity2 = velocity * velocity;
    fvector feq;
    LABICEX_UNROLL
    for (int i = 0; i < d2q9::q; ++i) {
      const rvector ci = d2q9::cf(i);
      const rvector ci2 = ci * ci;
      const ivector ci2i = d2q9::ci(i) * d2q9::ci(i);
      rvector tci2;
      LABICEX_UNROLL
      for (int j = 0; j < d2q9::d; ++j) tci2(j) = ipow(real(2), ci2i(j));
      const rvector a = (1 - 2 * ci2) / tci2;
      const rvector b = ci2 - 1 + ci * velocity + velocity2 + temperature;
      const real feqi = density * (a * b).prod();
      assert(!isnan(feqi));
      feq(i) = feqi;
    }
#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq);
    assert(abs(pveq.density() - density) < 1e-5);
    assert((pveq.velocity() - velocity).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq;
  }

  LABICEX_COMMON static grad_moments axpy(const primitive_variables& pv,
                                          real alpha, fvector& f) {
    real feq_density = 0;
    rvector feq_momentum = rvector::Zero();
    rtensor feq_pressure = rtensor::Zero();
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    const real& temperature = pv.temperature();
    const rvector velocity2 = velocity * velocity;
    LABICEX_UNROLL
    for (int i = 0; i < d2q9::q; ++i) {
      const rvector ci = d2q9::cf(i);
      const rvector ci2 = ci * ci;
      const ivector ci2i = d2q9::ci(i) * d2q9::ci(i);
      rvector tci2;
      LABICEX_UNROLL
      for (int j = 0; j < d2q9::d; ++j) tci2(j) = ipow(real(2), ci2i(j));
      const rvector a = (1 - 2 * ci2) / tci2;
      const rvector b = ci2 - 1 + ci * velocity + velocity2 + temperature;
      const real feqi = density * (a * b).prod();
      assert(!isnan(feqi));

      f(i) += alpha * feqi;
      feq_density += feqi;
      feq_momentum += feqi * d2q9::cf(i);
      LABICEX_UNROLL
      for (int a = 0; a < d2q9::d; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < d2q9::d; ++b)
          feq_pressure(a, b) += feqi * d2q9::cf(i)(a) * d2q9::cf(i)(b);
      }
    }
    const grad_moments feq_gm(feq_density, feq_momentum, feq_pressure);
#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq_gm);
    assert(abs(pveq.density() - density) < 1e-5);
    assert((pveq.velocity() - velocity).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq_gm;
  }

  LABICEX_COMMON static fvector gravity_difference(
      const primitive_variables& pv, const real g) {
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    const real& temperature = pv.temperature();
    fvector f;
    LABICEX_UNROLL
    for (int i = 0; i < d2q9::q; ++i) {
      const rvector ci = d2q9::cf(i);
      const rvector ci2 = ci * ci;
      const ivector ci2i = d2q9::ci(i) * d2q9::ci(i);
      rvector tci2;
      LABICEX_UNROLL
      for (int j = 0; j < d2q9::d; ++j) tci2(j) = ipow(real(2), ci2i(j));
      const rvector a = (1 - 2 * ci2) / tci2;
      const rvector b(
          ci2(0) - 1 + ci(0) * velocity(0) + velocity(0) + temperature,
          ci(1) - g + 2 * velocity(1));
      const real fi = -g * density * (a * b).prod();
      assert(!isnan(fi));
      f(i) = fi;
    }
    return f;
  }

  LABICEX_COMMON static void gravity_update(const primitive_variables& pv,
                                            const real g, fvector& f) {
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    const real& temperature = pv.temperature();
    LABICEX_UNROLL
    for (int i = 0; i < d2q9::q; ++i) {
      const rvector ci = d2q9::cf(i);
      const rvector ci2 = ci * ci;
      const ivector ci2i = d2q9::ci(i) * d2q9::ci(i);
      rvector tci2;
      LABICEX_UNROLL
      for (int j = 0; j < d2q9::d; ++j) tci2(j) = ipow(real(2), ci2i(j));
      const rvector a = (1 - 2 * ci2) / tci2;
      const rvector b(
          ci2(0) - 1 + ci(0) * velocity(0) + velocity(0) + temperature,
          ci(1) - g + 2 * velocity(1));
      const real fi = -g * density * (a * b).prod();
      assert(!isnan(fi));
      f(i) += fi;
    }
  }
};

}  // thermal

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_THERMAL_D2Q9_H

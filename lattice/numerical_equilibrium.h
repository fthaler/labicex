#ifndef LABICEX_LATTICE_NUMERICAL_EQUILIBRIUM_H
#define LABICEX_LATTICE_NUMERICAL_EQUILIBRIUM_H

namespace labicex {

namespace lattice {

template <class Lattice, bool UseMaxwellP = true>
struct numerical_equilibrium;

template <class Lattice>
struct numerical_equilibrium<Lattice, false> {
  typedef typename Lattice::conserved_moments conserved_moments;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  static const int n = Lattice::d + 2;
  static const int momentum_offset = 1;
  static const int density_offset = momentum_offset + Lattice::d;
  typedef typename vector_types<n>::rvector svector;
  typedef typename vector_types<n>::rtensor smatrix;

  LABICEX_COMMON static fvector get(const primitive_variables& pv) {
    svector lagrange = svector::Zero();
    int iteration = 0;
    real lambda = 1;
    do {
      svector gfeq;
      gfeq(0) = 0;
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        gfeq(0) -= pv.density() * pv.temperature() +
                   pv.density() * (pv.velocity()(a) * pv.velocity()(a));
        gfeq(momentum_offset + a) = -pv.density() * pv.velocity()(a);
      }
      gfeq(density_offset) = -pv.density();

      smatrix gfeq_jacobian = smatrix::Zero();
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        real lagrange_sum = 0;
        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          assert(!isnan(lagrange(j)));
          lagrange_sum += v(i, j) * lagrange(j);
        }

        const real feqi = pv.density() * Lattice::w(i, pv) * exp(lagrange_sum);
        assert(feqi > 0);
        assert(!isnan(feqi));

        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          gfeq(j) += v(i, j) * feqi;
        }

        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          LABICEX_UNROLL
          for (int k = j; k < n; ++k)
            gfeq_jacobian(j, k) += v(i, j) * v(i, k) * feqi;
        }
      }

      if (gfeq.matrix().norm() < 1e-7) break;

#ifdef LABICEX_FULLDEBUG
      smatrix gfeq_jacobian_copy = gfeq_jacobian;
      const svector gfeq_copy = gfeq;
      LABICEX_UNROLL
      for (int a = 1; a < n; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b)
          gfeq_jacobian_copy(a, b) = gfeq_jacobian_copy(b, a);
      }
#endif  // LABICEX_FULLDEBUG

      // in-place LDL^T decomposition
      svector ldl_d;
      LABICEX_UNROLL
      for (int a = 0; a < n; ++a) {
        ldl_d(a) = gfeq_jacobian(a, a);
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b) {
          gfeq_jacobian(b, a) = gfeq_jacobian(b, a);
          LABICEX_UNROLL
          for (int c = 0; c < b; ++c)
            gfeq_jacobian(b, a) -=
                gfeq_jacobian(c, a) * gfeq_jacobian(c, b) * ldl_d(c);
          gfeq_jacobian(b, a) /= ldl_d(b);
          ldl_d(a) -= gfeq_jacobian(b, a) * gfeq_jacobian(b, a) * ldl_d(b);
        }
      }

      // solve
      LABICEX_UNROLL
      for (int a = 1; a < n; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b) gfeq(a) -= gfeq_jacobian(b, a) * gfeq(b);
      }
      gfeq /= ldl_d;
      LABICEX_UNROLL
      for (int a = n - 2; a >= 0; --a) {
        LABICEX_UNROLL
        for (int b = a + 1; b < n; ++b)
          gfeq(a) -= gfeq_jacobian(a, b) * gfeq(b);
        assert(!isnan(gfeq(a)));
      }

#ifdef LABICEX_FULLDEBUG
      assert((gfeq_jacobian_copy.matrix() * gfeq.matrix().transpose() -
              gfeq_copy.matrix().transpose())
                 .norm() < 1e-5);
#endif  // LABICEX_FULLDEBUG

      lagrange -= lambda * gfeq;

      // use damping if there is no convergence
      if (iteration % 10 == 9) lambda *= 0.9;

      if (gfeq.matrix().norm() < 1e-7) break;
    } while (++iteration < 1000);
    assert(iteration < 1000);

    fvector feq;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      real lagrange_sum = 0;
      LABICEX_UNROLL
      for (int j = 0; j < n; ++j) {
        assert(!isnan(lagrange(j)));
        lagrange_sum += v(i, j) * lagrange(j);
      }
      feq(i) = pv.density() * Lattice::w(i, pv) * exp(lagrange_sum);
      assert(feq(i) > 0);
      assert(!isnan(feq(i)));
    }

#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq);
    assert(abs(pveq.density() - pv.density()) < 1e-5);
    assert((pveq.velocity() - pv.velocity()).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq;
  }

 private:
  // helper function to get the lagrange multipliers' factors
  LABICEX_COMMON_INLINE static real v(int i, int l) {
    assert(l >= 0 && l < n);
    if (Lattice::d == 1) {
      assert(n == 3 && momentum_offset == 1 && density_offset == 2);
      if (l == 0) return Lattice::cf2(i);
      if (l == 1) return Lattice::cf(i)(0);
      return 1;
    } else if (Lattice::d == 2) {
      assert(n == 4 && momentum_offset == 1 && density_offset == 3);
      if (l == 0) return Lattice::cf2(i);
      if (l == 1) return Lattice::cf(i)(0);
      if (l == 2) return Lattice::cf(i)(1);
      return 1;
    } else {
      assert(n == 5 && momentum_offset == 1 && density_offset == 4);
      if (l == 0) return Lattice::cf2(i);
      if (l == 1) return Lattice::cf(i)(0);
      if (l == 2) return Lattice::cf(i)(1);
      if (l == 3) return Lattice::cf(i)(2);
      return 1;
    }
  }
};

template <class Lattice>
struct numerical_equilibrium<Lattice, true> {
  typedef typename Lattice::conserved_moments conserved_moments;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  static const int n =
      (Lattice::d * Lattice::d + Lattice::d) / 2 + Lattice::d + 1;
  static const int momentum_offset = (Lattice::d * Lattice::d + Lattice::d) / 2;
  static const int density_offset = momentum_offset + Lattice::d;
  typedef typename vector_types<n>::rvector svector;
  typedef typename vector_types<n>::rtensor smatrix;

  LABICEX_COMMON static fvector get(const primitive_variables& pv) {
    svector lagrange = svector::Zero();
    int iteration = 0;
    real lambda = 1;
    do {
      svector gfeq;
      LABICEX_UNROLL
      for (int a = 0, idx = 0; a < Lattice::d; ++a) {
        LABICEX_UNROLL
        for (int b = a; b < Lattice::d; ++b) {
          gfeq(idx++) = -pv.density() * pv.temperature() * delta(a, b) -
                        pv.density() * (pv.velocity()(a) * pv.velocity()(b));
        }
        gfeq(momentum_offset + a) = -pv.density() * pv.velocity()(a);
      }
      gfeq(density_offset) = -pv.density();

      smatrix gfeq_jacobian = smatrix::Zero();
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        real lagrange_sum = 0;
        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          assert(!isnan(lagrange(j)));
          lagrange_sum += v(i, j) * lagrange(j);
        }

        const real feqi = pv.density() * Lattice::w(i, pv) * exp(lagrange_sum);
        assert(feqi > 0);
        assert(!isnan(feqi));

        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          gfeq(j) += v(i, j) * feqi;
        }

        LABICEX_UNROLL
        for (int j = 0; j < n; ++j) {
          LABICEX_UNROLL
          for (int k = j; k < n; ++k)
            gfeq_jacobian(j, k) += v(i, j) * v(i, k) * feqi;
        }
      }

      if (gfeq.matrix().norm() < 1e-7) break;

#ifdef LABICEX_FULLDEBUG
      smatrix gfeq_jacobian_copy = gfeq_jacobian;
      const svector gfeq_copy = gfeq;
      LABICEX_UNROLL
      for (int a = 1; a < n; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b)
          gfeq_jacobian_copy(a, b) = gfeq_jacobian_copy(b, a);
      }
#endif  // LABICEX_FULLDEBUG

      // in-place LDL^T decomposition
      svector ldl_d;
      LABICEX_UNROLL
      for (int a = 0; a < n; ++a) {
        ldl_d(a) = gfeq_jacobian(a, a);
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b) {
          gfeq_jacobian(b, a) = gfeq_jacobian(b, a);
          LABICEX_UNROLL
          for (int c = 0; c < b; ++c)
            gfeq_jacobian(b, a) -=
                gfeq_jacobian(c, a) * gfeq_jacobian(c, b) * ldl_d(c);
          gfeq_jacobian(b, a) /= ldl_d(b);
          ldl_d(a) -= gfeq_jacobian(b, a) * gfeq_jacobian(b, a) * ldl_d(b);
        }
      }

      // solve
      LABICEX_UNROLL
      for (int a = 1; a < n; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < a; ++b) gfeq(a) -= gfeq_jacobian(b, a) * gfeq(b);
      }
      gfeq /= ldl_d;
      LABICEX_UNROLL
      for (int a = n - 2; a >= 0; --a) {
        LABICEX_UNROLL
        for (int b = a + 1; b < n; ++b)
          gfeq(a) -= gfeq_jacobian(a, b) * gfeq(b);
        assert(!isnan(gfeq(a)));
      }

#ifdef LABICEX_FULLDEBUG
      assert((gfeq_jacobian_copy.matrix() * gfeq.matrix().transpose() -
              gfeq_copy.matrix().transpose())
                 .norm() < 1e-5);
#endif  // LABICEX_FULLDEBUG

      lagrange -= lambda * gfeq;

      // use damping if there is no convergence
      if (iteration % 10 == 9) lambda *= 0.9;

      if (gfeq.matrix().norm() < 1e-7) break;
    } while (++iteration < 1000);
    assert(iteration < 1000);

    fvector feq;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      real lagrange_sum = 0;
      LABICEX_UNROLL
      for (int j = 0; j < n; ++j) {
        assert(!isnan(lagrange(j)));
        lagrange_sum += v(i, j) * lagrange(j);
      }
      feq(i) = pv.density() * Lattice::w(i, pv) * exp(lagrange_sum);
      assert(feq(i) > 0);
      assert(!isnan(feq(i)));
    }

#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq);
    assert(abs(pveq.density() - pv.density()) < 1e-5);
    assert((pveq.velocity() - pv.velocity()).matrix().norm() < 1e-5);
    assert(abs(pveq.temperature() - pv.temperature()) < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq;
  }

 private:
  // helper function to get the lagrange multipliers' factors
  LABICEX_COMMON_INLINE static real v(int i, int l) {
    assert(l >= 0 && l < n);
    if (Lattice::d == 1) {
      assert(n == 3 && momentum_offset == 1 && density_offset == 2);
      if (l == 0) return Lattice::cf(i)(0) * Lattice::cf(i)(0);
      if (l == 1) return Lattice::cf(i)(0);
      return 1;
    } else if (Lattice::d == 2) {
      assert(n == 6 && momentum_offset == 3 && density_offset == 5);
      if (l == 0) return Lattice::cf(i)(0) * Lattice::cf(i)(0);
      if (l == 1) return Lattice::cf(i)(0) * Lattice::cf(i)(1);
      if (l == 2) return Lattice::cf(i)(1) * Lattice::cf(i)(1);
      if (l == 3) return Lattice::cf(i)(0);
      if (l == 4) return Lattice::cf(i)(1);
      return 1;
    } else {
      assert(n == 10 && momentum_offset == 6 && density_offset == 9);
      if (l == 0) return Lattice::cf(i)(0) * Lattice::cf(i)(0);
      if (l == 1) return Lattice::cf(i)(0) * Lattice::cf(i)(1);
      if (l == 2) return Lattice::cf(i)(0) * Lattice::cf(i)(2);
      if (l == 3) return Lattice::cf(i)(1) * Lattice::cf(i)(1);
      if (l == 4) return Lattice::cf(i)(1) * Lattice::cf(i)(2);
      if (l == 5) return Lattice::cf(i)(2) * Lattice::cf(i)(2);
      if (l == 6) return Lattice::cf(i)(0);
      if (l == 7) return Lattice::cf(i)(1);
      if (l == 8) return Lattice::cf(i)(2);
      return 1;
    }
  }
};

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_NUMERICAL_EQUILIBRIUM_H

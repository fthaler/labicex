#ifndef LABICEX_LATTICE_GRAD_H
#define LABICEX_LATTICE_GRAD_H

#include "base/common.h"

namespace labicex {

namespace lattice {

template <class Lattice>
class grad_moments {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  static const int count = 1 + Lattice::d + Lattice::d * Lattice::d;

  LABICEX_COMMON_INLINE grad_moments(const fvector& f)
      : density_(0), momentum_(rvector::Zero()), pressure_(rtensor::Zero()) {
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      density_ += f(i);
      momentum_ += f(i) * Lattice::cf(i);
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          pressure_(a, b) += f(i) * Lattice::cf(i)(a) * Lattice::cf(i)(b);
        }
      }
    }
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE grad_moments(const real density,
                                     const rvector& momentum,
                                     const rtensor& pressure)
      : density_(density), momentum_(momentum), pressure_(pressure) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE grad_moments() {}

  LABICEX_COMMON_INLINE const real& density() const { return density_; }
  LABICEX_COMMON_INLINE const rvector& momentum() const { return momentum_; }
  LABICEX_COMMON_INLINE real pressure_trace() const {
    return pressure_.matrix().trace();
  }
  LABICEX_COMMON_INLINE const rtensor& pressure() const { return pressure_; }
  LABICEX_COMMON_INLINE real& density() { return density_; }
  LABICEX_COMMON_INLINE rvector& momentum() { return momentum_; }
  LABICEX_COMMON_INLINE rtensor& pressure() { return pressure_; }

 private:
  real density_;
  rvector momentum_;
  rtensor pressure_;
};

template <class Lattice>
struct grad_approximation {
  typedef typename Lattice::grad_moments grad_moments;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::fvector fvector;

  LABICEX_COMMON static fvector get(const grad_moments& moments,
                                    bool use_qeq = false) {
    const primitive_variables pv(moments);
    fvector f;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      f(i) = moments.density();
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        f(i) += moments.momentum()(a) * Lattice::cf(i)(a) / pv.temperature();
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          f(i) += (moments.pressure()(a, b) -
                   moments.density() * pv.temperature() * delta(a, b)) *
                  (Lattice::cf(i)(a) * Lattice::cf(i)(b) -
                   pv.temperature() * delta(a, b)) /
                  (2 * pv.temperature() * pv.temperature());
          if (use_qeq) {
            // add equilibrium third order moment
            for (int c = 0; c < Lattice::d; ++c) {
              f(i) += moments.density() * pv.velocity()(a) * pv.velocity()(b) *
                      pv.velocity()(c);
            }
          }
        }
      }
      f(i) *= Lattice::w(i, pv);
    }
#ifdef LABICEX_FULLDEBUG
    grad_moments gm(f);
    assert(abs(gm.density() - moments.density()) < 1e-5);
    assert((gm.momentum() - moments.momentum()).matrix().norm() < 1e-5);
    assert((gm.pressure() - moments.pressure()).matrix().norm() < 1e-5);
#endif
    return f;
  }

  LABICEX_COMMON static void axpy(const grad_moments& moments, real alpha,
                                  fvector& f, bool use_qeq = false) {
    const primitive_variables pv(moments);

    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      asm_barrier(f);
      real fi = moments.density();
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        fi += moments.momentum()(a) * Lattice::cf(i)(a) / pv.temperature();
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          fi += (moments.pressure()(a, b) -
                 moments.density() * pv.temperature() * delta(a, b)) *
                (Lattice::cf(i)(a) * Lattice::cf(i)(b) -
                 pv.temperature() * delta(a, b)) /
                (2 * pv.temperature() * pv.temperature());
          if (use_qeq) {
            // add equilibrium third order moment
            LABICEX_UNROLL
            for (int c = 0; c < Lattice::d; ++c) {
              fi += moments.density() * pv.velocity()(a) * pv.velocity()(b) *
                    pv.velocity()(c);
            }
          }
        }
      }
      fi *= Lattice::w(i, pv);
      f(i) += alpha * fi;
    }
  }
};

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_GRAD_H

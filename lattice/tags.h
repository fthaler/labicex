#ifndef LABICEX_LATTICE_TAGS_H
#define LABICEX_LATTICE_TAGS_H

namespace labicex {

namespace lattice {

struct standard_tag {};

struct thermal_tag : standard_tag {};

}  // lattice

}  // labicex

#endif  //  LABICEX_LATTICE_TAGS_H

#ifndef LABICEX_LATTICE_THERMAL_H
#define LABICEX_LATTICE_THERMAL_H

#include "lattice/thermal/d2q25.h"
#include "lattice/thermal/d2q9.h"
#include "lattice/thermal/d3q125.h"

#endif  // LABICEX_LATTICE_THERMAL_H

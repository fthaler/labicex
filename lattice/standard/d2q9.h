#ifndef LABICEX_LATTICE_STANDARD_D2Q9_H
#define LABICEX_LATTICE_STANDARD_D2Q9_H

#include <cassert>

#include "lattice/standard/common.h"
#include "lattice/tags.h"

namespace labicex {

namespace lattice {

namespace standard {

struct d2q9 {
  typedef vector_types<2>::rvector rvector;
  typedef vector_types<2>::rtensor rtensor;
  typedef vector_types<2>::ivector ivector;
  typedef vector_types<9>::rvector fvector;
  typedef conserved_moments<d2q9> conserved_moments;
  typedef grad_moments<d2q9> grad_moments;
  typedef primitive_variables<d2q9> primitive_variables;
  typedef equilibrium<d2q9> equilibrium;
  typedef numerical_equilibrium<d2q9> numerical_equilibrium;
  typedef grad_approximation<d2q9> grad_approximation;
  typedef standard_tag lattice_category;
  static const int d = 2;
  static const int q = 9;
  static const int max_ci = 1;

  LABICEX_COMMON_INLINE static ivector ci(int i) {
    assert(i >= 0 && i < q);
    ivector ci;

    // x
    if (i == 1 || i == 5 || i == 8)
      ci(0) = 1;
    else if (i == 3 || i == 6 || i == 7)
      ci(0) = -1;
    else
      ci(0) = 0;

    // y
    if (i == 2 || i == 5 || i == 6)
      ci(1) = 1;
    else if (i == 4 || i == 7 || i == 8)
      ci(1) = -1;
    else
      ci(1) = 0;

    return ci;
  }

  LABICEX_COMMON_INLINE static rvector cf(int i) {
    assert(i >= 0 && i < q);
    rvector ci;

    // x
    if (i == 1 || i == 5 || i == 8)
      ci(0) = 1.0;
    else if (i == 3 || i == 6 || i == 7)
      ci(0) = -1.0;
    else
      ci(0) = 0.0;

    // y
    if (i == 2 || i == 5 || i == 6)
      ci(1) = 1.0;
    else if (i == 4 || i == 7 || i == 8)
      ci(1) = -1.0;
    else
      ci(1) = 0.0;

    return ci;
  }

  LABICEX_COMMON_INLINE static int ci2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i <= 4) return 1;
    return 2;
  }

  LABICEX_COMMON_INLINE static real cf2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 4) return 1.0;
    return 2.0;
  }

  LABICEX_COMMON_INLINE static real cf_magnitude(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 4) return 1.0;
    return 1.41421356237309514547;
  }

  LABICEX_COMMON_INLINE static int ci_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    return 1;
  }

  LABICEX_COMMON_INLINE static real cf_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    return 1.0;
  }

  LABICEX_COMMON_INLINE static real w(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 4.0 / 9.0;
    if (i <= 4) return 1.0 / 9.0;
    return 1.0 / 36.0;
  }

  LABICEX_COMMON_INLINE static real w(int i, const primitive_variables&) {
    return w(i);
  }

  LABICEX_COMMON_INLINE static int reverse_index(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i == 1) return 3;
    if (i == 2) return 4;
    if (i == 3) return 1;
    if (i == 4) return 2;
    if (i == 5) return 7;
    if (i == 6) return 8;
    if (i == 7) return 5;
    return 6;
  }

  LABICEX_COMMON_INLINE static int single_reverse_index(int i, int axis) {
    assert(i >= 0 && i < q);
    assert(axis >= 0 && axis < d);
    if (axis == 0) {
      if (i == 0) return 0;
      if (i == 1) return 3;
      if (i == 2) return 2;
      if (i == 3) return 1;
      if (i == 4) return 4;
      if (i == 5) return 6;
      if (i == 6) return 5;
      if (i == 7) return 8;
      return 7;
    } else {
      if (i == 0) return 0;
      if (i == 1) return 1;
      if (i == 2) return 4;
      if (i == 3) return 3;
      if (i == 4) return 2;
      if (i == 5) return 8;
      if (i == 6) return 7;
      if (i == 7) return 6;
      return 5;
    }
  }

  LABICEX_COMMON_INLINE static int unit_index(int j) {
    assert(j >= 0 && j < d);
    if (j == 0) return 1;
    return 2;
  }

  LABICEX_COMMON_INLINE static real reference_temperature() {
    return 1.0 / 3.0;
  }

  LABICEX_COMMON_INLINE static real natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      return 1.0;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      return 1.0;
    }
    if (i == 2) {
      if (j < 2) return 0.0;
      if (j < 3) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.0;
      if (j < 7) return 1.0;
      return -1.0;
    }
    if (i == 3) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      return 2.0;
    }
    if (i == 4) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return -1.0;
      if (j < 4) return 1.0;
      if (j < 5) return -1.0;
      return 0.0;
    }
    if (i == 5) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 8) return 1.0;
      return -1.0;
    }
    if (i == 6) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      return 1.0;
    }
    if (i == 7) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      return -1.0;
    }
    {
      if (j < 5) return 0.0;
      return 1.0;
    }
  }

  LABICEX_COMMON_INLINE static real inverse_natural_moment_matrix(int i,
                                                                  int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 8) return 0.0;
      return 1.0;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return 0.5;
      if (j < 3) return 0.0;
      if (j < 5) return 0.25;
      if (j < 6) return 0.0;
      if (j < 7) return -0.5;
      if (j < 8) return 0.0;
      return -0.5;
    }
    if (i == 2) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5;
      if (j < 4) return 0.25;
      if (j < 5) return -0.25;
      if (j < 7) return 0.0;
      return -0.5;
    }
    if (i == 3) {
      if (j < 1) return 0.0;
      if (j < 2) return -0.5;
      if (j < 3) return 0.0;
      if (j < 5) return 0.25;
      if (j < 6) return 0.0;
      if (j < 7) return 0.5;
      if (j < 8) return 0.0;
      return -0.5;
    }
    if (i == 4) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5;
      if (j < 4) return 0.25;
      if (j < 5) return -0.25;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      return -0.5;
    }
    if (i == 5) {
      if (j < 5) return 0.0;
      return 0.25;
    }
    if (i == 6) {
      if (j < 5) return 0.0;
      if (j < 7) return -0.25;
      return 0.25;
    }
    if (i == 7) {
      if (j < 5) return 0.0;
      if (j < 6) return 0.25;
      if (j < 8) return -0.25;
      return 0.25;
    }
    {
      if (j < 5) return 0.0;
      if (j < 6) return -0.25;
      if (j < 7) return 0.25;
      if (j < 8) return -0.25;
      return 0.25;
    }
  }

  LABICEX_COMMON_INLINE static real binary_mixture_natural_moment_matrix(
      int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);
    if (i == 0) {
      if (j < 9) return 1.0;
      return 0.0;
    }
    if (i == 1) {
      if (j < 9) return 0.0;
      return 1.0;
    }
    if (i == 2) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return 0.0;
      if (j < 11) return 1.0;
      if (j < 12) return 0.0;
      if (j < 13) return -1.0;
      if (j < 14) return 0.0;
      if (j < 15) return 1.0;
      if (j < 17) return -1.0;
      return 1.0;
    }
    if (i == 3) {
      if (j < 2) return 0.0;
      if (j < 3) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.0;
      if (j < 7) return 1.0;
      if (j < 9) return -1.0;
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -1.0;
      if (j < 16) return 1.0;
      return -1.0;
    }
    if (i == 4) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      if (j < 9) return 2.0;
      if (j < 10) return 0.0;
      if (j < 14) return 1.0;
      return 2.0;
    }
    if (i == 5) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      return 0.0;
    }
    if (i == 6) {
      if (j < 2) return 0.0;
      if (j < 3) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.0;
      if (j < 7) return 1.0;
      if (j < 9) return -1.0;
      return 0.0;
    }
    if (i == 7) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      if (j < 9) return 2.0;
      return 0.0;
    }
    if (i == 8) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 14) return 0.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      return -1.0;
    }
    if (i == 9) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      return 0.0;
    }
    if (i == 10) {
      if (j < 2) return 0.0;
      if (j < 3) return 1.0;
      if (j < 4) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 13) return 0.0;
      return 1.0;
    }
    if (i == 11) {
      if (j < 2) return 0.0;
      if (j < 3) return 1.0;
      if (j < 4) return 0.0;
      if (j < 9) return 1.0;
      return 0.0;
    }
    if (i == 12) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 14) return 0.0;
      if (j < 15) return 1.0;
      if (j < 17) return -1.0;
      return 1.0;
    }
    if (i == 13) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      return 0.0;
    }
    if (i == 14) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      if (j < 9) return -1.0;
      if (j < 14) return 0.0;
      if (j < 16) return 1.0;
      return -1.0;
    }
    if (i == 15) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      if (j < 9) return -1.0;
      return 0.0;
    }
    if (i == 16) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 14) return 0.0;
      return 1.0;
    }
    {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      return 0.0;
    }
  }

  LABICEX_COMMON_INLINE static real
  inverse_binary_mixture_natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 7) return 0.0;
      if (j < 8) return -1.0;
      if (j < 17) return 0.0;
      return 1.0;
    }
    if (i == 1) {
      if (j < 5) return 0.0;
      if (j < 6) return 0.5;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5;
      if (j < 13) return 0.0;
      if (j < 14) return -0.5;
      if (j < 17) return 0.0;
      return -0.5;
    }
    if (i == 2) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5;
      if (j < 15) return 0.0;
      if (j < 16) return -0.5;
      if (j < 17) return 0.0;
      return -0.5;
    }
    if (i == 3) {
      if (j < 5) return 0.0;
      if (j < 6) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5;
      if (j < 13) return 0.0;
      if (j < 14) return 0.5;
      if (j < 17) return 0.0;
      return -0.5;
    }
    if (i == 4) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.5;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5;
      if (j < 15) return 0.0;
      if (j < 16) return 0.5;
      if (j < 17) return 0.0;
      return -0.5;
    }
    if (i == 5) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.25;
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 15) return 0.0;
      if (j < 16) return 0.25;
      if (j < 17) return 0.0;
      return 0.25;
    }
    if (i == 6) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.25;
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 15) return 0.0;
      if (j < 16) return 0.25;
      if (j < 17) return 0.0;
      return 0.25;
    }
    if (i == 7) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.25;
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 15) return 0.0;
      if (j < 16) return -0.25;
      if (j < 17) return 0.0;
      return 0.25;
    }
    if (i == 8) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.25;
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 15) return 0.0;
      if (j < 16) return -0.25;
      if (j < 17) return 0.0;
      return 0.25;
    }
    if (i == 9) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.0;
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 16) return 0.0;
      if (j < 17) return 1.0;
      return -1.0;
    }
    if (i == 10) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5;
      if (j < 4) return 0.0;
      if (j < 5) return 0.5;
      if (j < 6) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 10) return 0.0;
      if (j < 11) return -0.5;
      if (j < 12) return 0.5;
      if (j < 13) return -0.5;
      if (j < 14) return 0.5;
      if (j < 16) return 0.0;
      if (j < 17) return -0.5;
      return 0.5;
    }
    if (i == 11) {
      if (j < 3) return 0.0;
      if (j < 4) return 0.5;
      if (j < 6) return 0.0;
      if (j < 7) return -0.5;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5;
      if (j < 12) return -0.5;
      if (j < 14) return 0.0;
      if (j < 15) return -0.5;
      if (j < 16) return 0.5;
      if (j < 17) return -0.5;
      return 0.5;
    }
    if (i == 12) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5;
      if (j < 4) return 0.0;
      if (j < 6) return 0.5;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 10) return 0.0;
      if (j < 11) return -0.5;
      if (j < 13) return 0.5;
      if (j < 14) return -0.5;
      if (j < 16) return 0.0;
      if (j < 17) return -0.5;
      return 0.5;
    }
    if (i == 13) {
      if (j < 3) return 0.0;
      if (j < 4) return -0.5;
      if (j < 6) return 0.0;
      if (j < 7) return 0.5;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5;
      if (j < 12) return -0.5;
      if (j < 14) return 0.0;
      if (j < 15) return 0.5;
      if (j < 17) return -0.5;
      return 0.5;
    }
    if (i == 14) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.25;
      if (j < 10) return -0.25;
      if (j < 12) return 0.0;
      if (j < 13) return 0.25;
      if (j < 14) return -0.25;
      if (j < 15) return 0.25;
      if (j < 16) return -0.25;
      if (j < 17) return 0.25;
      return -0.25;
    }
    if (i == 15) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.25;
      if (j < 10) return 0.25;
      if (j < 12) return 0.0;
      if (j < 13) return -0.25;
      if (j < 15) return 0.25;
      if (j < 16) return -0.25;
      if (j < 17) return 0.25;
      return -0.25;
    }
    if (i == 16) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.25;
      if (j < 10) return -0.25;
      if (j < 12) return 0.0;
      if (j < 13) return -0.25;
      if (j < 14) return 0.25;
      if (j < 15) return -0.25;
      if (j < 17) return 0.25;
      return -0.25;
    }
    {
      if (j < 8) return 0.0;
      if (j < 9) return -0.25;
      if (j < 10) return 0.25;
      if (j < 12) return 0.0;
      if (j < 13) return 0.25;
      if (j < 15) return -0.25;
      if (j < 17) return 0.25;
      return -0.25;
    }
  }
};  // d2q9

}  // standard

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_STANDARD_D2Q9_H

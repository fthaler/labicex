#ifndef LABICEX_LATTICE_STANDARD_COMMON_H
#define LABICEX_LATTICE_STANDARD_COMMON_H

#include <cassert>

#include "base/inline_math.h"
#include "lattice/grad.h"
#include "lattice/numerical_equilibrium.h"
#include "lattice/tags.h"

namespace labicex {

namespace lattice {

namespace standard {

template <class Lattice>
class conserved_moments {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::grad_moments grad_moments;
  static const int count = 1 + Lattice::d;

  LABICEX_COMMON_INLINE conserved_moments(const fvector& f)
      : density_(0), momentum_(rvector::Zero()) {
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      density_ += f(i);
      momentum_ += f(i) * Lattice::cf(i);
    }
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(real density, const rvector& momentum)
      : density_(density), momentum_(momentum) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(const primitive_variables& variables)
      : density_(variables.density()),
        momentum_(variables.density() * variables.velocity()) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments(const grad_moments& grad)
      : density_(grad.density()), momentum_(grad.momentum()) {
    assert(density_ >= 0);
  }

  LABICEX_COMMON_INLINE conserved_moments() {}

  LABICEX_COMMON_INLINE const real& density() const { return density_; }
  LABICEX_COMMON_INLINE const rvector& momentum() const { return momentum_; }

  LABICEX_COMMON_INLINE real& density() { return density_; }
  LABICEX_COMMON_INLINE rvector& momentum() { return momentum_; }

 private:
  real density_;
  rvector momentum_;
};

template <class Lattice>
class primitive_variables {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::conserved_moments conserved_moments;
  static const int count = 1 + Lattice::d;

  LABICEX_COMMON_INLINE primitive_variables(real density,
                                            const rvector& velocity)
      : density_(density), velocity_(velocity) {
    assert(density_ > 0);
  }

  LABICEX_COMMON_INLINE primitive_variables(const conserved_moments& moments)
      : density_(moments.density()),
        velocity_(moments.momentum() / moments.density()) {
    assert(density_ > 0);
  }

  LABICEX_COMMON_INLINE primitive_variables() {}

  LABICEX_COMMON_INLINE const real& density() const { return density_; }
  LABICEX_COMMON_INLINE const rvector& velocity() const { return velocity_; }
  LABICEX_COMMON_INLINE real temperature() const {
    return Lattice::reference_temperature();
  }

  LABICEX_COMMON_INLINE real& density() { return density_; }
  LABICEX_COMMON_INLINE rvector& velocity() { return velocity_; }

 private:
  real density_;
  rvector velocity_;
};

template <class Lattice>
struct equilibrium {
  typedef typename Lattice::conserved_moments conserved_moments;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;

  LABICEX_COMMON static fvector get(const primitive_variables& pv) {
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    rvector usqrt = (1 + 3 * velocity * velocity).sqrt();
    real common = density * (2 - usqrt).prod();
    rvector cpart = (2 * velocity + usqrt) / (1 - velocity);
    fvector feq;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      real feqi = common * Lattice::w(i);
      ivector ci = Lattice::ci(i);
      LABICEX_UNROLL
      for (int j = 0; j < Lattice::d; ++j) feqi *= ipow(cpart(j), ci(j));
      assert(!isnan(feqi));
      feq(i) = feqi;
    }
#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq);
    assert(abs(pveq.density() - density) < 1e-5);
    assert((pveq.velocity() - velocity).matrix().norm() < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq;
  }

  LABICEX_COMMON_INLINE static fvector gravity_difference(
      const primitive_variables& pv, real gravity) {
    const primitive_variables pv_acc(pv.density(),
                                     pv.velocity() + rvector(0, -gravity));
    return get(pv_acc) - get(pv);
  }
};

}  // standard

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_STANDARD_COMMON_H

#ifndef LABICEX_LATTICE_STANDARD_D3Q27_H
#define LABICEX_LATTICE_STANDARD_D3Q27_H

#include <cassert>

#include "lattice/standard/common.h"
#include "lattice/tags.h"

namespace labicex {

namespace lattice {

namespace standard {

struct d3q27 {
  typedef vector_types<3>::rvector rvector;
  typedef vector_types<3>::rtensor rtensor;
  typedef vector_types<3>::ivector ivector;
  typedef vector_types<27>::rvector fvector;
  typedef conserved_moments<d3q27> conserved_moments;
  typedef grad_moments<d3q27> grad_moments;
  typedef primitive_variables<d3q27> primitive_variables;
  typedef equilibrium<d3q27> equilibrium;
  typedef numerical_equilibrium<d3q27> numerical_equilibrium;
  typedef grad_approximation<d3q27> grad_approximation;
  typedef standard_tag lattice_category;
  static const int d = 3;
  static const int q = 27;
  static const int max_ci = 1;

  LABICEX_COMMON_INLINE static ivector ci(int i) {
    assert(i >= 0 && i < q);
    ivector ci;

    // x
    if (i == 1 || i == 7 || i == 9 || i == 11 || i == 13 || i == 19 ||
        i == 21 || i == 23 || i == 25)
      ci(0) = 1;
    else if (i == 2 || i == 8 || i == 10 || i == 12 || i == 14 || i == 20 ||
             i == 22 || i == 24 || i == 26)
      ci(0) = -1;
    else
      ci(0) = 0;

    // y
    if (i == 3 || i == 7 || i == 8 || i == 15 || i == 17 || i == 19 ||
        i == 20 || i == 23 || i == 24)
      ci(1) = 1;
    else if (i == 4 || i == 9 || i == 10 || i == 16 || i == 18 || i == 21 ||
             i == 22 || i == 25 || i == 26)
      ci(1) = -1;
    else
      ci(1) = 0;

    // z
    if (i == 5 || i == 11 || i == 12 || i == 15 || i == 16 || i == 19 ||
        i == 20 || i == 21 || i == 22)
      ci(2) = 1;
    else if (i == 6 || i == 13 || i == 14 || i == 17 || i == 18 || i == 23 ||
             i == 24 || i == 25 || i == 26)
      ci(2) = -1;
    else
      ci(2) = 0;

    return ci;
  }

  LABICEX_COMMON_INLINE static rvector cf(int i) {
    assert(i >= 0 && i < q);
    rvector ci;

    // x
    if (i == 1 || i == 7 || i == 9 || i == 11 || i == 13 || i == 19 ||
        i == 21 || i == 23 || i == 25)
      ci(0) = 1.0;
    else if (i == 2 || i == 8 || i == 10 || i == 12 || i == 14 || i == 20 ||
             i == 22 || i == 24 || i == 26)
      ci(0) = -1.0;
    else
      ci(0) = 0.0;

    // y
    if (i == 3 || i == 7 || i == 8 || i == 15 || i == 17 || i == 19 ||
        i == 20 || i == 23 || i == 24)
      ci(1) = 1.0;
    else if (i == 4 || i == 9 || i == 10 || i == 16 || i == 18 || i == 21 ||
             i == 22 || i == 25 || i == 26)
      ci(1) = -1.0;
    else
      ci(1) = 0.0;

    // z
    if (i == 5 || i == 11 || i == 12 || i == 15 || i == 16 || i == 19 ||
        i == 20 || i == 21 || i == 22)
      ci(2) = 1.0;
    else if (i == 6 || i == 13 || i == 14 || i == 17 || i == 18 || i == 23 ||
             i == 24 || i == 25 || i == 26)
      ci(2) = -1.0;
    else
      ci(2) = 0.0;

    return ci;
  }

  LABICEX_COMMON_INLINE static int ci2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i <= 6) return 1;
    if (i <= 18) return 2;
    return 3;
  }

  LABICEX_COMMON_INLINE static real cf2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 6) return 1.0;
    if (i <= 18) return 2.0;
    return 3.0;
  }

  LABICEX_COMMON_INLINE static real cf_magnitude(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 6) return 1.0;
    if (i <= 18) return 1.41421356237309514547;
    return 1.73205080756887719318;
  }

  LABICEX_COMMON_INLINE static int ci_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    return 1;
  }

  LABICEX_COMMON_INLINE static real cf_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    return 1.0;
  }

  LABICEX_COMMON_INLINE static real w(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 8.0 / 27.0;
    if (i <= 6) return 2.0 / 27.0;
    if (i <= 18) return 1.0 / 54.0;
    return 1.0 / 216.0;
  }

  LABICEX_COMMON_INLINE static real w(int i, const primitive_variables&) {
    return w(i);
  }

  LABICEX_COMMON_INLINE static int reverse_index(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i == 1) return 2;
    if (i == 2) return 1;
    if (i == 3) return 4;
    if (i == 4) return 3;
    if (i == 5) return 6;
    if (i == 6) return 5;
    if (i == 7) return 10;
    if (i == 8) return 9;
    if (i == 9) return 8;
    if (i == 10) return 7;
    if (i == 11) return 14;
    if (i == 12) return 13;
    if (i == 13) return 12;
    if (i == 14) return 11;
    if (i == 15) return 18;
    if (i == 16) return 17;
    if (i == 17) return 16;
    if (i == 18) return 15;
    if (i == 19) return 26;
    if (i == 20) return 25;
    if (i == 21) return 24;
    if (i == 22) return 23;
    if (i == 23) return 22;
    if (i == 24) return 21;
    if (i == 25) return 20;
    return 19;
  }

  LABICEX_COMMON_INLINE static int single_reverse_index(int i, int axis) {
    assert(i >= 0 && i < q);
    assert(axis >= 0 && axis < d);
    if (axis == 0) {
      if (i == 0) return 0;
      if (i == 1) return 2;
      if (i == 2) return 1;
      if (i == 3) return 3;
      if (i == 4) return 4;
      if (i == 5) return 5;
      if (i == 6) return 6;
      if (i == 7) return 8;
      if (i == 8) return 7;
      if (i == 9) return 10;
      if (i == 10) return 9;
      if (i == 11) return 12;
      if (i == 12) return 11;
      if (i == 13) return 14;
      if (i == 14) return 13;
      if (i == 15) return 15;
      if (i == 16) return 16;
      if (i == 17) return 17;
      if (i == 18) return 18;
      if (i == 19) return 20;
      if (i == 20) return 19;
      if (i == 21) return 22;
      if (i == 22) return 21;
      if (i == 23) return 24;
      if (i == 24) return 23;
      if (i == 25) return 26;
      return 25;
    } else if (axis == 1) {
      if (i == 0) return 0;
      if (i == 1) return 1;
      if (i == 2) return 2;
      if (i == 3) return 4;
      if (i == 4) return 3;
      if (i == 5) return 5;
      if (i == 6) return 6;
      if (i == 7) return 9;
      if (i == 8) return 10;
      if (i == 9) return 7;
      if (i == 10) return 8;
      if (i == 11) return 11;
      if (i == 12) return 12;
      if (i == 13) return 13;
      if (i == 14) return 14;
      if (i == 15) return 16;
      if (i == 16) return 15;
      if (i == 17) return 18;
      if (i == 18) return 17;
      if (i == 19) return 21;
      if (i == 20) return 22;
      if (i == 21) return 19;
      if (i == 22) return 20;
      if (i == 23) return 25;
      if (i == 24) return 26;
      if (i == 25) return 23;
      return 24;
    } else {
      if (i == 0) return 0;
      if (i == 1) return 1;
      if (i == 2) return 2;
      if (i == 3) return 3;
      if (i == 4) return 4;
      if (i == 5) return 6;
      if (i == 6) return 5;
      if (i == 7) return 7;
      if (i == 8) return 8;
      if (i == 9) return 9;
      if (i == 10) return 10;
      if (i == 11) return 13;
      if (i == 12) return 14;
      if (i == 13) return 11;
      if (i == 14) return 12;
      if (i == 15) return 17;
      if (i == 16) return 18;
      if (i == 17) return 15;
      if (i == 18) return 16;
      if (i == 19) return 23;
      if (i == 20) return 24;
      if (i == 21) return 25;
      if (i == 22) return 26;
      if (i == 23) return 19;
      if (i == 24) return 20;
      if (i == 25) return 21;
      return 22;
    }
  }

  LABICEX_COMMON_INLINE static int unit_index(int j) {
    assert(j >= 0 && j < d);
    if (j == 0) return 1;
    if (j == 1) return 3;
    return 5;
  }

  LABICEX_COMMON_INLINE static real reference_temperature() {
    return 1.0 / 3.0;
  }

  LABICEX_COMMON_INLINE static real natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      return 1.0;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return -1.0;
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      return -1.0;
    }
    if (i == 2) {
      if (j < 3) return 0.0;
      if (j < 4) return 1.0;
      if (j < 5) return -1.0;
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      return -1.0;
    }
    if (i == 3) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      return -1.0;
    }
    if (i == 4) {
      if (j < 1) return 0.0;
      if (j < 7) return 1.0;
      if (j < 19) return 2.0;
      return 3.0;
    }
    if (i == 5) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      if (j < 11) return 0.0;
      return 1.0;
    }
    if (i == 6) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 18) return -1.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      return 1.0;
    }
    if (i == 7) {
      if (j < 3) return 0.0;
      if (j < 5) return 1.0;
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 15) return 0.0;
      return 1.0;
    }
    if (i == 8) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      return 1.0;
    }
    if (i == 9) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 10) return -1.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      return 1.0;
    }
    if (i == 10) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 23) return 1.0;
      if (j < 24) return -1.0;
      if (j < 26) return 1.0;
      return -1.0;
    }
    if (i == 11) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      return -1.0;
    }
    if (i == 12) {
      if (j < 15) return 0.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      return -1.0;
    }
    if (i == 13) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      return -1.0;
    }
    if (i == 14) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      return -1.0;
    }
    if (i == 15) {
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      return -1.0;
    }
    if (i == 16) {
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      return -1.0;
    }
    if (i == 17) {
      if (j < 15) return 0.0;
      return 1.0;
    }
    if (i == 18) {
      if (j < 11) return 0.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      return 1.0;
    }
    if (i == 19) {
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      return 1.0;
    }
    if (i == 20) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      return 1.0;
    }
    if (i == 21) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      return 1.0;
    }
    if (i == 22) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      return 1.0;
    }
    if (i == 23) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      return -1.0;
    }
    if (i == 24) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      return -1.0;
    }
    if (i == 25) {
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      return -1.0;
    }
    {
      if (j < 19) return 0.0;
      return 1.0;
    }
  }

  LABICEX_COMMON_INLINE static real inverse_natural_moment_matrix(int i,
                                                                  int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.0;
      if (j < 17) return 0.0;
      if (j < 20) return 1.0;
      if (j < 26) return 0.0;
      return -1.0;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return 0.5;
      if (j < 4) return 0.0;
      if (j < 5) return 0.5;
      if (j < 6) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 13) return 0.0;
      if (j < 15) return -0.5;
      if (j < 18) return 0.0;
      if (j < 20) return -0.5;
      if (j < 23) return 0.0;
      if (j < 24) return 0.5;
      if (j < 26) return 0.0;
      return 0.5;
    }
    if (i == 2) {
      if (j < 1) return 0.0;
      if (j < 2) return -0.5;
      if (j < 4) return 0.0;
      if (j < 5) return 0.5;
      if (j < 6) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 13) return 0.0;
      if (j < 15) return 0.5;
      if (j < 18) return 0.0;
      if (j < 20) return -0.5;
      if (j < 23) return 0.0;
      if (j < 24) return -0.5;
      if (j < 26) return 0.0;
      return 0.5;
    }
    if (i == 3) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5;
      if (j < 16) return 0.0;
      if (j < 18) return -0.5;
      if (j < 19) return 0.0;
      if (j < 20) return -0.5;
      if (j < 24) return 0.0;
      if (j < 25) return 0.5;
      if (j < 26) return 0.0;
      return 0.5;
    }
    if (i == 4) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5;
      if (j < 16) return 0.0;
      if (j < 17) return 0.5;
      if (j < 18) return -0.5;
      if (j < 19) return 0.0;
      if (j < 20) return -0.5;
      if (j < 24) return 0.0;
      if (j < 25) return -0.5;
      if (j < 26) return 0.0;
      return 0.5;
    }
    if (i == 5) {
      if (j < 3) return 0.0;
      if (j < 4) return 0.5;
      if (j < 5) return 0.0;
      if (j < 6) return 0.5;
      if (j < 12) return 0.0;
      if (j < 13) return -0.5;
      if (j < 15) return 0.0;
      if (j < 16) return -0.5;
      if (j < 17) return 0.0;
      if (j < 19) return -0.5;
      if (j < 25) return 0.0;
      return 0.5;
    }
    if (i == 6) {
      if (j < 3) return 0.0;
      if (j < 4) return -0.5;
      if (j < 5) return 0.0;
      if (j < 6) return 0.5;
      if (j < 12) return 0.0;
      if (j < 13) return 0.5;
      if (j < 15) return 0.0;
      if (j < 16) return 0.5;
      if (j < 17) return 0.0;
      if (j < 19) return -0.5;
      if (j < 25) return 0.0;
      if (j < 26) return -0.5;
      return 0.5;
    }
    if (i == 7) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.25;
      if (j < 14) return 0.0;
      if (j < 15) return 0.25;
      if (j < 16) return 0.0;
      if (j < 17) return 0.25;
      if (j < 19) return 0.0;
      if (j < 20) return 0.25;
      if (j < 21) return -0.25;
      if (j < 23) return 0.0;
      if (j < 25) return -0.25;
      if (j < 26) return 0.0;
      return -0.25;
    }
    if (i == 8) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.25;
      if (j < 14) return 0.0;
      if (j < 15) return -0.25;
      if (j < 16) return 0.0;
      if (j < 17) return 0.25;
      if (j < 19) return 0.0;
      if (j < 21) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return 0.25;
      if (j < 25) return -0.25;
      if (j < 26) return 0.0;
      return -0.25;
    }
    if (i == 9) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.25;
      if (j < 14) return 0.0;
      if (j < 15) return 0.25;
      if (j < 16) return 0.0;
      if (j < 17) return -0.25;
      if (j < 19) return 0.0;
      if (j < 21) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return -0.25;
      if (j < 25) return 0.25;
      if (j < 26) return 0.0;
      return -0.25;
    }
    if (i == 10) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.25;
      if (j < 14) return 0.0;
      if (j < 15) return -0.25;
      if (j < 16) return 0.0;
      if (j < 17) return -0.25;
      if (j < 19) return 0.0;
      if (j < 20) return 0.25;
      if (j < 21) return -0.25;
      if (j < 23) return 0.0;
      if (j < 25) return 0.25;
      if (j < 26) return 0.0;
      return -0.25;
    }
    if (i == 11) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.25;
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 15) return 0.0;
      if (j < 16) return 0.25;
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 21) return 0.0;
      if (j < 22) return -0.25;
      if (j < 23) return 0.0;
      if (j < 24) return -0.25;
      if (j < 25) return 0.0;
      return -0.25;
    }
    if (i == 12) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.25;
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 15) return 0.0;
      if (j < 16) return 0.25;
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 21) return 0.0;
      if (j < 22) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return 0.25;
      if (j < 25) return 0.0;
      return -0.25;
    }
    if (i == 13) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.25;
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 15) return 0.0;
      if (j < 16) return -0.25;
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 21) return 0.0;
      if (j < 22) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return -0.25;
      if (j < 25) return 0.0;
      if (j < 26) return 0.25;
      return -0.25;
    }
    if (i == 14) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.25;
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 15) return 0.0;
      if (j < 16) return -0.25;
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 21) return 0.0;
      if (j < 22) return -0.25;
      if (j < 23) return 0.0;
      if (j < 24) return 0.25;
      if (j < 25) return 0.0;
      if (j < 26) return 0.25;
      return -0.25;
    }
    if (i == 15) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.25;
      if (j < 11) return 0.0;
      if (j < 13) return 0.25;
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return -0.25;
      if (j < 24) return 0.0;
      return -0.25;
    }
    if (i == 16) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.25;
      if (j < 11) return 0.0;
      if (j < 12) return -0.25;
      if (j < 13) return 0.25;
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return 0.25;
      if (j < 24) return 0.0;
      if (j < 25) return 0.25;
      return -0.25;
    }
    if (i == 17) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.25;
      if (j < 11) return 0.0;
      if (j < 12) return 0.25;
      if (j < 13) return -0.25;
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return 0.25;
      if (j < 24) return 0.0;
      if (j < 25) return -0.25;
      if (j < 26) return 0.25;
      return -0.25;
    }
    if (i == 18) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.25;
      if (j < 11) return 0.0;
      if (j < 13) return -0.25;
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return -0.25;
      if (j < 24) return 0.0;
      if (j < 26) return 0.25;
      return -0.25;
    }
    if (i == 19) {
      if (j < 10) return 0.0;
      if (j < 11) return 0.125;
      if (j < 20) return 0.0;
      return 0.125;
    }
    if (i == 20) {
      if (j < 10) return 0.0;
      if (j < 11) return -0.125;
      if (j < 20) return 0.0;
      if (j < 22) return -0.125;
      if (j < 23) return 0.125;
      if (j < 24) return -0.125;
      return 0.125;
    }
    if (i == 21) {
      if (j < 10) return 0.0;
      if (j < 11) return -0.125;
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 23) return -0.125;
      if (j < 24) return 0.125;
      if (j < 25) return -0.125;
      return 0.125;
    }
    if (i == 22) {
      if (j < 10) return 0.0;
      if (j < 11) return 0.125;
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 25) return -0.125;
      return 0.125;
    }
    if (i == 23) {
      if (j < 10) return 0.0;
      if (j < 11) return -0.125;
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 23) return -0.125;
      if (j < 25) return 0.125;
      if (j < 26) return -0.125;
      return 0.125;
    }
    if (i == 24) {
      if (j < 10) return 0.0;
      if (j < 11) return 0.125;
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 24) return -0.125;
      if (j < 25) return 0.125;
      if (j < 26) return -0.125;
      return 0.125;
    }
    if (i == 25) {
      if (j < 10) return 0.0;
      if (j < 11) return 0.125;
      if (j < 20) return 0.0;
      if (j < 22) return -0.125;
      if (j < 24) return 0.125;
      if (j < 26) return -0.125;
      return 0.125;
    }
    {
      if (j < 10) return 0.0;
      if (j < 11) return -0.125;
      if (j < 20) return 0.0;
      if (j < 23) return 0.125;
      if (j < 26) return -0.125;
      return 0.125;
    }
  }

  LABICEX_COMMON_INLINE static real binary_mixture_natural_moment_matrix(
      int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);
    if (i == 0) {
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 1) {
      if (j < 27) return 0.0;
      return 1.0;
    }
    if (i == 2) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return -1.0;
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      if (j < 28) return 0.0;
      if (j < 29) return 1.0;
      if (j < 30) return -1.0;
      if (j < 34) return 0.0;
      if (j < 35) return 1.0;
      if (j < 36) return -1.0;
      if (j < 37) return 1.0;
      if (j < 38) return -1.0;
      if (j < 39) return 1.0;
      if (j < 40) return -1.0;
      if (j < 41) return 1.0;
      if (j < 42) return -1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 50) return -1.0;
      if (j < 51) return 1.0;
      if (j < 52) return -1.0;
      if (j < 53) return 1.0;
      return -1.0;
    }
    if (i == 3) {
      if (j < 3) return 0.0;
      if (j < 4) return 1.0;
      if (j < 5) return -1.0;
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      if (j < 30) return 0.0;
      if (j < 31) return 1.0;
      if (j < 32) return -1.0;
      if (j < 34) return 0.0;
      if (j < 36) return 1.0;
      if (j < 38) return -1.0;
      if (j < 42) return 0.0;
      if (j < 43) return 1.0;
      if (j < 44) return -1.0;
      if (j < 45) return 1.0;
      if (j < 46) return -1.0;
      if (j < 48) return 1.0;
      if (j < 50) return -1.0;
      if (j < 52) return 1.0;
      return -1.0;
    }
    if (i == 4) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      if (j < 32) return 0.0;
      if (j < 33) return 1.0;
      if (j < 34) return -1.0;
      if (j < 38) return 0.0;
      if (j < 40) return 1.0;
      if (j < 42) return -1.0;
      if (j < 44) return 1.0;
      if (j < 46) return -1.0;
      if (j < 50) return 1.0;
      return -1.0;
    }
    if (i == 5) {
      if (j < 1) return 0.0;
      if (j < 7) return 1.0;
      if (j < 19) return 2.0;
      if (j < 27) return 3.0;
      if (j < 28) return 0.0;
      if (j < 34) return 1.0;
      if (j < 46) return 2.0;
      return 3.0;
    }
    if (i == 6) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 3) return -1.0;
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 7) {
      if (j < 3) return 0.0;
      if (j < 4) return 1.0;
      if (j < 5) return -1.0;
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 8) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 7) return -1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 9) {
      if (j < 1) return 0.0;
      if (j < 7) return 1.0;
      if (j < 19) return 2.0;
      if (j < 27) return 3.0;
      return 0.0;
    }
    if (i == 10) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      if (j < 11) return 0.0;
      if (j < 27) return 1.0;
      if (j < 32) return 0.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      return 1.0;
    }
    if (i == 11) {
      if (j < 5) return 0.0;
      if (j < 7) return 1.0;
      if (j < 11) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 12) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 18) return -1.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      if (j < 27) return 1.0;
      if (j < 42) return 0.0;
      if (j < 43) return 1.0;
      if (j < 45) return -1.0;
      if (j < 48) return 1.0;
      if (j < 52) return -1.0;
      return 1.0;
    }
    if (i == 13) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 18) return -1.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 14) {
      if (j < 3) return 0.0;
      if (j < 5) return 1.0;
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 15) return 0.0;
      if (j < 27) return 1.0;
      if (j < 30) return 0.0;
      if (j < 32) return 1.0;
      if (j < 34) return 0.0;
      if (j < 38) return 1.0;
      if (j < 42) return 0.0;
      return 1.0;
    }
    if (i == 15) {
      if (j < 3) return 0.0;
      if (j < 5) return 1.0;
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 15) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 16) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 1.0;
      if (j < 41) return -1.0;
      if (j < 42) return 1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 51) return -1.0;
      if (j < 52) return 1.0;
      if (j < 53) return -1.0;
      return 1.0;
    }
    if (i == 17) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 18) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 10) return -1.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      if (j < 34) return 0.0;
      if (j < 35) return 1.0;
      if (j < 37) return -1.0;
      if (j < 38) return 1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 49) return -1.0;
      if (j < 51) return 1.0;
      if (j < 53) return -1.0;
      return 1.0;
    }
    if (i == 19) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 10) return -1.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 20) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 23) return 1.0;
      if (j < 24) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 49) return -1.0;
      if (j < 50) return 1.0;
      if (j < 51) return -1.0;
      if (j < 53) return 1.0;
      return -1.0;
    }
    if (i == 21) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 23) return 1.0;
      if (j < 24) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 22) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      if (j < 42) return 0.0;
      if (j < 43) return 1.0;
      if (j < 44) return -1.0;
      if (j < 45) return 1.0;
      if (j < 46) return -1.0;
      if (j < 48) return 1.0;
      if (j < 50) return -1.0;
      if (j < 52) return 1.0;
      return -1.0;
    }
    if (i == 23) {
      if (j < 15) return 0.0;
      if (j < 16) return 1.0;
      if (j < 17) return -1.0;
      if (j < 18) return 1.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 24) {
      if (j < 15) return 0.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      if (j < 42) return 0.0;
      if (j < 44) return 1.0;
      if (j < 46) return -1.0;
      if (j < 50) return 1.0;
      return -1.0;
    }
    if (i == 25) {
      if (j < 15) return 0.0;
      if (j < 17) return 1.0;
      if (j < 19) return -1.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 26) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 1.0;
      if (j < 40) return -1.0;
      if (j < 41) return 1.0;
      if (j < 42) return -1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 50) return -1.0;
      if (j < 51) return 1.0;
      if (j < 52) return -1.0;
      if (j < 53) return 1.0;
      return -1.0;
    }
    if (i == 27) {
      if (j < 11) return 0.0;
      if (j < 12) return 1.0;
      if (j < 13) return -1.0;
      if (j < 14) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 28) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      if (j < 34) return 0.0;
      if (j < 35) return 1.0;
      if (j < 36) return -1.0;
      if (j < 37) return 1.0;
      if (j < 38) return -1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 50) return -1.0;
      if (j < 51) return 1.0;
      if (j < 52) return -1.0;
      if (j < 53) return 1.0;
      return -1.0;
    }
    if (i == 29) {
      if (j < 7) return 0.0;
      if (j < 8) return 1.0;
      if (j < 9) return -1.0;
      if (j < 10) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 30) {
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      if (j < 38) return 0.0;
      if (j < 40) return 1.0;
      if (j < 42) return -1.0;
      if (j < 46) return 0.0;
      if (j < 50) return 1.0;
      return -1.0;
    }
    if (i == 31) {
      if (j < 11) return 0.0;
      if (j < 13) return 1.0;
      if (j < 15) return -1.0;
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 32) {
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      if (j < 34) return 0.0;
      if (j < 36) return 1.0;
      if (j < 38) return -1.0;
      if (j < 46) return 0.0;
      if (j < 48) return 1.0;
      if (j < 50) return -1.0;
      if (j < 52) return 1.0;
      return -1.0;
    }
    if (i == 33) {
      if (j < 7) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return -1.0;
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 34) {
      if (j < 15) return 0.0;
      if (j < 27) return 1.0;
      if (j < 42) return 0.0;
      return 1.0;
    }
    if (i == 35) {
      if (j < 15) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 36) {
      if (j < 11) return 0.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      if (j < 38) return 0.0;
      if (j < 42) return 1.0;
      if (j < 46) return 0.0;
      return 1.0;
    }
    if (i == 37) {
      if (j < 11) return 0.0;
      if (j < 15) return 1.0;
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 38) {
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      if (j < 34) return 0.0;
      if (j < 38) return 1.0;
      if (j < 46) return 0.0;
      return 1.0;
    }
    if (i == 39) {
      if (j < 7) return 0.0;
      if (j < 11) return 1.0;
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 40) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 49) return -1.0;
      if (j < 51) return 1.0;
      if (j < 53) return -1.0;
      return 1.0;
    }
    if (i == 41) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 22) return -1.0;
      if (j < 24) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 42) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 51) return -1.0;
      if (j < 52) return 1.0;
      if (j < 53) return -1.0;
      return 1.0;
    }
    if (i == 43) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 24) return -1.0;
      if (j < 25) return 1.0;
      if (j < 26) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 44) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      if (j < 27) return 1.0;
      if (j < 46) return 0.0;
      if (j < 48) return 1.0;
      if (j < 52) return -1.0;
      return 1.0;
    }
    if (i == 45) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 25) return -1.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
    if (i == 46) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      if (j < 46) return 0.0;
      if (j < 47) return 1.0;
      if (j < 48) return -1.0;
      if (j < 49) return 1.0;
      if (j < 50) return -1.0;
      if (j < 51) return 1.0;
      if (j < 52) return -1.0;
      if (j < 53) return 1.0;
      return -1.0;
    }
    if (i == 47) {
      if (j < 19) return 0.0;
      if (j < 20) return 1.0;
      if (j < 21) return -1.0;
      if (j < 22) return 1.0;
      if (j < 23) return -1.0;
      if (j < 24) return 1.0;
      if (j < 25) return -1.0;
      if (j < 26) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 48) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      if (j < 46) return 0.0;
      if (j < 48) return 1.0;
      if (j < 50) return -1.0;
      if (j < 52) return 1.0;
      return -1.0;
    }
    if (i == 49) {
      if (j < 19) return 0.0;
      if (j < 21) return 1.0;
      if (j < 23) return -1.0;
      if (j < 25) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 50) {
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      if (j < 46) return 0.0;
      if (j < 50) return 1.0;
      return -1.0;
    }
    if (i == 51) {
      if (j < 19) return 0.0;
      if (j < 23) return 1.0;
      if (j < 27) return -1.0;
      return 0.0;
    }
    if (i == 52) {
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      if (j < 46) return 0.0;
      return 1.0;
    }
    {
      if (j < 19) return 0.0;
      if (j < 27) return 1.0;
      return 0.0;
    }
  }

  LABICEX_COMMON_INLINE static real
  inverse_binary_mixture_natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 9) return 0.0;
      if (j < 10) return -1.0;
      if (j < 35) return 0.0;
      if (j < 36) return 1.0;
      if (j < 37) return 0.0;
      if (j < 38) return 1.0;
      if (j < 39) return 0.0;
      if (j < 40) return 1.0;
      if (j < 53) return 0.0;
      return -1.0;
    }
    if (i == 1) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.5;
      if (j < 9) return 0.0;
      if (j < 10) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5;
      if (j < 15) return 0.0;
      if (j < 16) return -0.5;
      if (j < 27) return 0.0;
      if (j < 28) return -0.5;
      if (j < 29) return 0.0;
      if (j < 30) return -0.5;
      if (j < 37) return 0.0;
      if (j < 38) return -0.5;
      if (j < 39) return 0.0;
      if (j < 40) return -0.5;
      if (j < 47) return 0.0;
      if (j < 48) return 0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 2) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.5;
      if (j < 9) return 0.0;
      if (j < 10) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5;
      if (j < 15) return 0.0;
      if (j < 16) return -0.5;
      if (j < 27) return 0.0;
      if (j < 28) return 0.5;
      if (j < 29) return 0.0;
      if (j < 30) return 0.5;
      if (j < 37) return 0.0;
      if (j < 38) return -0.5;
      if (j < 39) return 0.0;
      if (j < 40) return -0.5;
      if (j < 47) return 0.0;
      if (j < 48) return -0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 3) {
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 15) return 0.0;
      if (j < 16) return 0.5;
      if (j < 23) return 0.0;
      if (j < 24) return -0.5;
      if (j < 33) return 0.0;
      if (j < 34) return -0.5;
      if (j < 35) return 0.0;
      if (j < 36) return -0.5;
      if (j < 39) return 0.0;
      if (j < 40) return -0.5;
      if (j < 49) return 0.0;
      if (j < 50) return 0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 4) {
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 15) return 0.0;
      if (j < 16) return 0.5;
      if (j < 23) return 0.0;
      if (j < 24) return 0.5;
      if (j < 33) return 0.0;
      if (j < 34) return 0.5;
      if (j < 35) return 0.0;
      if (j < 36) return -0.5;
      if (j < 39) return 0.0;
      if (j < 40) return -0.5;
      if (j < 49) return 0.0;
      if (j < 50) return -0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 5) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.5;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5;
      if (j < 25) return 0.0;
      if (j < 26) return -0.5;
      if (j < 31) return 0.0;
      if (j < 32) return -0.5;
      if (j < 35) return 0.0;
      if (j < 36) return -0.5;
      if (j < 37) return 0.0;
      if (j < 38) return -0.5;
      if (j < 51) return 0.0;
      if (j < 52) return 0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 6) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.5;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5;
      if (j < 25) return 0.0;
      if (j < 26) return 0.5;
      if (j < 31) return 0.0;
      if (j < 32) return 0.5;
      if (j < 35) return 0.0;
      if (j < 36) return -0.5;
      if (j < 37) return 0.0;
      if (j < 38) return -0.5;
      if (j < 51) return 0.0;
      if (j < 52) return -0.5;
      if (j < 53) return 0.0;
      return 0.5;
    }
    if (i == 7) {
      if (j < 19) return 0.0;
      if (j < 20) return 0.25;
      if (j < 29) return 0.0;
      if (j < 30) return 0.25;
      if (j < 33) return 0.0;
      if (j < 34) return 0.25;
      if (j < 39) return 0.0;
      if (j < 40) return 0.25;
      if (j < 41) return 0.0;
      if (j < 42) return -0.25;
      if (j < 47) return 0.0;
      if (j < 48) return -0.25;
      if (j < 49) return 0.0;
      if (j < 50) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 8) {
      if (j < 19) return 0.0;
      if (j < 20) return -0.25;
      if (j < 29) return 0.0;
      if (j < 30) return -0.25;
      if (j < 33) return 0.0;
      if (j < 34) return 0.25;
      if (j < 39) return 0.0;
      if (j < 40) return 0.25;
      if (j < 41) return 0.0;
      if (j < 42) return 0.25;
      if (j < 47) return 0.0;
      if (j < 48) return 0.25;
      if (j < 49) return 0.0;
      if (j < 50) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 9) {
      if (j < 19) return 0.0;
      if (j < 20) return -0.25;
      if (j < 29) return 0.0;
      if (j < 30) return 0.25;
      if (j < 33) return 0.0;
      if (j < 34) return -0.25;
      if (j < 39) return 0.0;
      if (j < 40) return 0.25;
      if (j < 41) return 0.0;
      if (j < 42) return 0.25;
      if (j < 47) return 0.0;
      if (j < 48) return -0.25;
      if (j < 49) return 0.0;
      if (j < 50) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 10) {
      if (j < 19) return 0.0;
      if (j < 20) return 0.25;
      if (j < 29) return 0.0;
      if (j < 30) return -0.25;
      if (j < 33) return 0.0;
      if (j < 34) return -0.25;
      if (j < 39) return 0.0;
      if (j < 40) return 0.25;
      if (j < 41) return 0.0;
      if (j < 42) return -0.25;
      if (j < 47) return 0.0;
      if (j < 48) return 0.25;
      if (j < 49) return 0.0;
      if (j < 50) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 11) {
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 27) return 0.0;
      if (j < 28) return 0.25;
      if (j < 31) return 0.0;
      if (j < 32) return 0.25;
      if (j < 37) return 0.0;
      if (j < 38) return 0.25;
      if (j < 43) return 0.0;
      if (j < 44) return -0.25;
      if (j < 47) return 0.0;
      if (j < 48) return -0.25;
      if (j < 51) return 0.0;
      if (j < 52) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 12) {
      if (j < 17) return 0.0;
      if (j < 18) return -0.25;
      if (j < 27) return 0.0;
      if (j < 28) return -0.25;
      if (j < 31) return 0.0;
      if (j < 32) return 0.25;
      if (j < 37) return 0.0;
      if (j < 38) return 0.25;
      if (j < 43) return 0.0;
      if (j < 44) return 0.25;
      if (j < 47) return 0.0;
      if (j < 48) return 0.25;
      if (j < 51) return 0.0;
      if (j < 52) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 13) {
      if (j < 17) return 0.0;
      if (j < 18) return -0.25;
      if (j < 27) return 0.0;
      if (j < 28) return 0.25;
      if (j < 31) return 0.0;
      if (j < 32) return -0.25;
      if (j < 37) return 0.0;
      if (j < 38) return 0.25;
      if (j < 43) return 0.0;
      if (j < 44) return 0.25;
      if (j < 47) return 0.0;
      if (j < 48) return -0.25;
      if (j < 51) return 0.0;
      if (j < 52) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 14) {
      if (j < 17) return 0.0;
      if (j < 18) return 0.25;
      if (j < 27) return 0.0;
      if (j < 28) return -0.25;
      if (j < 31) return 0.0;
      if (j < 32) return -0.25;
      if (j < 37) return 0.0;
      if (j < 38) return 0.25;
      if (j < 43) return 0.0;
      if (j < 44) return -0.25;
      if (j < 47) return 0.0;
      if (j < 48) return 0.25;
      if (j < 51) return 0.0;
      if (j < 52) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 15) {
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return 0.25;
      if (j < 25) return 0.0;
      if (j < 26) return 0.25;
      if (j < 35) return 0.0;
      if (j < 36) return 0.25;
      if (j < 45) return 0.0;
      if (j < 46) return -0.25;
      if (j < 49) return 0.0;
      if (j < 50) return -0.25;
      if (j < 51) return 0.0;
      if (j < 52) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 16) {
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 23) return 0.0;
      if (j < 24) return -0.25;
      if (j < 25) return 0.0;
      if (j < 26) return 0.25;
      if (j < 35) return 0.0;
      if (j < 36) return 0.25;
      if (j < 45) return 0.0;
      if (j < 46) return 0.25;
      if (j < 49) return 0.0;
      if (j < 50) return 0.25;
      if (j < 51) return 0.0;
      if (j < 52) return -0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 17) {
      if (j < 13) return 0.0;
      if (j < 14) return -0.25;
      if (j < 23) return 0.0;
      if (j < 24) return 0.25;
      if (j < 25) return 0.0;
      if (j < 26) return -0.25;
      if (j < 35) return 0.0;
      if (j < 36) return 0.25;
      if (j < 45) return 0.0;
      if (j < 46) return 0.25;
      if (j < 49) return 0.0;
      if (j < 50) return -0.25;
      if (j < 51) return 0.0;
      if (j < 52) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 18) {
      if (j < 13) return 0.0;
      if (j < 14) return 0.25;
      if (j < 23) return 0.0;
      if (j < 24) return -0.25;
      if (j < 25) return 0.0;
      if (j < 26) return -0.25;
      if (j < 35) return 0.0;
      if (j < 36) return 0.25;
      if (j < 45) return 0.0;
      if (j < 46) return -0.25;
      if (j < 49) return 0.0;
      if (j < 50) return 0.25;
      if (j < 51) return 0.0;
      if (j < 52) return 0.25;
      if (j < 53) return 0.0;
      return -0.25;
    }
    if (i == 19) {
      if (j < 21) return 0.0;
      if (j < 22) return 0.125;
      if (j < 41) return 0.0;
      if (j < 42) return 0.125;
      if (j < 43) return 0.0;
      if (j < 44) return 0.125;
      if (j < 45) return 0.0;
      if (j < 46) return 0.125;
      if (j < 47) return 0.0;
      if (j < 48) return 0.125;
      if (j < 49) return 0.0;
      if (j < 50) return 0.125;
      if (j < 51) return 0.0;
      if (j < 52) return 0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 20) {
      if (j < 21) return 0.0;
      if (j < 22) return -0.125;
      if (j < 41) return 0.0;
      if (j < 42) return -0.125;
      if (j < 43) return 0.0;
      if (j < 44) return -0.125;
      if (j < 45) return 0.0;
      if (j < 46) return 0.125;
      if (j < 47) return 0.0;
      if (j < 48) return -0.125;
      if (j < 49) return 0.0;
      if (j < 50) return 0.125;
      if (j < 51) return 0.0;
      if (j < 52) return 0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 21) {
      if (j < 21) return 0.0;
      if (j < 22) return -0.125;
      if (j < 41) return 0.0;
      if (j < 42) return -0.125;
      if (j < 43) return 0.0;
      if (j < 44) return 0.125;
      if (j < 45) return 0.0;
      if (j < 46) return -0.125;
      if (j < 47) return 0.0;
      if (j < 48) return 0.125;
      if (j < 49) return 0.0;
      if (j < 50) return -0.125;
      if (j < 51) return 0.0;
      if (j < 52) return 0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 22) {
      if (j < 21) return 0.0;
      if (j < 22) return 0.125;
      if (j < 41) return 0.0;
      if (j < 42) return 0.125;
      if (j < 43) return 0.0;
      if (j < 44) return -0.125;
      if (j < 45) return 0.0;
      if (j < 46) return -0.125;
      if (j < 47) return 0.0;
      if (j < 48) return -0.125;
      if (j < 49) return 0.0;
      if (j < 50) return -0.125;
      if (j < 51) return 0.0;
      if (j < 52) return 0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 23) {
      if (j < 21) return 0.0;
      if (j < 22) return -0.125;
      if (j < 41) return 0.0;
      if (j < 42) return 0.125;
      if (j < 43) return 0.0;
      if (j < 44) return -0.125;
      if (j < 45) return 0.0;
      if (j < 46) return -0.125;
      if (j < 47) return 0.0;
      if (j < 48) return 0.125;
      if (j < 49) return 0.0;
      if (j < 50) return 0.125;
      if (j < 51) return 0.0;
      if (j < 52) return -0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 24) {
      if (j < 21) return 0.0;
      if (j < 22) return 0.125;
      if (j < 41) return 0.0;
      if (j < 42) return -0.125;
      if (j < 43) return 0.0;
      if (j < 44) return 0.125;
      if (j < 45) return 0.0;
      if (j < 46) return -0.125;
      if (j < 47) return 0.0;
      if (j < 48) return -0.125;
      if (j < 49) return 0.0;
      if (j < 50) return 0.125;
      if (j < 51) return 0.0;
      if (j < 52) return -0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 25) {
      if (j < 21) return 0.0;
      if (j < 22) return 0.125;
      if (j < 41) return 0.0;
      if (j < 42) return -0.125;
      if (j < 43) return 0.0;
      if (j < 44) return -0.125;
      if (j < 45) return 0.0;
      if (j < 46) return 0.125;
      if (j < 47) return 0.0;
      if (j < 48) return 0.125;
      if (j < 49) return 0.0;
      if (j < 50) return -0.125;
      if (j < 51) return 0.0;
      if (j < 52) return -0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 26) {
      if (j < 21) return 0.0;
      if (j < 22) return -0.125;
      if (j < 41) return 0.0;
      if (j < 42) return 0.125;
      if (j < 43) return 0.0;
      if (j < 44) return 0.125;
      if (j < 45) return 0.0;
      if (j < 46) return 0.125;
      if (j < 47) return 0.0;
      if (j < 48) return -0.125;
      if (j < 49) return 0.0;
      if (j < 50) return -0.125;
      if (j < 51) return 0.0;
      if (j < 52) return -0.125;
      if (j < 53) return 0.0;
      return 0.125;
    }
    if (i == 27) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 9) return 0.0;
      if (j < 10) return 1.0;
      if (j < 34) return 0.0;
      if (j < 35) return 1.0;
      if (j < 36) return -1.0;
      if (j < 37) return 1.0;
      if (j < 38) return -1.0;
      if (j < 39) return 1.0;
      if (j < 40) return -1.0;
      if (j < 52) return 0.0;
      if (j < 53) return -1.0;
      return 1.0;
    }
    if (i == 28) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5;
      if (j < 5) return 0.0;
      if (j < 6) return 0.5;
      if (j < 7) return -0.5;
      if (j < 9) return 0.0;
      if (j < 11) return -0.5;
      if (j < 12) return 0.5;
      if (j < 14) return 0.0;
      if (j < 15) return -0.5;
      if (j < 16) return 0.5;
      if (j < 26) return 0.0;
      if (j < 27) return -0.5;
      if (j < 28) return 0.5;
      if (j < 29) return -0.5;
      if (j < 30) return 0.5;
      if (j < 36) return 0.0;
      if (j < 37) return -0.5;
      if (j < 38) return 0.5;
      if (j < 39) return -0.5;
      if (j < 40) return 0.5;
      if (j < 46) return 0.0;
      if (j < 47) return 0.5;
      if (j < 48) return -0.5;
      if (j < 52) return 0.0;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 29) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5;
      if (j < 5) return 0.0;
      if (j < 7) return 0.5;
      if (j < 9) return 0.0;
      if (j < 11) return -0.5;
      if (j < 12) return 0.5;
      if (j < 14) return 0.0;
      if (j < 15) return -0.5;
      if (j < 16) return 0.5;
      if (j < 26) return 0.0;
      if (j < 27) return 0.5;
      if (j < 28) return -0.5;
      if (j < 29) return 0.5;
      if (j < 30) return -0.5;
      if (j < 36) return 0.0;
      if (j < 37) return -0.5;
      if (j < 38) return 0.5;
      if (j < 39) return -0.5;
      if (j < 40) return 0.5;
      if (j < 46) return 0.0;
      if (j < 47) return -0.5;
      if (j < 48) return 0.5;
      if (j < 52) return 0.0;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 30) {
      if (j < 3) return 0.0;
      if (j < 4) return 0.5;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5;
      if (j < 14) return 0.0;
      if (j < 15) return 0.5;
      if (j < 16) return -0.5;
      if (j < 22) return 0.0;
      if (j < 23) return -0.5;
      if (j < 24) return 0.5;
      if (j < 32) return 0.0;
      if (j < 33) return -0.5;
      if (j < 34) return 0.5;
      if (j < 35) return -0.5;
      if (j < 36) return 0.5;
      if (j < 38) return 0.0;
      if (j < 39) return -0.5;
      if (j < 40) return 0.5;
      if (j < 48) return 0.0;
      if (j < 49) return 0.5;
      if (j < 50) return -0.5;
      if (j < 52) return 0.0;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 31) {
      if (j < 3) return 0.0;
      if (j < 4) return -0.5;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5;
      if (j < 14) return 0.0;
      if (j < 15) return 0.5;
      if (j < 16) return -0.5;
      if (j < 22) return 0.0;
      if (j < 23) return 0.5;
      if (j < 24) return -0.5;
      if (j < 32) return 0.0;
      if (j < 33) return 0.5;
      if (j < 35) return -0.5;
      if (j < 36) return 0.5;
      if (j < 38) return 0.0;
      if (j < 39) return -0.5;
      if (j < 40) return 0.5;
      if (j < 48) return 0.0;
      if (j < 49) return -0.5;
      if (j < 50) return 0.5;
      if (j < 52) return 0.0;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 32) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.5;
      if (j < 8) return 0.0;
      if (j < 9) return -0.5;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5;
      if (j < 12) return -0.5;
      if (j < 24) return 0.0;
      if (j < 25) return -0.5;
      if (j < 26) return 0.5;
      if (j < 30) return 0.0;
      if (j < 31) return -0.5;
      if (j < 32) return 0.5;
      if (j < 34) return 0.0;
      if (j < 35) return -0.5;
      if (j < 36) return 0.5;
      if (j < 37) return -0.5;
      if (j < 38) return 0.5;
      if (j < 50) return 0.0;
      if (j < 51) return 0.5;
      if (j < 52) return -0.5;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 33) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.5;
      if (j < 8) return 0.0;
      if (j < 9) return 0.5;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5;
      if (j < 12) return -0.5;
      if (j < 24) return 0.0;
      if (j < 25) return 0.5;
      if (j < 26) return -0.5;
      if (j < 30) return 0.0;
      if (j < 31) return 0.5;
      if (j < 32) return -0.5;
      if (j < 34) return 0.0;
      if (j < 35) return -0.5;
      if (j < 36) return 0.5;
      if (j < 37) return -0.5;
      if (j < 38) return 0.5;
      if (j < 50) return 0.0;
      if (j < 51) return -0.5;
      if (j < 53) return 0.5;
      return -0.5;
    }
    if (i == 34) {
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 20) return -0.25;
      if (j < 28) return 0.0;
      if (j < 29) return 0.25;
      if (j < 30) return -0.25;
      if (j < 32) return 0.0;
      if (j < 33) return 0.25;
      if (j < 34) return -0.25;
      if (j < 38) return 0.0;
      if (j < 39) return 0.25;
      if (j < 41) return -0.25;
      if (j < 42) return 0.25;
      if (j < 46) return 0.0;
      if (j < 47) return -0.25;
      if (j < 48) return 0.25;
      if (j < 49) return -0.25;
      if (j < 50) return 0.25;
      if (j < 52) return 0.0;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 35) {
      if (j < 18) return 0.0;
      if (j < 19) return -0.25;
      if (j < 20) return 0.25;
      if (j < 28) return 0.0;
      if (j < 29) return -0.25;
      if (j < 30) return 0.25;
      if (j < 32) return 0.0;
      if (j < 33) return 0.25;
      if (j < 34) return -0.25;
      if (j < 38) return 0.0;
      if (j < 39) return 0.25;
      if (j < 40) return -0.25;
      if (j < 41) return 0.25;
      if (j < 42) return -0.25;
      if (j < 46) return 0.0;
      if (j < 47) return 0.25;
      if (j < 49) return -0.25;
      if (j < 50) return 0.25;
      if (j < 52) return 0.0;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 36) {
      if (j < 18) return 0.0;
      if (j < 19) return -0.25;
      if (j < 20) return 0.25;
      if (j < 28) return 0.0;
      if (j < 29) return 0.25;
      if (j < 30) return -0.25;
      if (j < 32) return 0.0;
      if (j < 33) return -0.25;
      if (j < 34) return 0.25;
      if (j < 38) return 0.0;
      if (j < 39) return 0.25;
      if (j < 40) return -0.25;
      if (j < 41) return 0.25;
      if (j < 42) return -0.25;
      if (j < 46) return 0.0;
      if (j < 47) return -0.25;
      if (j < 49) return 0.25;
      if (j < 50) return -0.25;
      if (j < 52) return 0.0;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 37) {
      if (j < 18) return 0.0;
      if (j < 19) return 0.25;
      if (j < 20) return -0.25;
      if (j < 28) return 0.0;
      if (j < 29) return -0.25;
      if (j < 30) return 0.25;
      if (j < 32) return 0.0;
      if (j < 33) return -0.25;
      if (j < 34) return 0.25;
      if (j < 38) return 0.0;
      if (j < 39) return 0.25;
      if (j < 41) return -0.25;
      if (j < 42) return 0.25;
      if (j < 46) return 0.0;
      if (j < 47) return 0.25;
      if (j < 48) return -0.25;
      if (j < 49) return 0.25;
      if (j < 50) return -0.25;
      if (j < 52) return 0.0;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 38) {
      if (j < 16) return 0.0;
      if (j < 17) return 0.25;
      if (j < 18) return -0.25;
      if (j < 26) return 0.0;
      if (j < 27) return 0.25;
      if (j < 28) return -0.25;
      if (j < 30) return 0.0;
      if (j < 31) return 0.25;
      if (j < 32) return -0.25;
      if (j < 36) return 0.0;
      if (j < 37) return 0.25;
      if (j < 38) return -0.25;
      if (j < 42) return 0.0;
      if (j < 43) return -0.25;
      if (j < 44) return 0.25;
      if (j < 46) return 0.0;
      if (j < 47) return -0.25;
      if (j < 48) return 0.25;
      if (j < 50) return 0.0;
      if (j < 51) return -0.25;
      if (j < 52) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 39) {
      if (j < 16) return 0.0;
      if (j < 17) return -0.25;
      if (j < 18) return 0.25;
      if (j < 26) return 0.0;
      if (j < 27) return -0.25;
      if (j < 28) return 0.25;
      if (j < 30) return 0.0;
      if (j < 31) return 0.25;
      if (j < 32) return -0.25;
      if (j < 36) return 0.0;
      if (j < 37) return 0.25;
      if (j < 38) return -0.25;
      if (j < 42) return 0.0;
      if (j < 43) return 0.25;
      if (j < 44) return -0.25;
      if (j < 46) return 0.0;
      if (j < 47) return 0.25;
      if (j < 48) return -0.25;
      if (j < 50) return 0.0;
      if (j < 51) return -0.25;
      if (j < 52) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 40) {
      if (j < 16) return 0.0;
      if (j < 17) return -0.25;
      if (j < 18) return 0.25;
      if (j < 26) return 0.0;
      if (j < 27) return 0.25;
      if (j < 28) return -0.25;
      if (j < 30) return 0.0;
      if (j < 31) return -0.25;
      if (j < 32) return 0.25;
      if (j < 36) return 0.0;
      if (j < 37) return 0.25;
      if (j < 38) return -0.25;
      if (j < 42) return 0.0;
      if (j < 43) return 0.25;
      if (j < 44) return -0.25;
      if (j < 46) return 0.0;
      if (j < 47) return -0.25;
      if (j < 48) return 0.25;
      if (j < 50) return 0.0;
      if (j < 51) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 41) {
      if (j < 16) return 0.0;
      if (j < 17) return 0.25;
      if (j < 18) return -0.25;
      if (j < 26) return 0.0;
      if (j < 27) return -0.25;
      if (j < 28) return 0.25;
      if (j < 30) return 0.0;
      if (j < 31) return -0.25;
      if (j < 32) return 0.25;
      if (j < 36) return 0.0;
      if (j < 37) return 0.25;
      if (j < 38) return -0.25;
      if (j < 42) return 0.0;
      if (j < 43) return -0.25;
      if (j < 44) return 0.25;
      if (j < 46) return 0.0;
      if (j < 47) return 0.25;
      if (j < 48) return -0.25;
      if (j < 50) return 0.0;
      if (j < 51) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 42) {
      if (j < 12) return 0.0;
      if (j < 13) return 0.25;
      if (j < 14) return -0.25;
      if (j < 22) return 0.0;
      if (j < 23) return 0.25;
      if (j < 24) return -0.25;
      if (j < 25) return 0.25;
      if (j < 26) return -0.25;
      if (j < 34) return 0.0;
      if (j < 35) return 0.25;
      if (j < 36) return -0.25;
      if (j < 44) return 0.0;
      if (j < 45) return -0.25;
      if (j < 46) return 0.25;
      if (j < 48) return 0.0;
      if (j < 49) return -0.25;
      if (j < 50) return 0.25;
      if (j < 51) return -0.25;
      if (j < 52) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 43) {
      if (j < 12) return 0.0;
      if (j < 13) return -0.25;
      if (j < 14) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return -0.25;
      if (j < 25) return 0.25;
      if (j < 26) return -0.25;
      if (j < 34) return 0.0;
      if (j < 35) return 0.25;
      if (j < 36) return -0.25;
      if (j < 44) return 0.0;
      if (j < 45) return 0.25;
      if (j < 46) return -0.25;
      if (j < 48) return 0.0;
      if (j < 49) return 0.25;
      if (j < 51) return -0.25;
      if (j < 52) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 44) {
      if (j < 12) return 0.0;
      if (j < 13) return -0.25;
      if (j < 14) return 0.25;
      if (j < 22) return 0.0;
      if (j < 23) return 0.25;
      if (j < 25) return -0.25;
      if (j < 26) return 0.25;
      if (j < 34) return 0.0;
      if (j < 35) return 0.25;
      if (j < 36) return -0.25;
      if (j < 44) return 0.0;
      if (j < 45) return 0.25;
      if (j < 46) return -0.25;
      if (j < 48) return 0.0;
      if (j < 49) return -0.25;
      if (j < 51) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 45) {
      if (j < 12) return 0.0;
      if (j < 13) return 0.25;
      if (j < 14) return -0.25;
      if (j < 22) return 0.0;
      if (j < 23) return -0.25;
      if (j < 24) return 0.25;
      if (j < 25) return -0.25;
      if (j < 26) return 0.25;
      if (j < 34) return 0.0;
      if (j < 35) return 0.25;
      if (j < 36) return -0.25;
      if (j < 44) return 0.0;
      if (j < 45) return -0.25;
      if (j < 46) return 0.25;
      if (j < 48) return 0.0;
      if (j < 49) return 0.25;
      if (j < 50) return -0.25;
      if (j < 51) return 0.25;
      if (j < 53) return -0.25;
      return 0.25;
    }
    if (i == 46) {
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 22) return -0.125;
      if (j < 40) return 0.0;
      if (j < 41) return 0.125;
      if (j < 42) return -0.125;
      if (j < 43) return 0.125;
      if (j < 44) return -0.125;
      if (j < 45) return 0.125;
      if (j < 46) return -0.125;
      if (j < 47) return 0.125;
      if (j < 48) return -0.125;
      if (j < 49) return 0.125;
      if (j < 50) return -0.125;
      if (j < 51) return 0.125;
      if (j < 52) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 47) {
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 40) return 0.0;
      if (j < 41) return -0.125;
      if (j < 42) return 0.125;
      if (j < 43) return -0.125;
      if (j < 45) return 0.125;
      if (j < 47) return -0.125;
      if (j < 49) return 0.125;
      if (j < 50) return -0.125;
      if (j < 51) return 0.125;
      if (j < 52) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 48) {
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 40) return 0.0;
      if (j < 41) return -0.125;
      if (j < 43) return 0.125;
      if (j < 45) return -0.125;
      if (j < 47) return 0.125;
      if (j < 49) return -0.125;
      if (j < 51) return 0.125;
      if (j < 52) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 49) {
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 22) return -0.125;
      if (j < 40) return 0.0;
      if (j < 41) return 0.125;
      if (j < 43) return -0.125;
      if (j < 44) return 0.125;
      if (j < 45) return -0.125;
      if (j < 46) return 0.125;
      if (j < 47) return -0.125;
      if (j < 48) return 0.125;
      if (j < 49) return -0.125;
      if (j < 51) return 0.125;
      if (j < 52) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 50) {
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 40) return 0.0;
      if (j < 41) return 0.125;
      if (j < 43) return -0.125;
      if (j < 44) return 0.125;
      if (j < 45) return -0.125;
      if (j < 47) return 0.125;
      if (j < 48) return -0.125;
      if (j < 49) return 0.125;
      if (j < 51) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 51) {
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 22) return -0.125;
      if (j < 40) return 0.0;
      if (j < 41) return -0.125;
      if (j < 43) return 0.125;
      if (j < 45) return -0.125;
      if (j < 46) return 0.125;
      if (j < 47) return -0.125;
      if (j < 49) return 0.125;
      if (j < 51) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    if (i == 52) {
      if (j < 20) return 0.0;
      if (j < 21) return 0.125;
      if (j < 22) return -0.125;
      if (j < 40) return 0.0;
      if (j < 41) return -0.125;
      if (j < 42) return 0.125;
      if (j < 43) return -0.125;
      if (j < 45) return 0.125;
      if (j < 46) return -0.125;
      if (j < 47) return 0.125;
      if (j < 49) return -0.125;
      if (j < 50) return 0.125;
      if (j < 51) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
    {
      if (j < 20) return 0.0;
      if (j < 21) return -0.125;
      if (j < 22) return 0.125;
      if (j < 40) return 0.0;
      if (j < 41) return 0.125;
      if (j < 42) return -0.125;
      if (j < 43) return 0.125;
      if (j < 44) return -0.125;
      if (j < 45) return 0.125;
      if (j < 47) return -0.125;
      if (j < 48) return 0.125;
      if (j < 49) return -0.125;
      if (j < 50) return 0.125;
      if (j < 51) return -0.125;
      if (j < 53) return 0.125;
      return -0.125;
    }
  }
};

}  // standard

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_STANDARD_D3Q27_H

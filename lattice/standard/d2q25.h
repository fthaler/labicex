#ifndef LABICEX_LATTICE_STANDARD_D2Q25_H
#define LABICEX_LATTICE_STANDARD_D2Q25_H

#include <cassert>

#include "lattice/standard/common.h"
#include "lattice/tags.h"

namespace labicex {

namespace lattice {

namespace standard {

struct d2q25 {
  typedef vector_types<2>::rvector rvector;
  typedef vector_types<2>::rtensor rtensor;
  typedef vector_types<2>::ivector ivector;
  typedef vector_types<25>::rvector fvector;
  typedef conserved_moments<d2q25> conserved_moments;
  typedef grad_moments<d2q25> grad_moments;
  typedef primitive_variables<d2q25> primitive_variables;
  typedef equilibrium<d2q25> equilibrium;
  typedef numerical_equilibrium<d2q25> numerical_equilibrium;
  typedef grad_approximation<d2q25> grad_approximation;
  typedef standard_tag lattice_category;
  static const int d = 2;
  static const int q = 25;
  static const int max_ci = 3;

  LABICEX_COMMON_INLINE static ivector ci(int i) {
    assert(i >= 0 && i < q);
    ivector ci;

    // x
    if (i == 9 || i == 13 || i == 14 || i == 21 || i == 22) ci(0) = -3;
    if (i == 1 || i == 5 || i == 6 || i == 17 || i == 18) ci(0) = -1;
    if (i == 0 || i == 3 || i == 4 || i == 11 || i == 12) ci(0) = 0;
    if (i == 2 || i == 7 || i == 8 || i == 19 || i == 20) ci(0) = 1;
    if (i == 10 || i == 15 || i == 16 || i == 23 || i == 24) ci(0) = 3;

    // y
    if (i == 11 || i == 17 || i == 19 || i == 21 || i == 23) ci(1) = -3;
    if (i == 3 || i == 5 || i == 7 || i == 13 || i == 15) ci(1) = -1;
    if (i == 0 || i == 1 || i == 2 || i == 9 || i == 10) ci(1) = 0;
    if (i == 4 || i == 6 || i == 8 || i == 14 || i == 16) ci(1) = 1;
    if (i == 12 || i == 18 || i == 20 || i == 22 || i == 24) ci(1) = 3;

    return ci;
  }

  LABICEX_COMMON_INLINE static rvector cf(int i) {
    assert(i >= 0 && i < q);
    rvector ci;

    // x
    if (i == 9 || i == 13 || i == 14 || i == 21 || i == 22) ci(0) = -3.0;
    if (i == 1 || i == 5 || i == 6 || i == 17 || i == 18) ci(0) = -1.0;
    if (i == 0 || i == 3 || i == 4 || i == 11 || i == 12) ci(0) = 0.0;
    if (i == 2 || i == 7 || i == 8 || i == 19 || i == 20) ci(0) = 1.0;
    if (i == 10 || i == 15 || i == 16 || i == 23 || i == 24) ci(0) = 3.0;

    // y
    if (i == 11 || i == 17 || i == 19 || i == 21 || i == 23) ci(1) = -3.0;
    if (i == 3 || i == 5 || i == 7 || i == 13 || i == 15) ci(1) = -1.0;
    if (i == 0 || i == 1 || i == 2 || i == 9 || i == 10) ci(1) = 0.0;
    if (i == 4 || i == 6 || i == 8 || i == 14 || i == 16) ci(1) = 1.0;
    if (i == 12 || i == 18 || i == 20 || i == 22 || i == 24) ci(1) = 3.0;

    return ci;
  }

  LABICEX_COMMON_INLINE static int ci2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i <= 4) return 1;
    if (i <= 8) return 2;
    if (i <= 12) return 9;
    if (i <= 20) return 10;
    return 18;
  }

  LABICEX_COMMON_INLINE static real cf2(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 4) return 1.0;
    if (i <= 8) return 2.0;
    if (i <= 12) return 9.0;
    if (i <= 20) return 10.0;
    return 18.0;
  }

  LABICEX_COMMON_INLINE static real cf_magnitude(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 4) return 1.0;
    if (i <= 8) return 1.41421356237309514547;
    if (i <= 12) return 3.0;
    if (i <= 20) return 3.16227766016837952279;
    return 4.24264068711928477029;
  }

  LABICEX_COMMON_INLINE static int ci_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i <= 8) return 1;
    return 3;
  }

  LABICEX_COMMON_INLINE static real cf_maxnorm(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.0;
    if (i <= 8) return 1.0;
    return 3.0;
  }

  LABICEX_COMMON_INLINE static real w(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0.40531927926002597751;
    if (i <= 4) return 0.11549703546891171291;
    if (i <= 8) return 0.03291125264621145435;
    if (i <= 12) return 0.00016677646411437935;
    if (i <= 20) return 0.00004752349117555993;
    return 0.00000006862340482119;
  }

  LABICEX_COMMON_INLINE static real w(int i, const primitive_variables&) {
    return w(i);
  }

  LABICEX_COMMON_INLINE static int reverse_index(int i) {
    assert(i >= 0 && i < q);
    if (i == 0) return 0;
    if (i == 1) return 2;
    if (i == 2) return 1;
    if (i == 3) return 4;
    if (i == 4) return 3;
    if (i == 5) return 8;
    if (i == 6) return 7;
    if (i == 7) return 6;
    if (i == 8) return 5;
    if (i == 9) return 10;
    if (i == 10) return 9;
    if (i == 11) return 12;
    if (i == 12) return 11;
    if (i == 13) return 16;
    if (i == 14) return 15;
    if (i == 15) return 14;
    if (i == 16) return 13;
    if (i == 17) return 20;
    if (i == 18) return 19;
    if (i == 19) return 18;
    if (i == 20) return 17;
    if (i == 21) return 24;
    if (i == 22) return 23;
    if (i == 23) return 22;
    return 21;
  }

  LABICEX_COMMON_INLINE static int single_reverse_index(int i, int axis) {
    assert(i >= 0 && i < q);
    assert(axis >= 0 && axis < d);
    if (axis == 0) {
      if (i == 0) return 0;
      if (i == 1) return 2;
      if (i == 2) return 1;
      if (i == 3) return 3;
      if (i == 4) return 4;
      if (i == 5) return 7;
      if (i == 6) return 8;
      if (i == 7) return 5;
      if (i == 8) return 6;
      if (i == 9) return 10;
      if (i == 10) return 9;
      if (i == 11) return 11;
      if (i == 12) return 12;
      if (i == 13) return 15;
      if (i == 14) return 16;
      if (i == 15) return 13;
      if (i == 16) return 14;
      if (i == 17) return 19;
      if (i == 18) return 20;
      if (i == 19) return 17;
      if (i == 20) return 18;
      if (i == 21) return 23;
      if (i == 22) return 24;
      if (i == 23) return 21;
      return 22;
    } else {
      if (i == 0) return 0;
      if (i == 1) return 1;
      if (i == 2) return 2;
      if (i == 3) return 4;
      if (i == 4) return 3;
      if (i == 5) return 6;
      if (i == 6) return 5;
      if (i == 7) return 8;
      if (i == 8) return 7;
      if (i == 9) return 9;
      if (i == 10) return 10;
      if (i == 11) return 12;
      if (i == 12) return 11;
      if (i == 13) return 14;
      if (i == 14) return 13;
      if (i == 15) return 16;
      if (i == 16) return 15;
      if (i == 17) return 18;
      if (i == 18) return 17;
      if (i == 19) return 20;
      if (i == 20) return 19;
      if (i == 21) return 22;
      if (i == 22) return 21;
      if (i == 23) return 24;
      return 23;
    }
  }

  LABICEX_COMMON_INLINE static int unit_index(int j) {
    assert(j >= 0 && j < d);
    if (j == 0) return 2;
    return 4;
  }

  LABICEX_COMMON_INLINE static real reference_temperature() {
    return 0.36754446796632411765;
  }

  LABICEX_COMMON_INLINE static real natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      return 1.0;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -3.0;
      if (j < 11) return 3.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -3.0;
      return 3.0;
    }
    if (i == 2) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -3.0;
      if (j < 13) return 3.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -3.0;
      if (j < 23) return 3.0;
      if (j < 24) return -3.0;
      return 3.0;
    }
    if (i == 3) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      if (j < 9) return 2.0;
      if (j < 13) return 9.0;
      if (j < 21) return 10.0;
      return 18.0;
    }
    if (i == 4) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 9.0;
      if (j < 24) return -9.0;
      return 9.0;
    }
    if (i == 5) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 9.0;
      if (j < 17) return 1.0;
      return 9.0;
    }
    if (i == 6) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -27.0;
      if (j < 13) return 27.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      return 27.0;
    }
    if (i == 7) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -27.0;
      if (j < 11) return 27.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -27.0;
      return 27.0;
    }
    if (i == 8) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -27.0;
      return 27.0;
    }
    if (i == 9) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      return 27.0;
    }
    if (i == 10) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 9.0;
      return 81.0;
    }
    if (i == 11) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 81.0;
      if (j < 17) return 1.0;
      return 81.0;
    }
    if (i == 12) {
      if (j < 1) return 0.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 81.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 1.0;
      return 81.0;
    }
    if (i == 13) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 17) return 3.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      return 81.0;
    }
    if (i == 14) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 17) return 27.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      return 81.0;
    }
    if (i == 15) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -243.0;
      return 243.0;
    }
    if (i == 16) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      return 243.0;
    }
    if (i == 17) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      return 243.0;
    }
    if (i == 18) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -243.0;
      return 243.0;
    }
    if (i == 19) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 729.0;
      if (j < 24) return -729.0;
      return 729.0;
    }
    if (i == 20) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 9.0;
      if (j < 21) return 81.0;
      return 729.0;
    }
    if (i == 21) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 9.0;
      return 729.0;
    }
    if (i == 22) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -2187.0;
      return 2187.0;
    }
    if (i == 23) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -2187.0;
      if (j < 23) return 2187.0;
      if (j < 24) return -2187.0;
      return 2187.0;
    }
    {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 81.0;
      return 6561.0;
    }
  }

  LABICEX_COMMON_INLINE static real inverse_natural_moment_matrix(int i,
                                                                  int j) {
    assert(i >= 0 && i < q);
    assert(j >= 0 && j < q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 3) return 0.0;
      if (j < 4) return -1.11111111111111116045;
      if (j < 10) return 0.0;
      if (j < 11) return 1.2345679012345678327;
      if (j < 13) return 0.11111111111111110494;
      if (j < 17) return 0.0;
      if (j < 18) return -0.00000000000000005551;
      if (j < 20) return 0.0;
      if (j < 22) return -0.12345679012345678327;
      if (j < 23) return -0.00000000000000000867;
      if (j < 24) return -0.00000000000000000173;
      return 0.01234567901234567833;
    }
    if (i == 1) {
      if (j < 1) return 0.0;
      if (j < 2) return -0.5625;
      if (j < 3) return 0.0;
      if (j < 4) return 0.5625;
      if (j < 5) return 0.0;
      if (j < 6) return -0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return 0.0625;
      if (j < 9) return 0.625;
      if (j < 10) return 0.0;
      if (j < 11) return -0.625;
      if (j < 12) return 0.0;
      if (j < 13) return -0.0625;
      if (j < 15) return 0.0;
      if (j < 16) return -0.0625;
      if (j < 18) return 0.0;
      if (j < 19) return -0.06944444444444444753;
      if (j < 20) return 0.0;
      if (j < 21) return 0.0625;
      if (j < 22) return 0.06944444444444444753;
      if (j < 23) return 0.00694444444444444926;
      if (j < 24) return 0.00000000000000000347;
      return -0.00694444444444444319;
    }
    if (i == 2) {
      if (j < 1) return 0.0;
      if (j < 2) return 0.5625;
      if (j < 3) return 0.0;
      if (j < 4) return 0.5625;
      if (j < 5) return 0.0;
      if (j < 6) return -0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return -0.0625;
      if (j < 9) return -0.625;
      if (j < 10) return 0.0;
      if (j < 11) return -0.625;
      if (j < 12) return 0.0;
      if (j < 13) return -0.0625;
      if (j < 15) return 0.0;
      if (j < 16) return 0.0625;
      if (j < 18) return 0.0;
      if (j < 19) return 0.06944444444444444753;
      if (j < 20) return 0.0;
      if (j < 21) return 0.0625;
      if (j < 22) return 0.06944444444444444753;
      if (j < 23) return -0.00694444444444444059;
      if (j < 24) return 0.0000000000000000026;
      return -0.00694444444444444319;
    }
    if (i == 3) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5625;
      if (j < 5) return 0.0;
      if (j < 6) return 0.5625;
      if (j < 7) return 0.0625;
      if (j < 9) return 0.0;
      if (j < 10) return 0.625;
      if (j < 11) return -0.625;
      if (j < 12) return -0.0625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.0625;
      if (j < 18) return -0.06944444444444433651;
      if (j < 20) return 0.0;
      if (j < 21) return 0.06944444444444444753;
      if (j < 22) return 0.0625;
      if (j < 23) return 0.0000000000000000026;
      if (j < 24) return 0.00694444444444444319;
      return -0.00694444444444444232;
    }
    if (i == 4) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5625;
      if (j < 5) return 0.0;
      if (j < 6) return 0.5625;
      if (j < 7) return -0.0625;
      if (j < 9) return 0.0;
      if (j < 11) return -0.625;
      if (j < 12) return -0.0625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.0625;
      if (j < 18) return 0.06944444444444444753;
      if (j < 20) return 0.0;
      if (j < 21) return 0.06944444444444444753;
      if (j < 22) return 0.0625;
      if (j < 23) return 0.00000000000000000347;
      if (j < 24) return -0.00694444444444444493;
      return -0.00694444444444444232;
    }
    if (i == 5) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.31640625;
      if (j < 8) return 0.0;
      if (j < 10) return -0.31640625;
      if (j < 11) return 0.31640625;
      if (j < 13) return 0.0;
      if (j < 15) return -0.03515625;
      if (j < 17) return 0.03515625;
      if (j < 18) return 0.03515624999999997224;
      if (j < 19) return 0.03515625;
      if (j < 20) return 0.00390625;
      if (j < 21) return -0.03515625;
      if (j < 22) return -0.03515625000000000694;
      if (j < 23) return -0.00390625000000000173;
      if (j < 24) return -0.0039062500000000026;
      return 0.0039062499999999987;
    }
    if (i == 6) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.31640625;
      if (j < 8) return 0.0;
      if (j < 9) return -0.31640625;
      if (j < 11) return 0.31640625;
      if (j < 13) return 0.0;
      if (j < 16) return 0.03515625;
      if (j < 18) return -0.03515625;
      if (j < 19) return 0.03515625;
      if (j < 20) return -0.00390625;
      if (j < 22) return -0.03515625;
      if (j < 23) return -0.0039062500000000026;
      if (j < 24) return 0.00390625000000000087;
      return 0.0039062499999999987;
    }
    if (i == 7) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.31640625;
      if (j < 8) return 0.0;
      if (j < 9) return 0.31640625;
      if (j < 10) return -0.31640625;
      if (j < 11) return 0.31640625;
      if (j < 13) return 0.0;
      if (j < 15) return 0.03515625;
      if (j < 16) return -0.03515625;
      if (j < 17) return 0.03515625;
      if (j < 18) return 0.03515624999999997224;
      if (j < 19) return -0.03515625;
      if (j < 20) return -0.00390625;
      if (j < 22) return -0.03515625;
      if (j < 23) return 0.00390624999999999913;
      if (j < 24) return -0.00390624999999999913;
      return 0.0039062499999999987;
    }
    if (i == 8) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.31640625;
      if (j < 8) return 0.0;
      if (j < 11) return 0.31640625;
      if (j < 13) return 0.0;
      if (j < 19) return -0.03515625;
      if (j < 20) return 0.00390625;
      if (j < 22) return -0.03515625;
      if (j < 23) return 0.00390624999999999913;
      if (j < 24) return 0.00390624999999999827;
      return 0.00390624999999999913;
    }
    if (i == 9) {
      if (j < 1) return 0.0;
      if (j < 2) return 0.02083333333333333218;
      if (j < 3) return 0.0;
      if (j < 4) return -0.00694444444444444406;
      if (j < 5) return 0.0;
      if (j < 6) return 0.00694444444444444406;
      if (j < 7) return 0.0;
      if (j < 8) return -0.02083333333333333218;
      if (j < 9) return -0.02314814814814815033;
      if (j < 10) return 0.0;
      if (j < 11) return 0.00771604938271604982;
      if (j < 12) return 0.0;
      if (j < 13) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return 0.00231481481481481469;
      if (j < 18) return 0.0;
      if (j < 19) return 0.02314814814814815033;
      if (j < 20) return 0.0;
      if (j < 21) return -0.0007716049382716049;
      if (j < 22) return -0.00771604938271604982;
      if (j < 23) return -0.00231481481481481469;
      if (j < 24) return 0.0;
      return 0.0007716049382716049;
    }
    if (i == 10) {
      if (j < 1) return 0.0;
      if (j < 2) return -0.02083333333333333218;
      if (j < 3) return 0.0;
      if (j < 4) return -0.00694444444444444406;
      if (j < 5) return 0.0;
      if (j < 6) return 0.00694444444444444406;
      if (j < 7) return 0.0;
      if (j < 8) return 0.02083333333333333218;
      if (j < 9) return 0.02314814814814815033;
      if (j < 10) return 0.0;
      if (j < 11) return 0.00771604938271604895;
      if (j < 12) return 0.0;
      if (j < 13) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return -0.00231481481481481469;
      if (j < 18) return 0.0;
      if (j < 19) return -0.02314814814814815033;
      if (j < 20) return 0.0;
      if (j < 21) return -0.0007716049382716049;
      if (j < 22) return -0.00771604938271604895;
      if (j < 23) return 0.00231481481481481469;
      if (j < 24) return 0.0;
      return 0.0007716049382716049;
    }
    if (i == 11) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.02083333333333333218;
      if (j < 5) return 0.0;
      if (j < 6) return -0.00694444444444444406;
      if (j < 7) return -0.02083333333333333218;
      if (j < 8) return 0.0;
      if (j < 9) return 0.00000000000000000058;
      if (j < 10) return -0.02314814814814815033;
      if (j < 11) return 0.00771604938271604982;
      if (j < 12) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return -0.00000000000000000058;
      if (j < 17) return 0.00231481481481481469;
      if (j < 18) return 0.02314814814814815033;
      if (j < 19) return -0.00000000000000000014;
      if (j < 20) return 0.0;
      if (j < 21) return -0.00771604938271604982;
      if (j < 22) return -0.0007716049382716049;
      if (j < 23) return 0.00000000000000000014;
      if (j < 24) return -0.00231481481481481469;
      return 0.0007716049382716049;
    }
    if (i == 12) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.02083333333333333218;
      if (j < 5) return 0.0;
      if (j < 6) return -0.00694444444444444406;
      if (j < 7) return 0.02083333333333333218;
      if (j < 8) return 0.0;
      if (j < 9) return -0.00000000000000000058;
      if (j < 10) return 0.02314814814814815033;
      if (j < 11) return 0.00771604938271604895;
      if (j < 12) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return 0.00000000000000000058;
      if (j < 17) return -0.00231481481481481469;
      if (j < 18) return -0.02314814814814815033;
      if (j < 19) return 0.00000000000000000014;
      if (j < 20) return 0.0;
      if (j < 21) return -0.00771604938271604895;
      if (j < 22) return -0.0007716049382716049;
      if (j < 23) return -0.00000000000000000014;
      if (j < 24) return 0.00231481481481481469;
      return 0.0007716049382716049;
    }
    if (i == 13) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 10) return 0.00390625;
      if (j < 11) return -0.00390625000000000173;
      if (j < 13) return 0.0;
      if (j < 14) return 0.00130208333333333326;
      if (j < 15) return 0.01171875;
      if (j < 16) return -0.00130208333333333283;
      if (j < 17) return -0.00390625;
      if (j < 18) return -0.00043402777777777819;
      if (j < 19) return -0.01171875;
      if (j < 20) return -0.00130208333333333326;
      if (j < 21) return 0.0004340277777777777;
      if (j < 22) return 0.00390625000000000173;
      if (j < 23) return 0.00130208333333333326;
      if (j < 24) return 0.00043402777777777835;
      return -0.0004340277777777777;
    }
    if (i == 14) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return -0.00130208333333333326;
      if (j < 15) return -0.01171875;
      if (j < 16) return -0.00130208333333333391;
      if (j < 17) return 0.00390625;
      if (j < 18) return 0.00043402777777777819;
      if (j < 19) return -0.01171875;
      if (j < 20) return 0.00130208333333333326;
      if (j < 21) return 0.00043402777777777775;
      if (j < 22) return 0.00390625;
      if (j < 23) return 0.00130208333333333326;
      if (j < 24) return -0.00043402777777777835;
      return -0.0004340277777777777;
    }
    if (i == 15) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 10) return 0.00390625;
      if (j < 11) return -0.0039062499999999987;
      if (j < 13) return 0.0;
      if (j < 14) return -0.00130208333333333326;
      if (j < 15) return -0.01171875;
      if (j < 16) return 0.00130208333333333283;
      if (j < 17) return -0.00390625;
      if (j < 18) return -0.00043402777777777737;
      if (j < 19) return 0.01171875;
      if (j < 20) return 0.00130208333333333326;
      if (j < 21) return 0.00043402777777777781;
      if (j < 22) return 0.0039062499999999987;
      if (j < 23) return -0.00130208333333333326;
      if (j < 24) return 0.00043402777777777727;
      return -0.00043402777777777775;
    }
    if (i == 16) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return 0.00130208333333333326;
      if (j < 15) return 0.01171875;
      if (j < 16) return 0.00130208333333333391;
      if (j < 17) return 0.00390625;
      if (j < 18) return 0.00043402777777777737;
      if (j < 19) return 0.01171875;
      if (j < 20) return -0.00130208333333333326;
      if (j < 21) return 0.0004340277777777777;
      if (j < 22) return 0.00390625;
      if (j < 23) return -0.00130208333333333326;
      if (j < 24) return -0.00043402777777777727;
      return -0.00043402777777777775;
    }
    if (i == 17) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return 0.00390625;
      if (j < 10) return 0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return 0.01171875;
      if (j < 15) return 0.00130208333333333326;
      if (j < 16) return -0.00390625;
      if (j < 17) return -0.00130208333333333304;
      if (j < 18) return -0.01171875;
      if (j < 19) return -0.0004340277777777777;
      if (j < 20) return -0.00130208333333333326;
      if (j < 21) return 0.00390625;
      if (j < 22) return 0.00043402777777777775;
      if (j < 23) return 0.0004340277777777777;
      if (j < 24) return 0.00130208333333333304;
      return -0.00043402777777777775;
    }
    if (i == 18) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return 0.00390625;
      if (j < 10) return -0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return -0.01171875;
      if (j < 15) return -0.00130208333333333326;
      if (j < 16) return -0.00390625;
      if (j < 17) return 0.00130208333333333304;
      if (j < 18) return 0.01171875;
      if (j < 19) return -0.00043402777777777792;
      if (j < 20) return 0.00130208333333333326;
      if (j < 21) return 0.00390625;
      if (j < 22) return 0.0004340277777777777;
      if (j < 23) return 0.00043402777777777792;
      if (j < 24) return -0.00130208333333333304;
      return -0.0004340277777777777;
    }
    if (i == 19) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return -0.00390625000000000087;
      if (j < 10) return 0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return -0.01171875;
      if (j < 15) return -0.00130208333333333326;
      if (j < 16) return 0.00390625000000000087;
      if (j < 17) return -0.00130208333333333326;
      if (j < 18) return -0.01171875;
      if (j < 19) return 0.00043402777777777775;
      if (j < 20) return 0.00130208333333333326;
      if (j < 21) return 0.00390625;
      if (j < 22) return 0.0004340277777777777;
      if (j < 23) return -0.00043402777777777775;
      if (j < 24) return 0.00130208333333333326;
      return -0.0004340277777777777;
    }
    if (i == 20) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.01171875;
      if (j < 8) return 0.0;
      if (j < 9) return -0.00390624999999999957;
      if (j < 10) return -0.01171875;
      if (j < 11) return -0.00390625;
      if (j < 13) return 0.0;
      if (j < 14) return 0.01171875;
      if (j < 15) return 0.00130208333333333326;
      if (j < 16) return 0.00390624999999999957;
      if (j < 17) return 0.00130208333333333326;
      if (j < 18) return 0.01171875;
      if (j < 19) return 0.00043402777777777775;
      if (j < 20) return -0.00130208333333333326;
      if (j < 21) return 0.00390625;
      if (j < 22) return 0.00043402777777777775;
      if (j < 23) return -0.00043402777777777775;
      if (j < 24) return -0.00130208333333333326;
      return -0.00043402777777777775;
    }
    if (i == 21) {
      if (j < 4) return 0.0;
      if (j < 5) return 0.00043402777777777775;
      if (j < 8) return 0.0;
      if (j < 10) return -0.00014467592592592592;
      if (j < 11) return 0.00004822530864197531;
      if (j < 13) return 0.0;
      if (j < 15) return -0.00043402777777777775;
      if (j < 19) return 0.00014467592592592592;
      if (j < 20) return 0.00043402777777777775;
      if (j < 22) return -0.00004822530864197531;
      if (j < 24) return -0.00014467592592592592;
      return 0.00004822530864197531;
    }
    if (i == 22) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.00043402777777777775;
      if (j < 8) return 0.0;
      if (j < 9) return -0.00014467592592592592;
      if (j < 10) return 0.00014467592592592592;
      if (j < 11) return 0.00004822530864197531;
      if (j < 13) return 0.0;
      if (j < 15) return 0.00043402777777777775;
      if (j < 16) return 0.00014467592592592592;
      if (j < 18) return -0.00014467592592592592;
      if (j < 19) return 0.00014467592592592592;
      if (j < 20) return -0.00043402777777777775;
      if (j < 22) return -0.00004822530864197531;
      if (j < 23) return -0.00014467592592592592;
      if (j < 24) return 0.00014467592592592592;
      return 0.00004822530864197531;
    }
    if (i == 23) {
      if (j < 4) return 0.0;
      if (j < 5) return -0.00043402777777777775;
      if (j < 8) return 0.0;
      if (j < 9) return 0.00014467592592592592;
      if (j < 10) return -0.00014467592592592592;
      if (j < 11) return 0.00004822530864197531;
      if (j < 13) return 0.0;
      if (j < 15) return 0.00043402777777777775;
      if (j < 16) return -0.00014467592592592592;
      if (j < 18) return 0.00014467592592592592;
      if (j < 19) return -0.00014467592592592592;
      if (j < 20) return -0.00043402777777777775;
      if (j < 22) return -0.00004822530864197531;
      if (j < 23) return 0.00014467592592592592;
      if (j < 24) return -0.00014467592592592592;
      return 0.00004822530864197531;
    }
    {
      if (j < 4) return 0.0;
      if (j < 5) return 0.00043402777777777775;
      if (j < 8) return 0.0;
      if (j < 10) return 0.00014467592592592592;
      if (j < 11) return 0.00004822530864197531;
      if (j < 13) return 0.0;
      if (j < 15) return -0.00043402777777777775;
      if (j < 19) return -0.00014467592592592592;
      if (j < 20) return 0.00043402777777777775;
      if (j < 22) return -0.00004822530864197531;
      if (j < 24) return 0.00014467592592592592;
      return 0.00004822530864197531;
    }
  }

  LABICEX_COMMON_INLINE static real binary_mixture_natural_moment_matrix(
      int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);

    if (i == 0) {
      if (j < 25) return 1.0;
      return 0.0;
    }
    if (i == 1) {
      if (j < 25) return 0.0;
      return 1.0;
    }
    if (i == 2) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -3.0;
      if (j < 11) return 3.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -3.0;
      if (j < 25) return 3.0;
      if (j < 26) return 0.0;
      if (j < 27) return -1.0;
      if (j < 28) return 1.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 35) return -3.0;
      if (j < 36) return 3.0;
      if (j < 38) return 0.0;
      if (j < 40) return -3.0;
      if (j < 42) return 3.0;
      if (j < 44) return -1.0;
      if (j < 46) return 1.0;
      if (j < 48) return -3.0;
      return 3.0;
    }
    if (i == 3) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -3.0;
      if (j < 13) return 3.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -3.0;
      if (j < 23) return 3.0;
      if (j < 24) return -3.0;
      if (j < 25) return 3.0;
      if (j < 28) return 0.0;
      if (j < 29) return -1.0;
      if (j < 30) return 1.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 36) return 0.0;
      if (j < 37) return -3.0;
      if (j < 38) return 3.0;
      if (j < 39) return -1.0;
      if (j < 40) return 1.0;
      if (j < 41) return -1.0;
      if (j < 42) return 1.0;
      if (j < 43) return -3.0;
      if (j < 44) return 3.0;
      if (j < 45) return -3.0;
      if (j < 46) return 3.0;
      if (j < 47) return -3.0;
      if (j < 48) return 3.0;
      if (j < 49) return -3.0;
      return 3.0;
    }
    if (i == 4) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      if (j < 9) return 2.0;
      if (j < 13) return 9.0;
      if (j < 21) return 10.0;
      if (j < 25) return 18.0;
      if (j < 26) return 0.0;
      if (j < 30) return 1.0;
      if (j < 34) return 2.0;
      if (j < 38) return 9.0;
      if (j < 46) return 10.0;
      return 18.0;
    }
    if (i == 5) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -3.0;
      if (j < 11) return 3.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -3.0;
      if (j < 25) return 3.0;
      return 0.0;
    }
    if (i == 6) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -3.0;
      if (j < 13) return 3.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -3.0;
      if (j < 23) return 3.0;
      if (j < 24) return -3.0;
      if (j < 25) return 3.0;
      return 0.0;
    }
    if (i == 7) {
      if (j < 1) return 0.0;
      if (j < 5) return 1.0;
      if (j < 9) return 2.0;
      if (j < 13) return 9.0;
      if (j < 21) return 10.0;
      if (j < 25) return 18.0;
      return 0.0;
    }
    if (i == 8) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 9.0;
      if (j < 24) return -9.0;
      if (j < 25) return 9.0;
      if (j < 30) return 0.0;
      if (j < 31) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 3.0;
      if (j < 41) return -3.0;
      if (j < 43) return 3.0;
      if (j < 45) return -3.0;
      if (j < 46) return 3.0;
      if (j < 47) return 9.0;
      if (j < 49) return -9.0;
      return 9.0;
    }
    if (i == 9) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 9.0;
      if (j < 24) return -9.0;
      if (j < 25) return 9.0;
      return 0.0;
    }
    if (i == 10) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 9.0;
      if (j < 17) return 1.0;
      if (j < 25) return 9.0;
      if (j < 28) return 0.0;
      if (j < 34) return 1.0;
      if (j < 36) return 0.0;
      if (j < 38) return 9.0;
      if (j < 42) return 1.0;
      return 9.0;
    }
    if (i == 11) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 9.0;
      if (j < 17) return 1.0;
      if (j < 25) return 9.0;
      return 0.0;
    }
    if (i == 12) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -27.0;
      if (j < 13) return 27.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      if (j < 25) return 27.0;
      if (j < 28) return 0.0;
      if (j < 29) return -1.0;
      if (j < 30) return 1.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 36) return 0.0;
      if (j < 37) return -27.0;
      if (j < 38) return 27.0;
      if (j < 39) return -1.0;
      if (j < 40) return 1.0;
      if (j < 41) return -1.0;
      if (j < 42) return 1.0;
      if (j < 43) return -27.0;
      if (j < 44) return 27.0;
      if (j < 45) return -27.0;
      if (j < 46) return 27.0;
      if (j < 47) return -27.0;
      if (j < 48) return 27.0;
      if (j < 49) return -27.0;
      return 27.0;
    }
    if (i == 13) {
      if (j < 3) return 0.0;
      if (j < 4) return -1.0;
      if (j < 5) return 1.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 12) return -27.0;
      if (j < 13) return 27.0;
      if (j < 14) return -1.0;
      if (j < 15) return 1.0;
      if (j < 16) return -1.0;
      if (j < 17) return 1.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      if (j < 25) return 27.0;
      return 0.0;
    }
    if (i == 14) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -27.0;
      if (j < 11) return 27.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -27.0;
      if (j < 25) return 27.0;
      if (j < 26) return 0.0;
      if (j < 27) return -1.0;
      if (j < 28) return 1.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 35) return -27.0;
      if (j < 36) return 27.0;
      if (j < 38) return 0.0;
      if (j < 40) return -27.0;
      if (j < 42) return 27.0;
      if (j < 44) return -1.0;
      if (j < 46) return 1.0;
      if (j < 48) return -27.0;
      return 27.0;
    }
    if (i == 15) {
      if (j < 1) return 0.0;
      if (j < 2) return -1.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 10) return -27.0;
      if (j < 11) return 27.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -1.0;
      if (j < 21) return 1.0;
      if (j < 23) return -27.0;
      if (j < 25) return 27.0;
      return 0.0;
    }
    if (i == 16) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -27.0;
      if (j < 25) return 27.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 40) return -3.0;
      if (j < 42) return 3.0;
      if (j < 44) return -9.0;
      if (j < 46) return 9.0;
      if (j < 48) return -27.0;
      return 27.0;
    }
    if (i == 17) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -27.0;
      if (j < 25) return 27.0;
      return 0.0;
    }
    if (i == 18) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      if (j < 25) return 27.0;
      if (j < 30) return 0.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return -9.0;
      if (j < 40) return 9.0;
      if (j < 41) return -9.0;
      if (j < 42) return 9.0;
      if (j < 43) return -3.0;
      if (j < 44) return 3.0;
      if (j < 45) return -3.0;
      if (j < 46) return 3.0;
      if (j < 47) return -27.0;
      if (j < 48) return 27.0;
      if (j < 49) return -27.0;
      return 27.0;
    }
    if (i == 19) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -27.0;
      if (j < 23) return 27.0;
      if (j < 24) return -27.0;
      if (j < 25) return 27.0;
      return 0.0;
    }
    if (i == 20) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 9.0;
      if (j < 25) return 81.0;
      if (j < 30) return 0.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 46) return 9.0;
      return 81.0;
    }
    if (i == 21) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 9.0;
      if (j < 25) return 81.0;
      return 0.0;
    }
    if (i == 22) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 81.0;
      if (j < 17) return 1.0;
      if (j < 25) return 81.0;
      if (j < 28) return 0.0;
      if (j < 34) return 1.0;
      if (j < 36) return 0.0;
      if (j < 38) return 81.0;
      if (j < 42) return 1.0;
      return 81.0;
    }
    if (i == 23) {
      if (j < 3) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 0.0;
      if (j < 13) return 81.0;
      if (j < 17) return 1.0;
      if (j < 25) return 81.0;
      return 0.0;
    }
    if (i == 24) {
      if (j < 1) return 0.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 81.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 1.0;
      if (j < 25) return 81.0;
      if (j < 26) return 0.0;
      if (j < 28) return 1.0;
      if (j < 30) return 0.0;
      if (j < 34) return 1.0;
      if (j < 36) return 81.0;
      if (j < 38) return 0.0;
      if (j < 42) return 81.0;
      if (j < 46) return 1.0;
      return 81.0;
    }
    if (i == 25) {
      if (j < 1) return 0.0;
      if (j < 3) return 1.0;
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 11) return 81.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 1.0;
      if (j < 25) return 81.0;
      return 0.0;
    }
    if (i == 26) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 17) return 3.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      if (j < 25) return 81.0;
      if (j < 30) return 0.0;
      if (j < 31) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 3.0;
      if (j < 41) return -3.0;
      if (j < 42) return 3.0;
      if (j < 43) return 27.0;
      if (j < 45) return -27.0;
      if (j < 46) return 27.0;
      if (j < 47) return 81.0;
      if (j < 49) return -81.0;
      return 81.0;
    }
    if (i == 27) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 3.0;
      if (j < 16) return -3.0;
      if (j < 17) return 3.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      if (j < 25) return 81.0;
      return 0.0;
    }
    if (i == 28) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 17) return 27.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      if (j < 25) return 81.0;
      if (j < 30) return 0.0;
      if (j < 31) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 27.0;
      if (j < 41) return -27.0;
      if (j < 42) return 27.0;
      if (j < 43) return 3.0;
      if (j < 45) return -3.0;
      if (j < 46) return 3.0;
      if (j < 47) return 81.0;
      if (j < 49) return -81.0;
      return 81.0;
    }
    if (i == 29) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 17) return 27.0;
      if (j < 18) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return 81.0;
      if (j < 24) return -81.0;
      if (j < 25) return 81.0;
      return 0.0;
    }
    if (i == 30) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -243.0;
      if (j < 25) return 243.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 40) return -3.0;
      if (j < 42) return 3.0;
      if (j < 44) return -81.0;
      if (j < 46) return 81.0;
      if (j < 48) return -243.0;
      return 243.0;
    }
    if (i == 31) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -3.0;
      if (j < 17) return 3.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -243.0;
      if (j < 25) return 243.0;
      return 0.0;
    }
    if (i == 32) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      if (j < 25) return 243.0;
      if (j < 30) return 0.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return -81.0;
      if (j < 40) return 81.0;
      if (j < 41) return -81.0;
      if (j < 42) return 81.0;
      if (j < 43) return -3.0;
      if (j < 44) return 3.0;
      if (j < 45) return -3.0;
      if (j < 46) return 3.0;
      if (j < 47) return -243.0;
      if (j < 48) return 243.0;
      if (j < 49) return -243.0;
      return 243.0;
    }
    if (i == 33) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -3.0;
      if (j < 19) return 3.0;
      if (j < 20) return -3.0;
      if (j < 21) return 3.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      if (j < 25) return 243.0;
      return 0.0;
    }
    if (i == 34) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      if (j < 25) return 243.0;
      if (j < 30) return 0.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return -9.0;
      if (j < 40) return 9.0;
      if (j < 41) return -9.0;
      if (j < 42) return 9.0;
      if (j < 43) return -27.0;
      if (j < 44) return 27.0;
      if (j < 45) return -27.0;
      if (j < 46) return 27.0;
      if (j < 47) return -243.0;
      if (j < 48) return 243.0;
      if (j < 49) return -243.0;
      return 243.0;
    }
    if (i == 35) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -9.0;
      if (j < 15) return 9.0;
      if (j < 16) return -9.0;
      if (j < 17) return 9.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -243.0;
      if (j < 23) return 243.0;
      if (j < 24) return -243.0;
      if (j < 25) return 243.0;
      return 0.0;
    }
    if (i == 36) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -243.0;
      if (j < 25) return 243.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 40) return -27.0;
      if (j < 42) return 27.0;
      if (j < 44) return -9.0;
      if (j < 46) return 9.0;
      if (j < 48) return -243.0;
      return 243.0;
    }
    if (i == 37) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -9.0;
      if (j < 21) return 9.0;
      if (j < 23) return -243.0;
      if (j < 25) return 243.0;
      return 0.0;
    }
    if (i == 38) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 729.0;
      if (j < 24) return -729.0;
      if (j < 25) return 729.0;
      if (j < 30) return 0.0;
      if (j < 31) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return 27.0;
      if (j < 41) return -27.0;
      if (j < 43) return 27.0;
      if (j < 45) return -27.0;
      if (j < 46) return 27.0;
      if (j < 47) return 729.0;
      if (j < 49) return -729.0;
      return 729.0;
    }
    if (i == 39) {
      if (j < 5) return 0.0;
      if (j < 6) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return 27.0;
      if (j < 16) return -27.0;
      if (j < 18) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return 729.0;
      if (j < 24) return -729.0;
      if (j < 25) return 729.0;
      return 0.0;
    }
    if (i == 40) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 9.0;
      if (j < 21) return 81.0;
      if (j < 25) return 729.0;
      if (j < 30) return 0.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 42) return 9.0;
      if (j < 46) return 81.0;
      return 729.0;
    }
    if (i == 41) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 9.0;
      if (j < 21) return 81.0;
      if (j < 25) return 729.0;
      return 0.0;
    }
    if (i == 42) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 9.0;
      if (j < 25) return 729.0;
      if (j < 30) return 0.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 42) return 81.0;
      if (j < 46) return 9.0;
      return 729.0;
    }
    if (i == 43) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 17) return 81.0;
      if (j < 21) return 9.0;
      if (j < 25) return 729.0;
      return 0.0;
    }
    if (i == 44) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -2187.0;
      if (j < 25) return 2187.0;
      if (j < 30) return 0.0;
      if (j < 32) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 40) return -27.0;
      if (j < 42) return 27.0;
      if (j < 44) return -81.0;
      if (j < 46) return 81.0;
      if (j < 48) return -2187.0;
      return 2187.0;
    }
    if (i == 45) {
      if (j < 5) return 0.0;
      if (j < 7) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 15) return -27.0;
      if (j < 17) return 27.0;
      if (j < 19) return -81.0;
      if (j < 21) return 81.0;
      if (j < 23) return -2187.0;
      if (j < 25) return 2187.0;
      return 0.0;
    }
    if (i == 46) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -2187.0;
      if (j < 23) return 2187.0;
      if (j < 24) return -2187.0;
      if (j < 25) return 2187.0;
      if (j < 30) return 0.0;
      if (j < 31) return -1.0;
      if (j < 32) return 1.0;
      if (j < 33) return -1.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 39) return -81.0;
      if (j < 40) return 81.0;
      if (j < 41) return -81.0;
      if (j < 42) return 81.0;
      if (j < 43) return -27.0;
      if (j < 44) return 27.0;
      if (j < 45) return -27.0;
      if (j < 46) return 27.0;
      if (j < 47) return -2187.0;
      if (j < 48) return 2187.0;
      if (j < 49) return -2187.0;
      return 2187.0;
    }
    if (i == 47) {
      if (j < 5) return 0.0;
      if (j < 6) return -1.0;
      if (j < 7) return 1.0;
      if (j < 8) return -1.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 14) return -81.0;
      if (j < 15) return 81.0;
      if (j < 16) return -81.0;
      if (j < 17) return 81.0;
      if (j < 18) return -27.0;
      if (j < 19) return 27.0;
      if (j < 20) return -27.0;
      if (j < 21) return 27.0;
      if (j < 22) return -2187.0;
      if (j < 23) return 2187.0;
      if (j < 24) return -2187.0;
      if (j < 25) return 2187.0;
      return 0.0;
    }
    if (i == 48) {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 81.0;
      if (j < 25) return 6561.0;
      if (j < 30) return 0.0;
      if (j < 34) return 1.0;
      if (j < 38) return 0.0;
      if (j < 46) return 81.0;
      return 6561.0;
    }
    {
      if (j < 5) return 0.0;
      if (j < 9) return 1.0;
      if (j < 13) return 0.0;
      if (j < 21) return 81.0;
      if (j < 25) return 6561.0;
      return 0.0;
    }
  }

  LABICEX_COMMON_INLINE static real
  inverse_binary_mixture_natural_moment_matrix(int i, int j) {
    assert(i >= 0 && i < 2 * q);
    assert(j >= 0 && j < 2 * q);
    if (i == 0) {
      if (j < 1) return 1.0;
      if (j < 7) return 0.0;
      if (j < 8) return -1.11111111111111116045;
      if (j < 21) return 0.0;
      if (j < 22) return 1.2345679012345678327;
      if (j < 23) return 0.0;
      if (j < 24) return 0.11111111111111110494;
      if (j < 25) return 0.0;
      if (j < 26) return 0.11111111111111110494;
      if (j < 30) return 0.0;
      if (j < 31) return -0.00000000000000001388;
      if (j < 32) return 0.0;
      if (j < 33) return -0.00000000000000005551;
      if (j < 34) return 0.00000000000000004163;
      if (j < 35) return 0.0;
      if (j < 36) return 0.00000000000000002776;
      if (j < 40) return 0.0;
      if (j < 41) return 0.00000000000000007016;
      if (j < 42) return -0.12345679012345681103;
      if (j < 43) return 0.0;
      if (j < 44) return -0.12345679012345679715;
      if (j < 45) return -0.00000000000000003209;
      if (j < 46) return 0.00000000000000002429;
      if (j < 47) return 0.00000000000000000087;
      if (j < 48) return -0.00000000000000000173;
      if (j < 49) return -0.00000000000000002082;
      return 0.01234567901234568874;
    }
    if (i == 1) {
      if (j < 5) return 0.0;
      if (j < 6) return -0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5625;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5625;
      if (j < 15) return 0.0;
      if (j < 16) return 0.0625;
      if (j < 17) return 0.00000000000000002776;
      if (j < 18) return 0.625;
      if (j < 19) return 0.00000000000000011102;
      if (j < 20) return -0.00000000000000011102;
      if (j < 21) return 0.0;
      if (j < 22) return -0.625;
      if (j < 25) return 0.0;
      if (j < 26) return -0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000002082;
      if (j < 32) return -0.0625;
      if (j < 33) return 0.00000000000000001388;
      if (j < 34) return -0.00000000000000000694;
      if (j < 35) return -0.00000000000000020817;
      if (j < 36) return 0.00000000000000018041;
      if (j < 37) return 0.0;
      if (j < 38) return -0.06944444444444444753;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00000000000000007691;
      if (j < 42) return 0.06250000000000002776;
      if (j < 43) return 0.00000000000000013184;
      if (j < 44) return 0.06944444444444432263;
      if (j < 45) return 0.00000000000000002342;
      if (j < 46) return 0.00694444444444442324;
      if (j < 47) return -0.00000000000000002082;
      if (j < 48) return 0.00000000000000002168;
      if (j < 49) return 0.00000000000000003556;
      return -0.00694444444444447355;
    }
    if (i == 2) {
      if (j < 5) return 0.0;
      if (j < 6) return 0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return 0.5625;
      if (j < 11) return 0.0;
      if (j < 12) return -0.5625;
      if (j < 15) return 0.0;
      if (j < 16) return -0.0625;
      if (j < 17) return -0.00000000000000002776;
      if (j < 18) return -0.625;
      if (j < 19) return 0.00000000000000011102;
      if (j < 20) return -0.00000000000000011102;
      if (j < 21) return 0.0;
      if (j < 22) return -0.625;
      if (j < 25) return 0.0;
      if (j < 26) return -0.0625;
      if (j < 31) return 0.0;
      if (j < 32) return 0.0625;
      if (j < 33) return 0.00000000000000002776;
      if (j < 34) return -0.00000000000000002082;
      if (j < 35) return -0.00000000000000018041;
      if (j < 36) return 0.00000000000000015266;
      if (j < 37) return 0.0;
      if (j < 38) return 0.06944444444444444753;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00000000000000002197;
      if (j < 42) return 0.06249999999999997918;
      if (j < 43) return 0.00000000000000009021;
      if (j < 44) return 0.06944444444444436426;
      if (j < 45) return 0.00000000000000003556;
      if (j < 46) return -0.00694444444444447702;
      if (j < 47) return -0.00000000000000002342;
      if (j < 48) return 0.00000000000000002515;
      if (j < 49) return 0.0000000000000000026;
      return -0.00694444444444444059;
    }
    if (i == 3) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.5625;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5625;
      if (j < 13) return 0.0;
      if (j < 14) return 0.0625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000347;
      if (j < 18) return 0.0;
      if (j < 19) return -0.00000000000000022204;
      if (j < 20) return 0.62500000000000022204;
      if (j < 21) return 0.0;
      if (j < 22) return -0.625;
      if (j < 23) return 0.0;
      if (j < 24) return -0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000347;
      if (j < 32) return 0.0;
      if (j < 33) return 0.00000000000000003556;
      if (j < 34) return -0.06250000000000002776;
      if (j < 35) return 0.0000000000000004996;
      if (j < 36) return -0.06944444444444494713;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00000000000000001465;
      if (j < 42) return 0.06944444444444446141;
      if (j < 43) return -0.00000000000000010972;
      if (j < 44) return 0.06250000000000011102;
      if (j < 45) return 0.0000000000000000052;
      if (j < 46) return -0.00000000000000000347;
      if (j < 47) return 0.00000000000000004337;
      if (j < 48) return 0.00694444444444440069;
      if (j < 49) return -0.00000000000000000034;
      return -0.00694444444444444406;
    }
    if (i == 4) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.5625;
      if (j < 11) return 0.0;
      if (j < 12) return 0.5625;
      if (j < 13) return 0.0;
      if (j < 14) return -0.0625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000347;
      if (j < 19) return 0.0;
      if (j < 20) return -0.625;
      if (j < 21) return 0.0;
      if (j < 22) return -0.625;
      if (j < 23) return 0.0;
      if (j < 24) return -0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000347;
      if (j < 32) return 0.0;
      if (j < 33) return 0.00000000000000003383;
      if (j < 34) return 0.06249999999999997224;
      if (j < 35) return -0.00000000000000011102;
      if (j < 36) return 0.06944444444444455855;
      if (j < 40) return 0.0;
      if (j < 41) return 0.00000000000000002698;
      if (j < 42) return 0.06944444444444441977;
      if (j < 43) return -0.00000000000000011232;
      if (j < 44) return 0.06250000000000011102;
      if (j < 45) return -0.00000000000000000087;
      if (j < 46) return 0.0000000000000000026;
      if (j < 47) return -0.00000000000000000087;
      if (j < 48) return -0.00694444444444444579;
      if (j < 49) return -0.00000000000000000814;
      return -0.00694444444444443712;
    }
    if (i == 5) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000001908;
      if (j < 18) return -0.31640625;
      if (j < 19) return -0.00000000000000000173;
      if (j < 20) return -0.31640625;
      if (j < 21) return 0.0;
      if (j < 22) return 0.31640625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.03515625;
      if (j < 29) return 0.0;
      if (j < 30) return -0.03515625;
      if (j < 31) return 0.0000000000000000052;
      if (j < 32) return 0.03515624999999999306;
      if (j < 33) return -0.00000000000000000173;
      if (j < 34) return 0.03515625;
      if (j < 35) return 0.00000000000000000932;
      if (j < 36) return 0.03515625;
      if (j < 37) return -0.00000000000000000022;
      if (j < 38) return 0.03515625;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00390625;
      if (j < 41) return 0.00000000000000001207;
      if (j < 42) return -0.03515625000000000694;
      if (j < 43) return 0.00000000000000000108;
      if (j < 44) return -0.03515625000000000694;
      if (j < 45) return -0.00000000000000000889;
      if (j < 46) return -0.00390624999999999133;
      if (j < 47) return -0.00000000000000000412;
      if (j < 48) return -0.00390624999999999566;
      if (j < 49) return -0.00000000000000000224;
      return 0.00390625000000000173;
    }
    if (i == 6) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000867;
      if (j < 18) return -0.31640625;
      if (j < 19) return 0.00000000000000000173;
      if (j < 20) return 0.31640625;
      if (j < 21) return 0.0;
      if (j < 22) return 0.31640625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.03515625;
      if (j < 29) return 0.0;
      if (j < 30) return 0.03515625;
      if (j < 31) return -0.00000000000000002255;
      if (j < 32) return 0.03515625000000001388;
      if (j < 33) return -0.00000000000000001908;
      if (j < 34) return -0.03515624999999998612;
      if (j < 35) return 0.00000000000000000455;
      if (j < 36) return -0.03515625;
      if (j < 37) return 0.00000000000000000022;
      if (j < 38) return 0.03515625;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00390625;
      if (j < 41) return -0.00000000000000000181;
      if (j < 42) return -0.03515624999999999306;
      if (j < 43) return 0.00000000000000000239;
      if (j < 44) return -0.03515625000000000694;
      if (j < 45) return -0.00000000000000001149;
      if (j < 46) return -0.00390624999999998872;
      if (j < 47) return 0.00000000000000000152;
      if (j < 48) return 0.00390624999999999913;
      if (j < 49) return -0.00000000000000000181;
      return 0.00390625000000000173;
    }
    if (i == 7) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000001561;
      if (j < 18) return 0.31640625;
      if (j < 19) return 0.00000000000000000173;
      if (j < 20) return -0.31640625;
      if (j < 21) return 0.0;
      if (j < 22) return 0.31640625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.03515625;
      if (j < 29) return 0.0;
      if (j < 30) return 0.03515625;
      if (j < 31) return -0.00000000000000001561;
      if (j < 32) return -0.03515624999999998612;
      if (j < 33) return -0.00000000000000001995;
      if (j < 34) return 0.03515625000000001388;
      if (j < 35) return -0.00000000000000000932;
      if (j < 36) return 0.03515625000000002082;
      if (j < 37) return 0.00000000000000000022;
      if (j < 38) return -0.03515625;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00390625;
      if (j < 41) return 0.00000000000000000267;
      if (j < 42) return -0.03515625;
      if (j < 43) return -0.00000000000000000239;
      if (j < 44) return -0.03515625;
      if (j < 45) return -0.00000000000000000932;
      if (j < 46) return 0.00390625000000000867;
      if (j < 47) return 0.00000000000000000325;
      if (j < 48) return -0.00390625000000000347;
      if (j < 49) return -0.00000000000000000253;
      return 0.00390625000000000173;
    }
    if (i == 8) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000001214;
      if (j < 18) return 0.31640625;
      if (j < 19) return -0.00000000000000000173;
      if (j < 20) return 0.31640625;
      if (j < 21) return 0.0;
      if (j < 22) return 0.31640625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.03515625;
      if (j < 29) return 0.0;
      if (j < 30) return -0.03515625;
      if (j < 31) return 0.00000000000000001908;
      if (j < 32) return -0.03515625000000001388;
      if (j < 33) return -0.00000000000000001475;
      if (j < 34) return -0.03515624999999998612;
      if (j < 35) return -0.00000000000000000455;
      if (j < 36) return -0.03515624999999999306;
      if (j < 37) return -0.00000000000000000022;
      if (j < 38) return -0.03515625;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00390625;
      if (j < 41) return 0.00000000000000000267;
      if (j < 42) return -0.03515625;
      if (j < 43) return -0.00000000000000000108;
      if (j < 44) return -0.03515625;
      if (j < 45) return -0.00000000000000000152;
      if (j < 46) return 0.00390625000000000087;
      if (j < 47) return 0.00000000000000000022;
      if (j < 48) return 0.00390624999999999957;
      if (j < 49) return -0.0000000000000000021;
      return 0.00390625000000000173;
    }
    if (i == 9) {
      if (j < 5) return 0.0;
      if (j < 6) return 0.02083333333333333218;
      if (j < 7) return 0.0;
      if (j < 8) return -0.00694444444444444406;
      if (j < 11) return 0.0;
      if (j < 12) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return -0.02083333333333333218;
      if (j < 17) return -0.00000000000000000925;
      if (j < 18) return -0.02314814814814813992;
      if (j < 19) return 0.0;
      if (j < 20) return 0.00000000000000000173;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00771604938271604982;
      if (j < 25) return 0.0;
      if (j < 26) return 0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return -0.00000000000000000116;
      if (j < 32) return 0.00231481481481481599;
      if (j < 33) return 0.00000000000000000231;
      if (j < 34) return -0.00000000000000000443;
      if (j < 35) return 0.00000000000000000463;
      if (j < 36) return -0.00000000000000000472;
      if (j < 37) return 0.0;
      if (j < 38) return 0.02314814814814815033;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00000000000000000077;
      if (j < 42) return -0.00077160493827160446;
      if (j < 43) return 0.00000000000000000231;
      if (j < 44) return -0.00771604938271604982;
      if (j < 45) return -0.0000000000000000013;
      if (j < 46) return -0.00231481481481481339;
      if (j < 47) return -0.00000000000000000116;
      if (j < 48) return 0.0000000000000000014;
      if (j < 49) return -0.00000000000000000029;
      return 0.00077160493827160522;
    }
    if (i == 10) {
      if (j < 5) return 0.0;
      if (j < 6) return -0.02083333333333333218;
      if (j < 7) return 0.0;
      if (j < 8) return -0.00694444444444444406;
      if (j < 11) return 0.0;
      if (j < 12) return 0.00694444444444444406;
      if (j < 15) return 0.0;
      if (j < 16) return 0.02083333333333333218;
      if (j < 17) return 0.00000000000000000925;
      if (j < 18) return 0.02314814814814813992;
      if (j < 19) return 0.0;
      if (j < 20) return -0.00000000000000000173;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00771604938271604895;
      if (j < 25) return 0.0;
      if (j < 26) return 0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000116;
      if (j < 32) return -0.00231481481481481599;
      if (j < 33) return -0.00000000000000000231;
      if (j < 34) return 0.00000000000000000289;
      if (j < 35) return -0.00000000000000000463;
      if (j < 36) return 0.00000000000000000477;
      if (j < 37) return 0.0;
      if (j < 38) return -0.02314814814814815033;
      if (j < 40) return 0.0;
      if (j < 41) return 0.00000000000000000173;
      if (j < 42) return -0.00077160493827160663;
      if (j < 43) return -0.00000000000000000231;
      if (j < 44) return -0.00771604938271604635;
      if (j < 45) return 0.0000000000000000013;
      if (j < 46) return 0.00231481481481481339;
      if (j < 47) return 0.00000000000000000116;
      if (j < 48) return -0.0000000000000000013;
      if (j < 49) return 0.00000000000000000029;
      return 0.00077160493827160457;
    }
    if (i == 11) {
      if (j < 6) return 0.0;
      if (j < 7) return 0.02083333333333333218;
      if (j < 11) return 0.0;
      if (j < 12) return -0.00694444444444444406;
      if (j < 13) return 0.0;
      if (j < 14) return -0.02083333333333333218;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000116;
      if (j < 18) return 0.00000000000000000173;
      if (j < 19) return 0.0;
      if (j < 20) return -0.02314814814814815033;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00771604938271604982;
      if (j < 23) return 0.0;
      if (j < 24) return 0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000116;
      if (j < 32) return -0.00000000000000000173;
      if (j < 33) return 0.00000000000000000202;
      if (j < 34) return 0.00231481481481481295;
      if (j < 35) return 0.0;
      if (j < 36) return 0.02314814814814815033;
      if (j < 37) return 0.0;
      if (j < 38) return -0.00000000000000000014;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00771604938271604982;
      if (j < 43) return -0.00000000000000000043;
      if (j < 44) return -0.00077160493827160446;
      if (j < 45) return 0.0;
      if (j < 46) return 0.00000000000000000014;
      if (j < 47) return -0.00000000000000000202;
      if (j < 48) return -0.00231481481481481295;
      if (j < 49) return 0.00000000000000000043;
      return 0.00077160493827160446;
    }
    if (i == 12) {
      if (j < 6) return 0.0;
      if (j < 7) return -0.02083333333333333218;
      if (j < 11) return 0.0;
      if (j < 12) return -0.00694444444444444406;
      if (j < 13) return 0.0;
      if (j < 14) return 0.02083333333333333218;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000116;
      if (j < 18) return -0.00000000000000000173;
      if (j < 19) return 0.0;
      if (j < 20) return 0.02314814814814815033;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00771604938271604895;
      if (j < 23) return 0.0;
      if (j < 24) return 0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return -0.00000000000000000116;
      if (j < 32) return 0.00000000000000000173;
      if (j < 33) return -0.00000000000000000202;
      if (j < 34) return -0.00231481481481481295;
      if (j < 35) return 0.0;
      if (j < 36) return -0.02314814814814815033;
      if (j < 37) return 0.0;
      if (j < 38) return 0.00000000000000000014;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00771604938271604895;
      if (j < 43) return 0.00000000000000000043;
      if (j < 44) return -0.00077160493827160533;
      if (j < 45) return 0.0;
      if (j < 46) return -0.00000000000000000014;
      if (j < 47) return 0.00000000000000000202;
      if (j < 48) return 0.00231481481481481295;
      if (j < 49) return -0.00000000000000000043;
      return 0.00077160493827160533;
    }
    if (i == 13) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000463;
      if (j < 18) return 0.0117187499999999948;
      if (j < 19) return 0.00000000000000000058;
      if (j < 20) return 0.00390624999999999913;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.00130208333333333326;
      if (j < 29) return 0.0;
      if (j < 30) return 0.01171875;
      if (j < 31) return 0.0;
      if (j < 32) return -0.00130208333333333326;
      if (j < 33) return -0.00000000000000000289;
      if (j < 34) return -0.0039062499999999961;
      if (j < 35) return -0.00000000000000000311;
      if (j < 36) return -0.00043402777777777461;
      if (j < 37) return 0.0;
      if (j < 38) return -0.01171875;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.0000000000000000007;
      if (j < 42) return 0.00043402777777777721;
      if (j < 43) return -0.00000000000000000058;
      if (j < 44) return 0.00390625000000000087;
      if (j < 45) return 0.00000000000000000072;
      if (j < 46) return 0.00130208333333333261;
      if (j < 47) return 0.0000000000000000008;
      if (j < 48) return 0.00043402777777777683;
      if (j < 49) return -0.0000000000000000001;
      return -0.0004340277777777777;
    }
    if (i == 14) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000463;
      if (j < 18) return 0.0117187499999999948;
      if (j < 19) return -0.00000000000000000058;
      if (j < 20) return -0.00390625000000000087;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.00130208333333333326;
      if (j < 29) return 0.0;
      if (j < 30) return -0.01171875;
      if (j < 31) return 0.00000000000000000116;
      if (j < 32) return -0.00130208333333333456;
      if (j < 33) return 0.00000000000000000058;
      if (j < 34) return 0.00390625000000000087;
      if (j < 35) return -0.00000000000000000152;
      if (j < 36) return 0.00043402777777777933;
      if (j < 37) return 0.0;
      if (j < 38) return -0.01171875;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.0000000000000000007;
      if (j < 42) return 0.00043402777777777721;
      if (j < 43) return -0.00000000000000000058;
      if (j < 44) return 0.00390625000000000087;
      if (j < 45) return 0.00000000000000000087;
      if (j < 46) return 0.00130208333333333239;
      if (j < 47) return 0.00000000000000000036;
      if (j < 48) return -0.00043402777777777824;
      if (j < 49) return -0.0000000000000000001;
      return -0.0004340277777777777;
    }
    if (i == 15) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000463;
      if (j < 18) return -0.0117187499999999948;
      if (j < 19) return -0.00000000000000000058;
      if (j < 20) return 0.00390625000000000087;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.00130208333333333326;
      if (j < 29) return 0.0;
      if (j < 30) return -0.01171875;
      if (j < 31) return 0.0;
      if (j < 32) return 0.00130208333333333326;
      if (j < 33) return 0.00000000000000000289;
      if (j < 34) return -0.0039062500000000026;
      if (j < 35) return 0.00000000000000000311;
      if (j < 36) return -0.00043402777777778095;
      if (j < 37) return 0.0;
      if (j < 38) return 0.01171875;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return -0.0000000000000000008;
      if (j < 42) return 0.00043402777777777857;
      if (j < 43) return 0.00000000000000000058;
      if (j < 44) return 0.00390624999999999957;
      if (j < 45) return -0.00000000000000000072;
      if (j < 46) return -0.00130208333333333261;
      if (j < 47) return -0.0000000000000000008;
      if (j < 48) return 0.00043402777777777862;
      if (j < 49) return 0.0;
      return -0.00043402777777777775;
    }
    if (i == 16) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000463;
      if (j < 18) return -0.0117187499999999948;
      if (j < 19) return 0.00000000000000000058;
      if (j < 20) return -0.00390624999999999957;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.00130208333333333326;
      if (j < 29) return 0.0;
      if (j < 30) return 0.01171875;
      if (j < 31) return -0.00000000000000000116;
      if (j < 32) return 0.00130208333333333456;
      if (j < 33) return -0.00000000000000000058;
      if (j < 34) return 0.00390624999999999957;
      if (j < 35) return 0.00000000000000000152;
      if (j < 36) return 0.00043402777777777618;
      if (j < 37) return 0.0;
      if (j < 38) return 0.01171875;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return -0.0000000000000000008;
      if (j < 42) return 0.00043402777777777857;
      if (j < 43) return 0.00000000000000000058;
      if (j < 44) return 0.00390624999999999957;
      if (j < 45) return -0.00000000000000000087;
      if (j < 46) return -0.00130208333333333239;
      if (j < 47) return -0.00000000000000000036;
      if (j < 48) return -0.00043402777777777732;
      if (j < 49) return 0.0;
      return -0.00043402777777777775;
    }
    if (i == 17) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000173;
      if (j < 18) return 0.00390624999999999913;
      if (j < 19) return 0.0;
      if (j < 20) return 0.01171875;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.01171875;
      if (j < 29) return 0.0;
      if (j < 30) return 0.00130208333333333326;
      if (j < 31) return -0.00000000000000000173;
      if (j < 32) return -0.00390624999999999913;
      if (j < 33) return -0.00000000000000000116;
      if (j < 34) return -0.00130208333333333218;
      if (j < 35) return 0.0;
      if (j < 36) return -0.01171875;
      if (j < 37) return 0.00000000000000000007;
      if (j < 38) return -0.00043402777777777775;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.0;
      if (j < 42) return 0.00390625;
      if (j < 43) return 0.00000000000000000022;
      if (j < 44) return 0.00043402777777777748;
      if (j < 45) return -0.00000000000000000007;
      if (j < 46) return 0.00043402777777777775;
      if (j < 47) return 0.00000000000000000116;
      if (j < 48) return 0.00130208333333333218;
      if (j < 49) return -0.00000000000000000022;
      return -0.00043402777777777748;
    }
    if (i == 18) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000173;
      if (j < 18) return 0.00390625000000000087;
      if (j < 19) return 0.0;
      if (j < 20) return -0.01171875;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.01171875;
      if (j < 29) return 0.0;
      if (j < 30) return -0.00130208333333333326;
      if (j < 31) return 0.00000000000000000173;
      if (j < 32) return -0.00390625000000000087;
      if (j < 33) return 0.00000000000000000116;
      if (j < 34) return 0.00130208333333333218;
      if (j < 35) return 0.0;
      if (j < 36) return 0.01171875;
      if (j < 37) return -0.00000000000000000007;
      if (j < 38) return -0.00043402777777777775;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.0;
      if (j < 42) return 0.00390625;
      if (j < 43) return -0.00000000000000000022;
      if (j < 44) return 0.00043402777777777797;
      if (j < 45) return 0.00000000000000000007;
      if (j < 46) return 0.00043402777777777775;
      if (j < 47) return -0.00000000000000000116;
      if (j < 48) return -0.00130208333333333218;
      if (j < 49) return 0.00000000000000000022;
      return -0.00043402777777777797;
    }
    if (i == 19) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000058;
      if (j < 18) return -0.00390625000000000087;
      if (j < 19) return 0.0;
      if (j < 20) return 0.01171875;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return -0.01171875;
      if (j < 29) return 0.0;
      if (j < 30) return -0.00130208333333333326;
      if (j < 31) return 0.00000000000000000058;
      if (j < 32) return 0.00390625000000000087;
      if (j < 33) return -0.00000000000000000087;
      if (j < 34) return -0.00130208333333333239;
      if (j < 35) return 0.0;
      if (j < 36) return -0.01171875;
      if (j < 37) return -0.00000000000000000007;
      if (j < 38) return 0.00043402777777777797;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.0;
      if (j < 42) return 0.00390625;
      if (j < 43) return 0.00000000000000000022;
      if (j < 44) return 0.00043402777777777748;
      if (j < 45) return 0.00000000000000000007;
      if (j < 46) return -0.00043402777777777797;
      if (j < 47) return 0.00000000000000000087;
      if (j < 48) return 0.00130208333333333239;
      if (j < 49) return -0.00000000000000000022;
      return -0.00043402777777777748;
    }
    if (i == 20) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000058;
      if (j < 18) return -0.00390624999999999957;
      if (j < 19) return 0.0;
      if (j < 20) return -0.01171875;
      if (j < 21) return 0.0;
      if (j < 22) return -0.00390625;
      if (j < 27) return 0.0;
      if (j < 28) return 0.01171875;
      if (j < 29) return 0.0;
      if (j < 30) return 0.00130208333333333326;
      if (j < 31) return -0.00000000000000000058;
      if (j < 32) return 0.00390624999999999957;
      if (j < 33) return 0.00000000000000000087;
      if (j < 34) return 0.00130208333333333239;
      if (j < 35) return 0.0;
      if (j < 36) return 0.01171875;
      if (j < 37) return 0.00000000000000000007;
      if (j < 38) return 0.00043402777777777759;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.0;
      if (j < 42) return 0.00390625;
      if (j < 43) return -0.00000000000000000022;
      if (j < 44) return 0.00043402777777777797;
      if (j < 45) return -0.00000000000000000007;
      if (j < 46) return -0.00043402777777777759;
      if (j < 47) return -0.00000000000000000087;
      if (j < 48) return -0.00130208333333333239;
      if (j < 49) return 0.00000000000000000022;
      return -0.00043402777777777797;
    }
    if (i == 21) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.00043402777777777775;
      if (j < 17) return 0.0;
      if (j < 18) return -0.00014467592592592592;
      if (j < 19) return 0.0;
      if (j < 20) return -0.00014467592592592592;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00004822530864197531;
      if (j < 27) return 0.0;
      if (j < 28) return -0.00043402777777777775;
      if (j < 29) return 0.0;
      if (j < 30) return -0.00043402777777777775;
      if (j < 31) return 0.0;
      if (j < 32) return 0.00014467592592592592;
      if (j < 33) return 0.0;
      if (j < 34) return 0.00014467592592592592;
      if (j < 35) return 0.0;
      if (j < 36) return 0.00014467592592592592;
      if (j < 37) return 0.0;
      if (j < 38) return 0.00014467592592592592;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00043402777777777775;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00004822530864197531;
      if (j < 43) return 0.0;
      if (j < 44) return -0.00004822530864197531;
      if (j < 45) return 0.0;
      if (j < 46) return -0.00014467592592592592;
      if (j < 47) return 0.0;
      if (j < 48) return -0.00014467592592592592;
      if (j < 49) return 0.0;
      return 0.00004822530864197531;
    }
    if (i == 22) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.00043402777777777775;
      if (j < 17) return 0.0;
      if (j < 18) return -0.00014467592592592592;
      if (j < 19) return 0.0;
      if (j < 20) return 0.00014467592592592592;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00004822530864197531;
      if (j < 27) return 0.0;
      if (j < 28) return 0.00043402777777777775;
      if (j < 29) return 0.0;
      if (j < 30) return 0.00043402777777777775;
      if (j < 31) return 0.0;
      if (j < 32) return 0.00014467592592592592;
      if (j < 33) return 0.0;
      if (j < 34) return -0.00014467592592592592;
      if (j < 35) return 0.0;
      if (j < 36) return -0.00014467592592592592;
      if (j < 37) return 0.0;
      if (j < 38) return 0.00014467592592592592;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00043402777777777775;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00004822530864197531;
      if (j < 43) return 0.0;
      if (j < 44) return -0.00004822530864197531;
      if (j < 45) return 0.0;
      if (j < 46) return -0.00014467592592592592;
      if (j < 47) return 0.0;
      if (j < 48) return 0.00014467592592592592;
      if (j < 49) return 0.0;
      return 0.00004822530864197531;
    }
    if (i == 23) {
      if (j < 9) return 0.0;
      if (j < 10) return -0.00043402777777777775;
      if (j < 17) return 0.0;
      if (j < 18) return 0.00014467592592592592;
      if (j < 19) return 0.0;
      if (j < 20) return -0.00014467592592592592;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00004822530864197531;
      if (j < 27) return 0.0;
      if (j < 28) return 0.00043402777777777775;
      if (j < 29) return 0.0;
      if (j < 30) return 0.00043402777777777775;
      if (j < 31) return 0.0;
      if (j < 32) return -0.00014467592592592592;
      if (j < 33) return 0.0;
      if (j < 34) return 0.00014467592592592592;
      if (j < 35) return 0.0;
      if (j < 36) return 0.00014467592592592592;
      if (j < 37) return 0.0;
      if (j < 38) return -0.00014467592592592592;
      if (j < 39) return 0.0;
      if (j < 40) return -0.00043402777777777775;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00004822530864197531;
      if (j < 43) return 0.0;
      if (j < 44) return -0.00004822530864197531;
      if (j < 45) return 0.0;
      if (j < 46) return 0.00014467592592592592;
      if (j < 47) return 0.0;
      if (j < 48) return -0.00014467592592592592;
      if (j < 49) return 0.0;
      return 0.00004822530864197531;
    }
    if (i == 24) {
      if (j < 9) return 0.0;
      if (j < 10) return 0.00043402777777777775;
      if (j < 17) return 0.0;
      if (j < 18) return 0.00014467592592592592;
      if (j < 19) return 0.0;
      if (j < 20) return 0.00014467592592592592;
      if (j < 21) return 0.0;
      if (j < 22) return 0.00004822530864197531;
      if (j < 27) return 0.0;
      if (j < 28) return -0.00043402777777777775;
      if (j < 29) return 0.0;
      if (j < 30) return -0.00043402777777777775;
      if (j < 31) return 0.0;
      if (j < 32) return -0.00014467592592592592;
      if (j < 33) return 0.0;
      if (j < 34) return -0.00014467592592592592;
      if (j < 35) return 0.0;
      if (j < 36) return -0.00014467592592592592;
      if (j < 37) return 0.0;
      if (j < 38) return -0.00014467592592592592;
      if (j < 39) return 0.0;
      if (j < 40) return 0.00043402777777777775;
      if (j < 41) return 0.0;
      if (j < 42) return -0.00004822530864197531;
      if (j < 43) return 0.0;
      if (j < 44) return -0.00004822530864197531;
      if (j < 45) return 0.0;
      if (j < 46) return 0.00014467592592592592;
      if (j < 47) return 0.0;
      if (j < 48) return 0.00014467592592592592;
      if (j < 49) return 0.0;
      return 0.00004822530864197531;
    }
    if (i == 25) {
      if (j < 1) return 0.0;
      if (j < 2) return 1.0;
      if (j < 4) return 0.0;
      if (j < 5) return -1.11111111111111116045;
      if (j < 7) return 0.0;
      if (j < 8) return 1.11111111111111116045;
      if (j < 20) return 0.0;
      if (j < 21) return 1.2345679012345678327;
      if (j < 22) return -1.2345679012345678327;
      if (j < 23) return 0.11111111111111110494;
      if (j < 24) return -0.11111111111111110494;
      if (j < 25) return 0.11111111111111110494;
      if (j < 26) return -0.11111111111111110494;
      if (j < 32) return 0.0;
      if (j < 33) return 0.00000000000000004163;
      if (j < 34) return -0.00000000000000004163;
      if (j < 35) return 0.00000000000000002776;
      if (j < 36) return -0.00000000000000002776;
      if (j < 40) return 0.0;
      if (j < 41) return -0.12345679012345681103;
      if (j < 42) return 0.12345679012345681103;
      if (j < 43) return -0.12345679012345678327;
      if (j < 44) return 0.12345679012345678327;
      if (j < 45) return 0.00000000000000002255;
      if (j < 46) return -0.00000000000000002255;
      if (j < 48) return 0.0;
      if (j < 49) return 0.01234567901234569047;
      return -0.01234567901234569047;
    }
    if (i == 26) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.5625;
      if (j < 4) return 0.0;
      if (j < 6) return 0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5625;
      if (j < 10) return 0.0;
      if (j < 11) return -0.5625;
      if (j < 12) return 0.5625;
      if (j < 14) return 0.0;
      if (j < 15) return 0.0625;
      if (j < 16) return -0.0625;
      if (j < 17) return 0.625;
      if (j < 18) return -0.625;
      if (j < 19) return -0.00000000000000011102;
      if (j < 20) return 0.00000000000000011102;
      if (j < 21) return -0.625;
      if (j < 22) return 0.625;
      if (j < 24) return 0.0;
      if (j < 25) return -0.0625;
      if (j < 26) return 0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return -0.0625;
      if (j < 32) return 0.0625;
      if (j < 33) return -0.00000000000000000694;
      if (j < 34) return 0.00000000000000000694;
      if (j < 35) return 0.00000000000000018041;
      if (j < 36) return -0.00000000000000018041;
      if (j < 37) return -0.06944444444444444753;
      if (j < 38) return 0.06944444444444444753;
      if (j < 40) return 0.0;
      if (j < 41) return 0.06250000000000002776;
      if (j < 42) return -0.06250000000000002776;
      if (j < 43) return 0.06944444444444430875;
      if (j < 44) return -0.06944444444444430875;
      if (j < 45) return 0.00694444444444442584;
      if (j < 46) return -0.00694444444444442584;
      if (j < 47) return 0.00000000000000002168;
      if (j < 48) return -0.00000000000000002168;
      if (j < 49) return -0.00694444444444447528;
      return 0.00694444444444447528;
    }
    if (i == 27) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.5625;
      if (j < 4) return 0.0;
      if (j < 5) return 0.5625;
      if (j < 6) return -0.5625;
      if (j < 7) return 0.0;
      if (j < 8) return -0.5625;
      if (j < 10) return 0.0;
      if (j < 11) return -0.5625;
      if (j < 12) return 0.5625;
      if (j < 14) return 0.0;
      if (j < 15) return -0.0625;
      if (j < 16) return 0.0625;
      if (j < 17) return -0.625;
      if (j < 18) return 0.625;
      if (j < 19) return -0.00000000000000011102;
      if (j < 20) return 0.00000000000000011102;
      if (j < 21) return -0.625;
      if (j < 22) return 0.625;
      if (j < 24) return 0.0;
      if (j < 25) return -0.0625;
      if (j < 26) return 0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return 0.0625;
      if (j < 32) return -0.0625;
      if (j < 33) return -0.00000000000000002082;
      if (j < 34) return 0.00000000000000002082;
      if (j < 35) return 0.00000000000000015266;
      if (j < 36) return -0.00000000000000015266;
      if (j < 37) return 0.06944444444444444753;
      if (j < 38) return -0.06944444444444444753;
      if (j < 40) return 0.0;
      if (j < 41) return 0.06249999999999997918;
      if (j < 42) return -0.06249999999999997918;
      if (j < 43) return 0.06944444444444436426;
      if (j < 44) return -0.06944444444444436426;
      if (j < 45) return -0.00694444444444447528;
      if (j < 46) return 0.00694444444444447528;
      if (j < 47) return 0.00000000000000002515;
      if (j < 48) return -0.00000000000000002515;
      if (j < 49) return -0.00694444444444444059;
      return 0.00694444444444444059;
    }
    if (i == 28) {
      if (j < 3) return 0.0;
      if (j < 4) return -0.5625;
      if (j < 6) return 0.0;
      if (j < 7) return 0.5625;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5625;
      if (j < 12) return -0.5625;
      if (j < 13) return 0.0625;
      if (j < 14) return -0.0625;
      if (j < 18) return 0.0;
      if (j < 19) return 0.62500000000000022204;
      if (j < 20) return -0.62500000000000022204;
      if (j < 21) return -0.625;
      if (j < 22) return 0.625;
      if (j < 23) return -0.0625;
      if (j < 24) return 0.0625;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000694;
      if (j < 32) return -0.00000000000000000694;
      if (j < 33) return -0.06250000000000002776;
      if (j < 34) return 0.06250000000000002776;
      if (j < 35) return -0.06944444444444494713;
      if (j < 36) return 0.06944444444444494713;
      if (j < 40) return 0.0;
      if (j < 41) return 0.06944444444444446141;
      if (j < 42) return -0.06944444444444446141;
      if (j < 43) return 0.06250000000000011102;
      if (j < 44) return -0.06250000000000011102;
      if (j < 46) return 0.0;
      if (j < 47) return 0.00694444444444439982;
      if (j < 48) return -0.00694444444444439982;
      if (j < 49) return -0.00694444444444444406;
      return 0.00694444444444444406;
    }
    if (i == 29) {
      if (j < 3) return 0.0;
      if (j < 4) return 0.5625;
      if (j < 6) return 0.0;
      if (j < 7) return -0.5625;
      if (j < 10) return 0.0;
      if (j < 11) return 0.5625;
      if (j < 12) return -0.5625;
      if (j < 13) return -0.0625;
      if (j < 14) return 0.0625;
      if (j < 18) return 0.0;
      if (j < 19) return -0.625;
      if (j < 20) return 0.625;
      if (j < 21) return -0.625;
      if (j < 22) return 0.625;
      if (j < 23) return -0.0625;
      if (j < 24) return 0.0625;
      if (j < 32) return 0.0;
      if (j < 33) return 0.06249999999999997224;
      if (j < 34) return -0.06249999999999997224;
      if (j < 35) return 0.06944444444444455855;
      if (j < 36) return -0.06944444444444455855;
      if (j < 40) return 0.0;
      if (j < 41) return 0.06944444444444441977;
      if (j < 42) return -0.06944444444444441977;
      if (j < 43) return 0.06250000000000011102;
      if (j < 44) return -0.06250000000000011102;
      if (j < 45) return 0.00000000000000000434;
      if (j < 46) return -0.00000000000000000434;
      if (j < 47) return -0.00694444444444444666;
      if (j < 48) return 0.00694444444444444666;
      if (j < 49) return -0.00694444444444443712;
      return 0.00694444444444443712;
    }
    if (i == 30) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.31640625;
      if (j < 10) return -0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.31640625;
      if (j < 18) return 0.31640625;
      if (j < 19) return -0.31640625;
      if (j < 21) return 0.31640625;
      if (j < 22) return -0.31640625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.03515625;
      if (j < 28) return 0.03515625;
      if (j < 29) return -0.03515625;
      if (j < 30) return 0.03515625;
      if (j < 31) return 0.03515624999999998612;
      if (j < 32) return -0.03515624999999998612;
      if (j < 33) return 0.03515625;
      if (j < 34) return -0.03515625;
      if (j < 35) return 0.03515625;
      if (j < 36) return -0.03515625;
      if (j < 37) return 0.03515625;
      if (j < 38) return -0.03515625;
      if (j < 39) return 0.00390625;
      if (j < 40) return -0.00390625;
      if (j < 41) return -0.03515625000000000694;
      if (j < 42) return 0.03515625000000000694;
      if (j < 43) return -0.03515625000000000694;
      if (j < 44) return 0.03515625000000000694;
      if (j < 45) return -0.00390624999999999393;
      if (j < 46) return 0.00390624999999999393;
      if (j < 47) return -0.00390624999999999566;
      if (j < 48) return 0.00390624999999999566;
      if (j < 49) return 0.00390625000000000173;
      return -0.00390625000000000173;
    }
    if (i == 31) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.31640625;
      if (j < 10) return 0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return -0.31640625;
      if (j < 19) return 0.31640625;
      if (j < 20) return -0.31640625;
      if (j < 21) return 0.31640625;
      if (j < 22) return -0.31640625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.03515625;
      if (j < 28) return -0.03515625;
      if (j < 29) return 0.03515625;
      if (j < 30) return -0.03515625;
      if (j < 31) return 0.03515625000000001388;
      if (j < 32) return -0.03515625000000001388;
      if (j < 33) return -0.03515624999999998612;
      if (j < 34) return 0.03515624999999998612;
      if (j < 35) return -0.03515625;
      if (j < 37) return 0.03515625;
      if (j < 38) return -0.03515625;
      if (j < 39) return -0.00390625;
      if (j < 40) return 0.00390625;
      if (j < 41) return -0.03515624999999999306;
      if (j < 42) return 0.03515624999999999306;
      if (j < 43) return -0.03515625000000000694;
      if (j < 44) return 0.03515625000000000694;
      if (j < 45) return -0.00390624999999999046;
      if (j < 46) return 0.00390624999999999046;
      if (j < 47) return 0.00390624999999999913;
      if (j < 48) return -0.00390624999999999913;
      if (j < 49) return 0.00390625000000000173;
      return -0.00390625000000000173;
    }
    if (i == 32) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.31640625;
      if (j < 10) return 0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.31640625;
      if (j < 19) return -0.31640625;
      if (j < 21) return 0.31640625;
      if (j < 22) return -0.31640625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.03515625;
      if (j < 28) return -0.03515625;
      if (j < 29) return 0.03515625;
      if (j < 30) return -0.03515625;
      if (j < 31) return -0.03515624999999998612;
      if (j < 32) return 0.03515624999999998612;
      if (j < 33) return 0.03515625000000001388;
      if (j < 34) return -0.03515625000000001388;
      if (j < 35) return 0.03515625000000002082;
      if (j < 36) return -0.03515625000000002082;
      if (j < 37) return -0.03515625;
      if (j < 38) return 0.03515625;
      if (j < 39) return -0.00390625;
      if (j < 40) return 0.00390625;
      if (j < 41) return -0.03515625;
      if (j < 42) return 0.03515625;
      if (j < 43) return -0.03515625;
      if (j < 44) return 0.03515625;
      if (j < 45) return 0.00390625000000000694;
      if (j < 46) return -0.00390625000000000694;
      if (j < 47) return -0.00390625000000000347;
      if (j < 48) return 0.00390625000000000347;
      if (j < 49) return 0.00390625000000000173;
      return -0.00390625000000000173;
    }
    if (i == 33) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.31640625;
      if (j < 10) return -0.31640625;
      if (j < 16) return 0.0;
      if (j < 17) return 0.31640625;
      if (j < 18) return -0.31640625;
      if (j < 19) return 0.31640625;
      if (j < 20) return -0.31640625;
      if (j < 21) return 0.31640625;
      if (j < 22) return -0.31640625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.03515625;
      if (j < 28) return 0.03515625;
      if (j < 29) return -0.03515625;
      if (j < 30) return 0.03515625;
      if (j < 31) return -0.03515625000000001388;
      if (j < 32) return 0.03515625000000001388;
      if (j < 33) return -0.03515624999999998612;
      if (j < 34) return 0.03515624999999998612;
      if (j < 35) return -0.03515624999999999306;
      if (j < 36) return 0.03515624999999999306;
      if (j < 37) return -0.03515625;
      if (j < 38) return 0.03515625;
      if (j < 39) return 0.00390625;
      if (j < 40) return -0.00390625;
      if (j < 41) return -0.03515625;
      if (j < 42) return 0.03515625;
      if (j < 43) return -0.03515625;
      if (j < 44) return 0.03515625;
      if (j < 45) return 0.00390625;
      if (j < 46) return -0.00390625;
      if (j < 47) return 0.00390625;
      if (j < 48) return -0.00390625;
      if (j < 49) return 0.00390625000000000173;
      return -0.00390625000000000173;
    }
    if (i == 34) {
      if (j < 2) return 0.0;
      if (j < 3) return 0.02083333333333333218;
      if (j < 4) return 0.0;
      if (j < 5) return -0.00694444444444444406;
      if (j < 6) return -0.02083333333333333218;
      if (j < 7) return 0.0;
      if (j < 8) return 0.00694444444444444406;
      if (j < 10) return 0.0;
      if (j < 11) return 0.00694444444444444406;
      if (j < 12) return -0.00694444444444444406;
      if (j < 14) return 0.0;
      if (j < 15) return -0.02083333333333333218;
      if (j < 16) return 0.02083333333333333218;
      if (j < 17) return -0.02314814814814813992;
      if (j < 18) return 0.02314814814814813992;
      if (j < 20) return 0.0;
      if (j < 21) return 0.00771604938271604982;
      if (j < 22) return -0.00771604938271604982;
      if (j < 24) return 0.0;
      if (j < 25) return 0.00694444444444444406;
      if (j < 26) return -0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00231481481481481599;
      if (j < 32) return -0.00231481481481481599;
      if (j < 33) return -0.00000000000000000308;
      if (j < 34) return 0.00000000000000000308;
      if (j < 35) return -0.00000000000000000463;
      if (j < 36) return 0.00000000000000000463;
      if (j < 37) return 0.02314814814814815033;
      if (j < 38) return -0.02314814814814815033;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00077160493827160392;
      if (j < 42) return 0.00077160493827160392;
      if (j < 43) return -0.00771604938271604809;
      if (j < 44) return 0.00771604938271604809;
      if (j < 45) return -0.00231481481481481339;
      if (j < 46) return 0.00231481481481481339;
      if (j < 47) return 0.00000000000000000116;
      if (j < 48) return -0.00000000000000000116;
      if (j < 49) return 0.00077160493827160522;
      return -0.00077160493827160522;
    }
    if (i == 35) {
      if (j < 2) return 0.0;
      if (j < 3) return -0.02083333333333333218;
      if (j < 4) return 0.0;
      if (j < 5) return -0.00694444444444444406;
      if (j < 6) return 0.02083333333333333218;
      if (j < 7) return 0.0;
      if (j < 8) return 0.00694444444444444406;
      if (j < 10) return 0.0;
      if (j < 11) return 0.00694444444444444406;
      if (j < 12) return -0.00694444444444444406;
      if (j < 14) return 0.0;
      if (j < 15) return 0.02083333333333333218;
      if (j < 16) return -0.02083333333333333218;
      if (j < 17) return 0.02314814814814813992;
      if (j < 18) return -0.02314814814814813992;
      if (j < 20) return 0.0;
      if (j < 21) return 0.00771604938271604895;
      if (j < 22) return -0.00771604938271604895;
      if (j < 24) return 0.0;
      if (j < 25) return 0.00694444444444444406;
      if (j < 26) return -0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return -0.00231481481481481599;
      if (j < 32) return 0.00231481481481481599;
      if (j < 33) return 0.00000000000000000231;
      if (j < 34) return -0.00000000000000000231;
      if (j < 35) return 0.00000000000000000463;
      if (j < 36) return -0.00000000000000000463;
      if (j < 37) return -0.02314814814814815033;
      if (j < 38) return 0.02314814814814815033;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00077160493827160663;
      if (j < 42) return 0.00077160493827160663;
      if (j < 43) return -0.00771604938271604809;
      if (j < 44) return 0.00771604938271604809;
      if (j < 45) return 0.00231481481481481339;
      if (j < 46) return -0.00231481481481481339;
      if (j < 47) return -0.00000000000000000116;
      if (j < 48) return 0.00000000000000000116;
      if (j < 49) return 0.00077160493827160457;
      return -0.00077160493827160457;
    }
    if (i == 36) {
      if (j < 3) return 0.0;
      if (j < 4) return 0.02083333333333333218;
      if (j < 6) return 0.0;
      if (j < 7) return -0.02083333333333333218;
      if (j < 10) return 0.0;
      if (j < 11) return -0.00694444444444444406;
      if (j < 12) return 0.00694444444444444406;
      if (j < 13) return -0.02083333333333333218;
      if (j < 14) return 0.02083333333333333218;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00000000000000000116;
      if (j < 18) return -0.00000000000000000116;
      if (j < 19) return -0.02314814814814815033;
      if (j < 20) return 0.02314814814814815033;
      if (j < 21) return 0.00771604938271604982;
      if (j < 22) return -0.00771604938271604982;
      if (j < 23) return 0.00694444444444444406;
      if (j < 24) return -0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return -0.00000000000000000116;
      if (j < 32) return 0.00000000000000000116;
      if (j < 33) return 0.00231481481481481295;
      if (j < 34) return -0.00231481481481481295;
      if (j < 35) return 0.02314814814814815033;
      if (j < 36) return -0.02314814814814815033;
      if (j < 37) return -0.0000000000000000001;
      if (j < 38) return 0.0000000000000000001;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00771604938271604982;
      if (j < 42) return 0.00771604938271604982;
      if (j < 43) return -0.00077160493827160446;
      if (j < 44) return 0.00077160493827160446;
      if (j < 45) return 0.0000000000000000001;
      if (j < 46) return -0.0000000000000000001;
      if (j < 47) return -0.00231481481481481295;
      if (j < 48) return 0.00231481481481481295;
      if (j < 49) return 0.00077160493827160446;
      return -0.00077160493827160446;
    }
    if (i == 37) {
      if (j < 3) return 0.0;
      if (j < 4) return -0.02083333333333333218;
      if (j < 6) return 0.0;
      if (j < 7) return 0.02083333333333333218;
      if (j < 10) return 0.0;
      if (j < 11) return -0.00694444444444444406;
      if (j < 12) return 0.00694444444444444406;
      if (j < 13) return 0.02083333333333333218;
      if (j < 14) return -0.02083333333333333218;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00000000000000000116;
      if (j < 18) return 0.00000000000000000116;
      if (j < 19) return 0.02314814814814815033;
      if (j < 20) return -0.02314814814814815033;
      if (j < 21) return 0.00771604938271604895;
      if (j < 22) return -0.00771604938271604895;
      if (j < 23) return 0.00694444444444444406;
      if (j < 24) return -0.00694444444444444406;
      if (j < 30) return 0.0;
      if (j < 31) return 0.00000000000000000116;
      if (j < 32) return -0.00000000000000000116;
      if (j < 33) return -0.00231481481481481295;
      if (j < 34) return 0.00231481481481481295;
      if (j < 35) return -0.02314814814814815033;
      if (j < 36) return 0.02314814814814815033;
      if (j < 40) return 0.0;
      if (j < 41) return -0.00771604938271604895;
      if (j < 42) return 0.00771604938271604895;
      if (j < 43) return -0.00077160493827160533;
      if (j < 44) return 0.00077160493827160533;
      if (j < 46) return 0.0;
      if (j < 47) return 0.00231481481481481295;
      if (j < 48) return -0.00231481481481481295;
      if (j < 49) return 0.00077160493827160533;
      return -0.00077160493827160533;
    }
    if (i == 38) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.0117187499999999948;
      if (j < 18) return -0.0117187499999999948;
      if (j < 19) return 0.00390625;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.00130208333333333326;
      if (j < 28) return -0.00130208333333333326;
      if (j < 29) return 0.01171875;
      if (j < 30) return -0.01171875;
      if (j < 31) return -0.00130208333333333326;
      if (j < 32) return 0.00130208333333333326;
      if (j < 33) return -0.00390624999999999696;
      if (j < 34) return 0.00390624999999999696;
      if (j < 35) return -0.00043402777777777472;
      if (j < 36) return 0.00043402777777777472;
      if (j < 37) return -0.01171875;
      if (j < 38) return 0.01171875;
      if (j < 39) return -0.00130208333333333326;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.00043402777777777699;
      if (j < 42) return -0.00043402777777777699;
      if (j < 43) return 0.00390625000000000087;
      if (j < 44) return -0.00390625000000000087;
      if (j < 45) return 0.00130208333333333261;
      if (j < 46) return -0.00130208333333333261;
      if (j < 47) return 0.00043402777777777699;
      if (j < 48) return -0.00043402777777777699;
      if (j < 49) return -0.00043402777777777759;
      return 0.00043402777777777759;
    }
    if (i == 39) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.0117187499999999948;
      if (j < 18) return -0.0117187499999999948;
      if (j < 19) return -0.00390625;
      if (j < 20) return 0.00390625;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.00130208333333333326;
      if (j < 28) return 0.00130208333333333326;
      if (j < 29) return -0.01171875;
      if (j < 30) return 0.01171875;
      if (j < 31) return -0.00130208333333333456;
      if (j < 32) return 0.00130208333333333456;
      if (j < 33) return 0.00390625;
      if (j < 34) return -0.00390625;
      if (j < 35) return 0.00043402777777777933;
      if (j < 36) return -0.00043402777777777933;
      if (j < 37) return -0.01171875;
      if (j < 38) return 0.01171875;
      if (j < 39) return 0.00130208333333333326;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.00043402777777777699;
      if (j < 42) return -0.00043402777777777699;
      if (j < 43) return 0.00390625000000000087;
      if (j < 44) return -0.00390625000000000087;
      if (j < 45) return 0.00130208333333333239;
      if (j < 46) return -0.00130208333333333239;
      if (j < 47) return -0.00043402777777777819;
      if (j < 48) return 0.00043402777777777819;
      if (j < 49) return -0.00043402777777777759;
      return 0.00043402777777777759;
    }
    if (i == 40) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.0117187499999999948;
      if (j < 18) return 0.0117187499999999948;
      if (j < 19) return 0.00390625;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.00130208333333333326;
      if (j < 28) return 0.00130208333333333326;
      if (j < 29) return -0.01171875;
      if (j < 30) return 0.01171875;
      if (j < 31) return 0.00130208333333333326;
      if (j < 32) return -0.00130208333333333326;
      if (j < 33) return -0.0039062500000000026;
      if (j < 34) return 0.0039062500000000026;
      if (j < 35) return -0.00043402777777778084;
      if (j < 36) return 0.00043402777777778084;
      if (j < 37) return 0.01171875;
      if (j < 38) return -0.01171875;
      if (j < 39) return 0.00130208333333333326;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.00043402777777777857;
      if (j < 42) return -0.00043402777777777857;
      if (j < 43) return 0.00390624999999999957;
      if (j < 44) return -0.00390624999999999957;
      if (j < 45) return -0.00130208333333333261;
      if (j < 46) return 0.00130208333333333261;
      if (j < 47) return 0.00043402777777777857;
      if (j < 48) return -0.00043402777777777857;
      if (j < 49) return -0.00043402777777777775;
      return 0.00043402777777777775;
    }
    if (i == 41) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.0117187499999999948;
      if (j < 18) return 0.0117187499999999948;
      if (j < 19) return -0.00390625;
      if (j < 20) return 0.00390625;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.00130208333333333326;
      if (j < 28) return -0.00130208333333333326;
      if (j < 29) return 0.01171875;
      if (j < 30) return -0.01171875;
      if (j < 31) return 0.00130208333333333456;
      if (j < 32) return -0.00130208333333333456;
      if (j < 33) return 0.00390625;
      if (j < 34) return -0.00390625;
      if (j < 35) return 0.00043402777777777624;
      if (j < 36) return -0.00043402777777777624;
      if (j < 37) return 0.01171875;
      if (j < 38) return -0.01171875;
      if (j < 39) return -0.00130208333333333326;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.00043402777777777857;
      if (j < 42) return -0.00043402777777777857;
      if (j < 43) return 0.00390624999999999957;
      if (j < 44) return -0.00390624999999999957;
      if (j < 45) return -0.00130208333333333239;
      if (j < 46) return 0.00130208333333333239;
      if (j < 47) return -0.00043402777777777737;
      if (j < 48) return 0.00043402777777777737;
      if (j < 49) return -0.00043402777777777775;
      return 0.00043402777777777775;
    }
    if (i == 42) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00390624999999999913;
      if (j < 18) return -0.00390624999999999913;
      if (j < 19) return 0.01171875;
      if (j < 20) return -0.01171875;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.01171875;
      if (j < 28) return -0.01171875;
      if (j < 29) return 0.00130208333333333326;
      if (j < 30) return -0.00130208333333333326;
      if (j < 31) return -0.00390624999999999913;
      if (j < 32) return 0.00390624999999999913;
      if (j < 33) return -0.00130208333333333218;
      if (j < 34) return 0.00130208333333333218;
      if (j < 35) return -0.01171875;
      if (j < 36) return 0.01171875;
      if (j < 37) return -0.00043402777777777775;
      if (j < 38) return 0.00043402777777777775;
      if (j < 39) return -0.00130208333333333326;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.00390625;
      if (j < 42) return -0.00390625;
      if (j < 43) return 0.00043402777777777748;
      if (j < 44) return -0.00043402777777777748;
      if (j < 45) return 0.00043402777777777775;
      if (j < 46) return -0.00043402777777777775;
      if (j < 47) return 0.00130208333333333218;
      if (j < 48) return -0.00130208333333333218;
      if (j < 49) return -0.00043402777777777748;
      return 0.00043402777777777748;
    }
    if (i == 43) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00390625000000000087;
      if (j < 18) return -0.00390625000000000087;
      if (j < 19) return -0.01171875;
      if (j < 20) return 0.01171875;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.01171875;
      if (j < 28) return 0.01171875;
      if (j < 29) return -0.00130208333333333326;
      if (j < 30) return 0.00130208333333333326;
      if (j < 31) return -0.00390625000000000087;
      if (j < 32) return 0.00390625000000000087;
      if (j < 33) return 0.00130208333333333218;
      if (j < 34) return -0.00130208333333333218;
      if (j < 35) return 0.01171875;
      if (j < 36) return -0.01171875;
      if (j < 37) return -0.0004340277777777777;
      if (j < 38) return 0.0004340277777777777;
      if (j < 39) return 0.00130208333333333326;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.00390625;
      if (j < 42) return -0.00390625;
      if (j < 43) return 0.00043402777777777797;
      if (j < 44) return -0.00043402777777777797;
      if (j < 45) return 0.0004340277777777777;
      if (j < 46) return -0.0004340277777777777;
      if (j < 47) return -0.00130208333333333218;
      if (j < 48) return 0.00130208333333333218;
      if (j < 49) return -0.00043402777777777797;
      return 0.00043402777777777797;
    }
    if (i == 44) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.01171875;
      if (j < 10) return -0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00390625;
      if (j < 18) return 0.00390625;
      if (j < 19) return 0.01171875;
      if (j < 20) return -0.01171875;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return -0.01171875;
      if (j < 28) return 0.01171875;
      if (j < 29) return -0.00130208333333333326;
      if (j < 30) return 0.00130208333333333326;
      if (j < 31) return 0.00390625;
      if (j < 32) return -0.00390625;
      if (j < 33) return -0.00130208333333333239;
      if (j < 34) return 0.00130208333333333239;
      if (j < 35) return -0.01171875;
      if (j < 36) return 0.01171875;
      if (j < 37) return 0.00043402777777777786;
      if (j < 38) return -0.00043402777777777786;
      if (j < 39) return 0.00130208333333333326;
      if (j < 40) return -0.00130208333333333326;
      if (j < 41) return 0.00390625;
      if (j < 42) return -0.00390625;
      if (j < 43) return 0.00043402777777777748;
      if (j < 44) return -0.00043402777777777748;
      if (j < 45) return -0.00043402777777777786;
      if (j < 46) return 0.00043402777777777786;
      if (j < 47) return 0.00130208333333333239;
      if (j < 48) return -0.00130208333333333239;
      if (j < 49) return -0.00043402777777777748;
      return 0.00043402777777777748;
    }
    if (i == 45) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.01171875;
      if (j < 10) return 0.01171875;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00390625;
      if (j < 18) return 0.00390625;
      if (j < 19) return -0.01171875;
      if (j < 20) return 0.01171875;
      if (j < 21) return -0.00390625;
      if (j < 22) return 0.00390625;
      if (j < 26) return 0.0;
      if (j < 27) return 0.01171875;
      if (j < 28) return -0.01171875;
      if (j < 29) return 0.00130208333333333326;
      if (j < 30) return -0.00130208333333333326;
      if (j < 31) return 0.00390625;
      if (j < 32) return -0.00390625;
      if (j < 33) return 0.00130208333333333239;
      if (j < 34) return -0.00130208333333333239;
      if (j < 35) return 0.01171875;
      if (j < 36) return -0.01171875;
      if (j < 37) return 0.0004340277777777777;
      if (j < 38) return -0.0004340277777777777;
      if (j < 39) return -0.00130208333333333326;
      if (j < 40) return 0.00130208333333333326;
      if (j < 41) return 0.00390625;
      if (j < 42) return -0.00390625;
      if (j < 43) return 0.00043402777777777797;
      if (j < 44) return -0.00043402777777777797;
      if (j < 45) return -0.0004340277777777777;
      if (j < 46) return 0.0004340277777777777;
      if (j < 47) return -0.00130208333333333239;
      if (j < 48) return 0.00130208333333333239;
      if (j < 49) return -0.00043402777777777797;
      return 0.00043402777777777797;
    }
    if (i == 46) {
      if (j < 8) return 0.0;
      if (j < 9) return 0.00043402777777777775;
      if (j < 10) return -0.00043402777777777775;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00014467592592592592;
      if (j < 18) return 0.00014467592592592592;
      if (j < 19) return -0.00014467592592592592;
      if (j < 20) return 0.00014467592592592592;
      if (j < 21) return 0.00004822530864197531;
      if (j < 22) return -0.00004822530864197531;
      if (j < 26) return 0.0;
      if (j < 27) return -0.00043402777777777775;
      if (j < 28) return 0.00043402777777777775;
      if (j < 29) return -0.00043402777777777775;
      if (j < 30) return 0.00043402777777777775;
      if (j < 31) return 0.00014467592592592592;
      if (j < 32) return -0.00014467592592592592;
      if (j < 33) return 0.00014467592592592592;
      if (j < 34) return -0.00014467592592592592;
      if (j < 35) return 0.00014467592592592592;
      if (j < 36) return -0.00014467592592592592;
      if (j < 37) return 0.00014467592592592592;
      if (j < 38) return -0.00014467592592592592;
      if (j < 39) return 0.00043402777777777775;
      if (j < 40) return -0.00043402777777777775;
      if (j < 41) return -0.00004822530864197531;
      if (j < 42) return 0.00004822530864197531;
      if (j < 43) return -0.00004822530864197531;
      if (j < 44) return 0.00004822530864197531;
      if (j < 45) return -0.00014467592592592592;
      if (j < 46) return 0.00014467592592592592;
      if (j < 47) return -0.00014467592592592592;
      if (j < 48) return 0.00014467592592592592;
      if (j < 49) return 0.00004822530864197531;
      return -0.00004822530864197531;
    }
    if (i == 47) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.00043402777777777775;
      if (j < 10) return 0.00043402777777777775;
      if (j < 16) return 0.0;
      if (j < 17) return -0.00014467592592592592;
      if (j < 19) return 0.00014467592592592592;
      if (j < 20) return -0.00014467592592592592;
      if (j < 21) return 0.00004822530864197531;
      if (j < 22) return -0.00004822530864197531;
      if (j < 26) return 0.0;
      if (j < 27) return 0.00043402777777777775;
      if (j < 28) return -0.00043402777777777775;
      if (j < 29) return 0.00043402777777777775;
      if (j < 30) return -0.00043402777777777775;
      if (j < 31) return 0.00014467592592592592;
      if (j < 33) return -0.00014467592592592592;
      if (j < 34) return 0.00014467592592592592;
      if (j < 35) return -0.00014467592592592592;
      if (j < 37) return 0.00014467592592592592;
      if (j < 38) return -0.00014467592592592592;
      if (j < 39) return -0.00043402777777777775;
      if (j < 40) return 0.00043402777777777775;
      if (j < 41) return -0.00004822530864197531;
      if (j < 42) return 0.00004822530864197531;
      if (j < 43) return -0.00004822530864197531;
      if (j < 44) return 0.00004822530864197531;
      if (j < 45) return -0.00014467592592592592;
      if (j < 47) return 0.00014467592592592592;
      if (j < 48) return -0.00014467592592592592;
      if (j < 49) return 0.00004822530864197531;
      return -0.00004822530864197531;
    }
    if (i == 48) {
      if (j < 8) return 0.0;
      if (j < 9) return -0.00043402777777777775;
      if (j < 10) return 0.00043402777777777775;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00014467592592592592;
      if (j < 19) return -0.00014467592592592592;
      if (j < 20) return 0.00014467592592592592;
      if (j < 21) return 0.00004822530864197531;
      if (j < 22) return -0.00004822530864197531;
      if (j < 26) return 0.0;
      if (j < 27) return 0.00043402777777777775;
      if (j < 28) return -0.00043402777777777775;
      if (j < 29) return 0.00043402777777777775;
      if (j < 30) return -0.00043402777777777775;
      if (j < 31) return -0.00014467592592592592;
      if (j < 33) return 0.00014467592592592592;
      if (j < 34) return -0.00014467592592592592;
      if (j < 35) return 0.00014467592592592592;
      if (j < 37) return -0.00014467592592592592;
      if (j < 38) return 0.00014467592592592592;
      if (j < 39) return -0.00043402777777777775;
      if (j < 40) return 0.00043402777777777775;
      if (j < 41) return -0.00004822530864197531;
      if (j < 42) return 0.00004822530864197531;
      if (j < 43) return -0.00004822530864197531;
      if (j < 44) return 0.00004822530864197531;
      if (j < 45) return 0.00014467592592592592;
      if (j < 47) return -0.00014467592592592592;
      if (j < 48) return 0.00014467592592592592;
      if (j < 49) return 0.00004822530864197531;
      return -0.00004822530864197531;
    }
    {
      if (j < 8) return 0.0;
      if (j < 9) return 0.00043402777777777775;
      if (j < 10) return -0.00043402777777777775;
      if (j < 16) return 0.0;
      if (j < 17) return 0.00014467592592592592;
      if (j < 18) return -0.00014467592592592592;
      if (j < 19) return 0.00014467592592592592;
      if (j < 20) return -0.00014467592592592592;
      if (j < 21) return 0.00004822530864197531;
      if (j < 22) return -0.00004822530864197531;
      if (j < 26) return 0.0;
      if (j < 27) return -0.00043402777777777775;
      if (j < 28) return 0.00043402777777777775;
      if (j < 29) return -0.00043402777777777775;
      if (j < 30) return 0.00043402777777777775;
      if (j < 31) return -0.00014467592592592592;
      if (j < 32) return 0.00014467592592592592;
      if (j < 33) return -0.00014467592592592592;
      if (j < 34) return 0.00014467592592592592;
      if (j < 35) return -0.00014467592592592592;
      if (j < 36) return 0.00014467592592592592;
      if (j < 37) return -0.00014467592592592592;
      if (j < 38) return 0.00014467592592592592;
      if (j < 39) return 0.00043402777777777775;
      if (j < 40) return -0.00043402777777777775;
      if (j < 41) return -0.00004822530864197531;
      if (j < 42) return 0.00004822530864197531;
      if (j < 43) return -0.00004822530864197531;
      if (j < 44) return 0.00004822530864197531;
      if (j < 45) return 0.00014467592592592592;
      if (j < 46) return -0.00014467592592592592;
      if (j < 47) return 0.00014467592592592592;
      if (j < 48) return -0.00014467592592592592;
      if (j < 49) return 0.00004822530864197531;
      return -0.00004822530864197531;
    }
  }
};

// specialization for better equilibrium accuarcy
template <>
struct equilibrium<d2q25> {
  typedef typename d2q25::conserved_moments conserved_moments;
  typedef typename d2q25::primitive_variables primitive_variables;
  typedef typename d2q25::rvector rvector;
  typedef typename d2q25::ivector ivector;
  typedef typename d2q25::fvector fvector;

  LABICEX_COMMON static fvector get(const primitive_variables& pv) {
    const real& density = pv.density();
    const rvector& velocity = pv.velocity();
    const real t0 = d2q25::reference_temperature();
    const real t02 = t0 * t0;
    const real t03 = t02 * t0;
    fvector feq;
    LABICEX_UNROLL
    for (int i = 0; i < 25; ++i) {
      real sum = 1;
      LABICEX_UNROLL
      for (int a = 0; a < 2; ++a) {
        sum += d2q25::cf(i)(a) * velocity(a) / t0;
        LABICEX_UNROLL
        for (int b = 0; b < 2; ++b) {
          sum += velocity(a) * velocity(b) / (2 * t02) *
                 (d2q25::cf(i)(a) * d2q25::cf(i)(b) - (a == b ? t0 : 0));
          LABICEX_UNROLL
          for (int c = 0; c < 2; ++c)
            sum += velocity(a) * velocity(b) * velocity(c) / (6 * t03) *
                   d2q25::cf(i)(c) *
                   (d2q25::cf(i)(a) * d2q25::cf(i)(b) - (a == b ? 3 * t0 : 0));
        }
      }
      const real feqi = density * d2q25::w(i) * sum;
      assert(!isnan(feqi));
      feq(i) = feqi;
    }
#ifdef LABICEX_FULLDEBUG
    primitive_variables pveq(feq);
    assert(abs(pveq.density() - density) < 1e-5);
    assert((pveq.velocity() - velocity).matrix().norm() < 1e-5);
#endif  // LABICEX_FULLDEBUG
    return feq;
  }

  LABICEX_COMMON static fvector gravity_difference(
      const primitive_variables& pv, real gravity) {
    const primitive_variables pv_accelerated(
        pv.density(), pv.velocity() + rvector(0, -gravity));
    return get(pv_accelerated) - get(pv);
  }
};

}  // standard

}  // lattice

}  // labicex

#endif  // LABICEX_LATTICE_STANDARD_D2Q25_H

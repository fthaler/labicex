#ifndef LABICEX_LATTICE_STANDARD_H
#define LABICEX_LATTICE_STANDARD_H

#include "lattice/standard/d2q25.h"
#include "lattice/standard/d2q9.h"
#include "lattice/standard/d3q27.h"

#endif  // LABICEX_LATTICE_STANDARD_H

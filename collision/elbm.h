#ifndef LABICEX_COLLISION_ELBM_H
#define LABICEX_COLLISION_ELBM_H

#include <boost/type_traits/is_same.hpp>
#include <boost/utility/enable_if.hpp>

#include "base/common.h"
#include "lattice/tags.h"

namespace labicex {

namespace collision {

template <class Lattice>
class elbm {
 public:
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::equilibrium equilibrium;

  elbm(real mu) : mu_(mu) {}

  LABICEX_COMMON_INLINE void collide(fvector& f) const {
    const primitive_variables pv(f);
    const real nu = mu_ / pv.density();
    const real beta = 1 / (6 * nu + 1);
    const fvector feq = equilibrium::get(pv);
    const real alpha = get_alpha(f, feq);
    f += alpha * beta * (feq - f);
  }

 private:
  template <class L, class Enable = void>
  class w_helper {
   public:
    LABICEX_COMMON_INLINE w_helper(const typename L::fvector&) {}
    LABICEX_COMMON_INLINE real operator()(int i) const { return L::w(i); }
  };

  template <class L>
  class w_helper<
      L, typename boost::enable_if<boost::is_same<
             typename L::lattice_category, lattice::thermal_tag> >::type> {
   public:
    LABICEX_COMMON_INLINE w_helper(const typename L::fvector& f)
        : temperature_(typename L::primitive_variables(f).temperature()) {}
    LABICEX_COMMON_INLINE real operator()(int i) const {
      return L::w(i, temperature_);
    }

   private:
    real temperature_;
  };

  LABICEX_COMMON_NOINLINE static real alpha_solve(const fvector& f,
                                                  const fvector& df,
                                                  real alpha_max) {
    assert(!isnan(alpha_max));
    real alpha_min = 1;
    real hf = 0;
    const w_helper<Lattice> w(f);
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) hf += f(i) * log(f(i) / w(i));
    assert(!isnan(hf));
    real guess = alpha_max < 2 ? alpha_max : 2;
    real alpha = guess;

    const real factor = 128;
    real delta = 1;
    real delta1 = 1e100, delta2;

    int count = 50;

    do {
      delta2 = delta1;
      delta1 = delta;

      real h0 = -hf;
      real h1 = 0;
      const fvector fdf = f + alpha * df;
      const w_helper<Lattice> wfdf(fdf);
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        assert(fdf(i) > 1e-12);
        const real logfiw = log(fdf(i) / wfdf(i));
        h0 += fdf(i) * logfiw;
        h1 += df(i) * logfiw;
      }

      assert(h0 != 0);
      assert(h1 != 0);

      delta = h0 / h1;

      if (abs(delta * 2) > abs(delta2))
        delta = delta > 0 ? (alpha - alpha_min) / 2 : (alpha - alpha_max) / 2;

      guess = alpha;
      alpha -= delta;

      if (alpha <= alpha_min) {
        delta = (guess - alpha_min) / 2;
        alpha = guess - delta;
        if (alpha == alpha_min || alpha == alpha_max) break;
      } else if (alpha >= alpha_max) {
        delta = (guess - alpha_max) / 2;
        alpha = guess - delta;
        if (alpha == alpha_min || alpha == alpha_max) break;
      }

      if (delta > 0)
        alpha_max = guess;
      else
        alpha_min = guess;
    } while (--count && abs(alpha * factor) < abs(delta));
    assert(count > 0);
    assert(!isnan(alpha));

    return alpha;
  }

  LABICEX_COMMON_INLINE static real get_alpha(const fvector& f,
                                              const fvector& feq) {
    real alpha_max = 1000;
    real delta_max = 0;

    const fvector df = feq - f;

    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      const real delta = abs(df(i) / feq(i));
      delta_max = delta > delta_max ? delta : delta_max;
      if (df(i) < 0) {
        const real alpha = -f(i) / df(i);
        alpha_max = alpha < alpha_max ? alpha : alpha_max;
      }
    }

    if (delta_max < 1e-2) return 2;

    if (alpha_max < 2) return alpha_max;

    return alpha_solve(f, df, alpha_max);
  }
  real mu_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_ELBM_H

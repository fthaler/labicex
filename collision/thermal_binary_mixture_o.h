#ifndef LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_O_H
#define LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_O_H

#include <cassert>

#include "base/common.h"
#include "base/inline_math.h"
#include "collision/binary_mixture_parameters.h"

namespace labicex {

namespace collision {

template <class Lattice, class Parameters = binary_mixture_parameters1,
          bool FullyThermal = true>
class thermal_binary_mixture_o {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::conserved_moments conserved_moments;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::equilibrium equilibrium;
  typedef typename Lattice::grad_moments grad_moments;
  typedef typename Lattice::grad_approximation grad_approximation;

  thermal_binary_mixture_o(real parameter1, real parameter2, real m_a, real m_b,
                           real t_phys)
      : parameters_(parameter1, parameter2),
        m_a_(m_a),
        m_b_(m_b),
        t_phys_(t_phys) {}

  thermal_binary_mixture_o(real parameter1, real parameter2, real m_a, real m_b)
      : parameters_(parameter1, parameter2), m_a_(m_a), m_b_(m_b) {
    assert(FullyThermal);
  }

  LABICEX_COMMON_INLINE void collide(fvector& f_a, fvector& f_b) const {
    assert(!isnan(f_a.sum()) && f_a.sum() > 0);
    assert(!isnan(f_b.sum()) && f_b.sum() > 0);
    const grad_moments gm_a(f_a);
    asm_barrier(f_a);
    asm_barrier(f_b);
    const grad_moments gm_b(f_b);
    asm_barrier(f_a);
    asm_barrier(f_b);

    const primitive_variables pv_ab((f_a + f_b).eval());
    const real n_a = gm_a.density() / m_a_;
    const real n_b = gm_b.density() / m_b_;
    const real n_ab = n_a + n_b;
    asm_barrier(f_a);
    asm_barrier(f_b);

    // physical temperature
    real t_ab;
    if (FullyThermal)
      t_ab = pv_ab.temperature() * pv_ab.density() / n_ab;
    else
      t_ab = t_phys_;

    const real x_a = n_a / n_ab;
    const real x_b = n_b / n_ab;
    const real y_a = gm_a.density() / pv_ab.density();
    const real y_b = gm_b.density() / pv_ab.density();

    const real diffcoef = parameters_.diffusion_coefficient(pv_ab.density());

    const real rd = gm_a.density() * gm_b.density() / pv_ab.density();

    const real p = n_ab * t_ab;
    const real tau1 = parameters_.dynamic_viscosity(pv_ab.density()) / p;
    const real tau2 = diffcoef / p * rd / (x_a * x_b);
    const real omega1 = 1 / (2 * tau1 + 1);
    assert(omega1 >= 0);
    assert(omega1 <= 1);
    const real omega2 = 1 / (2 * tau2 + 1);
    assert(omega2 >= 0);
    assert(omega2 <= 1);

    asm_barrier(f_a);
    asm_barrier(f_b);
    f_a *= (1 - 2 * omega1);
    f_b *= (1 - 2 * omega1);
    asm_barrier(f_a);
    asm_barrier(f_b);

    const primitive_variables pveq_a(gm_a.density(), pv_ab.velocity(),
                                     t_ab / m_a_);
    const primitive_variables pveq_b(gm_b.density(), pv_ab.velocity(),
                                     t_ab / m_b_);

    const real feq_fac = 2 * omega1 * tau1 / tau2;
    asm_barrier(f_a);
    asm_barrier(f_b);
    equilibrium::axpy(pveq_a, feq_fac, f_a);
    asm_barrier(f_a);
    asm_barrier(f_b);
    equilibrium::axpy(pveq_b, feq_fac, f_b);
    asm_barrier(f_a);
    asm_barrier(f_b);
    assert(!isnan(f_a.sum()));
    assert(!isnan(f_b.sum()));

    const real fstar_fac = 2 * omega1 * (tau2 - tau1) / tau2;

    const conserved_moments cmeq_a(pveq_a);
    const conserved_moments cmeq_b(pveq_b);
    const primitive_variables pvfc_a(conserved_moments(
        cmeq_a.density(),
        ((1 - omega2) * gm_a.momentum() + omega2 * cmeq_a.momentum()).eval(),
        (1 - omega2) * gm_a.pressure_trace() +
            omega2 * cmeq_a.pressure_trace()));
    const primitive_variables pvfc_b(conserved_moments(
        cmeq_b.density(),
        ((1 - omega2) * gm_b.momentum() + omega2 * cmeq_b.momentum()).eval(),
        (1 - omega2) * gm_b.pressure_trace() +
            omega2 * cmeq_b.pressure_trace()));

    asm_barrier(f_a);
    asm_barrier(f_b);
    equilibrium::axpy(pvfc_a, fstar_fac, f_a);
    asm_barrier(f_a);
    asm_barrier(f_b);
    equilibrium::axpy(pvfc_b, fstar_fac, f_b);
    asm_barrier(f_a);
    asm_barrier(f_b);

    assert(!isnan(f_a.sum()) && f_a.sum() > 0);
    assert(!isnan(f_b.sum()) && f_b.sum() > 0);
  }

 private:
  Parameters parameters_;
  real m_a_, m_b_, t_phys_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_O_H

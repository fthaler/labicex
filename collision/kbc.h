#ifndef LABICEX_COLLISION_KBC_H
#define LABICEX_COLLISION_KBC_H

#include "base/common.h"

namespace labicex {

namespace collision {

template <class Lattice, bool Newton = false,
          int K = Lattice::conserved_moments::count,
          int S = (Lattice::d * Lattice::d + Lattice::d) / 2>
class kbc {
 public:
  typedef Lattice lattice;
  typedef typename lattice::fvector fvector;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::equilibrium equilibrium;
  static const int k_moments_count = K;
  static const int s_moments_count = S;

  kbc(real mu) : mu_(mu) {}

  LABICEX_COMMON_INLINE void collide(fvector& f) const {
    const primitive_variables pv(f);
    const real nu = mu_ / pv.density();
    const real beta = 1 / (6 * nu + 1);

    // compute equilibrium
    const fvector feq = equilibrium::get(pv);

    // compute shear moment difference ds
    fvector ds = fvector::Zero();
    LABICEX_UNROLL
    for (int i = k_moments_count; i < k_moments_count + s_moments_count; ++i) {
      // compute ds in natural moment basis
      real dsi = 0;
      LABICEX_UNROLL
      for (int j = 0; j < Lattice::q; ++j)
        dsi += Lattice::natural_moment_matrix(i, j) * (f(j) - feq(j));
      // transform back
      LABICEX_UNROLL
      for (int j = 0; j < Lattice::q; ++j)
        ds(j) += Lattice::inverse_natural_moment_matrix(j, i) * dsi;
    }
    // store higher order moment difference dh in f
    f -= feq + ds;

    // compute gamma
    const real gamma = get_gamma(feq, ds, f, beta);
    assert(abs(f.sum()) < 1e-5);

    f = feq + (1 - 2 * beta) * ds + (1 - gamma * beta) * f;
  }

 private:
  LABICEX_COMMON_INLINE static real get_gamma(const fvector& feq,
                                              const fvector& ds,
                                              const fvector& dh,
                                              const real beta) {
    if (Newton) {
      // compute gamma using newton iteration
      real gamma = 2;
      const real factor = 128;
      real delta = 1;
      int count = 50;
      do {
        real h0 = 0, h1 = 0;
        LABICEX_UNROLL
        for (int i = 0; i < Lattice::q; ++i) {
          const real x = 1 + (1 - 2 * beta) * ds(i) / feq(i) +
                         (1 - gamma * beta) * dh(i) / feq(i);
          h0 -= dh(i) * log(x);
          h1 += dh(i) / x * beta * dh(i) / feq(i);
        }
        assert(h0 != 0);
        assert(h1 != 0);

        delta = h0 / h1;
        gamma -= delta;
      } while (--count && abs(gamma * factor) < abs(delta));
      assert(count > 0);
      assert(!isnan(gamma));
      return gamma;
    } else {
      // compute gamma using fast approximation
      real dsdh = 0, dhdh = 0;
      LABICEX_UNROLL
      for (int i = 0; i < lattice::q; ++i) {
        dsdh += ds(i) * dh(i) / feq(i);
        dhdh += dh(i) * dh(i) / feq(i);
      }
      const real gamma = 1 / beta + (1 / beta - 2) * dsdh / dhdh;
      assert(!isnan(gamma));
      return gamma;
    }
  }

  real mu_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_KBC_H

#ifndef LABICEX_COLLISION_BINARY_MIXTURE_PARAMETERS_H
#define LABICEX_COLLISION_BINARY_MIXTURE_PARAMETERS_H

namespace labicex {

namespace collision {

class binary_mixture_parameters1 {
 public:
  binary_mixture_parameters1(real kinematic_viscosity, real schmidt_number)
      : kinematic_viscosity_(kinematic_viscosity),
        schmidt_number_(schmidt_number) {}

  LABICEX_COMMON_INLINE real kinematic_viscosity(real) const {
    return kinematic_viscosity_;
  }

  LABICEX_COMMON_INLINE real dynamic_viscosity(real density) const {
    return kinematic_viscosity_ * density;
  }

  LABICEX_COMMON_INLINE real schmidt_number(real) const {
    return schmidt_number_;
  }

  LABICEX_COMMON_INLINE real diffusion_coefficient(real) const {
    return kinematic_viscosity_ / schmidt_number_;
  }

 private:
  real kinematic_viscosity_, schmidt_number_;
};

class binary_mixture_parameters2 {
 public:
  binary_mixture_parameters2(real dynamic_viscosity, real schmidt_number)
      : dynamic_viscosity_(dynamic_viscosity),
        schmidt_number_(schmidt_number) {}

  LABICEX_COMMON_INLINE real kinematic_viscosity(real density) const {
    return dynamic_viscosity_ / density;
  }

  LABICEX_COMMON_INLINE real dynamic_viscosity(real) const {
    return dynamic_viscosity_;
  }

  LABICEX_COMMON_INLINE real schmidt_number(real) const {
    return schmidt_number_;
  }

  LABICEX_COMMON_INLINE real diffusion_coefficient(real density) const {
    return dynamic_viscosity_ / (density * schmidt_number_);
  }

 private:
  real dynamic_viscosity_, schmidt_number_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_BINARY_MIXTURE_PARAMETERS_H

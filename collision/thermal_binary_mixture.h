#ifndef LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_H
#define LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_H

#include <cassert>

#include "base/common.h"
#include "base/entropy.h"
#include "base/inline_math.h"
#include "collision/binary_mixture_parameters.h"

namespace labicex {

namespace collision {

template <class Lattice, class Parameters = binary_mixture_parameters1,
          bool FullyThermal = true, bool Newton = false, int K = Lattice::d + 3,
          int S = Lattice::d* Lattice::d + 2 * Lattice::d - 1>
class thermal_binary_mixture {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::rvector rvector;
  typedef typename lattice::fvector fvector;
  typedef typename lattice::rtensor rtensor;
  typedef typename lattice::conserved_moments conserved_moments;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::equilibrium equilibrium;
  typedef typename lattice::numerical_equilibrium numerical_equilibrium;
  static const int k_moments_count = K;
  static const int s_moments_count = S;

  thermal_binary_mixture(real parameter1, real parameter2, real m_a, real m_b,
                         real t_phys)
      : parameters_(parameter1, parameter2),
        m_a_(m_a),
        m_b_(m_b),
        t_phys_(t_phys) {}

  thermal_binary_mixture(real parameter1, real parameter2, real m_a, real m_b)
      : parameters_(parameter1, parameter2), m_a_(m_a), m_b_(m_b) {
    assert(FullyThermal);
  }

  LABICEX_COMMON_INLINE void collide(fvector& f_a, fvector& f_b) const {
#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ "thermal_binary_mixture::collide"
#define LABICEX_PRETTY_FIX
#endif
    assert(!isnan(f_a.sum()) && f_a.sum() > 0);
    assert(!isnan(f_b.sum()) && f_b.sum() > 0);
    const primitive_variables pv_a(f_a);
    const primitive_variables pv_b(f_b);

    const primitive_variables pv_ab((f_a + f_b).eval());
    const real n_a = pv_a.density() / m_a_;
    const real n_b = pv_b.density() / m_b_;
    const real n_ab = n_a + n_b;

    // physical temperature
    real t_ab;
    if (FullyThermal)
      t_ab = pv_ab.temperature() * pv_ab.density() / n_ab;
    else
      t_ab = t_phys_;

    const real x_a = n_a / n_ab;
    const real x_b = n_b / n_ab;
    const real y_a = pv_a.density() / pv_ab.density();
    const real y_b = pv_b.density() / pv_ab.density();

    const real diffcoef = parameters_.diffusion_coefficient(pv_ab.density());

    const real rd = pv_a.density() * pv_b.density() / pv_ab.density();

    const real p = n_ab * t_ab;
    const real tau1 = parameters_.dynamic_viscosity(pv_ab.density()) / p;
    const real tau2 = diffcoef / p * rd / (x_a * x_b);
    const real omega1 = 1 / (2 * tau1 + 1);
    assert(omega1 >= 0);
    assert(omega1 <= 1);
    const real omega2 = 1 / (2 * tau2 + 1);
    assert(omega2 >= 0);
    assert(omega2 <= 1);

    const primitive_variables pveq_a(pv_a.density(), pv_ab.velocity(),
                                     t_ab / m_a_);
    const primitive_variables pveq_b(pv_b.density(), pv_ab.velocity(),
                                     t_ab / m_b_);

    fvector feq_a = equilibrium::get(pveq_a);
    fvector feq_b = equilibrium::get(pveq_b);
    assert(!isnan(feq_a.sum()) && feq_a.sum() > 0);
    assert(!isnan(feq_b.sum()) && feq_b.sum() > 0);

    const fvector fc_a = (1 - omega2) * f_a + omega2 * feq_a;
    const fvector fc_b = (1 - omega2) * f_b + omega2 * feq_b;
    assert(!isnan(fc_a.sum()) && fc_a.sum() > 0);
    assert(!isnan(fc_b.sum()) && fc_b.sum() > 0);

    const primitive_variables pvfc_a(fc_a);
    const primitive_variables pvfc_b(fc_b);

    fvector fstar_a, fstar_b;
    fstar_a = equilibrium::get(pvfc_a);
    fstar_b = equilibrium::get(pvfc_b);
    assert(!isnan(fstar_a.sum()) && fc_a.sum() > 0);
    assert(!isnan(fstar_b.sum()) && fc_b.sum() > 0);

    if (K < lattice::q) {
      // KBC
      feq_a = tau1 / tau2 * feq_a + (tau2 - tau1) / tau2 * fstar_a;
      feq_b = tau1 / tau2 * feq_b + (tau2 - tau1) / tau2 * fstar_b;

#ifdef LABICEX_FULLDEBUG
      // check conservation
      const conserved_moments cmfeq_check_a(feq_a), cmfeq_check_b(feq_b);
      const conserved_moments cmfeq_check_ab((feq_a + feq_b).eval());
      const conserved_moments cmf_check_a(f_a), cmf_check_b(f_b);
      const conserved_moments cmf_check_ab((f_a + f_b).eval());

      assert(abs(cmfeq_check_a.density() - cmf_check_a.density()) < 1e-5);
      assert(abs(cmfeq_check_b.density() - cmf_check_b.density()) < 1e-5);
      assert(abs(cmfeq_check_ab.density() - cmf_check_ab.density()) < 1e-5);
      assert((cmfeq_check_ab.momentum() - cmf_check_ab.momentum())
                 .matrix()
                 .norm() < 1e-5);
      assert(abs(cmfeq_check_ab.pressure_trace() -
                 cmf_check_ab.pressure_trace()) < 1e-5);
      // compute precollision entropy
      const real h_precollision = entropy<lattice>(f_a) + entropy<lattice>(f_b);
#endif

      // compute shear moment difference ds
      fvector ds_a = fvector::Zero(), ds_b = fvector::Zero();
      LABICEX_UNROLL
      for (int i = k_moments_count; i < k_moments_count + s_moments_count;
           ++i) {
        // compute ds in natural moment basis
        real dsi = 0;
        LABICEX_UNROLL
        for (int j = 0; j < lattice::q; ++j) {
          dsi += Lattice::binary_mixture_natural_moment_matrix(i, j) *
                 (f_a(j) - feq_a(j));
          dsi +=
              Lattice::binary_mixture_natural_moment_matrix(i, j + Lattice::q) *
              (f_b(j) - feq_b(j));
        }
        // transform back
        LABICEX_UNROLL
        for (int j = 0; j < lattice::q; ++j) {
          ds_a(j) +=
              Lattice::inverse_binary_mixture_natural_moment_matrix(j, i) * dsi;
          ds_b(j) += Lattice::inverse_binary_mixture_natural_moment_matrix(
                         j + Lattice::q, i) *
                     dsi;
        }
      }
      // store higher order moment difference dh
      f_a -= feq_a + ds_a;
      f_b -= feq_b + ds_b;

      // compute gamma
      const real gamma = get_gamma(feq_a, feq_b, ds_a, ds_b, f_a, f_b, omega1);

      // compute postcollision state
      f_a = feq_a + (1 - 2 * omega1) * ds_a + (1 - gamma * omega1) * f_a;
      f_b = feq_b + (1 - 2 * omega1) * ds_b + (1 - gamma * omega1) * f_b;

#ifdef LABICEX_FULLDEBUG
      // check entropy production
      const real h_postcollision =
          entropy<lattice>(f_a) + entropy<lattice>(f_b);
      assert(h_postcollision - 1e-3 <= h_precollision);
#endif
    } else {
      // no KBC
      f_a += 2 * omega1 *
             (tau1 / tau2 * feq_a + (tau2 - tau1) / tau2 * fstar_a - f_a);
      f_b += 2 * omega1 *
             (tau1 / tau2 * feq_b + (tau2 - tau1) / tau2 * fstar_b - f_b);
    }
    assert(!isnan(feq_a.sum()) && feq_a.sum() > 0);
    assert(!isnan(feq_b.sum()) && feq_b.sum() > 0);
#ifdef LABICEX_PRETTY_FIX
#undef __PRETTY_FUNCTION__
#endif
  }

 private:
  LABICEX_COMMON_INLINE static rtensor pressure_tensor(const fvector& f) {
    rtensor p = rtensor::Zero();
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b)
          p(a, b) += f(i) * Lattice::ci(i)(a) * Lattice::ci(i)(b);
      }
    }
    return p;
  }

  LABICEX_COMMON_INLINE static fvector grad(const primitive_variables& pv,
                                            const rtensor& p) {
    fvector f;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      real fi = pv.density();
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        fi += pv.density() * pv.velocity()(a) * Lattice::cf(i)(a) /
              pv.temperature();
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          fi += (p(a, b) - pv.density() * pv.temperature() * delta(a, b)) *
                (Lattice::cf(i)(a) * Lattice::cf(i)(b) -
                 pv.temperature() * delta(a, b)) /
                (2 * pv.temperature() * pv.temperature());
          LABICEX_UNROLL
          for (int c = 0; c < Lattice::d; ++c) {
            fi += (pv.density() * pv.velocity()(a) * pv.velocity()(b) *
                   pv.velocity()(c)) *
                  (Lattice::cf(i)(a) * Lattice::cf(i)(b) * Lattice::cf(i)(c) -
                   3 * pv.temperature() * Lattice::cf(i)(c) * delta(a, b)) /
                  (6 * pv.temperature() * pv.temperature() * pv.temperature());
          }
        }
      }
      f(i) = Lattice::w(i, pv) * fi;
    }
    return f;
  }

  LABICEX_COMMON_INLINE static fvector fstarp(const primitive_variables& pv,
                                              const rtensor& p) {
    fvector f;
    const real jfac = 1 / pv.temperature();
    const real pfac = 1 / (2 * pv.temperature() * pv.temperature());
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      real fi = pv.density();
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        fi += jfac * pv.density() * pv.velocity()(a) * Lattice::ci(i)(a);
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b)
          fi += pfac *
                (p(a, b) - pv.density() * pv.temperature() * delta(a, b)) *
                (Lattice::ci(i)(a) * Lattice::ci(i)(b) -
                 pv.temperature() * delta(a, b));
      }
      f(i) = Lattice::w(i, pv) * fi;
    }
    return f;
  }

  LABICEX_COMMON_INLINE static real get_gamma(
      const fvector& feq_a, const fvector& feq_b, const fvector& ds_a,
      const fvector& ds_b, const fvector& dh_a, const fvector& dh_b,
      const real beta) {
    if (Newton) {
      // compute gamma using newton solver

      // compute limits for gamma to avoid negative arguments for log()
      real gamma_max = 1e10, gamma_min = -1e10;
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        const real lim_a = 1 / beta + (1 / beta - 2) * ds_a(i) / dh_a(i) +
                           feq_a(i) / (beta * dh_a(i));
        const real lim_b = 1 / beta + (1 / beta - 2) * ds_b(i) / dh_b(i) +
                           feq_b(i) / (beta * dh_b(i));
        if (beta * dh_a(i) / feq_a(i) < 0)
          gamma_min = lim_a > gamma_min ? lim_a : gamma_min;
        else
          gamma_max = lim_a < gamma_max ? lim_a : gamma_max;
        if (beta * dh_b(i) / feq_b(i) < 0)
          gamma_min = lim_b > gamma_min ? lim_b : gamma_min;
        else
          gamma_max = lim_b < gamma_max ? lim_b : gamma_max;
      }
      assert(!isnan(gamma_max) && !isnan(gamma_min));
      assert(gamma_min <= gamma_max);
      gamma_max -= 1e-5;
      gamma_min += 1e-5;
      real gamma = gamma_max < 2 ? gamma_max : gamma_min > 2 ? gamma_min : 2;
      assert(gamma >= gamma_min && gamma <= gamma_max);

      // netwon iterations
      real delta = 1;
      int count = 50;
      real h0, h1;
      do {
        assert(gamma >= gamma_min && gamma <= gamma_max);
        h0 = 0, h1 = 0;
        LABICEX_UNROLL
        for (int i = 0; i < Lattice::q; ++i) {
          const real x_a = 1 + (1 - 2 * beta) * ds_a(i) / feq_a(i) +
                           (1 - gamma * beta) * dh_a(i) / feq_a(i);
          const real x_b = 1 + (1 - 2 * beta) * ds_b(i) / feq_b(i) +
                           (1 - gamma * beta) * dh_b(i) / feq_b(i);
          h0 -= dh_a(i) * log(x_a);
          h0 -= dh_b(i) * log(x_b);
          h1 += dh_a(i) / x_a * beta * dh_a(i) / feq_a(i);
          h1 += dh_b(i) / x_b * beta * dh_b(i) / feq_b(i);
        }
        assert(!isnan(h0));
        assert(!isnan(h1));
        assert(h0 != 0);
        assert(h1 != 0);

        delta = h0 / h1;
        gamma -= delta;
      } while (--count && (abs(gamma * 1e3) < abs(delta) || abs(h0) > 1e-5));
      assert(count > 0);
      assert(abs(h0) <= 1e-5);
      assert(!isnan(gamma));
      return gamma;
    } else {
      // compute gamma using fast approximation
      real dsdh = 0, dhdh = 0;
      LABICEX_UNROLL
      for (int i = 0; i < lattice::q; ++i) {
        dsdh += ds_a(i) * dh_a(i) / feq_a(i) + ds_b(i) * dh_b(i) / feq_b(i);
        dhdh += dh_a(i) * dh_a(i) / feq_a(i) + dh_b(i) * dh_b(i) / feq_b(i);
      }
      real gamma = 1 / beta;
      if (abs(dsdh) > 1e-12) gamma += (1 / beta - 2) * dsdh / dhdh;
      assert(!isnan(gamma));
      return gamma;
    }
  }

  Parameters parameters_;
  real m_a_, m_b_, t_phys_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_THERMAL_BINARY_MIXTURE_H

#ifndef LABICEX_COLLISION_LBGK_H
#define LABICEX_COLLISION_LBGK_H

#include "base/common.h"

namespace labicex {

namespace collision {

template <class Lattice>
class lbgk {
 public:
  typedef Lattice lattice;
  typedef typename lattice::fvector fvector;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::equilibrium equilibrium;

  lbgk(real mu) : mu_(mu) {}

  LABICEX_COMMON_INLINE void collide(fvector& f) const {
    const primitive_variables pv(f);
    const real nu = mu_ / pv.density();
    const real beta = 1 / (6 * nu + 1);
    f *= 1 - 2 * beta;
    f += 2 * beta * equilibrium::get(pv);
  }

 private:
  real mu_;
};

}  // collision

}  // labicex

#endif  // LABICEX_COLLISION_LBGK_H

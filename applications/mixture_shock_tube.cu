#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <boost/algorithm/string/case_conv.hpp>

#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "base/stream.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "boundary/dummy_updater.h"
#include "collision/thermal_binary_mixture_o.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/split.h"
#include "lattice/thermal.h"
#include "mpi/distributed_grid.h"
#include "mpi/setup.h"
#include "solver/binary_mixture.h"

using namespace labicex;

template <class Lattice, bool FullyThermal>
void run(int n, int t, real nu, real sc, real m_a, real m_b, real x,
         real temperature, real shock_ratio, int ofreq) {
  typedef typename vector_types<Lattice::d>::ivector ivector;
  typedef typename vector_types<Lattice::d>::rvector rvector;
  typedef typename vector_types<Lattice::d>::bvector bvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef forcing::dummy_updater<Lattice> forcing_type;
  typedef initial_condition::split<Lattice> ic_type;
  typedef boundary::dummy_updater<Lattice> boundary_updater;
  typedef collision::thermal_binary_mixture_o<
      Lattice, collision::binary_mixture_parameters1, FullyThermal>
      collision_type;

  typedef solver::binary_mixture<Lattice, collision_type, forcing_type,
                                 boundary_updater, boundary_updater>
      solver_type;
  typedef mpi::process_grid<Lattice::d> process_grid;
  typedef mpi::distributed_grid<Lattice> distributed_grid;

  ivector dims = ivector::Constant(1);
  dims(0) = mpi::setup::global_size();
  ivector local_extents = ivector::Constant(16);
  local_extents(0) = n;

  process_grid processes(dims, bvector::Constant(true));
  distributed_grid grid(processes, local_extents);

  boundary_updater boundary;
  solver_type s(grid, collision_type(nu, sc, m_a, m_b), forcing_type(),
                boundary, boundary);

  const ivector global_extents = grid.global().global_domain().extents();

  const primitive_variables pv_left_a(x * m_a * shock_ratio, rvector::Zero(),
                                      temperature / m_a);
  const primitive_variables pv_right_a(x * m_a, rvector::Zero(),
                                       temperature / m_a);
  const primitive_variables pv_left_b((1 - x) * m_b * shock_ratio,
                                      rvector::Zero(), temperature / m_b);
  const primitive_variables pv_right_b((1 - x) * m_b, rvector::Zero(),
                                       temperature / m_b);

  const ic_type ic_a(global_extents(ic_type::split_d) / 2, pv_left_a,
                     pv_right_a);
  const ic_type ic_b(global_extents(ic_type::split_d) / 2, pv_left_b,
                     pv_right_b);

  storage_generate(grid, s.pvs_a(), ic_a);
  storage_generate(grid, s.pvs_b(), ic_b);
  equilibrium_from_primitive_variables(grid, s.pvs_a(), s.f_a());
  equilibrium_from_primitive_variables(grid, s.pvs_b(), s.f_b());

  typename solver_type::xdmf_writer xdmf("mixture_shock_tube.xdmf", s.grid(),
                                         t);

  xdmf.write_information("m_a", m_a);
  xdmf.write_information("m_b", m_b);
  xdmf.write_information("temperature", temperature);
  xdmf.write_information("x", x);

  s.run_simulation(t, xdmf, ofreq);
}

int main(int argc, char* argv[]) {
  typedef void (*run_function)(int, int, real, real, real, real, real, real,
                               real, int);

  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q25");
  int resolution = args.value<int>("resolution", "local resolution", 500, 1);
  int time_steps = args.value<int>("time-steps", "time steps", 100, 1);
  int ofreq =
      args.value<int>("output-frequency", "XDMF output frequency", 10, 1);
  real viscosity =
      args.value<real>("viscosity", "kinematice viscosity", 0.1, 0);
  real sc = args.value<real>("schmidt-number", "schmidt number", 1, 0);
  real m_a = args.value<real>("mass-a", "molar mass of component a", 1, 0);
  real m_b = args.value<real>("mass-b", "molar mass of component b", 1, 0);
  real x = args.value<real>("x", "molar fraction of component a", 0.5, 0, 1);
  real temperature =
      args.value<real>("temperature", "physical temperature", 1.0 / 3.0, 0);
  real shock_ratio =
      args.value<real>("shock-ratio", "initial shock density ratio", 1.001, 0);
  bool const_temp = args.flag("constant-temperature",
                              "forces spatially constant mixture temperature");
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  std::string mode = const_temp ? "basic" : "fully";

  std::map<std::string, run_function> map;

  {
    using namespace lattice::thermal;
    map["d2q9 fully"] = run<d2q9, true>;
    map["d2q25 fully"] = run<d2q25, true>;

    map["d2q9 basic"] = run<d2q9, false>;
    map["d2q25 basic"] = run<d2q25, false>;
  }

  if (map.find(lattice + " " + mode) == map.end()) {
    std::cerr << "found no matching function for lattice '" + lattice << mode
              << " thermal model" << std::endl;
    return -1;
  }

  setup_device();

  map[lattice + " " + mode](resolution, time_steps, viscosity, sc, m_a, m_b, x,
                            temperature, shock_ratio, ofreq);

  return 0;
}

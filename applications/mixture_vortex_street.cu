#include <iostream>

#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "collision/thermal_binary_mixture.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/split.h"
#include "lattice/thermal/d2q9.h"
#include "scene/karman_vortex_street.h"
#include "solver/binary_mixture.h"

using namespace labicex;

typedef lattice::thermal::d2q9 lattice_type;
typedef typename vector_types<lattice_type::d>::ivector ivector;
typedef typename vector_types<lattice_type::d>::rvector rvector;
typedef typename vector_types<lattice_type::d>::bvector bvector;
typedef typename lattice_type::primitive_variables primitive_variables;
typedef initial_condition::split<lattice_type, 1> base_function;
typedef scene::karman_vortex_street<lattice_type, base_function> kvs_type;
typedef collision::thermal_binary_mixture<lattice_type> mixture_type;
typedef forcing::dummy_updater<lattice_type> forcing_type;
typedef typename kvs_type::initial_condition ic_type;
typedef typename kvs_type::boundary_condition bc_type;
typedef solver::binary_mixture<lattice_type, mixture_type, forcing_type,
                               bc_type, bc_type>
    solver_type;

void run(int d_approx, int t, real re, real sc, real u, real x, int ofreq) {
  mpi::process_grid<lattice_type::d> processes(
      mpi::setup::dims<lattice_type::d>(), bvector::Constant(false));
  mpi::distributed_grid<lattice_type> grid(
      processes, kvs_type::local_extents_from_d(processes, d_approx));

  const ivector global_extents = grid.local().global_domain().extents();
  const ivector local_extents = grid.local().full_domain().extents();

  const base_function bf_a(global_extents(1) / 2,
                           primitive_variables(1 - x, rvector(u, 0)),
                           primitive_variables(x, rvector(u, 0)));
  const base_function bf_b(global_extents(1) / 2,
                           primitive_variables(x, rvector(u, 0)),
                           primitive_variables(1 - x, rvector(u, 0)));

  const kvs_type kvs_a(grid, bf_a, u, 0.1, re);
  const kvs_type kvs_b(grid, bf_b, u, 0.1, re);

  const real viscosity = u * kvs_a.d() / re;

  std::cout << "simulation info:" << std::endl;
  std::cout << "  global extents: " << global_extents << std::endl;
  std::cout << "  local extents:  " << local_extents << std::endl;
  std::cout << "  diameter:       " << kvs_a.d() << std::endl;
  std::cout << "  viscosity:      " << viscosity << std::endl;
  std::cout << "  diffusion:      " << (viscosity / sc) << std::endl;
  std::cout << "  approx. beta:   " << (1 / (6 * viscosity + 1)) << std::endl;

  const ic_type ic_a = kvs_a.get_initial_condition();
  const ic_type ic_b = kvs_b.get_initial_condition();
  const bc_type bc_a = kvs_a.get_boundary_condition();
  const bc_type bc_b = kvs_b.get_boundary_condition();

  solver_type s(grid, mixture_type(viscosity, sc, 1, 1), forcing_type(), bc_a,
                bc_b);
  storage_generate(grid, s.pvs_a(), ic_a);
  storage_generate(grid, s.pvs_b(), ic_b);
  equilibrium_from_primitive_variables(grid, s.pvs_a(), s.f_a());
  equilibrium_from_primitive_variables(grid, s.pvs_b(), s.f_b());

  typename solver_type::xdmf_writer xdmf("mixture_vortex_street.xdmf", s.grid(),
                                         t);

  xdmf.write_information("diameter", kvs_a.d());
  xdmf.write_information("velocity", u);
  xdmf.write_information("reynolds_number", re);

  s.run_simulation(t, xdmf, ofreq);
}

int main(int argc, char* argv[]) {
  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  int diameter =
      args.value<int>("diameter", "approx. resolution of the circle", 20, 1);
  int time_steps =
      args.value<int>("time-steps", "number of time steps", 10000, 0);
  int ofreq =
      args.value<int>("output-frequency", "XDMF output frequency", 100, 1);
  real re = args.value<real>("reynolds-number", "reynolds number", 100, 0);
  real sc = args.value<real>("schmidt-number", "schmidt number", 0.1, 0);
  real velocity = args.value<real>("velocity", "inflow velocity", 5e-2, 0);
  real x = args.value<real>("x", "initial molar fraction", 0.6, 0, 1);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  setup_device();
  run(diameter, time_steps, re, sc, velocity, x, ofreq);

  return 0;
}

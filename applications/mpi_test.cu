#include <iomanip>
#include <iostream>

#include <thrust/fill.h>

#include "base/storage_traits.h"
#include "base/transfer.h"
#include "io/hdf5.h"
#include "io/xdmf.h"
#include "lattice/standard/d2q25.h"
#include "mpi/halo_exchanger.h"
#include "mpi/process_grid.h"
#include "mpi/setup.h"

using namespace labicex;

int main(int argc, char* argv[]) {
  typedef typename vector_types<2>::ivector ivector;
  typedef typename vector_types<2>::bvector bvector;

  mpi::setup mpi(argc, argv);
  const int size = mpi.global_size();

  const bvector periodic(true, true);

  mpi::process_grid<2> processes(mpi.dims<2>(), periodic);
  std::cout << "process grid size: " << processes.extents()(0) << "x"
            << processes.extents()(1) << std::endl;
  setup_device();

  mpi.set_cout_filter(false);
  for (int r = 0; r < size; ++r) {
    if (r == processes.local_process().rank()) {
      std::cout << "rank " << std::setw(2) << r << " neighbors:" << std::endl;
      for (int dy = 1; dy >= -1; --dy) {
        for (int dx = -1; dx <= 1; ++dx) {
          const mpi::neighbor_process<2>* nb =
              processes.neighbor_with_direction(
                  vector_types<2>::ivector(dx, dy));
          if (nb)
            std::cout << std::setw(5) << nb->rank();
          else
            std::cout << std::setw(5) << "-";
        }
        std::cout << std::endl;
      }
    }
    mpi.global_barrier();
  }
  mpi.set_cout_filter(true);

  const ivector local_extents(32, 16);
  typedef lattice::standard::d2q25 lattice;
  typedef device_storage_traits<lattice>::population_storage population;
  typedef device_storage_traits<lattice>::primitive_variables_storage
      primitive_variables;
  typedef host_storage_traits<lattice>::population_storage host_population;
  typedef host_storage_traits<lattice>::primitive_variables_storage
      host_primitive_variables;

  population f(local_extents);
  thrust::fill(f.data().begin(), f.data().end(),
               processes.local_process().rank());
  primitive_variables pvs(local_extents);
  thrust::fill(pvs.data().begin(), pvs.data().end(),
               processes.local_process().rank());

  mpi::distributed_grid<lattice> grid(processes, local_extents);
  mpi::halo_exchanger<lattice> halo(grid);

  halo.start_exchange(f, pvs);
  halo.finish_exchange(f, pvs);

  host_population fh(local_extents);
  storage_copy(f, fh);
  host_primitive_variables pvsh(local_extents);
  storage_copy(pvs, pvsh);

  /*mpi.set_cout_filter(false);
  host_population::const_accessor_type fa = fh.caccessor();
  for (int r = 0; r < size; ++r) {
    if (r == processes.local_process().rank()) {
      std::cout << "rank " << std::setw(2) << r << ":" << std::endl;
      for (int i = 0; i < population::lattice::q; ++i) {
        std::cout << "  f" << i << " (" << population::lattice::ci(i)
                  << "):" << std::endl;
        for (int y = local_extents(1) - 1; y >= 0; --y) {
          std::cout << "    ";
          for (int x = 0; x < local_extents(0); ++x) {
            std::cout << std::setw(5) << fa.load_single(i, ivector(x, y))
                      << " ";
          }
          std::cout << std::endl;
        }
      }
    }
    mpi.global_barrier();
  }
  mpi.set_cout_filter(true);*/

  {
    // write HDF5
    io::hdf5_ptr hf =
        io::hdf5_parallel_file::create("mpi_test.h5", processes.comm());

    hf->write_all_haloed("f", fh, grid.halo_width(),
                         grid.global().global_domain().extents(),
                         grid.global().nohalo_domain().min());
    hf->write_all_haloed("pvs", pvsh, grid.halo_width(),
                         grid.global().global_domain().extents(),
                         grid.global().nohalo_domain().min());
  }

  {
    std::stringstream h5name;
    h5name << "mpi_test_r" << processes.local_process().rank() << ".h5";
    io::hdf5_ptr hf = io::hdf5_file::create(h5name.str());

    hf->write_all("f", fh);
    hf->write_all("pvs", pvsh);
  }

  {
    // write XDMF
    io::xdmf_writer<mpi::distributed_grid<lattice> > xdmf("mpi_test.xdmf", grid,
                                                          0);
    xdmf.write_population(fh);
    xdmf.write_primitive(pvsh);
  }

  return 0;
}

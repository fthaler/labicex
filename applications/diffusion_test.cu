#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <boost/algorithm/string/case_conv.hpp>

#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "base/stream.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "boundary/dummy_updater.h"
#include "collision/thermal_binary_mixture.h"
#include "collision/thermal_binary_mixture_o.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/split.h"
#include "lattice/thermal.h"
#include "mpi/distributed_grid.h"
#include "mpi/setup.h"
#include "solver/binary_mixture.h"

using namespace labicex;

template <class Collision>
struct collision_creator {
  static Collision create(real nu, real sc, real m_a, real m_b) {
    return Collision(nu, sc, m_a, m_b);
  }
};

template <class Lattice, class Collision>
void run(int n, int t, real nu, real sc, real m_a, real m_b, real x,
         real temperature, int ofreq) {
  typedef typename vector_types<Lattice::d>::ivector ivector;
  typedef typename vector_types<Lattice::d>::rvector rvector;
  typedef typename vector_types<Lattice::d>::bvector bvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  // taylor green vortex analytical solution
  typedef initial_condition::split<Lattice> ic_type;
  typedef forcing::dummy_updater<Lattice> forcing_updater;
  typedef boundary::dummy_updater<Lattice> boundary_updater;
  // binary_mixture solver
  typedef solver::binary_mixture<Lattice, Collision, forcing_updater,
                                 boundary_updater, boundary_updater>
      solver_type;

  typedef mpi::process_grid<Lattice::d> process_grid;
  typedef mpi::distributed_grid<Lattice> distributed_grid;

  ivector dims = ivector::Constant(1);
  dims(0) = mpi::setup::global_size();
  ivector local_extents = ivector::Constant(16);
  local_extents(0) = n;

  process_grid processes(dims, bvector::Constant(true));
  distributed_grid grid(processes, local_extents);

  boundary_updater boundary;
  solver_type s(grid, collision_creator<Collision>::create(nu, sc, m_a, m_b),
                forcing_updater(), boundary, boundary);

  const ivector global_extents = grid.global().global_domain().extents();

  const primitive_variables pv_left_a(x * m_a, rvector::Zero(),
                                      temperature / m_a);
  const primitive_variables pv_right_a((1 - x) * m_a, rvector::Zero(),
                                       temperature / m_a);
  const primitive_variables pv_left_b((1 - x) * m_b, rvector::Zero(),
                                      temperature / m_b);
  const primitive_variables pv_right_b(x * m_b, rvector::Zero(),
                                       temperature / m_b);
  const ic_type ic_a(global_extents(ic_type::split_d) / 2, pv_left_a,
                     pv_right_a);
  const ic_type ic_b(global_extents(ic_type::split_d) / 2, pv_left_b,
                     pv_right_b);

  storage_generate(grid, s.pvs_a(), ic_a);
  storage_generate(grid, s.pvs_b(), ic_b);
  equilibrium_from_primitive_variables(grid, s.pvs_a(), s.f_a());
  equilibrium_from_primitive_variables(grid, s.pvs_b(), s.f_b());

  typename solver_type::xdmf_writer xdmf("diffusion_test.xdmf", s.grid(), t);

  xdmf.write_information("m_a", m_a);
  xdmf.write_information("m_b", m_b);
  xdmf.write_information("temperature", temperature);
  xdmf.write_information("x", x);
  xdmf.write_information("nu", nu);
  xdmf.write_information("sc", sc);

  s.run_simulation(t, xdmf, ofreq);
}

int main(int argc, char* argv[]) {
  typedef void (*run_function)(int, int, real, real, real, real, real, real,
                               int);

  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q25");
  std::string solver = args.value<std::string>(
      "solver", "solver type (unstabilized or kbc)", "unstabilized");
  int resolution = args.value<int>("resolution", "local resolution", 500, 1);
  int time_steps = args.value<int>("time-steps", "time steps", 1000, 1);
  int ofreq =
      args.value<int>("output-frequency", "XDMF output frequency", 100, 1);
  real viscosity = args.value<real>("viscosity", "kinematic viscosity", 0.1, 0);
  real sc = args.value<real>("schmidt-number", "schmidt number", 0.1, 0);
  real m_a = args.value<real>("mass-a", "mass of component a", 1, 0);
  real m_b = args.value<real>("mass-b", "mass of component b", 1, 0);
  real x = args.value<real>("x", "initial molar fraction", 0.6, 0, 1);
  real temperature =
      args.value<real>("temperature", "temperature", 1.0 / 3.0, 0);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  boost::algorithm::to_lower(solver);

  std::map<std::string, run_function> map;

  {
    using namespace lattice::thermal;
    map["d2q9 unstabilized"] =
        run<d2q9, collision::thermal_binary_mixture_o<d2q9> >;
    map["d2q9 kbc"] = run<d2q9, collision::thermal_binary_mixture<d2q9> >;
    map["d2q25 unstabilized"] =
        run<d2q25, collision::thermal_binary_mixture_o<d2q25> >;
    map["d2q25 kbc"] = run<d2q25, collision::thermal_binary_mixture<d2q25> >;
  }

  if (map.find(lattice + " " + solver) == map.end()) {
    std::cerr << "found no matching function for lattice '" << lattice
              << "' and solver '" << solver << "'" << std::endl;
    return -1;
  }

  setup_device();
  map[lattice + " " + solver](resolution, time_steps, viscosity, sc, m_a, m_b,
                              x, temperature, ofreq);

  return 0;
}

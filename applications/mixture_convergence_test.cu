#include <iomanip>
#include <iostream>
#include <map>
#include <string>

#include <boost/algorithm/string/case_conv.hpp>

#include "applications/error_helpers.h"
#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "base/storage_traits.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "boundary/dummy_updater.h"
#include "collision/thermal_binary_mixture.h"
#include "collision/thermal_binary_mixture_o.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/taylor_green_vortex.h"
#include "io/xdmf.h"
#include "lattice/standard.h"
#include "lattice/thermal.h"
#include "mpi/halo_exchanger.h"
#include "mpi/setup.h"
#include "solver/binary_mixture.h"

using namespace labicex;

template <class Lattice>
class mixture_taylor_green_vortex
    : public initial_condition::taylor_green_vortex<Lattice> {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef initial_condition::taylor_green_vortex<Lattice> base;

  mixture_taylor_green_vortex(const ivector& domain_size, real uref, real mu,
                              real cs, real x, real m, real temperature,
                              real t = 0)
      : base(domain_size, uref, mu, cs, t),
        x_(x),
        m_(m),
        temperature_(temperature) {}

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const ivector& x) const {
    primitive_variables pv = base::operator()(space, x);
    pv.density() *= x_;
    pv.temperature() = temperature_ / m_;
    return pv;
  }

 private:
  real x_, m_, temperature_;
};

template <class Lattice, class Collision>
void run(int n, int t, real re, real uref, real x, real sc, real m_a, real m_b,
         real temperature, real& l1error, real& l2error, real& linferror,
         real& time, real& mlups) {
  typedef typename vector_types<Lattice::d>::ivector ivector;
  typedef typename vector_types<Lattice::d>::rvector rvector;
  typedef typename vector_types<Lattice::d>::bvector bvector;
  // taylor green vortex analytical solution
  typedef mixture_taylor_green_vortex<Lattice> solution_type;
  typedef forcing::dummy_updater<Lattice> forcing_updater;
  typedef boundary::dummy_updater<Lattice> boundary_updater;
  // basic solver
  typedef solver::binary_mixture<Lattice, Collision, forcing_updater,
                                 boundary_updater, boundary_updater>
      solver_type;
  typedef mpi::process_grid<Lattice::d> process_grid;
  typedef mpi::distributed_grid<Lattice> distributed_grid;

  process_grid processes(mpi::setup::dims<Lattice::d>(),
                         bvector::Constant(true));
  const ivector local_extents = distributed_grid::local_extents_from_global(
      processes, ivector::Constant(n));
  distributed_grid grid(processes, local_extents);

  const real mu = (uref * grid.global().global_domain().extents()(0)) / re;
  // TODO: correct speed of sound
  const real cs = sqrt(2.0 * temperature / (m_a + m_b));

  solver_type s(grid, Collision(mu, sc, m_a, m_b), forcing_updater(),
                boundary_updater(), boundary_updater());
  const ivector global_extents = grid.global().global_domain().extents();

  const solution_type ic_a(global_extents, uref, mu, cs, x, m_a, temperature);
  const solution_type ic_b(global_extents, uref, mu, cs, 1 - x, m_b,
                           temperature);
  storage_generate(grid, s.pvs_a(), ic_a);
  storage_generate(grid, s.pvs_b(), ic_b);
  equilibrium_from_primitive_variables(grid, s.pvs_a(), s.f_a());
  equilibrium_from_primitive_variables(grid, s.pvs_b(), s.f_b());

  mlups_timer timer(grid, t - 1);
  timer.start();
  for (int ti = 0; ti < t - 1; ++ti) s.step(false);
  timer.stop();

  s.step(true);

  typename solver_type::primitive_variables_storage pvs(s.pvs_a().extents());
  const solution_type sol(global_extents, uref, mu, cs, x, t, m_a, temperature);
  storage_generate(grid, pvs, sol);

  s.compute_stream().synchronize();
  compute_velocity_errors(grid, s.pvs_a(), pvs, uref, l1error, l2error,
                          linferror);

  time = timer.elapsed_seconds();
  mlups = timer.mlups();
}

template <class Lattice, class Collision>
void check_convergence(int n_min, int n_max, int t_min, real re, real uref,
                       real x, real sc, real m_a, real m_b, real temperature) {
  const int w = 12;
  std::cout << std::setw(w) << "n" << std::setw(w) << "t" << std::setw(w)
            << "L1" << std::setw(w) << "L2" << std::setw(w) << "Linf"
            << std::setw(w) << "L1 exp." << std::setw(w) << "L2 exp."
            << std::setw(w) << "Linf exp." << std::setw(w) << "seconds"
            << std::setw(w) << "MLUPS" << std::endl;

#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
  real last_l1, last_l2, last_linf;
  for (int n = n_min, t = t_min; n <= n_max; n *= 2, t *= 2) {
    real l1, l2, linf, time, mlups;
    run<Lattice, Collision>(n, t, re, uref, x, sc, m_a, m_b, temperature, l1,
                            l2, linf, time, mlups);

    std::cout << std::setw(w) << n << std::setw(w) << t << std::setw(w) << l1
              << std::setw(w) << l2 << std::setw(w) << linf;

    if (n > n_min) {
      real dl1 = log(l1 / last_l1) / log(2);
      real dl2 = log(l2 / last_l2) / log(2);
      real dlinf = log(linf / last_linf) / log(2);
      std::cout << std::setw(w) << dl1 << std::setw(w) << dl2 << std::setw(w)
                << dlinf;
    } else {
      std::cout << std::setw(w) << "-" << std::setw(w) << "-" << std::setw(w)
                << "-";
    }

    std::cout << std::setw(w) << time << std::setw(w) << mlups << std::endl;

    last_l1 = l1;
    last_l2 = l2;
    last_linf = linf;
  }
}

int main(int argc, char* argv[]) {
  typedef void (*check_function)(int, int, int, real, real, real, real, real,
                                 real, real);

  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q25t");
  std::string solver =
      args.value<std::string>("solver", "solver type", "unstabilized");
  int min_resolution =
      args.value<int>("min-resolution", "minimum resolution", 8, 1);
  int max_resolution =
      args.value<int>("max-resolution", "maximum resolution", 256, 1);
  int time_steps =
      args.value<int>("time-steps", "time steps for min. resolution", 4, 1);
  real re = args.value<real>("reynolds-number", "reynolds number", 1000, 0);
  real velocity = args.value<real>("velocity", "reference velocity", 5e-5, 0);
  real x = args.value<real>("x", "molar fraction", 0.5, 0, 1);
  real sc = args.value<real>("schmidt-number", "schmidt number", 1, 0);
  real m_a = args.value<real>("mass-a", "mass of species a", 1, 0);
  real m_b = args.value<real>("mass-b", "mass of species b", 1, 0);
  real temperature =
      args.value<real>("temperature", "physical temperature", 0.4, 0);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  boost::algorithm::to_lower(solver);

  std::map<std::string, check_function> map;

  // thermal lattices
  {
    using namespace lattice::thermal;
    using namespace collision;

    map["d2q9t unstabilized"] =
        check_convergence<d2q9, thermal_binary_mixture_o<d2q9> >;
    map["d2q25t unstabilized"] =
        check_convergence<d2q25, thermal_binary_mixture_o<d2q25> >;

    map["d2q9t kbc"] = check_convergence<d2q9, thermal_binary_mixture<d2q9> >;
    map["d2q25t kbc"] =
        check_convergence<d2q25, thermal_binary_mixture<d2q25> >;
    /* map["d3q125t kbc"] =
        check_convergence<d3q125, thermal_binary_mixture<d3q125> >; */
  }

  if (map.find(lattice + " " + solver) == map.end()) {
    std::cerr << "found no matching function for lattice '" << lattice
              << "' and solver '" << solver << "'" << std::endl;
    return -1;
  }

  setup_device();
  map[lattice + " " + solver](min_resolution, max_resolution, time_steps, re,
                              velocity, x, sc, m_a, m_b, temperature);

  return 0;
}

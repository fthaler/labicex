#include <iostream>

#include "base/arguments.h"
#include "base/common.h"
#include "base/storage_helpers.h"
#include "collision/kbc.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/uniform.h"
#include "lattice/standard/d2q9.h"
#include "scene/karman_vortex_street.h"
#include "solver/basic.h"

using namespace labicex;

typedef lattice::standard::d2q9 lattice_type;
typedef typename vector_types<lattice_type::d>::ivector ivector;
typedef typename vector_types<lattice_type::d>::rvector rvector;
typedef typename vector_types<lattice_type::d>::bvector bvector;
typedef typename lattice_type::primitive_variables primitive_variables;
typedef initial_condition::uniform<lattice_type> base_function;
typedef scene::karman_vortex_street<lattice_type, base_function> kvs_type;
typedef collision::kbc<lattice_type> collision_type;
typedef forcing::dummy_updater<lattice_type> forcing_type;
typedef typename kvs_type::initial_condition ic_type;
typedef typename kvs_type::boundary_condition bc_type;
typedef solver::basic<lattice_type, collision_type, forcing_type, bc_type>
    solver_type;

void run(int d_approx, int t, real re, real u, int ofreq) {
  bvector periodic = bvector::Constant(false);
  if (lattice_type::d > 2) periodic(2) = true;
  mpi::process_grid<lattice_type::d> processes(
      mpi::setup::dims<lattice_type::d>(2), periodic);
  mpi::distributed_grid<lattice_type> grid(
      processes, kvs_type::local_extents_from_d(processes, d_approx));

  const ivector global_extents = grid.local().global_domain().extents();
  const ivector local_extents = grid.local().full_domain().extents();

  rvector velocity = rvector::Zero();
  velocity(0) = u;
  kvs_type kvs(grid, base_function(primitive_variables(1, velocity)), u, 0.1,
               re);

  const real viscosity = u * kvs.d() / re;

  std::cout << "simulation info:" << std::endl;
  std::cout << "  global extents: " << global_extents << std::endl;
  std::cout << "  local extents:  " << local_extents << std::endl;
  std::cout << "  diameter:       " << kvs.d() << std::endl;
  std::cout << "  viscosity:      " << viscosity << std::endl;
  std::cout << "  approx. beta:   " << (1 / (6 * viscosity + 1)) << std::endl;

  const ic_type ic = kvs.get_initial_condition();
  const bc_type bc = kvs.get_boundary_condition();

  solver_type s(grid, collision_type(viscosity), forcing_type(), bc);
  storage_generate(grid, s.pvs(), ic);
  equilibrium_from_primitive_variables(grid, s.pvs(), s.f());

  typename solver_type::xdmf_writer xdmf("karman_vortex_street.xdmf", s.grid(),
                                         t);

  xdmf.write_information("diameter", kvs.d());
  xdmf.write_information("velocity", u);
  xdmf.write_information("reynolds_number", re);

  s.run_simulation(t, xdmf, ofreq);
}

int main(int argc, char* argv[]) {
  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  int diameter =
      args.value<int>("diameter", "approx. resolution of the circle", 20, 1);
  int time_steps =
      args.value<int>("time-steps", "number of time steps", 10000, 0);
  int ofreq =
      args.value<int>("output-frequency", "XDMF output frequency", 100, 1);
  real re = args.value<real>("reynolds-number", "reynolds number", 100, 0);
  real velocity = args.value<real>("velocity", "inflow velocity", 5e-2, 0);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  setup_device();
  run(diameter, time_steps, re, velocity, ofreq);

  return 0;
}

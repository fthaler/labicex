#ifndef LABICEX_APPLICATIONS_ERROR_HELPERS_H
#define LABICEX_APPLICATIONS_ERROR_HELPERS_H

#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/tuple.h>

#include "base/device_helpers.h"
#include "base/domain.h"
#include "mpi/setup.h"

namespace labicex {

namespace kernel {

template <class PrimitiveVariables, class Domain,
          class PrimitveVariablesAccessor>
LABICEX_KERNEL void velocity_errors(const Domain domain,
                                    PrimitveVariablesAccessor a,
                                    PrimitveVariablesAccessor b, real uref,
                                    real* ds) {
  typedef typename vector_types<Domain::d>::ivector ivector;
  typedef typename vector_types<Domain::d>::rvector rvector;

  const ivector block_x = thread_index<Domain::d>();
  const ivector x = block_x + domain.min();

  if (!domain.inside(x)) return;

  PrimitiveVariables pv_a, pv_b;
  a.load(x, pv_a);
  b.load(x, pv_b);

  ivector strides;
  strides(0) = 1;
  for (int i = 1; i < Domain::d; ++i)
    strides(i) = strides(i - 1) * domain.extents()(i - 1);

  const rvector dv = (pv_a.velocity() - pv_b.velocity()) / uref;
  const real d = sqrt((dv * dv).sum());

  const int idx = (strides * block_x).sum();
  assert(idx < domain.extents().prod());
  ds[idx] = d;
}

}  // kernel

template <class DistributedGrid, class PrimitveVariablesStorage>
void compute_velocity_errors(const DistributedGrid& grid,
                             PrimitveVariablesStorage& a,
                             PrimitveVariablesStorage& b, real uref,
                             real& l1error, real& l2error, real& linferror) {
  assert((a.extents() == b.extents()).all());
  domain::box<DistributedGrid::d> nohalo_domain = grid.local().nohalo_domain();
  thrust::device_vector<real> d(nohalo_domain.extents().prod());

  dim3 grid_size, block_size;
  get_default_launch_configuration(nohalo_domain.extents(), grid_size,
                                   block_size);
  kernel::velocity_errors<typename DistributedGrid::lattice::
                              primitive_variables><<<grid_size, block_size>>>(
      nohalo_domain, a.caccessor(), b.caccessor(), uref,
      thrust::raw_pointer_cast(d.data()));
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());

  const real l1_local = thrust::reduce(d.begin(), d.end());
  const real l2_local =
      thrust::inner_product(d.begin(), d.end(), d.begin(), real(0));
  const real linf_local =
      thrust::reduce(d.begin(), d.end(), real(0), thrust::maximum<real>());

  MPI_Allreduce(&l1_local, &l1error, 1, mpi::mpi_type<real>::type(), MPI_SUM,
                grid.processes().comm());
  MPI_Allreduce(&l2_local, &l2error, 1, mpi::mpi_type<real>::type(), MPI_SUM,
                grid.processes().comm());
  MPI_Allreduce(&linf_local, &linferror, 1, mpi::mpi_type<real>::type(),
                MPI_MAX, grid.processes().comm());

  l1error = l1error / grid.global().global_domain().extents().prod();
  l2error = sqrt(l2error / grid.global().global_domain().extents().prod());
}

}  // labicex

#endif  // LABICEX_APPLICATIONS_ERROR_HELPERS_H

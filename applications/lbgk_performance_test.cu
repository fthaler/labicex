#include <iomanip>
#include <iostream>
#include <string>

#include <boost/algorithm/string/case_conv.hpp>

#include "advector/onload.h"
#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "base/storage_traits.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "collision/kbc.h"
#include "collision/lbgk.h"
#include "initial_condition/taylor_green_vortex.h"
#include "lattice/standard.h"
#include "lattice/thermal.h"
#include "solver/serial_basic.h"

using namespace labicex;

template <class Lattice, class Collision, int n>
void run(int t, real re, real uref) {
  typedef typename Lattice::ivector ivector_type;
  // fixed-size extents
  typedef typename static_ivector_maker<Lattice::d>::template constant<n>::type
      extents_type;
  // periodic indexing
  typedef index_mapping::fixed_size_periodic<Lattice, extents_type>
      mapping_type;
  // some advector
  typedef advector::onload<Lattice, mapping_type> advector_type;
  // taylor green vortex analytical solution
  typedef initial_condition::taylor_green_vortex<Lattice> solution_type;
  // storage types
  typedef typename device_storage_traits<Lattice>::template fixed_size<
      extents_type>::population_storage f_storage;
  typedef typename device_storage_traits<Lattice>::template fixed_size<
      extents_type>::primitive_variables_storage pv_storage;
  typedef typename host_storage_traits<Lattice>::template fixed_size<
      extents_type>::population_storage host_f_storage;
  typedef typename host_storage_traits<Lattice>::template fixed_size<
      extents_type>::primitive_variables_storage host_pv_storage;
  // basic solver
  typedef solver::serial_basic<Lattice, Collision, advector_type, f_storage,
                               pv_storage>
      solver_type;

  const ivector_type extents = ivector_cast(extents_type());
  const real mu = (uref * n) / re;
  const real cs = sqrt(1.0 / 3.0);

  solver_type s(extents, Collision(mu), advector_type(mapping_type()));

  const solution_type ic(extents, uref, mu, cs);
  storage_generate(s.pvs(), ic);
  equilibrium_from_primitive_variables<Lattice>(s.pvs(), s.f());

  {
    mlups_auto_timer timer("computation", extents.prod(), t);
    for (int ti = 0; ti < t; ++ti) s.step(false);
    s.compute_stream().synchronize();
  }
}

int main(int argc, char* argv[]) {
  typedef void (*run_function)(int, real, real);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q9");
  std::string solver = args.value<std::string>("solver", "solver type", "lbgk");
  int resolution = args.value<int>("resolution", "grid resolution", 128, 1);
  int time_steps = args.value<int>("time-steps", "number of time steps", 32, 1);
  real re = args.value<real>("reynolds-number", "reynolds number", 1000, 0);
  real velocity = args.value<real>("velocity", "reference velocity", 5e-5, 0);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  boost::algorithm::to_lower(solver);

  std::map<std::string, run_function> map;

  // standard lattices
  {
    using namespace lattice::standard;
    using namespace collision;

    map["d2q9 lbgk 2048"] = run<d2q9, lbgk<d2q9>, 2048>;
    map["d2q25 lbgk 2048"] = run<d2q25, lbgk<d2q25>, 2048>;
    map["d3q27 lbgk 128"] = run<d3q27, lbgk<d3q27>, 128>;

    map["d2q9 kbc 2048"] = run<d2q9, kbc<d2q9>, 2048>;
    map["d2q25 kbc 2048"] = run<d2q25, kbc<d2q25>, 2048>;
    map["d3q27 kbc 128"] = run<d3q27, kbc<d3q27>, 128>;
  }

  // thermal lattices
  {
    using namespace lattice::thermal;
    using namespace collision;

    map["d2q9t lbgk 2048"] = run<d2q9, lbgk<d2q9>, 2048>;
    map["d2q25t lbgk 2048"] = run<d2q25, lbgk<d2q25>, 2048>;
    // map["d3q125t lbgk 128"] = run<d3q125, lbgk<d3q125>, 128>;

    map["d2q9t kbc 2048"] = run<d2q9, kbc<d2q9>, 2048>;
    map["d2q25t kbc 2048"] = run<d2q25, kbc<d2q25>, 2048>;
    // map["d3q125t kbc 128"] = run<d3q125, kbc<d3q125>, 128>;
  }

  std::stringstream ln;
  ln << lattice << " " << solver << " " << resolution;

  if (map.find(ln.str()) == map.end()) {
    std::cerr << "found no matching function for lattice '" << lattice
              << "' and resolution '" << resolution << "'" << std::endl;
    return -1;
  }

  setup_device();
  map[ln.str()](time_steps, re, velocity);

  return 0;
}

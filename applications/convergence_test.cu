#include <iomanip>
#include <iostream>
#include <map>
#include <string>

#include <boost/algorithm/string/case_conv.hpp>

#include "applications/error_helpers.h"
#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "base/storage_traits.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "boundary/dummy_updater.h"
#include "collision/elbm.h"
#include "collision/kbc.h"
#include "collision/lbgk.h"
#include "forcing/dummy_updater.h"
#include "initial_condition/taylor_green_vortex.h"
#include "io/xdmf.h"
#include "lattice/standard.h"
#include "lattice/thermal.h"
#include "mpi/halo_exchanger.h"
#include "mpi/setup.h"
#include "solver/basic.h"

using namespace labicex;

template <class Lattice, class Collision>
void run(int n, int t, real re, real uref, real& l1error, real& l2error,
         real& linferror, real& time, real& mlups) {
  typedef typename vector_types<Lattice::d>::ivector ivector;
  typedef typename vector_types<Lattice::d>::rvector rvector;
  typedef typename vector_types<Lattice::d>::bvector bvector;
  // taylor green vortex analytical solution
  typedef initial_condition::taylor_green_vortex<Lattice> solution_type;
  typedef forcing::dummy_updater<Lattice> forcing_updater;
  typedef boundary::dummy_updater<Lattice> boundary_updater;
  // basic solver
  typedef solver::basic<Lattice, Collision, forcing_updater, boundary_updater>
      solver_type;
  typedef mpi::process_grid<Lattice::d> process_grid;
  typedef mpi::distributed_grid<Lattice> distributed_grid;

  process_grid processes(mpi::setup::dims<Lattice::d>(),
                         bvector::Constant(true));
  const ivector local_extents = distributed_grid::local_extents_from_global(
      processes, ivector::Constant(n));
  distributed_grid grid(processes, local_extents);

  const real mu = (uref * grid.global().global_domain().extents()(0)) / re;
  // TODO: correct speed of sound
  const real cs = sqrt(1.0 / 3.0);

  solver_type s(grid, Collision(mu), forcing_updater(), boundary_updater());
  const ivector global_extents = grid.global().global_domain().extents();

  const solution_type ic(global_extents, uref, mu, cs);
  storage_generate(grid, s.pvs(), ic);
  equilibrium_from_primitive_variables(grid, s.pvs(), s.f());

  mlups_timer timer(grid, t - 1);
  timer.start();
  for (int ti = 0; ti < t - 1; ++ti) s.step(false);
  timer.stop();

  s.step(true);

  typename solver_type::primitive_variables_storage pvs(s.pvs().extents());
  const solution_type sol(global_extents, uref, mu, cs, t);
  storage_generate(grid, pvs, sol);

  s.compute_stream().synchronize();
  compute_velocity_errors(grid, s.pvs(), pvs, uref, l1error, l2error,
                          linferror);

  time = timer.elapsed_seconds();
  mlups = timer.mlups();
}

template <class Lattice, class Collision>
void check_convergence(int n_min, int n_max, int t_min, real re, real uref) {
  const int w = 12;
  std::cout << std::setw(w) << "n" << std::setw(w) << "t" << std::setw(w)
            << "L1" << std::setw(w) << "L2" << std::setw(w) << "Linf"
            << std::setw(w) << "L1 exp." << std::setw(w) << "L2 exp."
            << std::setw(w) << "Linf exp." << std::setw(w) << "seconds"
            << std::setw(w) << "MLUPS" << std::endl;

#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
  real last_l1, last_l2, last_linf;
  for (int n = n_min, t = t_min; n <= n_max; n *= 2, t *= 2) {
    real l1, l2, linf, time, mlups;
    run<Lattice, Collision>(n, t, re, uref, l1, l2, linf, time, mlups);

    std::cout << std::setw(w) << n << std::setw(w) << t << std::setw(w) << l1
              << std::setw(w) << l2 << std::setw(w) << linf;

    if (n > n_min) {
      real dl1 = log(l1 / last_l1) / log(2);
      real dl2 = log(l2 / last_l2) / log(2);
      real dlinf = log(linf / last_linf) / log(2);
      std::cout << std::setw(w) << dl1 << std::setw(w) << dl2 << std::setw(w)
                << dlinf;
    } else {
      std::cout << std::setw(w) << "-" << std::setw(w) << "-" << std::setw(w)
                << "-";
    }

    std::cout << std::setw(w) << time << std::setw(w) << mlups << std::endl;

    last_l1 = l1;
    last_l2 = l2;
    last_linf = linf;
  }
}

int main(int argc, char* argv[]) {
  typedef void (*check_function)(int, int, int, real, real);

  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q9");
  std::string solver = args.value<std::string>("solver", "solver type", "lbgk");
  int min_resolution =
      args.value<int>("min-resolution", "minimum resolution", 8, 1);
  int max_resolution =
      args.value<int>("max-resolution", "maximum resolution", 256, 1);
  int time_steps =
      args.value<int>("time-steps", "time steps for min. resolution", 4, 1);
  real re = args.value<real>("reynolds-number", "reynolds number", 1000, 0);
  real velocity = args.value<real>("velocity", "reference velocity", 5e-5, 0);
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  boost::algorithm::to_lower(solver);

  std::map<std::string, check_function> map;

  // standard lattices
  {
    using namespace lattice::standard;
    using namespace collision;

    map["d2q9 lbgk"] = check_convergence<d2q9, lbgk<d2q9> >;
    map["d2q25 lbgk"] = check_convergence<d2q25, lbgk<d2q25> >;
    map["d3q27 lbgk"] = check_convergence<d3q27, lbgk<d3q27> >;

    /* map["d2q9 elbm"] = check_convergence<d2q9, elbm<d2q9> >;
    map["d2q25 elbm"] = check_convergence<d2q25, elbm<d2q25> >;
    map["d3q27 elbm"] = check_convergence<d3q27, elbm<d3q27> >; */

    map["d2q9 kbc"] = check_convergence<d2q9, kbc<d2q9> >;
    map["d2q25 kbc"] = check_convergence<d2q25, kbc<d2q25> >;
    map["d3q27 kbc"] = check_convergence<d3q27, kbc<d3q27> >;
  }

  // thermal lattices
  {
    using namespace lattice::thermal;
    using namespace collision;

    map["d2q9t lbgk"] = check_convergence<d2q9, lbgk<d2q9> >;
    map["d2q25t lbgk"] = check_convergence<d2q25, lbgk<d2q25> >;
    map["d3q125t lbgk"] = check_convergence<d3q125, lbgk<d3q125> >;

    /* map["d2q9t elbm"] = check_convergence<d2q9, elbm<d2q9> >;
    map["d2q25t elbm"] = check_convergence<d2q25, elbm<d2q25> >;
    map["d3q125t elbm"] = check_convergence<d3q125, elbm<d3q125> >; */

    map["d2q9t kbc"] = check_convergence<d2q9, kbc<d2q9> >;
    map["d2q25t kbc"] = check_convergence<d2q25, kbc<d2q25> >;
    // map["d3q125t kbc"] = check_convergence<d3q125, kbc<d3q125> >;
  }

  if (map.find(lattice + " " + solver) == map.end()) {
    std::cerr << "found no matching function for lattice '" << lattice
              << "' and solver '" << solver << "'" << std::endl;
    return -1;
  }

  setup_device();
  map[lattice + " " + solver](min_resolution, max_resolution, time_steps, re,
                              velocity);

  return 0;
}

#include <iostream>
#include <map>
#include <string>

#include <boost/algorithm/string/case_conv.hpp>

#include "base/arguments.h"
#include "base/storage_helpers.h"
#include "collision/thermal_binary_mixture_o.h"
#include "lattice/thermal/d2q25.h"
#include "lattice/thermal/d2q9.h"
#include "scene/rayleigh_taylor_instability.h"
#include "solver/binary_mixture.h"

using namespace labicex;

typedef typename vector_types<2>::ivector ivector;
typedef typename vector_types<2>::rvector rvector;
typedef typename vector_types<2>::bvector bvector;

template <class Lattice, bool FullyThermal>
void run(int resolution, real aspect_ratio, int t, int ofreq, real nu, real sc,
         real x, real m_a, real m_b, real t_phys, real density_factor,
         real gravity, real split_position, real smoothing, real sine_magnitude,
         real sine_scale) {
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef scene::rayleigh_taylor_lst_perturbation<Lattice> perturbation_type;
  typedef collision::thermal_binary_mixture_o<
      Lattice, collision::binary_mixture_parameters1, FullyThermal>
      mixture_type;
  typedef scene::rayleigh_taylor_instability<Lattice, perturbation_type,
                                             FullyThermal>
      rti_type;
  typedef typename rti_type::initial_condition ic_type;
  typedef typename rti_type::boundary_condition bc_type;
  typedef typename rti_type::forcing forcing_type;
  typedef solver::binary_mixture<Lattice, mixture_type, forcing_type, bc_type,
                                 bc_type>
      solver_type;

  bvector periodic = bvector::Constant(true);
  periodic(1) = false;
  mpi::process_grid<2> processes(mpi::setup::dims<2>(), periodic);
  ivector approx_global_extents = ivector::Constant(resolution);
  approx_global_extents(1) = static_cast<int>(resolution * aspect_ratio);
  const ivector local_extents =
      mpi::distributed_grid<Lattice>::local_extents_from_global(
          processes, approx_global_extents, true);
  const mpi::distributed_grid<Lattice> grid(processes, local_extents);
  const ivector global_extents = grid.local().global_domain().extents();
  split_position *= global_extents(1);
  sine_magnitude *= global_extents(0);
  sine_scale /= global_extents(0);
  smoothing *= global_extents(0);

  const real re = pow(global_extents(0) / (2 * M_PI), 4.0 / 3.0);
  const real pe = re * sc;
  const real rho_l = (x * m_a + (1 - x) * m_b) * density_factor;
  const real rho_h = ((1 - x) * m_a + x * m_b) * density_factor;
  const real atwood = (rho_h - rho_l) / (rho_h + rho_l);
  const real re2 = sqrt(global_extents(0) * gravity) * global_extents(0) / nu;
  std::cout << "simulation info:" << std::endl;
  std::cout << "  global_extents: " << global_extents << std::endl;
  std::cout << "  local_extents: " << local_extents << std::endl;
  std::cout << "  Re: " << re << std::endl;
  std::cout << "  Re2: " << re2 << std::endl;
  std::cout << "  A: " << atwood << std::endl;
  std::cout << "  sine magnitude: " << sine_magnitude << std::endl;
  std::cout << "  split position: " << split_position << std::endl;

  const real k = 2 * M_PI * sine_scale;
  // Duff et al. 1962
  const real n = sqrt(atwood * gravity * k + nu * nu * k * k * k * k) -
                 (nu + nu / sc) * k * k;
  std::cout << "  initial n: " << n << std::endl;
  const perturbation_type perturbation(sine_magnitude, n, k);
  const rti_type rti(grid, perturbation, split_position, pe, m_a, m_b,
                     density_factor, t_phys, x, smoothing, gravity);

  const ic_type ic_a = rti.get_initial_condition_a();
  const ic_type ic_b = rti.get_initial_condition_b();
  const bc_type bc_a = rti.get_boundary_condition_a();
  const bc_type bc_b = rti.get_boundary_condition_b();
  const forcing_type forcing = rti.get_forcing();

  solver_type s(grid, mixture_type(nu, sc, m_a, m_b, t_phys), forcing, bc_a,
                bc_b);

  storage_generate(grid, s.pvs_a(), ic_a);
  storage_generate(grid, s.pvs_b(), ic_b);
  equilibrium_from_primitive_variables(grid, s.pvs_a(), s.f_a());
  equilibrium_from_primitive_variables(grid, s.pvs_b(), s.f_b());

  typename solver_type::xdmf_writer xdmf("rayleigh_taylor_instability.xdmf",
                                         s.grid(), t);
  xdmf.write_information("m_a", m_a);
  xdmf.write_information("m_b", m_b);
  xdmf.write_information("atwood_number", atwood);
  xdmf.write_information("gravity", gravity);
  xdmf.write_information("split_position", split_position);
  xdmf.write_information("kinematic_viscosity", nu);
  xdmf.write_information("schmidt_number", sc);
  xdmf.write_information("initial_magnitude", sine_magnitude * k);
  xdmf.write_information("smoothing", smoothing);
  xdmf.write_information("x", x);

  s.run_simulation(t, xdmf, ofreq);
}

int main(int argc, char *argv[]) {
  typedef void (*run_function)(int, real, int, int, real, real, real, real,
                               real, real, real, real, real, real, real, real);

  mpi::setup mpi(argc, argv);

  arguments args(argc, argv);
  std::string lattice =
      args.value<std::string>("lattice", "lattice type", "d2q25");
  int resolution =
      args.value<int>("resolution", "approx. total grid size", 100, 16);
  real aspect_ratio =
      args.value<real>("aspect-ratio", "approx. grid aspect ratio", 4, 0);
  int time_steps =
      args.value<int>("time-steps", "number of time steps", 1000, 0);
  int ofreq =
      args.value<int>("output-frequency", "XDMF output frequency", 100, 1);
  real viscosity =
      args.value<real>("viscosity", "kinematic viscosity", 1e-1, 0);
  real sc = args.value<real>("schmidt-number", "schmidt number", 1, 0);
  real x = args.value<real>("x", "initial molar fraction", 0.6, 0, 1);
  real m_a = args.value<real>("mass-a", "molecular mass of component a", 1, 0);
  real m_b = args.value<real>("mass-b", "molecular mass of component b", 2, 0);
  real temperature =
      args.value<real>("temperature", "physical temperature", 0.7, 0);
  real density_factor =
      args.value<real>("density-factor", "density scaling factor", 1, 0);
  real gravity =
      args.value<real>("gravity", "gravitational acceleration", 1e-4, 0);
  real split_position =
      args.value<real>("split-position", "relative split position", 0.5, 0, 1);
  real x_smoothing =
      args.value<real>("x-smoothing", "smoothing for initial x", 0.05, 0, 1);
  real sine_magnitude =
      args.value<real>("sine-magnitude", "relative sine magnitude", 0.05, 0);
  real sine_scale = args.value<real>("sine-scale", "relative sine scale", 1, 0);
  bool const_temp = args.flag("constant-temperature",
                              "forces spatially constant mixture temperature");
  if (args.flag("help")) {
    std::cout << args.help_text();
    return 0;
  }
  std::cout << args;

  boost::algorithm::to_lower(lattice);
  std::string mode = const_temp ? "basic" : "fully";

  std::map<std::string, run_function> map;

  {
    using namespace lattice::thermal;

    map["d2q9 fully"] = run<d2q9, true>;
    map["d2q25 fully"] = run<d2q25, true>;

    map["d2q9 basic"] = run<d2q9, false>;
    map["d2q25 basic"] = run<d2q25, false>;
  }

  if (map.find(lattice + " " + mode) == map.end()) {
    std::cerr << "found no matching function for lattice '" + lattice << mode
              << " thermal model" << std::endl;
    return -1;
  }

  setup_device();

  map[lattice + " " + mode](resolution, aspect_ratio, time_steps, ofreq,
                            viscosity, sc, x, m_a, m_b, temperature,
                            density_factor, gravity, split_position,
                            x_smoothing, sine_magnitude, sine_scale);

  return 0;
}

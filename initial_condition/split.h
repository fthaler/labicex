#ifndef LABICEX_INITIAL_CONDITION_SPLIT_H
#define LABICEX_INITIAL_CONDITION_SPLIT_H

#include "base/common.h"
#include "base/kernel_space.h"

namespace labicex {

namespace initial_condition {

template <class Lattice, int SplitD = 0>
class split {
 public:
  static const int split_d = SplitD;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::primitive_variables primitive_variables;

  split(int split_coordinate, const primitive_variables& left,
        const primitive_variables& right)
      : split_coordinate_(split_coordinate), left_(left), right_(right) {}

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const ivector& xl) const {
    const ivector x = space.local_to_global(xl);
    if (x(SplitD) < split_coordinate_) return left_;
    return right_;
  }

 private:
  int split_coordinate_;
  primitive_variables left_, right_;
};

}  // initial_condition

}  // labicex

#endif  // LABICEX_INITIAL_CONDITION_SIMPLE_DIFFUSION_H

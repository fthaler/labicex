#ifndef LABICEX_INITIAL_CONDITION_UNIFORM_H
#define LABICEX_INITIAL_CONDITION_UNIFORM_H

#include "base/common.h"
#include "base/kernel_space.h"

namespace labicex {

namespace initial_condition {

template <class Lattice>
class uniform {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::primitive_variables primitive_variables;

  uniform(const primitive_variables& pv) : pv_(pv) {}

  LABICEX_COMMON_INLINE const primitive_variables& operator()(
      const kernel_space<Lattice::d>&, const ivector&) const {
    return pv_;
  }

 private:
  primitive_variables pv_;
};

}  // initial_condition

}  // labicex

#endif  // LABICEX_INITIAL_CONDITION_UNIFORM_H

#ifndef LABICEX_INITIAL_CONDITION_APPLY_H
#define LABICEX_INITIAL_CONDITION_APPLY_H

#include "base/device_helpers.h"

namespace labicex {

namespace initial_condition {

namespace kernel {

template <class Lattice, class PopulationStorageAccessor,
          class InitialCondition, class Rvector>
LABICEX_KERNEL void apply(PopulationStorageAccessor population,
                          InitialCondition ic, const Rvector offset) {
  typedef typename Lattice::equilibrium equilibrium;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  const ivector x = thread_index<Lattice::d>();

  if (outside(x, population.extents())) return;

  real density;
  rvector velocity;
  ic.get(x.template cast<real>() + offset, density, velocity);

  fvector feq = equilibrium::get(primitive_variables(density, velocity));

  population.store(x, feq);
}

template <class Lattice, class PopulationStorageAccessor,
          class InitialCondition, class Rvector>
LABICEX_KERNEL void apply_thermal(PopulationStorageAccessor population,
                                  InitialCondition ic, const Rvector offset) {
  typedef typename Lattice::equilibrium equilibrium;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  const ivector x = thread_index<Lattice::d>();

  if (outside(x, population.extents())) return;

  real density;
  rvector velocity;
  real temperature;
  ic.get(x.template cast<real>() + offset, density, velocity, temperature);

  fvector feq =
      equilibrium::get(primitive_variables(density, velocity, temperature));

  population.store(x, feq);
}

template <class Lattice, class PrimitiveVariablesAccessor,
          class InitialCondition, class Rvector>
LABICEX_KERNEL void get(PrimitiveVariablesAccessor pva, InitialCondition ic,
                        const Rvector offset) {
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  const ivector x = thread_index<Lattice::d>();

  if (outside(x, pva.extents())) return;

  real density;
  rvector velocity;
  ic.get(x.template cast<real>() + offset, density, velocity);

  pva.store(x, primitive_variables(density, velocity));
}

}  // kernel

template <class Lattice, class PopulationStorage, class InitialCondition,
          class Rvector>
void apply(PopulationStorage& f, const InitialCondition& ic,
           const Rvector& offset) {
  dim3 grid_size, block_size;
  get_launch_configuration(f.extents(), 32, grid_size, block_size);
  kernel::apply<Lattice><<<grid_size, block_size>>>(f.accessor(), ic, offset);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());
}

template <class Lattice, class PopulationStorage, class InitialCondition>
void apply(PopulationStorage& f, const InitialCondition& ic) {
  apply<Lattice>(f, ic, InitialCondition::rvector::Zero());
}

template <class Lattice, class PopulationStorage, class InitialCondition,
          class Rvector>
void apply_thermal(PopulationStorage& f, const InitialCondition& ic,
                   const Rvector& offset) {
  dim3 grid_size, block_size;
  get_launch_configuration(f.extents(), 32, grid_size, block_size);
  kernel::apply_thermal<Lattice><<<grid_size, block_size>>>(f.accessor(), ic,
                                                              offset);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());
}

template <class Lattice, class PopulationStorage, class InitialCondition>
void apply_thermal(PopulationStorage& f, const InitialCondition& ic) {
  apply_thermal<Lattice>(f, ic, InitialCondition::rvector::Zero());
}

template <class Lattice, class PrimitiveVariablesStorage,
          class InitialCondition, class Rvector>
void get(PrimitiveVariablesStorage& pvs, const InitialCondition& ic,
         const Rvector& offset) {
  dim3 grid_size, block_size;
  get_launch_configuration(pvs.extents(), 32, grid_size, block_size);
  kernel::get<Lattice><<<grid_size, block_size>>>(pvs.accessor(), ic, offset);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());
}

template <class Lattice, class PrimitiveVariablesStorage,
          class InitialCondition>
void get(PrimitiveVariablesStorage& pvs, const InitialCondition& ic) {
  get<Lattice>(pvs, ic, InitialCondition::rvector::Zero());
}

}  // initial_condition

}  // labicex

#endif  // LABICEX_INITIAL_CONDITION_APPLY_H

#ifndef LABICEX_INITIAL_CONDITION_TAYLOR_GREEN_VORTEX_H
#define LABICEX_INITIAL_CONDITION_TAYLOR_GREEN_VORTEX_H

#include "base/common.h"
#include "base/kernel_space.h"

namespace labicex {

namespace initial_condition {

template <class Lattice>
class taylor_green_vortex {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  taylor_green_vortex(const ivector& domain_size, real uref, real mu, real cs,
                      real t = 0)
      : domain_size_(domain_size.template cast<real>()),
        uref_(uref),
        mu_(mu),
        cs_(cs),
        t_(t) {
    assert(uref_ > 0);
    assert(mu_ > 0);
    assert(cs_ > 0);
    assert(t_ >= 0);
  }

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const ivector& xl) const {
    const ivector x = space.local_to_global(xl);

    const rvector k = rvector::Constant(2 * M_PI) / domain_size_;
    const rvector k2 = k * k;
    const real knorm = sqrt(k2.sum());
    const real ma = uref_ / cs_;

    const real density =
        1 -
        (ma * ma) / (2 * knorm * knorm) *
            (k2(1) * cos(2 * k(0) * x(0)) + k2(0) * cos(2 * k(1) * x(1)));
    const real nu = mu_ / density;
    rvector velocity = rvector::Zero();
    velocity(0) = -uref_ * k(1) / knorm * exp(-nu * knorm * knorm * t_) *
                  sin(k(1) * x(1)) * cos(k(0) * x(0));
    velocity(1) = uref_ * k(0) / knorm * exp(-nu * knorm * knorm * t_) *
                  sin(k(0) * x(0)) * cos(k(1) * x(1));

    return primitive_variables(density, velocity);
  }

 private:
  rvector domain_size_;
  real uref_, mu_, cs_, t_;
};

}  // initial_condition

}  // labicex

#endif  // LABICEX_INITIAL_CONDITION_TAYLOR_GREEN_VORTEX_H

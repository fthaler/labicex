#!/usr/bin/env python3

import numpy as np
from scipy.signal import periodogram
from scipy.interpolate import interp1d
from matplotlib import pyplot as plt
import sys
import xdmf


if __name__ == '__main__':
    res = []

    def f(t, rho, ux, uy, *args):
        try:
            rho_a, rho_b = rho
            uy_a, uy_b = uy
            uy = (rho_a * uy_a + rho_b * uy_b) / (rho_a + rho_b)
        except ValueError:
            pass
        ny, nx = uy.shape
        x = nx // 4
        y = ny // 2
        res.append((t, uy[y, x]))
    filename = sys.argv[1]
    info = xdmf.get_info(filename)
    tmin, tmax = xdmf.get_time_range(filename)
    xdmf.apply_all(filename, f, tmin=(tmin + tmax) // 2)

    ts, us = np.transpose(res)

    plt.figure()
    plt.plot(ts, us)
    dt = np.diff(ts)
    assert np.all(dt[0] == dt)
    dt = dt[0]

    #plt.figure()
    #fs, pxx = periodogram(us, 1.0 / dt, scaling='spectrum', nfft=2**16)
    #plt.plot(fs, pxx)

    i0, = np.nonzero(np.logical_xor(us[:-1] < 0, us[1:] < 0))
    ts0 = ts[i0] - dt * us[i0] / (us[i0 + 1] - us[i0])

    fs = 1.0 / (2 * np.diff(ts0))
    diameter = float(info['diameter'])
    velocity = float(info['velocity'])
    re = float(info['reynolds_number'])
    st = fs * diameter / velocity
    print('Shedding frequency: {} +- {}'.format(np.mean(fs), np.std(fs)))
    print('Diameter: {}'.format(diameter))
    print('Velocity: {}'.format(velocity))
    print('Strouhal number: {} +- {}'.format(np.mean(st), np.std(st)))
    if 47 <= re <= 190:
        print('Emprirical (Norberg): {}'.format(0.2663 - 1.019 / np.sqrt(re)))

    plt.show()

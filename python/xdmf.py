import h5py
import numpy as np
import os
import xml.etree.ElementTree as ET


def get_info(filename):
    tree = ET.parse(filename)
    return {i.get('Name'): i.get('Value') for i in tree.findall('Information')}


def get_time_range(filename):
    tree = ET.parse(filename)
    tmin, tmax = None, None
    for grid in tree.find('Domain/Grid').findall('Grid'):
        t = int(grid.find('Time').get('Value'))
        if tmin is None or t < tmin:
            tmin = t
        if tmax is None or t > tmax:
            tmax = t
    return tmin, tmax


def get_data(dirname, grid):
    def get_attribute_data(name):
        data_item = grid.find('Attribute[@Name="' + name + '"]/DataItem')
        if data_item is None:
            raise KeyError('Attribute not found')
        f_path, h5_path = data_item.text.strip().split(':')
        with h5py.File(os.path.join(dirname, f_path), 'r') as f:
            h5data = f[h5_path]
            data = np.empty(h5data.shape, dtype='float32')
            data[:] = h5data[:]
        # f = h5py.File(os.path.join(dirname, f_path), 'r')
        # data = f[h5_path]
        return data

    # try reading single component data
    try:
        rho = get_attribute_data('rho')
        ux = get_attribute_data('ux')
        uy = get_attribute_data('uy')
        try:
            t = get_attribute_data('T')
            return rho, ux, uy, t
        except KeyError:
            pass
        return rho, ux, uy
    except KeyError:
        pass

    # try reading binary mixture data
    try:
        rho_a = get_attribute_data('rho_a')
        ux_a = get_attribute_data('ux_a')
        uy_a = get_attribute_data('uy_a')
        rho_b = get_attribute_data('rho_b')
        ux_b = get_attribute_data('ux_b')
        uy_b = get_attribute_data('uy_b')
        try:
            t_a = get_attribute_data('T_a')
            t_b = get_attribute_data('T_b')
            return (rho_a, rho_b), (ux_a, ux_b), (uy_a, uy_b), (t_a, t_b)
        except KeyError:
            pass
        return (rho_a, rho_b), (ux_a, ux_b), (uy_a, uy_b)
    except KeyError:
        pass


def apply_single(filename, function, t):
    dirname = os.path.dirname(filename)
    tree = ET.parse(filename)
    grid = tree.find('Domain/Grid').find('Grid/Time[@Value="{}"]/..'.format(t))
    data = get_data(dirname, grid)
    return function(t, *data)


def apply_all(filename, function, tmin=None, tmax=None):
    dirname = os.path.dirname(filename)
    tree = ET.parse(filename)
    res = []
    for grid in tree.find('Domain/Grid').findall('Grid'):
        t = int(grid.find('Time').get('Value'))
        if tmin is not None and t < tmin:
            continue
        if tmax is not None and t > tmax:
            break
        data = get_data(dirname, grid)
        if not data:
            print('Warning: no data found at t = {}'.format(t))
        else:
            r = function(t, *data)
            if r is not None:
                res.append(r)
    return res

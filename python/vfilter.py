#!/usr/bin/env python3

import subprocess
import re
import sys


def demangle(s):
    if s.startswith('-'):
        return s
    if s.startswith("'") and s.endswith("'"):
        s = s[1:-1]
    if s.startswith('_'):
        while s[-1:].isnumeric():
            s = s[:-1]
        try:
            return subprocess.check_output(['c++filt', s]).decode('ascii')[:-1]
        except subprocess.CalledProcessError:
            return s
    else:
        return s


def remove_parentheses(s, o='(', c=')', partial=False, maxlen=5):
    n = 0
    start = 0
    remove = []
    for i, si in enumerate(s):
        if si == o:
            if n == 0:
                start = i + 1
            n += 1
        if si == c:
            n -= 1
            if n == 0:
                remove.append((start, i))
    re_lattice = re.compile('(standard|thermal)::d\d+q\d+')
    re_collision = re.compile('collision::([^:<, ]+)')
    while remove:
        start, end = remove.pop()
        if end - start > maxlen:
            placeholder = '...'
            if partial:
                m = re_lattice.search(s[start:end])
                if m:
                    placeholder = m.group(0) + ', ' + placeholder
                m = re_collision.search(s[start:end])
                if m:
                    placeholder = m.group(1) + ', ' + placeholder
            s = s[:start] + placeholder + s[end:]
    return s


def ptxvfilter(f):
    ignore = False
    for line in f:
        if not line:
            print()
            continue
        if line.startswith('ptxas info    : '):
            line = line[16:]
        if line.startswith('Compiling'):
            if line.find('thrust') != -1:
                ignore = True
            else:
                ignore = False
                print()
        if ignore:
            continue
        split_line = line.split()
        print(' '.join(remove_parentheses(remove_parentheses(demangle(s)),
                                          '<', '>', True) for s in split_line))


if __name__ == '__main__':
    ptxvfilter(sys.stdin)

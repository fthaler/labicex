from math import erf, sqrt


# Visit python expression for diffusion reference solution computation
# diffusion coefficient and position have to be set manually...
class ExactDiffusion(SimplePythonExpression):
    def __init__(self):
        """
        Constructor.
        """
        SimplePythonExpression.__init__(self)
        # set to provide a name & description for your expression.
        self.name = "ExactDiffusion"
        self.description = "Custom Python Expression"
        # output_is_point_var:
        # true  if output centering is nodal
        # false if output centering is zonal
        self.output_is_point_var = True
        # output_dimension:
        # set to indicate number of components in the output tuple.
        self.output_dimension = 1

    def derive_variable(self, ds_in,domain_id):
        """
        Called to process each chunk.

        Use self.input_var_names & self.arguments to access expression
        variable names and const arguments.

        Return a new vtkDataArray with expression result values.
        """
        npoints = ds_in.GetNumberOfPoints()
        res = vtk.vtkFloatArray()
        res.SetNumberOfComponents(1)
        res.SetNumberOfTuples(npoints)
        ts = ds_in.GetPointData().GetArray(self.input_var_names[0])
        for i in xrange(npoints):
            x = ds_in.GetPoint(i)[1] - 116
            t = ts.GetTuple(i)[0]
            y = 0.5 - 0.1 * erf(x / sqrt(4 * 0.01 * t + 1e-12))
            res.SetTuple(i, (y,))
        return res

    def modify_contract(self, contract):
        """
        Implement this method to modify the pipeline contract.
        """
        pass


py_filter = ExactDiffusion

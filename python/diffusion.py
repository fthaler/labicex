#!/usr/bin/env python3

import numpy as np
from scipy.special import erf
from matplotlib import pyplot as plt
import subprocess
import xdmf


def exact_solution(n, x, nu, sc, t):
    c = np.mgrid[:n] - n / 2
    return 0.5 - (x - 0.5) * erf(c / (np.sqrt(4 * nu / sc * t + 1e-12)))


def run(**kwargs):
    args = ['../bin/diffusion_test']
    for k, v in kwargs.items():
        args += ['--' + k.replace('_', '-'), str(v)]
    subprocess.check_call(args)
    
    fn = 'diffusion_test.xdmf'
    info = xdmf.get_info(fn)
    m_a = float(info['m_a'])
    m_b = float(info['m_b'])
    nu = float(info['nu'])
    sc = float(info['sc'])
    x = float(info['x'])
    def f(t, rho, ux, uy, temp, *args):
        rho_a, rho_b = rho
        temp_a, temp_b = temp
        ny, nx = rho_a.shape
        rho_a = np.ravel(rho_a[ny // 2, nx // 4:nx * 3 // 4])
        rho_b = np.ravel(rho_b[ny // 2, nx // 4:nx * 3 // 4])
        temp_a = np.ravel(temp_a[ny // 2, nx // 4:nx * 3 // 4])
        temp_b = np.ravel(temp_b[ny // 2, nx // 4:nx * 3 // 4])
        x_a = rho_a / m_a / (rho_a / m_a + rho_b / m_b)
        x_a_exact = exact_solution(x_a.size, x, nu, sc, t)
        c = np.mgrid[:x_a.size]
        plt.plot(c, x_a)
        plt.plot(c, x_a_exact, ls='--', c='k')
        plt.ylim([0, 1])
    _, tmax = xdmf.get_time_range(fn)
    xdmf.apply_all(fn, f, tmin=tmax)


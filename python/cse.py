#!/usr/bin/env python3

from sympy import cse, symbols, ccode, Symbol

rho, ux, uy, uz, t, g = symbols('rho, ux, uy, uz, t, g')


def optimize(feq, lattice, dimension, axpy=False, double=True):
    feq = [f.factor() for f in feq]

    # get common subexpressions
    cses, csefeqs = cse(feq, optimizations='basic')
    print('Number of common subexpressions: {}'.format(len(cses)))

    # get all symbols in cses
    csesyms = [sym for sym, _ in cses]

    # get dependencies of equilibrium computations
    deps = [f.atoms(Symbol) for f in csefeqs]

    # get position of symbol with highest index
    pos = [max(csesyms.index(d) for d in ds if d in csesyms) for ds in deps]

    # inline ptx assembly for compile-time instruction reordering barrier
    # to reduce register spills
    # asm_rw = ('"+d"' if double else '"+f"') + '(f({}))'
    # asm_barrier = 'asm volatile ("": {});'.format(', '.join(asm_rw.format(i)
    #                                                         for i in
    #                                                         range(len(feq))))
    asm_barrier = 'asm_barrier(f);'

    if axpy:
        feq_code = ('const real feq{i} = {f};\n'
                    'f({i}) += alpha * feq{i};\n'
                    'feq_density += feq{i};\n'
                    'feq_momentum += feq{i} * {l}::cf({i});\n'
                    .format(l=lattice, i='{0}', f='{1}'))
        for a in range(dimension):
            for b in range(a, dimension):
                feq_code += ('feq_pressure({a}, {b}) += feq{i} * '
                             '{l}::cf({i})({a}) * '
                             '{l}::cf({i})({b});\n'
                             .format(a=a, b=b, l=lattice, i='{0}'))
    else:
        feq_code = ('f({0}) = {1};\n')

    i = 0
    for j, (var, val) in enumerate(cses):
        val = ccode(val.evalf(20)).replace('pow', 'ipow')
        print('const real {} = {};'.format(var, val))
        while i < len(pos) and pos[i] <= j:
            f = ccode(csefeqs[i].evalf(20)).replace('pow', 'ipow')
            print(asm_barrier)
            print(feq_code.format(i, f), end='')
            i += 1
    print(asm_barrier)
    if axpy:
        for a in range(dimension):
            for b in range(a):
                print('feq_pressure({a}, {b}) = feq_pressure({b}, {a});\n'
                      .format(a=a, b=b), end='')

feq = [0] * 25
feq[0] = (rho*(9 - 10*t + 3*t**2)**2*(2 - (ux**2 + uy**2)/t))/162
feq[1] = -(rho*(-27 + 39*t - 19*t**2 + 3*t**3)*(6*t**3 - ux**3 + 3*t*ux*(ux + ux**2 + uy**2) - 3*t**2*(2*ux + ux**2 + uy**2)))/(288*t**2)
feq[2] = -(rho*(-27 + 39*t - 19*t**2 + 3*t**3)*(6*t**3 + ux**3 - 3*t**2*(-2*ux + ux**2 + uy**2) - 3*t*ux*(-ux + ux**2 + uy**2)))/(288*t**2)
feq[3] = -(rho*(-27 + 39*t - 19*t**2 + 3*t**3)*(6*t**3 - uy**3 + 3*t*uy*(ux**2 + uy + uy**2) - 3*t**2*(ux**2 + uy*(2 + uy))))/(288*t**2)
feq[4] = -(rho*(-27 + 39*t - 19*t**2 + 3*t**3)*(6*t**3 + uy**3 - 3*t**2*(ux**2 + (-2 + uy)*uy) - 3*t*uy*(ux**2 + (-1 + uy)*uy)))/(288*t**2)
feq[5] = (3*rho*(-3 + t)**2*(6*t**3 - (ux + uy)**3 - 3*t**2*(2*ux + ux**2 + uy*(2 + uy)) + 3*t*(ux**3 + ux**2*(1 + uy) + uy**2*(1 + uy) + ux*uy*(2 + uy))))/(512*t)
feq[6] = (3*rho*(-3 + t)**2*(6*t**3 - (ux - uy)**3 - 3*t**2*(2*ux + ux**2 + (-2 + uy)*uy) + 3*t*(ux**3 - ux**2*(-1 + uy) + ux*(-2 + uy)*uy - (-1 + uy)*uy**2)))/(512*t)
feq[7] = (3*rho*(-3 + t)**2*(6*t**3 + (ux - uy)**3 - 3*t**2*(-2*ux + ux**2 + uy*(2 + uy)) - 3*t*(ux**3 - ux**2*(1 + uy) - uy**2*(1 + uy) + ux*uy*(2 + uy))))/(512*t)
feq[8] = (3*rho*(-3 + t)**2*(6*t**3 + (ux + uy)**3 - 3*t**2*(-2*ux + ux**2 + (-2 + uy)*uy) - 3*t*(ux**3 + ux**2*(-1 + uy) + ux*(-2 + uy)*uy + (-1 + uy)*uy**2)))/(512*t)
feq[9] = (rho*(-9 + 37*t - 33*t**2 + 9*t**3)*(2*t**3 - 9*ux**3 + 3*t*ux*(3*ux + ux**2 + uy**2) - t**2*(6*ux + ux**2 + uy**2)))/(2592*t**2)
feq[10] = (rho*(-9 + 37*t - 33*t**2 + 9*t**3)*(2*t**3 + 9*ux**3 - t**2*(-6*ux + ux**2 + uy**2) - 3*t*ux*(-3*ux + ux**2 + uy**2)))/(2592*t**2)
feq[11] = (rho*(-9 + 37*t - 33*t**2 + 9*t**3)*(2*t**3 - 9*uy**3 + 3*t*uy*(ux**2 + uy*(3 + uy)) - t**2*(ux**2 + uy*(6 + uy))))/(2592*t**2)
feq[12] = (rho*(-9 + 37*t - 33*t**2 + 9*t**3)*(2*t**3 + 9*uy**3 - t**2*(ux**2 + (-6 + uy)*uy) - 3*t*uy*(ux**2 + (-3 + uy)*uy)))/(2592*t**2)
feq[13] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 - (3*ux + uy)**3 - 3*t**2*(6*ux + ux**2 + uy*(2 + uy)) + 3*t*(3*ux**3 + uy**2*(1 + uy) + 3*ux*uy*(2 + uy) + ux**2*(9 + uy))))/(4608*t)
feq[14] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 - (3*ux - uy)**3 - 3*t**2*(6*ux + ux**2 + (-2 + uy)*uy) + 3*t*(3*ux**3 - ux**2*(-9 + uy) + 3*ux*(-2 + uy)*uy - (-1 + uy)*uy**2)))/(4608*t)
feq[15] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 + (3*ux - uy)**3 - 3*t**2*(-6*ux + ux**2 + uy*(2 + uy)) + 3*t*(-3*ux**3 + uy**2*(1 + uy) - 3*ux*uy*(2 + uy) + ux**2*(9 + uy))))/(4608*t)
feq[16] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 + (3*ux + uy)**3 - 3*t**2*(-6*ux + ux**2 + (-2 + uy)*uy) - 3*t*(3*ux**3 + ux**2*(-9 + uy) + 3*ux*(-2 + uy)*uy + (-1 + uy)*uy**2)))/(4608*t)
feq[17] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 - (ux + 3*uy)**3 - 3*t**2*(2*ux + ux**2 + uy*(6 + uy)) + 3*t*(ux**3 + 3*uy**2*(3 + uy) + ux*uy*(6 + uy) + ux**2*(1 + 3*uy))))/(4608*t)
feq[18] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 - (ux - 3*uy)**3 - 3*t**2*(2*ux + ux**2 + (-6 + uy)*uy) + 3*t*(ux**3 + ux**2*(1 - 3*uy) + ux*(-6 + uy)*uy - 3*(-3 + uy)*uy**2)))/(4608*t)
feq[19] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 + (ux - 3*uy)**3 - 3*t**2*(-2*ux + ux**2 + uy*(6 + uy)) - 3*t*(ux**3 - 3*uy**2*(3 + uy) + ux*uy*(6 + uy) - ux**2*(1 + 3*uy))))/(4608*t)
feq[20] = -(rho*(3 - 10*t + 3*t**2)*(6*t**3 + (ux + 3*uy)**3 - 3*t**2*(-2*ux + ux**2 + (-6 + uy)*uy) - 3*t*(ux**3 + ux*(-6 + uy)*uy + 3*(-3 + uy)*uy**2 + ux**2*(-1 + 3*uy))))/(4608*t)
feq[21] = (rho*(1 - 3*t)**2*(2*t**3 - 9*(ux + uy)**3 - t**2*(6*ux + ux**2 + uy*(6 + uy)) + 3*t*(ux**3 + ux**2*(3 + uy) + uy**2*(3 + uy) + ux*uy*(6 + uy))))/(41472*t)
feq[22] = (rho*(1 - 3*t)**2*(2*t**3 - 9*(ux - uy)**3 - t**2*(6*ux + ux**2 + (-6 + uy)*uy) + 3*t*(ux**3 - ux**2*(-3 + uy) + ux*(-6 + uy)*uy - (-3 + uy)*uy**2)))/(41472*t)
feq[23] = (rho*(1 - 3*t)**2*(2*t**3 + 9*(ux - uy)**3 - t**2*(-6*ux + ux**2 + uy*(6 + uy)) - 3*t*(ux**3 - ux**2*(3 + uy) - uy**2*(3 + uy) + ux*uy*(6 + uy))))/(41472*t)
feq[24] = (rho*(1 - 3*t)**2*(2*t**3 + 9*(ux + uy)**3 - t**2*(-6*ux + ux**2 + (-6 + uy)*uy) - 3*t*(ux**3 + ux**2*(-3 + uy) + ux*(-6 + uy)*uy + (-3 + uy)*uy**2)))/(41472*t)

optimize(feq, 'd2q25', 2, False)

feq_diff = [(f.subs(uy, uy - g) - f).factor() for f in feq]
#optimize(feq_diff, 'd2q25', 2, False)

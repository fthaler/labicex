import numpy as np
import itertools


def uniqueformat(r, as_float):
    values = np.sort(np.unique(r))
    last = -1
    for v in values:
        indices = np.arange(len(r))[r == v]
        assert indices[0] == last + 1
        last = indices[-1]

        if last == 0:
            print('if (i == 0) ', end='')
        elif last < len(r) - 1:
            print('if (i <= {}) '.format(last), end='')
        if as_float:
            print('return {:.20f};'.format(v))
        else:
            print('return {};'.format(v))
    print()


def simpleuniqueformat(r, as_float=False):
    start = 0
    while start < len(r):
        end = start
        while end < len(r) and r[end] == r[start]:
            end += 1
        if end == len(r):
            if as_float:
                print('return {:.20f};'.format(float(r[start])))
            else:
                print('return {};'.format(r[start]))
        else:
            if as_float:
                print('if (j < {}) return {:.20f};'.format(end,
                                                           float(r[start])))
            else:
                print('if (j < {}) return {};'.format(end, r[start]))
        start = end


def cformat(*cs, floatize=False):
    for d, c in enumerate(cs):
        values = np.unique(c)
        for v in values:
            indices = np.arange(len(c))[c == v]
            print('if (' + ' || '.join('i == {}'.format(idx)
                                       for idx in indices) + ')')
            print('  ci({}) = {};'.format(d, float(v) if floatize else v))
        print()


def wformat(w):
    uniqueformat(w, True)


def bbformat(*cs, axis=None):
    c = list(zip(*cs))

    def bb(ci):
        if axis is not None:
            cibb = list(ci)
            cibb[axis] = -cibb[axis]
            return tuple(cibb)
        else:
            return tuple(-cij for cij in ci)

    bounce_back = [c.index(bb(ci)) for ci in c]
    for i, v in enumerate(bounce_back[:-1]):
        print('if (i == {}) return {};'.format(i, v))
    print('return {};'.format(bounce_back[-1]))
    print()


def normformat(*cs):
    c = list(zip(*cs))
    norm22 = [np.sum(np.array(ci)**2) for ci in c]
    norm2 = [np.sqrt(np.sum(np.array(ci)**2)) for ci in c]
    normmax = [np.amax(np.abs(ci)) for ci in c]
    uniqueformat(norm22, False)
    uniqueformat(norm22, True)
    uniqueformat(norm2, True)
    uniqueformat(normmax, False)
    uniqueformat(normmax, True)


def matrixformat(m):
    for i, mi in enumerate(m):
        if i < len(m) - 1:
            print('if (i == {})'.format(i) + ' {')
        else:
            print('{')
        start = 0
        while start < len(mi):
            end = start
            while end < len(mi) and mi[end] == mi[start]:
                end += 1
            print('  ', end='')
            if end == len(mi):
                print('return {:.20f};'.format(float(mi[start])))
            else:
                print('if (j < {}) return {:.20f};'.format(end,
                                                           float(mi[start])))
            start = end
        print('}')


def unitformat(*cs):
    c = list(zip(*cs))
    for j in range(len(cs)):
        u = [0] * len(cs)
        u[j] = 1
        if j < len(cs) - 1:
            print('if (j == {}) '.format(j), end='')
        print('return {};'.format(c.index(tuple(u))))
    print()


def moment(cs, es):
    d = len(es)
    es = np.asanyarray(es)
    if np.all(es == 0):
        name = 'rho'
    elif np.all(es == np.array([1, 0, 0][:d])):
        name = 'jx'
    elif np.all(es == np.array([0, 1, 0][:d])):
        name = 'jy'
    elif np.all(es == np.array([0, 0, 1][:d])):
        name = 'jz'
    elif np.all(es == np.array([2, 0, 0][:d])):
        name = 'Pxx'
    elif np.all(es == np.array([0, 2, 0][:d])):
        name = 'Pyy'
    elif np.all(es == np.array([0, 0, 2][:d])):
        name = 'Pzz'
    elif np.all(es == np.array([1, 1, 0][:d])):
        name = 'Pxy'
    elif np.all(es == np.array([1, 0, 1][:d])):
        name = 'Pxz'
    elif np.all(es == np.array([0, 1, 1][:d])):
        name = 'Pyz'
    else:
        name = 'H' + str(sum(es))
        for coord, e in zip(['x', 'y', 'z'], es):
            name += coord * e
    return name, np.prod(np.asanyarray(cs)**np.array([es]).T, axis=0)


def momentmix(cs, es, component):
    name, m = moment(cs, es)
    if component == 'ab':
        return name + '_ab', np.hstack([m, m])
    elif component == 'a':
        return name + '_a', np.hstack([m, np.zeros_like(m)])
    elif component == 'b':
        return name + '_b', np.hstack([np.zeros_like(m), m])


def naturalmomentgen(*cs):
    def mom(cs, es, end='\n'):
        name, m = moment(cs, es)
        print(name, end=end)
        return m

    def momsum(cs, ess, end='\n'):
        names = []
        msum = np.zeros(len(cs[0]), dtype=int)
        for es in ess:
            n, m = moment(cs, es)
            names.append(n)
            msum += m
        print(' + '.join(names), end=end)
        return msum

    def momdiff(cs, ess, end='\n'):
        names = []
        msum = np.zeros(len(cs[0]), dtype=int)
        for i, es in enumerate(ess):
            n, m = moment(cs, es)
            names.append(n)
            msum += m if i == 0 else -m
        print(' - '.join(names), end=end)
        return msum

    def single(i):
        s = np.zeros(d, dtype=int)
        s[i] = 1
        return s

    d = len(cs)
    # rho
    A = mom(cs, np.zeros(d, dtype=int))
    # j
    for i in range(d):
        A = np.vstack([A, mom(cs, single(i))])
    # tr(P)
    A = np.vstack([A, momsum(cs, [2 * single(i) for i in range(d)])])
    assert np.linalg.matrix_rank(A) == A.shape[0]
    # rest automatically
    exponent = 1
    while A.shape[0] < A.shape[1]:
        prod = itertools.product(range(exponent + 1), repeat=d)
        exps = [e for e in prod if sum(e) == exponent]
        exps = sorted(exps, key=lambda e: len(np.unique(e)) * (exponent + 1) +
                      np.amin(e))
        for e in exps:
            Ap = np.vstack([A, mom(cs, e, end='')])
            if np.linalg.matrix_rank(Ap) == Ap.shape[0]:
                A = Ap
                print(' ' * 20)
            else:
                print('\r', end='')
        exponent += 1
    print('\r' + ' ' * 20)
    return A


def naturalmomentgenmix(*cs):
    def mom(cs, es, component, end='\n'):
        name, m = momentmix(cs, es, component)
        print(name, end=end)
        return m

    def momsum(cs, ess, component, end='\n'):
        names = []
        msum = np.zeros(2 * len(cs[0]), dtype=int)
        for es in ess:
            n, m = momentmix(cs, es, component)
            names.append(n)
            msum += m
        print(' + '.join(names), end=end)
        return msum

    def momdiff(cs, ess, component, end='\n'):
        names = []
        msum = np.zeros(2 * len(cs[0]), dtype=int)
        for i, es in enumerate(ess):
            n, m = momentmix(cs, es, component)
            names.append(n)
            msum += m if i == 0 else -m
        print(' - '.join(names), end=end)
        return msum

    def single(i):
        s = np.zeros(d, dtype=int)
        s[i] = 1
        return s

    d = len(cs)
    # rho_a, rho_b
    A = np.vstack([mom(cs, np.zeros(d, dtype=int), 'a'),
                   mom(cs, np.zeros(d, dtype=int), 'b')])
    # j_ab
    for i in range(d):
        A = np.vstack([A, mom(cs, single(i), 'ab')])
    # tr(P_ab)
    A = np.vstack([A, momsum(cs, [2 * single(i) for i in range(d)], 'ab')])
    # j_a
    for i in range(d):
        A = np.vstack([A, mom(cs, single(i), 'a')])
    # tr(P_a)
    A = np.vstack([A, momsum(cs, [2 * single(i) for i in range(d)], 'a')])
    assert np.linalg.matrix_rank(A) == A.shape[0]
    # rest automatically
    exponent = 1
    while A.shape[0] < A.shape[1]:
        prod = itertools.product(range(exponent + 1), repeat=d)
        exps = [e for e in prod if sum(e) == exponent]
        exps = sorted(exps, key=lambda e: len(np.unique(e)) * (exponent + 1) +
                      np.amin(e))
        for e in exps:
            Ap = np.vstack([A, mom(cs, e, 'ab', end='')])
            if np.linalg.matrix_rank(Ap) == Ap.shape[0]:
                A = Ap
                print(' ' * 20)
            else:
                print('\r', end='')
            Ap = np.vstack([A, mom(cs, e, 'a', end='')])
            if np.linalg.matrix_rank(Ap) == Ap.shape[0]:
                A = Ap
                print(' ' * 20)
            else:
                print('\r', end='')
        exponent += 1
    print('\r' + ' ' * 20)
    return A

#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from scipy.stats.mstats import mode
import subprocess
import xdmf


def shock_speed(t, rho):
    rho_min = np.amin(rho)
    rho_max = np.amax(rho)
    rho_n = (rho - rho_min) / (rho_max - rho_min)

    center = np.logical_xor(rho_n[1:] < 0.5, rho_n[:-1] < 0.5)
    assert np.sum(center) == 1
    center = np.argmax(center)

    if np.mean(rho_n[center:]) < 0.5:
        rho_n = 1.0 - rho_n
    assert(rho_n[center] <= 0.5 and rho_n[center + 1] >= 0.5)

    rho_rnd = np.round(rho_n * 100) / 100
    lower_mode = mode(rho_rnd[:center][rho_rnd[:center] > 0.05])[0][0]
    upper_mode = mode(rho_rnd[center:][rho_rnd[center:] < 0.95])[0][0]
    lower_key = lower_mode / 2 - 0.01
    upper_key = (upper_mode + 1) / 2 + 0.01

    lower = np.logical_xor(rho_n[1:] < lower_key, rho_n[:-1] < lower_key)
    assert np.sum(lower) == 1
    lower = np.argmax(lower)
    assert(rho_n[lower] <= lower_key and rho_n[lower + 1] >= lower_key)

    upper = np.logical_xor(rho_n[1:] < upper_key, rho_n[:-1] < upper_key)
    assert np.sum(upper) == 1
    upper = np.argmax(upper)
    assert(rho_n[upper] <= upper_key and rho_n[upper + 1] >= upper_key)

    lower_subgrid = lower + (lower_key - rho_n[lower]) / (rho_n[lower + 1] -
                                                          rho_n[lower])
    upper_subgrid = upper + (upper_key - rho_n[upper]) / (rho_n[upper + 1] -
                                                          rho_n[upper])

    cs = (upper_subgrid - lower_subgrid) / (2 * t)
    return cs


def median_shock_speed(filename):
    info = xdmf.get_info(filename)
    m_a = float(info['m_a'])
    m_b = float(info['m_b'])
    x = float(info['x'])
    temperature = float(info['temperature'])
    gamma = 2

    def f(t, rho, *args):
        rho_a, rho_b = rho
        nonlocal gamma
        cv = len(rho_a.shape) / 2
        gamma = (cv + 1) / cv
        s = [n // 2 for n in rho_a.shape]
        s[-1] = slice(rho_a.shape[-1] // 4, rho_a.shape[-1] * 3 // 4)
        rho_ab = np.ravel(rho_a[s] + rho_b[s])
        if t > 0:
            return shock_speed(t, rho_ab)
    cs = xdmf.apply_all(filename, f)
    theoretical_cs = np.sqrt(gamma * temperature / (x * m_a + (1 - x) * m_b))
    return np.median(cs), theoretical_cs


def run_and_measure(**kwargs):
    args = ['../bin/mixture_shock_tube']
    for k, v in kwargs.items():
        args += ['--' + k.replace('_', '-'), str(v)]
    subprocess.check_call(args)
    return median_shock_speed('mixture_shock_tube.xdmf')


def run_and_plot_x_series(xs, **kwargs):
    res = []
    for x in xs:
        res.append(run_and_measure(x=x, **kwargs))
    css, theoretical_css = np.transpose(res)

    with open('cs_vs_x.csv', 'w') as f:
        f.write('x, measured, theory\n')
        for x, cs, theoretical_cs in zip(xs, css, theoretical_css):
            f.write('{}, {}, {}\n'.format(x, cs, theoretical_cs))

    plt.title(r'Mixture Speed of Sound vs. Molar Fraction')
    plt.plot(xs, theoretical_css, 'k--', label='theoretical')
    plt.plot(xs, css, 'o', label='measured')
    plt.xlabel(r'$X_A$')
    plt.ylabel(r'$c_s$')
    plt.legend(loc='upper left')
    plt.margins(x=0.05)
    plt.tight_layout()


def run_and_plot_temperature_series(temperatures, **kwargs):
    res = []
    for temperature in temperatures:
        res.append(run_and_measure(temperature=temperature, **kwargs))
    css, theoretical_css = np.transpose(res)

    with open('cs_vs_temperature.csv', 'w') as f:
        f.write('T, measured, theory\n')
        for temperature, cs, theoretical_cs in zip(temperatures, css,
                                                   theoretical_css):
            f.write('{}, {}, {}\n'.format(temperature, cs, theoretical_cs))

    plt.title(r'Mixture Speed of Sound vs. Mixture Temperature')
    plt.plot(temperatures, theoretical_css, 'k--', label='theoretical')
    plt.plot(temperatures, css, 'o', label='measured')
    plt.xlabel(r'$T$')
    plt.ylabel(r'$c_s$')
    plt.legend(loc='upper left')
    plt.margins(x=0.05)
    plt.tight_layout()


if __name__ == '__main__':
    run_and_plot_x_series(np.linspace(0.05, 0.8, 20), mass_b=4, temp=1.4,
                          shock_ratio=1.01)
    run_and_plot_temperature_series(np.linspace(0.6, 1.4, 20), mass_b=2,
                                    shock_ratio=1.01)

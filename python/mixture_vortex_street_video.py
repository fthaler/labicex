#!/usr/bin/env python3

from matplotlib import pyplot as plt, animation
import numpy as np
import xdmf
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('infile')
parser.add_argument('outbase')
args = parser.parse_args()

data = []
def f(t, rho, *args):
    rho_a, rho_b = rho
    y = rho_a / (rho_a + rho_b)
    data.append(y)
xdmf.apply_all(args.infile, f)

fig = plt.figure(figsize=(16, 16 * data[0].shape[0] / data[0].shape[1]), facecolor='k')
ax = plt.Axes(fig, [0, 0, 1, 1])
ax.set_axis_off()
fig.add_axes(ax)
im = ax.imshow(data[0], cmap='viridis', animated=True, origin='lower', aspect='auto', vmin=0.4, vmax=0.6)
d = float(xdmf.get_info(args.infile)['diameter'])
cir = ax.add_artist(plt.Circle((10 * d, 10 * d), d / 2, color='k'))

frame = 0
def update(*args):
    global frame
    if frame < len(data):
        im.set_array(data[frame])
    frame += 1
    return im, cir

ani = animation.FuncAnimation(fig, update, frames=len(data), interval=40, blit=True)
ani.save(args.outbase + '.mp4', bitrate=-1, codec='libx264', dpi=1920 / 16, fps=25)
fig.savefig(args.outbase + '.png')
    
plt.show()

#!/usr/bin/env python3

from matplotlib import pyplot as plt, animation
import numpy as np
import xdmf
import argparse
import gc

parser = argparse.ArgumentParser()
parser.add_argument('infile')
parser.add_argument('outbase')
args = parser.parse_args()

data = []
info = xdmf.get_info(args.infile)
m_a, m_b = float(info['m_a']), float(info['m_b'])
def f(t, rho, *args):
    print('t = {}'.format(t))
    rho_a, rho_b = rho
    ny, nx = rho_a.shape
    rho_a = rho_a[:, :]
    rho_b = rho_b[:, :]
    x = rho_a / m_a / (rho_a / m_a + rho_b / m_b)
    x = np.roll(x, nx // 4, axis=1)
    data.append(x)
xdmf.apply_all(args.infile, f)

fig = plt.figure(figsize=(9 * data[0].shape[1] / data[0].shape[0], 9), facecolor='k')
ax = plt.Axes(fig, [0, 0, 1, 1])
ax.set_axis_off()
fig.add_axes(ax)
im = ax.imshow(data[0], cmap='viridis', animated=True, origin='lower', aspect='auto', vmin=0.3, vmax=0.7)

frame = 0
def update(*args):
    global frame
    print('frame {} out of {}'.format(frame, len(data)))
    if frame < len(data):
        im.set_array(data[frame])
    frame += 1
    return im,

ani = animation.FuncAnimation(fig, update, frames=len(data), interval=40, blit=True)
ani.save(args.outbase + '.mp4', bitrate=-1, codec='libx264', dpi=1080 / 9, fps=25)
fig.savefig(args.outbase + '.png')
    
plt.show()

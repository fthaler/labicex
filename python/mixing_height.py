#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import sys
from scipy.optimize import curve_fit
import xdmf
import subprocess


def get_amplitude(molar_fraction):
    ny, nx = molar_fraction.shape
    up = molar_fraction[1:, :]
    lo = molar_fraction[:-1, :]
    w = (0.5 - lo) / (up - lo + 1e-12)
    inside = np.logical_xor(up < 0.5, lo < 0.5)
    y, x = np.mgrid[:ny, :nx].astype('float')
    yw = y[:-1, :] + w
    ydiff = np.amax(yw[inside]) - np.amin(yw[inside])
    return ydiff / 2


class Rti(object):
    def __init__(self, filename='rayleigh_taylor_instability.xdmf'):
        info = xdmf.get_info(filename)
        m_a, m_b = float(info['m_a']), float(info['m_b'])
        res = []
        molar_fraction = None
        k = None
        def f(t, rho, *args):
            rho_a, rho_b = rho
            nonlocal molar_fraction, k
            molar_fraction = rho_a[:, :] / m_a / (rho_a[:, :] / m_a + rho_b[:, :] / m_b)
            amplitude = get_amplitude(molar_fraction)
            res.append((t, amplitude))
            print(t, amplitude)
            self.k = 2 * np.pi / rho_a.shape[-1]
        xdmf.apply_all(filename, f)
        self.ts, self.etas = np.transpose(res)
        self.x_final = molar_fraction
        self.m_a = float(info['m_a'])
        self.m_b = float(info['m_b'])
        self.atwood = float(info['atwood_number'])
        self.gravity = float(info['gravity'])
        self.nu = float(info['kinematic_viscosity'])
        self.sc = float(info['schmidt_number'])
        self.smoothing = float(info['smoothing'])
        self.x = float(info['x'])
        #self.eta0 = float(info['initial_magnitude'])
        self.eta0 = self.etas[0]
        self.diffcoef = self.nu / self.sc

    def clamp_initial(self, i):
        self.ts = self.ts[i:]
        self.etas = self.etas[i:]
        self.ts -= self.ts[0]
        self.eta0 = self.etas[0]

    def measured_ns(self):
        ns = np.log(self.etas[1:] / self.etas[0]) / self.ts[1:]
        ns = np.hstack([ns[:1], ns])
        return ns

    def measured_psis(self):
        psis = (self.atwood * self.gravity * self.k /
               ((self.measured_ns() + (self.nu + self.diffcoef) * self.k**2)**2
                - self.nu**2 * self.k**4))
        return psis

    def t0(self):
        drho = (1 - 2 * self.x) * self.m_a + (2 * self.x - 1) * self.m_b
        d0 = np.sqrt(np.pi) * self.smoothing * drho / 2
        t0 = (d0 / 2)**2 / (np.pi * self.diffcoef)
        return t0

    def make_nondimensional(self):
        length_scale = 1 / self.k
        time_scale = np.sqrt(1 / (self.k * self.gravity))
        self.ts /= time_scale
        self.etas /= length_scale
        self.eta0 /= length_scale
        self.gravity /= length_scale / time_scale**2
        self.k /= 1 / length_scale
        self.nu /= length_scale**2 / time_scale
        self.diffcoef /= length_scale**2 / time_scale
        self.smoothing /= length_scale

    def __str__(self):
        return 'A = {}, g = {}, nu = {}, sc = {}, eta0 = {}, D = {}'.format(
                self.atwood, self.gravity, self.nu, self.sc, self.eta0, self.diffcoef)

    def ns_lst(self):
        return np.sqrt(self.atwood * self.gravity * self.k) * np.ones_like(self.etas)


    def ns_duff(self, psi):
        return (np.sqrt(self.atwood * self.gravity * self.k / psi + self.nu**2 * self.k**4) -
                (self.nu + self.diffcoef) * self.k**2)

    def etas_from_ns(self, ns):
        return self.eta0 * np.exp(ns * self.ts)




def plot_lst_growth():
    rti = Rti()
    rti.clamp_initial(10)

    print(rti)
    #rti.make_nondimensional()
    #print(rti)

    print('eta0 * k = {}'.format(rti.eta0 * rti.k))

    eps = 2 * np.sqrt(rti.diffcoef * (rti.ts + rti.t0()))
    a = 1 / (rti.k * eps + 1e-12)
    #assert np.all(a >= 1)
    assert abs(rti.atwood - 0.243) < 0.01
    
    ns_measured = rti.measured_ns()
    psis_measured = rti.measured_psis()
    print('mean measured psi = {}'.format(np.mean(psis_measured)))

    #psis_duff = 1 + 0.883 / a
    psis_duff = np.median(psis_measured) * np.ones_like(rti.etas)
    ns_duff = rti.ns_duff(psis_duff)
    etas_duff = rti.etas_from_ns(ns_duff)

    ns_duff_nd = rti.ns_duff(np.ones_like(rti.etas))
    etas_duff_nd = rti.etas_from_ns(ns_duff_nd)

    ns_lst = rti.ns_lst()
    etas_lst = rti.etas_from_ns(ns_lst)

    fig, (ax0, ax1) = plt.subplots(2)
    fig.suptitle('$\eta$')

    ax0.plot(rti.ts, rti.etas, 'r-', label='measured')
    ax0.plot(rti.ts, etas_lst, 'k-.', label='LST inviscid/immiscible')
    ax0.plot(rti.ts, etas_duff, 'k--', label='LST Duff et al.')
    ax0.plot(rti.ts, etas_duff_nd, 'k:', label='LST no dyn. visc.')
    ax0.legend(loc='upper left')

    ax1.plot(rti.ts, rti.etas, 'r-', label='measured')
    ax1.plot(rti.ts, etas_lst, 'k-.', label='LST inviscid/immiscible')
    ax1.plot(rti.ts, etas_duff, 'k--', label='LST Duff et al.')
    ax1.plot(rti.ts, etas_duff_nd, 'k:', label='LST no dyn. visc.')
    ax1.set_yscale('log')
    ax1.legend(loc='upper left')

    fig, (ax0, ax1) = plt.subplots(2)

    ax0.set_title('$n$')
    ax0.plot(rti.ts, ns_measured, 'r-', label='measured')
    ax0.plot(rti.ts, ns_lst, 'r-.', label='LST invisc/immiscible')
    ax0.plot(rti.ts, ns_duff, 'k--', label='LST Duff et al.')
    ax0.plot(rti.ts, ns_duff_nd, 'k:', label='LST no dyn. visc.')
    ax0.legend()

    ax1.set_title('$\psi$')
    ax1.plot(rti.ts, psis_measured, 'r-', label='measured')
    ax1.plot(rti.ts, psis_duff, 'k--', label='LST Duff et al.')
    ax1.legend()

    fig, ax = plt.subplots()
    ax.imshow(rti.x_final)


def run_lst(**kwargs):
    args = ['../bin/rayleigh_taylor_instability']
    for k, v in kwargs.items():
        args += ['--' + k.replace('_', '-'), str(v)]
    subprocess.check_call(args)


def run_psi_test(scs=[0.5, 1, 2, 5, 10, 20, 50, 100], **kwargs):
    res = []
    fig, ax = plt.subplots()
    for sc in scs:
        args = kwargs
        args.update({'sc': sc})
        run_lst(**args)
        rti = Rti()
        rti.clamp_initial(10)
        psis = rti.measured_psis()
        ax.plot(rti.ts, psis)
        res.append(np.mean(psis))
    fig, ax = plt.subplots()
    ax.plot(scs, res)
    return np.array(scs), res

def run_scaling_test(ls=[122, 250, 506, 1018], **kwargs):
    res = []
    fig, ax = plt.subplots()
    for l in ls:
        args = kwargs
        args.update({'res': l})
        run_lst(**args)
        rti = Rti()
        rti.clamp_initial(10)
        ns = rti.measured_ns()
        ax.plot(rti.ts, ns)
        res.append(np.mean(ns))
    fig, ax = plt.subplots()
    ax.plot(ls, res)
    return np.array(ls), res


if __name__ == '__main__':
    # for rayleigh_taylor_instability --x-sm=0.001 --visc=0.05 --temp=0.8
    # --sine-mag=0.005 --mass-b=2 --o=80 --grav=1e-3 --ti=4000 --res=506
    # --aspect=1
    plot_lst_growth()
    plt.show()

    # final: ./bin/rayleigh_taylor_instability --x-sm=0.001 --visc=0.05
    # --temp=0.8 --sine-mag=0.001 --mass-b=2 --o=80 --grav=2e-4 --ti=4000
    # --res=506 --aspect=2 --x=0.865

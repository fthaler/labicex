#include <cstdlib>
#include <iostream>

#include "base/device_helpers.h"

namespace labicex {

void cuda_check(cudaError_t code, const std::string& file, int line) {
  if (code != cudaSuccess) {
    std::cerr << "CUDA error in file " << file << ":" << line << ":"
              << cudaGetErrorString(code) << std::endl;
    std::abort();
  }
}

void setup_device() {
#ifndef LABICEX_DEVICE_ID
#define LABICEX_DEVICE_ID 0
#endif

  LABICEX_CUDA_CHECK(cudaSetDevice(LABICEX_DEVICE_ID));

  cudaDeviceProp prop;
  LABICEX_CUDA_CHECK(cudaGetDeviceProperties(&prop, LABICEX_DEVICE_ID));
  std::cout << "device:" << std::endl;
  std::cout << "  name: " << prop.name << std::endl;
  std::cout << "  CC:   " << prop.major << "." << prop.minor << std::endl;
  std::cout << "  ECC:  " << (prop.ECCEnabled ? "enabled" : "disabled")
            << std::endl;

  if (sizeof(real) == 8)
    LABICEX_CUDA_CHECK(
        cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
  LABICEX_CUDA_CHECK(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1));
  default_block_size = vector_types<3>::ivector::Constant(1);
  default_block_size(0) = 128;
}

typename vector_types<3>::ivector default_block_size =
    vector_types<3>::ivector::Constant(-1);

}  // labicex

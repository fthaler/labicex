#ifndef LABICEX_BASE_DOMAIN_H
#define LABICEX_BASE_DOMAIN_H

#include <iterator>

#include "boost/array.hpp"

#include "base/device_helpers.h"

namespace labicex {

namespace domain {

namespace kernel {

template <class Domain, class StorageAccessor>
LABICEX_KERNEL void reset(const Domain domain, StorageAccessor s) {
  typedef typename Domain::ivector ivector;

  const ivector x = thread_index<Domain::d>() + domain.min();

  if (!domain.inside(x)) return;

  typename StorageAccessor::value_type zeros[StorageAccessor::array_count];
  for (int i = 0; i < StorageAccessor::array_count; ++i) zeros[i] = 0;

  s.store(x, zeros);
}

}  // kernel

template <class Domain, class Population>
void reset(const Domain& domain, Population& f) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(domain.extents(), grid_size, block_size);

  kernel::reset<<<grid_size, block_size>>>(domain, f.accessor());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());
}

template <int D>
class box_forward_iterator;

template <int D>
class box {
 public:
  const static int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef box_forward_iterator<d> iterator;

  box(const ivector& min, const ivector& max) : min_(min), max_(max) {}
  box(const ivector& max) : min_(ivector::Zero()), max_(max) {}
  box() {}

  LABICEX_COMMON_INLINE bool inside(const ivector& x) const {
    bool inside = true;
    LABICEX_UNROLL
    for (int i = 0; i < d; ++i)
      inside = inside && x(i) >= min_(i) && x(i) < max_(i);
    return inside;
  }

  LABICEX_COMMON_INLINE const ivector& min() const { return min_; }
  LABICEX_COMMON_INLINE const ivector& max() const { return max_; }
  LABICEX_COMMON_INLINE ivector& min() { return min_; }
  LABICEX_COMMON_INLINE ivector& max() { return max_; }
  LABICEX_COMMON_INLINE ivector extents() const { return max_ - min_; }

  box_forward_iterator<d> begin() const {
    return box_forward_iterator<d>(*this, min_);
  }

  box_forward_iterator<d> end() const {
    box_forward_iterator<d> it(*this, max_ - 1);
    return ++it;
  }

  bool operator==(const box& other) const {
    return (min_ == other.min_).all() && (max_ == other.max_).all();
  }

 private:
  ivector min_, max_;
};

template <int D>
class box_halo {
 public:
  const static int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef boost::array<box<d>, 2 * d> box_container;

  box_halo(const box<d>& inner_box, const box<d>& outer_box)
      : inner_box_(inner_box), outer_box_(outer_box) {}
  box_halo() {}

  LABICEX_COMMON_INLINE bool inside(const ivector& x) const {
    return outer_box_.inside(x) && !inner_box_.inside(x);
  }

  box_container sub_boxes() const {
    box_container boxes;
    for (int i = 0; i < d; ++i) {
      ivector lower_min, lower_max, upper_min, upper_max;

      for (int j = 0; j < d; ++j) {
        if (j < i) {
          lower_min(j) = upper_min(j) = inner_box_.min()(j);
          lower_max(j) = upper_max(j) = inner_box_.max()(j);
        } else if (j > i) {
          lower_min(j) = upper_min(j) = outer_box_.min()(j);
          lower_max(j) = upper_max(j) = outer_box_.max()(j);
        } else {
          lower_min(j) = outer_box_.min()(j);
          lower_max(j) = inner_box_.min()(j);
          upper_min(j) = inner_box_.max()(j);
          upper_max(j) = outer_box_.max()(j);
        }
      }

      boxes[2 * i + 0] = box<d>(lower_min, lower_max);
      boxes[2 * i + 1] = box<d>(upper_min, upper_max);
    }
    return boxes;
  }

  LABICEX_COMMON_INLINE const ivector& min() const { return outer_box_.min(); }
  LABICEX_COMMON_INLINE const ivector& max() const { return outer_box_.max(); }
  LABICEX_COMMON_INLINE ivector extents() const { return outer_box_.extents(); }

  const box<d>& inner_box() const { return inner_box_; }
  const box<d>& outer_box() const { return outer_box_; }

 private:
  box<d> inner_box_, outer_box_;
};

template <int D>
class box_forward_iterator {
 public:
  static const int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef ivector value_type;
  typedef const ivector& reference;
  typedef const ivector* pointer;
  typedef int difference_type;
  typedef std::forward_iterator_tag iterator_category;

  box_forward_iterator() {}
  box_forward_iterator(const box<d>& limits, const ivector& x)
      : limits_(limits), x_(x) {}

  const ivector& operator*() const { return x_; }

  box_forward_iterator& operator++() {
    for (int i = 0; i < d; ++i) {
      ++x_(i);
      if (x_(i) < limits_.max()(i) || i == d - 1) break;
      x_(i) = limits_.min()(i);
    }
    return *this;
  }

  box_forward_iterator operator++(int) {
    ivector x = x_;
    ++(*this);
    return x;
  }

  const ivector* operator->() const { return &x_; }

  bool operator==(const box_forward_iterator& other) const {
    assert((limits_.min() == other.limits_.min()).all());
    assert((limits_.max() == other.limits_.max()).all());
    return (x_ == other.x_).all();
  }

  bool operator!=(const box_forward_iterator& other) const {
    return !(*this == other);
  }

 private:
  box<d> limits_;
  ivector x_;
};

}  // domain

}  // labicex

#endif  // LABICEX_BASE_DOMAIN_H

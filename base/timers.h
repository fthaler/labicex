#ifndef LABICEX_BASE_TIMERS_H
#define LABICEX_BASE_TIMERS_H

#include <stdexcept>
#include <string>

#include <boost/chrono.hpp>
#include <boost/cstdint.hpp>

#include "mpi/distributed_grid.h"

namespace labicex {

class timer {
 public:
  typedef boost::chrono::steady_clock clock;
  typedef boost::chrono::time_point<clock> time_point;

  timer() : valid_start_(false), running_(false), total_elapsed_(0) {}

  void start() {
    if (running_) throw std::logic_error("tried to start running timer");
    valid_start_ = running_ = true;
    start_ = clock::now();
  }

  void stop() {
    time_point stop = clock::now();
    if (!running_) throw std::logic_error("tried to stop inactive timer");
    running_ = false;
    total_elapsed_ +=
        boost::chrono::duration_cast<boost::chrono::nanoseconds>(stop - start_)
            .count();
  }

  boost::uint64_t elapsed_nanoseconds() const {
    if (!valid_start_)
      throw std::logic_error("tried to get time of never-started timer");
    boost::uint64_t elapsed = total_elapsed_;
    if (running_) {
      time_point current = clock::now();
      elapsed += boost::chrono::duration_cast<boost::chrono::nanoseconds>(
                     current - start_)
                     .count();
    }
    return elapsed;
  }

  double elapsed_seconds() const {
    const boost::uint64_t nanoseconds = elapsed_nanoseconds();
    const boost::uint64_t seconds_lead = nanoseconds / 1000000000ull;
    const boost::uint64_t remainder =
        nanoseconds - seconds_lead * 1000000000ull;
    return seconds_lead + remainder / 1.0e9;
  }

  void report(const std::string& title) const;

 private:
  time_point start_;
  bool valid_start_, running_;
  boost::uint64_t total_elapsed_;
};

class mlups_timer : public timer {
 public:
  mlups_timer(boost::uint64_t lattice_nodes, boost::uint64_t time_steps)
      : lattice_nodes_(lattice_nodes), time_steps_(time_steps) {}

  template <class Lattice>
  mlups_timer(const mpi::distributed_grid<Lattice>& grid,
              boost::uint64_t time_steps)
      : lattice_nodes_(1), time_steps_(time_steps) {
    for (int i = 0; i < Lattice::d; ++i) {
      const boost::uint64_t e = grid.global().global_domain().extents()(i);
      lattice_nodes_ *= e;
    }
  }

  double mlups() const {
    const boost::uint64_t nanoseconds = elapsed_nanoseconds();
    const boost::uint64_t lup = lattice_nodes_ * time_steps_;
    const boost::uint64_t lups_lead = (lup * 1000ull) / nanoseconds;
    const boost::uint64_t remainder = lup - lups_lead * nanoseconds / 1000ull;
    return lups_lead + remainder * 1000.0 / nanoseconds;
  }

  void report(const std::string& title) const;

 private:
  boost::uint64_t lattice_nodes_, time_steps_;
};

class auto_timer {
 public:
  auto_timer(const std::string& title) : title_(title) { timer_.start(); }
  ~auto_timer() {
    timer_.stop();
    timer_.report(title_);
  }

 private:
  std::string title_;
  timer timer_;
};

class mlups_auto_timer {
 public:
  mlups_auto_timer(const std::string& title, boost::uint64_t lattice_nodes,
                   boost::uint64_t time_steps)
      : title_(title), timer_(lattice_nodes, time_steps) {
    timer_.start();
  }

  template <class Lattice>
  mlups_auto_timer(const std::string& title,
                   const mpi::distributed_grid<Lattice>& grid,
                   boost::uint64_t time_steps)
      : title_(title), timer_(grid, time_steps) {
    timer_.start();
  }

  ~mlups_auto_timer() {
    timer_.stop();
    timer_.report(title_);
  }

 private:
  std::string title_;
  mlups_timer timer_;
};

}  // labicex

#endif  // LABICEX_BASE_TIMERS_H

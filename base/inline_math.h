#ifndef LABICEX_INLINE_MATH_H
#define LABICEX_INLINE_MATH_H

#include <cassert>

#include "base/common.h"

namespace labicex {

template <class ScalarType>
LABICEX_COMMON_INLINE ScalarType ipow(ScalarType b, int e) {
  assert(e >= -4 && e <= 4);
  if (e == -4) return 1.0 / (b * b * b * b);
  if (e == -3) return 1.0 / (b * b * b);
  if (e == -2) return 1.0 / (b * b);
  if (e == -1) return 1.0 / b;
  if (e == 0) return 1.0;
  if (e == 1) return b;
  if (e == 2) return b * b;
  if (e == 3) return b * b * b;
  if (e == 4) return b * b * b * b;
  return 1.0 / 0.0;
}

LABICEX_COMMON_INLINE real delta(int a, int b) {
  if (a == b) return 1;
  return 0;
}

}  // labicex

#endif  // LABICEX_INLINE_MATH_H

#ifndef LABICEX_SOA_STORAGE_H
#define LABICEX_SOA_STORAGE_H

#include <boost/core/enable_if.hpp>
#include <boost/type_traits.hpp>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/system/cuda/experimental/pinned_allocator.h>
#include <thrust/transform_reduce.h>

#include "base/common.h"
#include "base/device_helpers.h"
#include "base/indexer.h"

namespace labicex {

template <int ArrayCount, int D>
class soa_indexer {
 public:
  static const int array_count = ArrayCount;
  static const int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef const ivector& extents_type;

  soa_indexer(const ivector& extents)
      : nd_indexer_(extents), array_size_(extents.prod()) {}

  LABICEX_COMMON_INLINE int array_base_offset(int array_index) const {
    assert(array_index >= 0 && array_index < array_count);
    return array_index * array_size_;
  }

  LABICEX_COMMON_INLINE int array_linear_index(const ivector& nd_index) const {
    return nd_indexer_.linear_index(nd_index);
  }

  LABICEX_COMMON_INLINE int linear_index(int array_index,
                                         const ivector& nd_index) const {
    return array_base_offset(array_index) + array_linear_index(nd_index);
  }

  LABICEX_COMMON_INLINE const ivector& extents() const {
    return nd_indexer_.extents();
  }

 private:
  indexer<d> nd_indexer_;
  int array_size_;
};

template <int ArrayCount, class Extents>
class fixed_size_soa_indexer {
 public:
  static const int array_count = ArrayCount;
  typedef Extents extents_type;
  static const int d = extents_type::d;
  typedef typename vector_types<d>::ivector ivector;

  fixed_size_soa_indexer(const ivector& extents) : nd_indexer_(extents) {}
  fixed_size_soa_indexer() {}

  LABICEX_COMMON_INLINE int array_base_offset(int array_index) const {
    assert(array_index >= 0 && array_index < array_count);
    return array_index * array_size;
  }

  LABICEX_COMMON_INLINE int array_linear_index(const ivector& nd_index) const {
    assert(!outside(nd_index, extents()));
    return nd_indexer_.linear_index(nd_index);
  }

  LABICEX_COMMON_INLINE int linear_index(int array_index,
                                         const ivector& nd_index) const {
    return array_base_offset(array_index) + array_linear_index(nd_index);
  }

  LABICEX_COMMON_INLINE extents_type extents() const {
    return nd_indexer_.extents();
  }

 private:
  static const int array_size = extents_type::static_prod;

  fixed_size_indexer<extents_type> nd_indexer_;
};

template <class ValueType, class Indexer>
class soa_ro_accessor {
 public:
  static const int array_count = Indexer::array_count;
  static const int d = Indexer::d;
  typedef typename vector_types<d>::ivector ivector;
  typedef ValueType value_type;

  soa_ro_accessor(const Indexer& indexer, const value_type* data_ptr)
      : indexer_(indexer), data_ptr_(data_ptr) {}

  LABICEX_COMMON_INLINE value_type load_single(int array_index,
                                               const ivector& nd_index) const {
    return ldg(&data_ptr_[indexer_.linear_index(array_index, nd_index)]);
  }

  LABICEX_COMMON_INLINE void load(const ivector& nd_index,
                                  value_type values[array_count]) const {
    int ali = indexer_.array_linear_index(nd_index);
    LABICEX_UNROLL
    for (int i = 0; i < array_count; ++i)
      values[i] = ldg(&data_ptr_[indexer_.array_base_offset(i) + ali]);
  }

  template <class CompoundType>
  LABICEX_COMMON_INLINE
      typename boost::enable_if_c<sizeof(CompoundType) ==
                                  sizeof(value_type) * array_count>::type
      load(const ivector& nd_index, CompoundType& values) const {
    load(nd_index, reinterpret_cast<value_type*>(&values));
  }

  LABICEX_COMMON_INLINE typename Indexer::extents_type extents() const {
    return indexer_.extents();
  }

 private:
  Indexer indexer_;
  const value_type* __restrict__ data_ptr_;
};

template <class ValueType, class Indexer>
class soa_rw_accessor {
 public:
  static const int array_count = Indexer::array_count;
  static const int d = Indexer::d;
  typedef typename vector_types<d>::ivector ivector;
  typedef ValueType value_type;

  soa_rw_accessor(const Indexer& indexer, value_type* data_ptr)
      : indexer_(indexer), data_ptr_(data_ptr) {}

  LABICEX_COMMON_INLINE value_type load_single(int array_index,
                                               const ivector& nd_index) const {
    return data_ptr_[indexer_.linear_index(array_index, nd_index)];
  }

  LABICEX_COMMON_INLINE void store_single(int array_index,
                                          const ivector& nd_index,
                                          const value_type& value) {
    data_ptr_[indexer_.linear_index(array_index, nd_index)] = value;
  }

  LABICEX_COMMON_INLINE void load(const ivector& nd_index,
                                  value_type values[array_count]) const {
    int ali = indexer_.array_linear_index(nd_index);
    LABICEX_UNROLL
    for (int i = 0; i < array_count; ++i)
      values[i] = data_ptr_[indexer_.array_base_offset(i) + ali];
  }

  LABICEX_COMMON_INLINE void store(const ivector& nd_index,
                                   const value_type values[array_count]) {
    int ali = indexer_.array_linear_index(nd_index);
    LABICEX_UNROLL
    for (int i = 0; i < array_count; ++i)
      data_ptr_[indexer_.array_base_offset(i) + ali] = values[i];
  }

  template <class CompoundType>
  LABICEX_COMMON_INLINE
      typename boost::enable_if_c<sizeof(CompoundType) ==
                                  sizeof(value_type) * array_count>::type
      load(const ivector& nd_index, CompoundType& values) const {
    load(nd_index, reinterpret_cast<value_type*>(&values));
  }

  template <class CompoundType>
  LABICEX_COMMON_INLINE
      typename boost::enable_if_c<sizeof(CompoundType) ==
                                  sizeof(value_type) * array_count>::type
      store(const ivector& nd_index, const CompoundType& values) {
    store(nd_index, reinterpret_cast<const value_type*>(&values));
  }

  LABICEX_COMMON_INLINE typename Indexer::extents_type extents() const {
    return indexer_.extents();
  }

 private:
  Indexer indexer_;
  value_type* __restrict__ data_ptr_;
};

template <class PtrValueType, class Indexer>
class soa_accessor {
 public:
  static const int array_count = Indexer::array_count;
  static const int d = Indexer::d;
  typedef typename vector_types<d>::ivector ivector;
  typedef PtrValueType ptr_value_type;
  typedef typename boost::remove_const<ptr_value_type>::type value_type;

  soa_accessor(const Indexer& indexer, ptr_value_type* data_ptr)
      : indexer_(indexer), data_ptr_(data_ptr) {}

  LABICEX_COMMON_INLINE value_type load_single(int array_index,
                                               const ivector& nd_index) const {
    return data_ptr_[indexer_.linear_index(array_index, nd_index)];
  }

  LABICEX_COMMON_INLINE void store_single(int array_index,
                                          const ivector& nd_index,
                                          const value_type& value) {
    data_ptr_[indexer_.linear_index(array_index, nd_index)] = value;
  }

  LABICEX_COMMON_INLINE void load(const ivector& nd_index,
                                  value_type values[array_count]) const {
    int ali = indexer_.array_linear_index(nd_index);
    LABICEX_UNROLL
    for (int i = 0; i < array_count; ++i)
      values[i] = data_ptr_[indexer_.array_base_offset(i) + ali];
  }

  LABICEX_COMMON_INLINE void store(const ivector& nd_index,
                                   const value_type values[array_count]) {
    int ali = indexer_.array_linear_index(nd_index);
    LABICEX_UNROLL
    for (int i = 0; i < array_count; ++i)
      data_ptr_[indexer_.array_base_offset(i) + ali] = values[i];
  }

  template <class CompoundType>
  LABICEX_COMMON_INLINE
      typename boost::enable_if_c<sizeof(CompoundType) ==
                                  sizeof(value_type) * array_count>::type
      load(const ivector& nd_index, CompoundType& values) const {
    load(nd_index, reinterpret_cast<value_type*>(&values));
  }

  template <class CompoundType>
  LABICEX_COMMON_INLINE
      typename boost::enable_if_c<sizeof(CompoundType) ==
                                  sizeof(value_type) * array_count>::type
      store(const ivector& nd_index, const CompoundType& values) {
    store(nd_index, reinterpret_cast<const value_type*>(&values));
  }

  LABICEX_COMMON_INLINE typename Indexer::extents_type extents() const {
    return indexer_.extents();
  }

 private:
  Indexer indexer_;
  ptr_value_type* __restrict__ data_ptr_;
};

class soa_storage_base {};

template <class ValueType, class Indexer>
class pinned_soa_storage : public soa_storage_base {
  struct checker {
    bool operator()(const ValueType& v) const { return !isnan(v); }
  };

 public:
  static const int array_count = Indexer::array_count;
  static const int d = Indexer::d;
  typedef typename vector_types<d>::ivector ivector;
  typedef ValueType value_type;
  typedef soa_rw_accessor<value_type, Indexer> accessor_type;
  typedef soa_ro_accessor<value_type, Indexer> const_accessor_type;
  typedef thrust::cuda::experimental::pinned_allocator<value_type>
      allocator_type;
  typedef thrust::host_vector<value_type, allocator_type> data_type;

  pinned_soa_storage(const ivector& extents)
      : indexer_(extents), data_(extents.prod() * array_count) {}

  accessor_type accessor() {
    return accessor_type(indexer_, thrust::raw_pointer_cast(data_.data()));
  }
  const_accessor_type caccessor() const {
    return const_accessor_type(indexer_,
                               thrust::raw_pointer_cast(data_.data()));
  }
  const_accessor_type accessor() const { return caccessor(); }

  const data_type& data() const { return data_; }
  data_type& data() { return data_; }

  const value_type* array_start(int array_index) const {
    return thrust::raw_pointer_cast(
        &data_[indexer_.array_base_offset(array_index)]);
  }
  value_type* array_start(int array_index) {
    return thrust::raw_pointer_cast(
        &data_[indexer_.array_base_offset(array_index)]);
  }

  ivector extents() const { return ivector_cast(indexer_.extents()); }

  void swap(pinned_soa_storage& other) { data_.swap(other.data_); }

  bool check() const {
    return thrust::transform_reduce(data_.begin(), data_.end(), checker(), true,
                                    thrust::logical_and<bool>());
  }

 private:
  Indexer indexer_;
  data_type data_;
};

template <class ValueType, class Indexer>
class device_soa_storage : soa_storage_base {
 public:
  static const int array_count = Indexer::array_count;
  static const int d = Indexer::d;
  typedef typename vector_types<d>::ivector ivector;
  typedef ValueType value_type;
  typedef soa_rw_accessor<value_type, Indexer> accessor_type;
  typedef soa_ro_accessor<value_type, Indexer> const_accessor_type;
  typedef thrust::device_vector<value_type> data_type;

  device_soa_storage(const ivector& extents)
      : indexer_(extents), data_(extents.prod() * array_count) {}

  accessor_type accessor() {
    return accessor_type(indexer_, thrust::raw_pointer_cast(data_.data()));
  }
  const_accessor_type caccessor() const {
    return const_accessor_type(indexer_,
                               thrust::raw_pointer_cast(data_.data()));
  }
  const_accessor_type accessor() const { return caccessor(); }

  const data_type& data() const { return data_; }
  data_type& data() { return data_; }

  const value_type* array_start(int array_index) const {
    return thrust::raw_pointer_cast(
        &data_[indexer_.array_base_offset(array_index)]);
  }
  value_type* array_start(int array_index) {
    return thrust::raw_pointer_cast(
        &data_[indexer_.array_base_offset(array_index)]);
  }

  ivector extents() const { return ivector_cast(indexer_.extents()); }

  void swap(device_soa_storage& other) { data_.swap(other.data_); }

 private:
  Indexer indexer_;
  data_type data_;
};

}  // labicex

#endif  // LABICEX_SOA_STORAGE_H

#ifndef LABICEX_BASE_KERNEL_SPACE_H
#define LABICEX_BASE_KERNEL_SPACE_H

#include "base/common.h"
#include "base/domain.h"
#include "mpi/distributed_grid.h"

namespace labicex {

template <int D>
class kernel_space {
 public:
  typedef typename vector_types<D>::ivector ivector;
  typedef typename vector_types<D>::rvector rvector;
  static const int d = D;

  LABICEX_COMMON_INLINE kernel_space() {}

  LABICEX_COMMON_INLINE kernel_space(const ivector& kernel_offset,
                                     const ivector& global_offset)
      : kernel_offset_(kernel_offset), global_offset_(global_offset) {}

  template <class Lattice>
  kernel_space(const mpi::distributed_grid<Lattice>& grid,
               const ivector& kernel_offset = ivector::Zero())
      : kernel_offset_(kernel_offset),
        global_offset_(grid.global().full_domain().min()) {}

  LABICEX_COMMON_INLINE ivector kernel_to_local(const ivector& x) const {
    return x + kernel_offset_;
  }

  LABICEX_COMMON_INLINE ivector local_to_global(const ivector& x) const {
    return x + global_offset_;
  }

  LABICEX_COMMON_INLINE ivector kernel_to_global(const ivector& x) const {
    return local_to_global(kernel_to_local(x));
  }

  LABICEX_COMMON_INLINE ivector local_to_kernel(const ivector& x) const {
    return x - kernel_offset_;
  }

  LABICEX_COMMON_INLINE ivector global_to_local(const ivector& x) const {
    return x - global_offset_;
  }

  LABICEX_COMMON_INLINE ivector global_to_kernel(const ivector& x) const {
    return local_to_kernel(global_to_local(x));
  }

  LABICEX_COMMON_INLINE rvector kernel_to_local(const rvector& x) const {
    return x + kernel_offset_.template cast<real>();
  }

  LABICEX_COMMON_INLINE rvector local_to_global(const rvector& x) const {
    return x + global_offset_.template cast<real>();
  }

  LABICEX_COMMON_INLINE rvector kernel_to_global(const rvector& x) const {
    return local_to_global(kernel_to_local(x));
  }

  LABICEX_COMMON_INLINE rvector local_to_kernel(const rvector& x) const {
    return x - kernel_offset_.template cast<real>();
  }

  LABICEX_COMMON_INLINE rvector global_to_local(const rvector& x) const {
    return x - global_offset_.template cast<real>();
  }

  LABICEX_COMMON_INLINE rvector global_to_kernel(const rvector& x) const {
    return local_to_kernel(global_to_local(x));
  }

 private:
  ivector kernel_offset_, global_offset_;
};

}  // labicex

#endif  // LABICEX_BASE_KERNEL_SPACE_H

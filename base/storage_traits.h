#ifndef LABICEX_BASE_STORAGE_TRAITS_H
#define LABICEX_BASE_STORAGE_TRAITS_H

#include "base/soa_storage.h"

namespace labicex {

template <class Lattice>
struct device_storage_traits {
  typedef device_soa_storage<real, soa_indexer<Lattice::q, Lattice::d> >
      population_storage;
  typedef device_soa_storage<
      real, soa_indexer<Lattice::primitive_variables::count, Lattice::d> >
      primitive_variables_storage;

  template <class Extents>
  struct fixed_size {
    typedef device_soa_storage<real,
                               fixed_size_soa_indexer<Lattice::q, Extents> >
        population_storage;
    typedef device_soa_storage<
        real,
        fixed_size_soa_indexer<Lattice::primitive_variables::count, Extents> >
        primitive_variables_storage;
  };
};

template <class Lattice>
struct host_storage_traits {
  typedef pinned_soa_storage<real, soa_indexer<Lattice::q, Lattice::d> >
      population_storage;
  typedef pinned_soa_storage<
      real, soa_indexer<Lattice::primitive_variables::count, Lattice::d> >
      primitive_variables_storage;

  template <class Extents>
  struct fixed_size {
    typedef pinned_soa_storage<real,
                               fixed_size_soa_indexer<Lattice::q, Extents> >
        population_storage;
    typedef pinned_soa_storage<
        real,
        fixed_size_soa_indexer<Lattice::primitive_variables::count, Extents> >
        primitive_variables_storage;
  };
};

}  // labicex

#endif  // LABICEX_BASE_STORAGE_TRAITS_H

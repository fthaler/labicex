#ifndef LABICEX_BASE_COMMON_H
#define LABICEX_BASE_COMMON_H

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int
#include <Eigen/Core>

#define LABICEX_INLINE __forceinline__
#define LABICEX_NOINLINE __noinline__
#define LABICEX_UNROLL #pragma unroll

#define LABICEX_KERNEL __global__
#define LABICEX_HOST __host__
#define LABICEX_DEVICE __device__
#define LABICEX_COMMON LABICEX_HOST LABICEX_DEVICE

#define LABICEX_HOST_INLINE LABICEX_HOST LABICEX_INLINE
#define LABICEX_DEVICE_INLINE LABICEX_DEVICE LABICEX_INLINE
#define LABICEX_COMMON_INLINE LABICEX_COMMON LABICEX_INLINE

#define LABICEX_HOST_NOINLINE LABICEX_HOST LABICEX_NOINLINE
#define LABICEX_DEVICE_NOINLINE LABICEX_DEVICE LABICEX_NOINLINE
#define LABICEX_COMMON_NOINLINE LABICEX_COMMON LABICEX_NOINLINE

namespace labicex {

typedef double real;
#define LABICEX_ASM_REAL "d"

template <int D>
struct vector_types {
  static const int d = D;
  typedef Eigen::Array<real, 1, d> rvector;
  typedef Eigen::Array<int, 1, d> ivector;
  typedef Eigen::Array<bool, 1, d> bvector;
  typedef Eigen::Array<real, d, d> rtensor;
};

template <class Vector>
struct vector_traits;

template <class T, int D>
struct vector_traits<Eigen::Array<T, 1, D> > {
  template <int NewD>
  struct changed_dimension {
    typedef Eigen::Array<T, 1, NewD> type;
  };

  template <class NewT>
  struct changed_type {
    typedef Eigen::Array<NewT, 1, D> type;
  };
};

}  // labicex

#endif  // LABICEX_BASE_COMMON_H

#ifndef LABICEX_BASE_TRANSFER_H
#define LABICEX_BASE_TRANSFER_H

#include <thrust/copy.h>
#include <thrust/system/cuda/execution_policy.h>

#include "base/stream.h"

namespace labicex {

template <class SrcStorage, class DstStorage>
struct storage_copy_helper {
  static void copy(const SrcStorage& src, DstStorage& dst,
                   const stream& copy_stream = stream::zero()) {
    thrust::copy(thrust::system::cuda::par.on(copy_stream), src.data().begin(),
                 src.data().end(), dst.data().begin());
  }
};

template <class SrcStorage, class DstStorage>
void storage_copy(const SrcStorage& src, DstStorage& dst,
                  const stream& copy_stream = stream::zero()) {
  storage_copy_helper<SrcStorage, DstStorage>::copy(src, dst, copy_stream);
}

}  // labicex

#endif  // LABICEX_BASE_TRANSFER_H

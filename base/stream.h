#ifndef LABICEX_BASE_STREAM_H
#define LABICEX_BASE_STREAM_H

#include <boost/shared_ptr.hpp>

namespace labicex {

class stream {
 public:
  stream();
  stream(const stream&);

  void synchronize() const;

  operator cudaStream_t() const {
    if (ptr_) return ptr_->cuda_stream();
    return 0;
  }

  static stream zero() { return stream(0); }

 private:
  stream(int);

  class stream_holder {
   public:
    stream_holder();
    ~stream_holder();
    cudaStream_t cuda_stream() { return cuda_stream_; }

   private:
    cudaStream_t cuda_stream_;
  };

  boost::shared_ptr<stream_holder> ptr_;
};

}  // labicex

#endif  // LABICEX_BASE_STREAM_H

#ifndef LABICEX_BASE_INDEXER_H
#define LABICEX_BASE_INDEXER_H

#include "base/common.h"
#include "base/static_ivector.h"

namespace labicex {

template <int D>
class indexer {
 public:
  static const int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef const ivector& extents_type;

  indexer(const ivector& extents)
      : extents_(extents), strides_(strides_from_extents(extents)) {}

  LABICEX_COMMON_INLINE int linear_index(const ivector& nd_index) const {
    assert(!outside(nd_index, extents_));
    return (nd_index * strides_).sum();
  }

  LABICEX_COMMON_INLINE const ivector& extents() const { return extents_; }

 private:
  static ivector strides_from_extents(const ivector& extents) {
    ivector strides;
    strides(0) = 1;
    for (int i = 1; i < d; ++i) strides(i) = strides(i - 1) * extents(i - 1);
    return strides;
  }

  ivector extents_, strides_;
};

template <class Extents>
class fixed_size_indexer {
 public:
  typedef Extents extents_type;
  static const int d = extents_type::d;
  typedef typename vector_types<d>::ivector ivector;

  fixed_size_indexer(const ivector& extents) {
    assert((extents == extents_type::as_ivector()).all());
  }

  fixed_size_indexer() {}

  LABICEX_COMMON_INLINE int linear_index(const ivector& nd_index) const {
    assert(!outside(nd_index, extents()));
    return helper<extents_type>::linear_index(nd_index);
  }

  LABICEX_COMMON_INLINE extents_type extents() const { return extents_type(); }

 private:
  template <class>
  struct helper;

  template <int X>
  struct helper<static_ivector<X> > {
    LABICEX_COMMON_INLINE static int linear_index(const ivector& nd_index) {
      return nd_index(0);
    }
  };

  template <int X, int Y>
  struct helper<static_ivector<X, Y> > {
    LABICEX_COMMON_INLINE static int linear_index(const ivector& nd_index) {
      return nd_index(0) + X * nd_index(1);
    }
  };

  template <int X, int Y, int Z>
  struct helper<static_ivector<X, Y, Z> > {
    LABICEX_COMMON_INLINE static int linear_index(const ivector& nd_index) {
      return nd_index(0) + X * (nd_index(1) + Y * nd_index(2));
    }
  };
};

}  // labicex

#endif  // LABICEX_BASE_INDEXER_H

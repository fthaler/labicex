#include <iomanip>
#include <iostream>

#include "base/timers.h"

namespace labicex {

void timer::report(const std::string& title) const {
  std::cout << title << ":" << std::endl;
  std::cout << "  elapsed time: " << std::setw(6) << elapsed_seconds() << "s"
            << std::endl;
}

void mlups_timer::report(const std::string& title) const {
  std::cout << title << ":" << std::endl;
  std::cout << "  elapsed time: " << std::setw(6) << elapsed_seconds() << "s"
            << std::endl;
  std::cout << "  performance:  " << std::setw(6) << mlups() << "MLUPS"
            << std::endl;
}

}  // labicex

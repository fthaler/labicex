#ifndef LABICEX_BASE_DEVICE_HELPERS_H
#define LABICEX_BASE_DEVICE_HELPERS_H

#include <stdexcept>
#include <string>

#include "base/common.h"

#define LABICEX_CUDA_CHECK(x) ::labicex::cuda_check((x), __FILE__, __LINE__)

namespace labicex {

void cuda_check(cudaError_t code, const std::string& file, int line);

void setup_device();

template <int D>
LABICEX_DEVICE_INLINE typename vector_types<D>::ivector thread_index();

template <>
LABICEX_DEVICE_INLINE typename vector_types<1>::ivector thread_index<1>() {
  return
      typename vector_types<1>::ivector(blockIdx.x * blockDim.x + threadIdx.x);
}

template <>
LABICEX_DEVICE_INLINE typename vector_types<2>::ivector thread_index<2>() {
  return
      typename vector_types<2>::ivector(blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);
}

template <>
LABICEX_DEVICE_INLINE typename vector_types<3>::ivector thread_index<3>() {
  return
      typename vector_types<3>::ivector(blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y,
                                        blockIdx.z * blockDim.z + threadIdx.z);
}

template <class T>
LABICEX_COMMON_INLINE T ldg(const T* t) {
#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ >= 350
  return __ldg(t);
#else
  return *t;
#endif
}

template <class T>
LABICEX_DEVICE_INLINE T shuffle(T value, int offset, int width = warpSize);

template <>
LABICEX_DEVICE_INLINE float shuffle<float>(float value, int offset, int width) {
  assert(width <= warpSize);
  if (offset > 0) return __shfl_up(value, offset, width);
  if (offset < 0) return __shfl_down(value, -offset, width);
  return value;
}

template <>
LABICEX_DEVICE_INLINE double shuffle<double>(double value, int offset,
                                             int width) {
  assert(width <= warpSize);
  if (offset == 0) return value;
  int2 iv = *reinterpret_cast<int2*>(&value);
  if (offset > 0) {
    iv.x = __shfl_up(iv.x, offset, width);
    iv.y = __shfl_up(iv.y, offset, width);
  } else if (offset < 0) {
    iv.x = __shfl_down(iv.x, -offset, width);
    iv.y = __shfl_down(iv.y, -offset, width);
  }
  return *reinterpret_cast<double*>(&iv);
}

template <int D>
LABICEX_COMMON_INLINE void asm_barrier(Eigen::Array<real, 1, D>& v) {
#ifndef LABICEX_NO_ASM_BARRIER
#ifdef __CUDA_ARCH__
  LABICEX_UNROLL
  for (int i = 0; i < D; ++i) {
    real& vi = v(i);
    asm volatile("" : "+" LABICEX_ASM_REAL(vi));
  }
#endif
#endif
}

template <class Vector1, class Vector2>
LABICEX_COMMON_INLINE bool outside(const Vector1& x,
                                   const Vector2& domain_size) {
  LABICEX_UNROLL
  for (int i = 0; i < x.size(); ++i) {
    if (x(i) < 0 || x(i) >= domain_size(i)) return true;
  }
  return false;
}

struct launch_error : std::runtime_error {
  launch_error(const std::string& what) : std::runtime_error(what) {}
};

template <class Ivector, class Ivector2>
void get_launch_configuration(const Ivector& domain_size,
                              const Ivector2& desired_block_size,
                              dim3& grid_size, dim3& block_size) {
  typedef vector_types<3>::ivector ivector3;
  static int max_threads_per_block;
  static ivector3 max_block_size;
  static bool limits_set = false;

  if (!limits_set) {
    int device;
    LABICEX_CUDA_CHECK(cudaGetDevice(&device));
    cudaDeviceProp prop;
    LABICEX_CUDA_CHECK(cudaGetDeviceProperties(&prop, device));

    max_threads_per_block = prop.maxThreadsPerBlock;
    for (int i = 0; i < 3; ++i) max_block_size(i) = prop.maxThreadsDim[i];

    limits_set = true;
  }

  if (desired_block_size.prod() > max_threads_per_block)
    throw launch_error("too many threads per block");

  assert(domain_size.size() <= 3);
  ivector3 bs = ivector3::Constant(1);
  for (int i = 0; i < domain_size.size(); ++i) {
    if (desired_block_size(i) < 1)
      throw launch_error("invalid desired block size");
    bs(i) = desired_block_size(i);
  }

  ivector3 domain_size3 = ivector3::Constant(1);
  for (int i = 0; i < domain_size.size(); ++i) {
    if (domain_size(i) < 1) throw launch_error("invalid domain size");
    domain_size3(i) = domain_size(i);
  }

  if (domain_size.size() > 1) {
    while (bs(0) > domain_size3(0)) {
      bs(0) /= 2;
      bs(1) *= 2;

      if (domain_size.size() > 2) {
        while (bs(1) > domain_size3(1)) {
          bs(1) /= 2;
          bs(2) *= 2;
        }
      }
    }
  }

  bs = bs.min(max_block_size);
  assert(bs.prod() <= max_threads_per_block);

  ivector3 gs = (domain_size3 + bs - 1) / bs;
  assert((gs * bs >= domain_size3).all());

  grid_size.x = gs(0);
  grid_size.y = gs(1);
  grid_size.z = gs(2);
  block_size.x = bs(0);
  block_size.y = bs(1);
  block_size.z = bs(2);
}

extern typename vector_types<3>::ivector default_block_size;

template <class Ivector>
void get_default_launch_configuration(const Ivector& domain_size,
                                      dim3& grid_size, dim3& block_size) {
  get_launch_configuration(domain_size, default_block_size, grid_size,
                           block_size);
}

}  // labicex

#endif  // LABICEX_BASE_DEVICE_HELPERS_H

#ifndef LABICEX_BASE_ENTROPY_H
#define LABICEX_BASE_ENTROPY_H

#include "base/common.h"
#include "lattice/tags.h"

namespace labicex {

template <class Lattice>
LABICEX_COMMON_INLINE real entropy(const typename Lattice::fvector& f) {
  real h = 0;
  const typename Lattice::primitive_variables pv(f);
  LABICEX_UNROLL
  for (int i = 0; i < Lattice::q; ++i) {
    if (f(i) > 1e-12) h += f(i) * log(f(i) / Lattice::w(i, pv));
  }
  assert(!isnan(h));
  return h;
}

}  // labicex

#endif  // LABICEX_BASE_ENTROPY_H

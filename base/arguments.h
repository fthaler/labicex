#ifndef LABICEX_APPLICATIONS_ARGUMENTS_H
#define LABICEX_APPLICATIONS_ARGUMENTS_H

#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <boost/lexical_cast.hpp>

#include "base/common.h"

namespace labicex {

struct argument_parse_error : std::runtime_error {
  argument_parse_error(const std::string& what) : std::runtime_error(what) {}
};

struct argument_value_error : std::runtime_error {
  argument_value_error(const std::string& what) : std::runtime_error(what) {}
};

namespace detail {

template <class T>
struct argument_parser {
  static T parse(const std::vector<std::string>& tokens) {
    if (tokens.size() != 1)
      throw argument_parse_error("invalid number of tokens");
    T value;
    if (!boost::conversion::try_lexical_convert<T>(tokens[0], value))
      throw argument_parse_error("can not parse token '" + tokens[0] + "'");
    return value;
  }
};

template <>
struct argument_parser<std::string> {
  static std::string parse(const std::vector<std::string>& tokens) {
    if (tokens.empty()) throw argument_parse_error("invalid number of tokens");
    std::string s = tokens[0];
    for (std::size_t i = 1; i < tokens.size(); ++i) s += " " + tokens[i];
    return s;
  }
};

template <class T, int d>
struct argument_parser<Eigen::Array<T, 1, d> > {
  static Eigen::Array<T, 1, d> parse(const std::vector<std::string>& tokens) {
    if (tokens.size() != d)
      throw argument_parse_error("invalid number of tokens");
    Eigen::Array<T, 1, d> value;
    for (int i = 0; i < d; ++i) value[i] = boost::lexical_cast<T>(tokens[i]);
    return value;
  }
};

}  // detail

class arguments {
  struct argument {
    argument(const std::string& name) : name(name), used(false) {}

    std::string name;
    std::vector<std::string> tokens;
    bool used;
  };

  typedef std::vector<argument> argument_vector;
  typedef typename argument_vector::iterator argument_iterator;
  typedef std::vector<std::pair<std::string, std::string> > info_vector;
  typedef typename info_vector::const_iterator info_iterator;

 public:
  arguments(int argc, char* argv[]);
  ~arguments();

  template <class T>
  T value(const std::string& name, const std::string& description,
          const T& default_value) {
    T value = default_value;
    argument_iterator i = get_argument(name);
    if (i != arguments_.end()) {
      try {
        value = detail::argument_parser<T>::parse(i->tokens);
      } catch (const argument_parse_error& e) {
        throw argument_parse_error("error while parsing argument '" + name +
                                   "', " + e.what());
      }
    }
    std::stringstream value_stream;
    value_stream << value;
    std::stringstream help_stream;
    help_stream << description << " (default: " << default_value << ")";
    help_strings_.push_back(std::make_pair(name, help_stream.str()));
    value_strings_.push_back(std::make_pair(name, value_stream.str()));
    return value;
  }

  template <class T>
  T value(const std::string& name, const std::string& description,
          const T& default_value, const T& min_value) {
    const T v = value<T>(name, description, default_value);
    if (v < min_value)
      throw argument_value_error("value for argument '" + name +
                                 "' below allowed minimum");
    return v;
  }

  template <class T>
  T value(const std::string& name, const std::string& description,
          const T& default_value, const T& min_value, const T& max_value) {
    const T v = value<T>(name, description, default_value, min_value);
    if (v > max_value)
      throw argument_value_error("value for argument '" + name +
                                 "' above allowed maximum");
    return v;
  }

  bool flag(const std::string& name);
  bool flag(const std::string& name, const std::string& description);

  std::string help_text() const;
  std::string value_text() const;

 private:
  argument_iterator get_argument(const std::string& name);

  argument_vector arguments_;
  info_vector help_strings_, value_strings_;
};

std::ostream& operator<<(std::ostream& out, const arguments& args);

}  // labicex

#endif  //  LABICEX_APPLICATIONS_ARGUMENTS_H

#ifndef LABICEX_BASE_STORAGE_HELPERS_H
#define LABICEX_BASE_STORAGE_HELPERS_H

#include "base/device_helpers.h"
#include "base/kernel_space.h"
#include "base/stream.h"
#include "mpi/distributed_grid.h"

namespace labicex {

namespace kernel {

template <class LocalDomain, class KernelSpace, class StorageAccessor,
          class Function>
LABICEX_KERNEL void storage_generate(const LocalDomain domain,
                                     const KernelSpace space,
                                     StorageAccessor sa,
                                     const Function function) {
  typedef typename LocalDomain::ivector ivector;

  const ivector x = space.kernel_to_local(thread_index<LocalDomain::d>());

  if (!domain.inside(x)) return;

  sa.store(x, function(space, x));
}

template <class StorageAccessor, class Function>
LABICEX_KERNEL void storage_generate(StorageAccessor sa,
                                     const Function function) {
  typedef typename StorageAccessor::ivector ivector;

  const ivector x = thread_index<StorageAccessor::d>();

  if (outside(x, sa.extents())) return;

  sa.store(x, function(kernel_space<StorageAccessor::d>(ivector::Zero(),
                                                        ivector::Zero()),
                       x));
}

template <class Lattice, class LocalDomain, class KernelSpace,
          class PopulationAccessor, class PrimitiveVariablesAccessor>
LABICEX_KERNEL void primitive_variables_from_population(
    const LocalDomain domain, const KernelSpace space,
    const PopulationAccessor population, PrimitiveVariablesAccessor pva) {
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  const ivector x = space.kernel_to_local(thread_index<LocalDomain::d>());

  if (!domain.inside(x)) return;

  fvector f;
  population.load(x, f);
  const primitive_variables pv(f);
  pva.store(x, pv);
}

template <class Lattice, class LocalDomain, class KernelSpace,
          class PopulationAccessor, class PrimitiveVariablesAccessor>
LABICEX_KERNEL void equilibrium_from_primitive_variables(
    const LocalDomain domain, const KernelSpace space,
    const PrimitiveVariablesAccessor pva, PopulationAccessor population) {
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::equilibrium equilibrium;

  const ivector x = space.kernel_to_local(thread_index<LocalDomain::d>());

  if (!domain.inside(x)) return;

  primitive_variables pv;
  pva.load(x, pv);
  const fvector f = equilibrium::get(pv);
  population.store(x, f);
}

template <class Lattice, class PopulationAccessor,
          class PrimitiveVariablesAccessor>
LABICEX_KERNEL void equilibrium_from_primitive_variables(
    const PrimitiveVariablesAccessor pva, PopulationAccessor population) {
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::equilibrium equilibrium;

  const ivector x = thread_index<Lattice::d>();

  if (outside(x, pva.extents())) return;

  primitive_variables pv;
  pva.load(x, pv);
  const fvector f = equilibrium::get(pv);
  population.store(x, f);
}

}  // kernel

template <class Lattice, class Storage, class Function>
void storage_generate(const mpi::distributed_grid<Lattice>& grid,
                      Storage& storage, const Function& function,
                      const stream& s = stream::zero()) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(grid.local().full_domain().extents(),
                                   grid_size, block_size);

  kernel::storage_generate<<<grid_size, block_size, 0, s>>>(
      grid.local().full_domain(), kernel_space<Lattice::d>(grid),
      storage.accessor(), function);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
}

template <class Storage, class Function>
void storage_generate(Storage& storage, const Function& function,
                      const stream& s = stream::zero()) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(storage.extents(), grid_size, block_size);

  kernel::storage_generate<<<grid_size, block_size, 0, s>>>(
      storage.accessor(), function);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
}

template <class Lattice, class PopulationStorage,
          class PrimitiveVariablesStorage>
void primitive_variables_from_population(
    const mpi::distributed_grid<Lattice>& grid, const PopulationStorage& f,
    PrimitiveVariablesStorage& pvs, const stream& s = stream::zero()) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(grid.local().full_domain().extents(),
                                   grid_size, block_size);

  kernel::primitive_variables_from_population<
      Lattice><<<grid_size, block_size, 0, s>>>(
      grid.local().full_domain(), kernel_space<Lattice::d>(grid), f.caccessor(),
      pvs.accessor());
}

template <class Lattice, class PrimitiveVariablesStorage,
          class PopulationStorage>
void equilibrium_from_primitive_variables(
    const mpi::distributed_grid<Lattice>& grid,
    const PrimitiveVariablesStorage& pvs, PopulationStorage& f,
    const stream& s = stream::zero()) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(grid.local().full_domain().extents(),
                                   grid_size, block_size);

  kernel::equilibrium_from_primitive_variables<
      Lattice><<<grid_size, block_size, 0, s>>>(
      grid.local().full_domain(), kernel_space<Lattice::d>(grid),
      pvs.caccessor(), f.accessor());
}

template <class Lattice, class PrimitiveVariablesStorage,
          class PopulationStorage>
void equilibrium_from_primitive_variables(const PrimitiveVariablesStorage& pvs,
                                          PopulationStorage& f,
                                          const stream& s = stream::zero()) {
  dim3 grid_size, block_size;
  get_default_launch_configuration(pvs.extents(), grid_size, block_size);

  kernel::equilibrium_from_primitive_variables<
      Lattice><<<grid_size, block_size, 0, s>>>(pvs.caccessor(),
                                                  f.accessor());
}

}  // labicex

#endif  // LABICEX_BASE_STORAGE_HELPERS_H

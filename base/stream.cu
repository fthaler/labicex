#include "base/device_helpers.h"
#include "base/stream.h"

namespace labicex {

stream::stream() : ptr_(new stream_holder()) {}

stream::stream(const stream&) : ptr_(new stream_holder()) {}

stream::stream(int) {}

void stream::synchronize() const {
  LABICEX_CUDA_CHECK(cudaStreamSynchronize(static_cast<cudaStream_t>(*this)));
}

stream::stream_holder::stream_holder() {
  LABICEX_CUDA_CHECK(cudaStreamCreate(&cuda_stream_));
}

stream::stream_holder::~stream_holder() {
  LABICEX_CUDA_CHECK(cudaStreamDestroy(cuda_stream_));
}

}  // labicex

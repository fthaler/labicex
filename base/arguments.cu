#include "base/arguments.h"

namespace labicex {

arguments::arguments(int argc, char* argv[]) {
  std::vector<std::string> split_argv;
  for (int i = 1; i < argc; ++i) {
    std::string arg(argv[i]);
    int arg_length = arg.size();
    int last_split = -1;
    for (int j = 0; j < arg_length; ++j) {
      if (arg[j] == ' ' || arg[j] == '=') {
        if (j > last_split + 1)
          split_argv.push_back(arg.substr(last_split + 1, j));
        last_split = j;
      }
    }
    if (last_split < arg_length - 1)
      split_argv.push_back(arg.substr(last_split + 1));
  }

  argument* current = NULL;
  for (typename std::vector<std::string>::const_iterator i = split_argv.begin();
       i != split_argv.end(); ++i) {
    const std::string& arg = *i;
    assert(arg.size() > 0);
    if (arg.size() > 2 && arg[0] == '-' && arg[1] == '-') {
      if (current) {
        arguments_.push_back(*current);
        delete current;
        current = NULL;
      }
      if (arg[2] == '-')
        throw argument_parse_error("invalid token '" + arg + "'");
      current = new argument(arg.substr(2));
    } else {
      if (!current) throw argument_parse_error("invalid token '" + arg + "'");
      current->tokens.push_back(arg);
    }
  }

  if (current) {
    arguments_.push_back(*current);
    delete current;
  }
}

arguments::~arguments() {
  for (argument_iterator i = arguments_.begin(); i != arguments_.end(); ++i) {
    if (!i->used)
      std::cerr << "warning: argument '--" << i->name << "' has never been used"
                << std::endl;
  }
}

bool arguments::flag(const std::string& name) {
  return get_argument(name) != arguments_.end();
}

bool arguments::flag(const std::string& name, const std::string& description) {
  bool value = flag(name);
  std::stringstream value_stream;
  value_stream << std::boolalpha << value;
  help_strings_.push_back(
      std::make_pair(name + " (flag)", description + " (default: false)"));
  value_strings_.push_back(std::make_pair(name, value_stream.str()));
  return value;
}

std::string arguments::help_text() const {
  std::size_t max_length = 0;
  for (info_iterator i = help_strings_.begin(); i != help_strings_.end(); ++i)
    max_length = std::max(max_length, i->first.size());
  std::stringstream stream;
  stream << "available arguments:" << std::endl;
  for (info_iterator i = help_strings_.begin(); i != help_strings_.end(); ++i) {
    stream << "  " << std::setw(max_length + 3) << std::left
           << ("--" + i->first + ":") << " " << i->second << std::endl;
  }
  return stream.str();
}

std::string arguments::value_text() const {
  std::size_t max_length = 0;
  for (info_iterator i = value_strings_.begin(); i != value_strings_.end(); ++i)
    max_length = std::max(max_length, i->first.size());
  std::stringstream stream;
  stream << "command line argument values:" << std::endl;
  for (info_iterator i = value_strings_.begin(); i != value_strings_.end();
       ++i) {
    stream << "  " << std::setw(max_length + 1) << std::left << (i->first + ":")
           << " " << i->second << std::endl;
  }
  return stream.str();
}

arguments::argument_iterator arguments::get_argument(const std::string& name) {
  argument_iterator result = arguments_.end();
  for (argument_iterator i = arguments_.begin(); i < arguments_.end(); ++i) {
    if (i->name == name) {
      result = i;
      break;
    }
  }
  if (result == arguments_.end()) {
    std::size_t max_match = 0;
    for (argument_iterator i = arguments_.begin(); i < arguments_.end(); ++i) {
      if (i->name.size() >= max_match && i->name.size() <= name.size() &&
          i->name == name.substr(0, i->name.size())) {
        if (i->name.size() == max_match)
          throw argument_parse_error("ambiguous argument names found");
        result = i;
        max_match = i->name.size();
      }
    }
  }
  if (result != arguments_.end()) {
    if (result->used)
      throw argument_parse_error("ambiguous argument names found");
    else
      result->used = true;
  }
  return result;
}

std::ostream& operator<<(std::ostream& out, const arguments& args) {
  out << args.value_text();
  return out;
}

}  // labicex

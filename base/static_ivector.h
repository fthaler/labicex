#ifndef LABICEX_STATIC_IVECTOR_H
#define LABICEX_STATIC_IVECTOR_H

#include "base/common.h"

namespace labicex {

template <int X, int Y = -1, int Z = -1>
class static_ivector {
 public:
  static const int d = 3;
  static const int static_prod = X * Y * Z;
  static const int static_sum = X + Y + Z;
  typedef typename vector_types<d>::ivector ivector;

  template <int D, int V>
  struct set;
  template <int V>
  struct set<0, V> {
    typedef static_ivector<V, Y, Z> type;
  };
  template <int V>
  struct set<1, V> {
    typedef static_ivector<X, V, Z> type;
  };
  template <int V>
  struct set<2, V> {
    typedef static_ivector<X, Y, V> type;
  };

  template <int D, class Dummy = void>
  struct get;
  template <class Dummy>
  struct get<0, Dummy> {
    static const int value = X;
  };
  template <class Dummy>
  struct get<1, Dummy> {
    static const int value = Y;
  };
  template <class Dummy>
  struct get<2, Dummy> {
    static const int value = Z;
  };

  LABICEX_COMMON_INLINE int operator()(int i) const {
    assert(i >= 0 && i < 3);
    if (i == 0) return X;
    if (i == 1) return Y;
    return Z;
  }

  LABICEX_COMMON_INLINE int size() const { return 3; }
  LABICEX_COMMON_INLINE int prod() const { return static_prod; }
  LABICEX_COMMON_INLINE int sum() const { return static_sum; }

  LABICEX_COMMON_INLINE static ivector as_ivector() { return ivector(X, Y, Z); }
};

template <int X, int Y>
class static_ivector<X, Y, -1> {
 public:
  static const int d = 2;
  static const int static_prod = X * Y;
  static const int static_sum = X + Y;
  typedef typename vector_types<d>::ivector ivector;

  template <int D, int V>
  struct set;
  template <int V>
  struct set<0, V> {
    typedef static_ivector<V, Y, -1> type;
  };
  template <int V>
  struct set<1, V> {
    typedef static_ivector<X, V, -1> type;
  };

  template <int D, class Dummy = void>
  struct get;
  template <class Dummy>
  struct get<0, Dummy> {
    static const int value = X;
  };
  template <class Dummy>
  struct get<1, Dummy> {
    static const int value = Y;
  };

  LABICEX_COMMON_INLINE int operator()(int i) const {
    assert(i >= 0 && i < 2);
    if (i == 0) return X;
    return Y;
  }

  LABICEX_COMMON_INLINE int size() const { return 2; }
  LABICEX_COMMON_INLINE int prod() const { return static_prod; }
  LABICEX_COMMON_INLINE int sum() const { return static_sum; }

  LABICEX_COMMON_INLINE static ivector as_ivector() { return ivector(X, Y); }
};

template <int X>
class static_ivector<X, -1, -1> {
 public:
  static const int d = 1;
  static const int static_prod = X;
  static const int static_sum = X;
  typedef typename vector_types<d>::ivector ivector;

  template <int D, int V>
  struct set;
  template <int V>
  struct set<0, V> {
    typedef static_ivector<V, -1, -1> type;
  };

  template <int D, class Dummy = void>
  struct get;
  template <class Dummy>
  struct get<0, Dummy> {
    static const int value = X;
  };

  LABICEX_COMMON_INLINE int operator()(int i) const {
    assert(i == 0);
    return X;
  }

  LABICEX_COMMON_INLINE int size() const { return 1; }
  LABICEX_COMMON_INLINE int prod() const { return static_prod; }
  LABICEX_COMMON_INLINE int sum() const { return static_sum; }

  LABICEX_COMMON_INLINE static ivector as_ivector() { return ivector(X); }
};

template <int D>
struct static_ivector_maker;

template <>
struct static_ivector_maker<1> {
  template <int C>
  struct constant {
    typedef static_ivector<C> type;
  };
};

template <>
struct static_ivector_maker<2> {
  template <int C>
  struct constant {
    typedef static_ivector<C, C> type;
  };
};

template <>
struct static_ivector_maker<3> {
  template <int C>
  struct constant {
    typedef static_ivector<C, C, C> type;
  };
};

template <class Ivector>
struct static_ivector_caster {
  typedef const Ivector& return_type;
  LABICEX_COMMON_INLINE static return_type cast(const Ivector& x) { return x; }
};

template <int X, int Y, int Z>
struct static_ivector_caster<static_ivector<X, Y, Z> > {
  typedef typename static_ivector<X, Y, Z>::ivector return_type;
  LABICEX_COMMON_INLINE static return_type cast(
      const static_ivector<X, Y, Z>&) {
    return static_ivector<X, Y, Z>::as_ivector();
  }
};

template <class Ivector>
typename static_ivector_caster<Ivector>::return_type ivector_cast(
    const Ivector& x) {
  return static_ivector_caster<Ivector>::cast(x);
}

}  // labicex

#endif  // LABICEX_STATIC_IVECTOR_H

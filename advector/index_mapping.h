#ifndef LABICEX_ADVECTOR_INDEX_MAPPING_H
#define LABICEX_ADVECTOR_INDEX_MAPPING_H

#include <cassert>

#include "base/common.h"

namespace labicex {

namespace index_mapping {

template <class Lattice>
class dummy {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;

  LABICEX_COMMON_INLINE ivector operator()(const ivector& x) const { return x; }
};

template <class Lattice>
class periodic {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;

  periodic(const ivector& extents) : extents_(extents) {}

  LABICEX_COMMON_INLINE ivector operator()(ivector x) const {
    LABICEX_UNROLL
    for (int i = 0; i < lattice::d; ++i)
      x(i) = x(i) < 0 ? x(i) + extents_(i)
                      : x(i) >= extents_(i) ? x(i) - extents_(i) : x(i);
    assert(!outside(x, extents_));
    return x;
  }

 private:
  ivector extents_;
};

template <class Lattice, class Extents>
class fixed_size_periodic {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;

  LABICEX_COMMON_INLINE ivector operator()(ivector x) const {
    Extents e;
    LABICEX_UNROLL
    for (int i = 0; i < lattice::d; ++i)
      x(i) = x(i) < 0 ? x(i) + e(i) : x(i) >= e(i) ? x(i) - e(i) : x(i);
    assert(!outside(x, e));
    return x;
  }
};

}  // index_mapping

}  // labicex

#endif  // LABICEX_ADVECTOR_INDEX_MAPPING_H

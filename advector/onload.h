#ifndef LABICEX_ADVECTOR_ONLOAD_H
#define LABICEX_ADVECTOR_ONLOAD_H

#include "advector/index_mapping.h"
#include "base/common.h"

namespace labicex {

namespace advector {

template <class Lattice, class IndexMapper = index_mapping::dummy<Lattice> >
class onload {
 public:
  typedef Lattice lattice;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef IndexMapper index_mapper;

  explicit onload(const index_mapper& mapper = index_mapper())
      : mapper_(mapper) {}

  template <class PopulationAccessor>
  LABICEX_COMMON_INLINE fvector load(const PopulationAccessor& population,
                                     const ivector& x) const {
    fvector f;
    LABICEX_UNROLL
    for (int i = 0; i < lattice::q; ++i)
      f(i) = population.load_single(i, mapper_(x - lattice::ci(i)));
    return f;
  }

  template <class PopulationAccessor>
  LABICEX_COMMON_INLINE void store(PopulationAccessor& population,
                                   const ivector& x, const fvector& f) const {
    population.store(x, f);
  }

 private:
  index_mapper mapper_;
};

}  // advector

}  // labicex

#endif  // LABICEX_ADVECTOR_ONLOAD_H

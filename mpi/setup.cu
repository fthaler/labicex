#include <iostream>

#include "mpi/setup.h"

namespace labicex {

namespace mpi {

setup::setup(int& argc, char**& argv) {
  MPI_Init(&argc, &argv);
  set_cout_filter(true);
}

setup::~setup() {
  set_cout_filter(false);
  MPI_Finalize();
}

int setup::global_size() {
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  return size;
}

int setup::global_rank() {
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
}

void setup::global_barrier() { MPI_Barrier(MPI_COMM_WORLD); }

void setup::global_abort(int code) {
  if (code == EXIT_SUCCESS) {
    MPI_Finalize();
    std::exit(0);
  } else {
    MPI_Abort(MPI_COMM_WORLD, code);
  }
}

void setup::set_cout_filter(bool filter) {
  if (filter && global_rank() != 0)
    std::cout.setstate(std::ios_base::failbit);
  else
    std::cout.clear();
}

}  // mpi

}  // labicex

#ifndef LABICEX_MPI_SETUP_H
#define LABICEX_MPI_SETUP_H

#include <cstdlib>

#include <mpi.h>

#include "base/common.h"

namespace labicex {

namespace mpi {

template <class T>
struct mpi_type;

template <>
struct mpi_type<float> {
  static MPI_Datatype type() { return MPI_FLOAT; }
};

template <>
struct mpi_type<double> {
  static MPI_Datatype type() { return MPI_DOUBLE; }
};

class setup {
 public:
  setup(int& argc, char**& argv);
  ~setup();

  static int global_size();
  static int global_rank();
  static void global_barrier();
  static void global_abort(int code = EXIT_FAILURE);

  static void set_cout_filter(bool filter);

  template <int D>
  static typename vector_types<D>::ivector dims(int free_dims = D) {
    int dims[D];
    for (int i = 0; i < D; ++i) dims[i] = i < free_dims ? 0 : 1;
    MPI_Dims_create(global_size(), D, dims);
    typename vector_types<D>::ivector idims;
    for (int i = 0; i < D; ++i) idims(i) = dims[i];
    return idims;
  }

 private:
  setup(const setup&);
  setup& operator=(const setup&);
};

}  // mpi

}  // labicex

#endif  // LABICEX_MPI_SETUP_H

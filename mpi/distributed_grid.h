#ifndef LABICEX_MPI_DISTRIBUTED_GRID_H
#define LABICEX_MPI_DISTRIBUTED_GRID_H

#include "base/domain.h"
#include "mpi/process_grid.h"

namespace labicex {

namespace mpi {

template <class Lattice>
class distributed_grid_perspective {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  static const int d = lattice::d;
  typedef domain::box<d> box;
  typedef domain::box_halo<d> box_halo;

  box inner_domain() const {
    return box(full_domain_.min() + 2 * halo_width(),
               full_domain_.max() - 2 * halo_width());
  }

  box_halo outer_domain() const {
    return box_halo(inner_domain(), nohalo_domain());
  }

  box nohalo_domain() const {
    return box(full_domain_.min() + halo_width(),
               full_domain_.max() - halo_width());
  }

  box_halo halo_domain() const {
    return box_halo(nohalo_domain(), full_domain_);
  }

  const box& full_domain() const { return full_domain_; }

  const box& global_domain() const { return global_domain_; }

  int halo_width() const { return lattice::max_ci; }

 private:
  distributed_grid_perspective() {}
  distributed_grid_perspective(const box& full_domain, const box& global_domain)
      : full_domain_(full_domain), global_domain_(global_domain) {}

  template <class>
  friend class distributed_grid;

  box full_domain_, global_domain_;
};

template <class Lattice>
class distributed_grid {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  static const int d = lattice::d;
  typedef distributed_grid_perspective<lattice> perspective;
  typedef domain::box<d> box;

  distributed_grid(const process_grid<d>& p, const ivector& local_extents)
      : processes_(p) {
    if ((local_extents - 4 * halo_width() < 1).any())
      throw std::invalid_argument("local domain size too small");
    const ivector nohalo_extents = local_extents - 2 * halo_width();
    const ivector local_offsets =
        processes_.local_process().location() * nohalo_extents - halo_width();
    const ivector global_extents = processes_.extents() * nohalo_extents;

    local_ = perspective(box(local_extents),
                         box(-local_offsets, -local_offsets + global_extents));

    global_ = perspective(box(local_offsets, local_offsets + local_extents),
                          box(global_extents));
  }

  const process_grid<d>& processes() const { return processes_; }

  const perspective& local() const { return local_; }

  const perspective& global() const { return global_; }

  static int halo_width() { return lattice::max_ci; }

  static ivector local_extents_from_global(const process_grid<d>& p,
                                           const ivector& global_extents,
                                           bool exact = true) {
    ivector local_extents = (global_extents / p.extents()) + 2 * halo_width();
    if (!exact) {
      const int new_width = ((local_extents(0) + 31) / 32) * 32;
      local_extents = (local_extents * new_width) / local_extents(0);
    }
    return local_extents;
  }

 private:
  const process_grid<d>& processes_;
  perspective local_, global_;
};

}  // mpi

}  // labicex

#endif  // LABICEX_MPI_DISTRIBUTED_GRID_H

#ifndef LABICEX_MPI_HALO_EXCHANGER_H
#define LABICEX_MPI_HALO_EXCHANGER_H

#include <algorithm>
#include <map>
#include <vector>

#include <thrust/device_vector.h>

#include "base/domain.h"
#include "base/stream.h"
#include "mpi/distributed_grid.h"
#include "mpi/setup.h"

namespace labicex {

namespace mpi {

namespace kernel {

template <int D>
struct halo_domain {
  domain::box<D> box;
  real* __restrict__ data;
  int i;
};

template <int D>
struct halo_container {
  static const int d = D;
  static const int max_size = 64;

  halo_domain<d> halos[max_size];
  int size;
};

template <class HaloContainer, class PopulationAccessor>
LABICEX_KERNEL void pack_halos(const HaloContainer hc,
                               const PopulationAccessor f) {
  typedef typename vector_types<HaloContainer::d>::ivector ivector;

  const ivector x_halo = thread_index<HaloContainer::d>();

  LABICEX_UNROLL
  for (int i = 0; i < HaloContainer::max_size; ++i) {
    if (i < hc.size) {
      const ivector x = x_halo + hc.halos[i].box.min();

      if (hc.halos[i].box.inside(x)) {
        const real fi = f.load_single(hc.halos[i].i, x);

        const ivector extents = hc.halos[i].box.extents();

        int index = x_halo(0);
        int stride = extents(0);
        LABICEX_UNROLL
        for (int j = 1; j < HaloContainer::d; ++j) {
          index += stride * x_halo(j);
          stride *= extents(j);
        }
        assert(index >= 0 && index < extents.prod());

        hc.halos[i].data[index] = fi;
      }
    }
  }
}

template <class HaloContainer, class PopulationAccessor>
LABICEX_KERNEL void unpack_halos(const HaloContainer hc, PopulationAccessor f) {
  typedef typename vector_types<HaloContainer::d>::ivector ivector;

  const ivector x_halo = thread_index<HaloContainer::d>();

  LABICEX_UNROLL
  for (int i = 0; i < HaloContainer::max_size; ++i) {
    if (i < hc.size) {
      const ivector x = x_halo + hc.halos[i].box.min();

      if (hc.halos[i].box.inside(x)) {
        const ivector extents = hc.halos[i].box.extents();

        int index = x_halo(0);
        int stride = extents(0);
        LABICEX_UNROLL
        for (int j = 1; j < HaloContainer::d; ++j) {
          index += stride * x_halo(j);
          stride *= extents(j);
        }
        assert(index >= 0 && index < extents.prod());

        const real fi = hc.halos[i].data[index];
        f.store_single(hc.halos[i].i, x, fi);
      }
    }
  }
}

}  // kernel

namespace detail {

template <int D>
class halo {
 public:
  halo(int rank, int id, int i, const domain::box<D>& box)
      : box_(box), data_(NULL), rank_(rank), id_(id), i_(i) {}

  kernel::halo_domain<D> kernel_halo() const {
    assert(data_ != NULL);
    kernel::halo_domain<D> h = {box_, data_, i_};
    return h;
  }

  void set_data(real* data) { data_ = data; }

  bool operator<(const halo& other) const { return id_ < other.id_; }

  const domain::box<D>& box() const { return box_; }
  int rank() const { return rank_; }
  int id() const { return id_; }
  int i() const { return i_; }

 private:
  domain::box<D> box_;
  real* data_;
  int rank_, id_, i_;
};

template <int D>
class halo_collection {
 public:
  typedef typename vector_types<D>::ivector ivector;
  typedef std::vector<halo<D> > halo_vector;
  typedef typename halo_vector::iterator halo_vector_iterator;
  typedef typename halo_vector::const_iterator halo_vector_const_iterator;
  typedef std::map<int, halo_vector> halo_map;
  typedef typename halo_map::iterator halo_map_iterator;
  typedef typename halo_map::const_iterator halo_map_const_iterator;
  typedef std::vector<kernel::halo_container<D> > kernel_vector;
  typedef typename kernel_vector::iterator kernel_vector_iterator;
  typedef std::map<int, thrust::device_vector<real> > buffer_map;
  typedef typename buffer_map::iterator buffer_map_iterator;
  typedef typename buffer_map::const_iterator buffer_map_const_iterator;

  halo_collection(int local_rank) : local_rank_(local_rank) {}

  void add_halo(const halo<D>& h) { halos_[h.rank()].push_back(h); }

  void init_buffers();

  kernel_vector kernel_halo_containers() const;

  void send_remote_buffers(MPI_Comm comm);
  void recv_remote_buffers(MPI_Comm comm);
  void wait();

  void copy_local_buffers(const halo_collection<D>& src, const stream& s);

 private:
  struct extents_compare {
    bool operator()(const ivector& a, const ivector& b) const {
      for (int i = 0; i < D; ++i)
        if (a(i) != b(i)) return a(i) < b(i);
      return false;
    }
  };

  int local_rank_;
  halo_map halos_;
  buffer_map buffers_;
  std::vector<MPI_Request> reqs_;
};

template <int D>
void halo_collection<D>::init_buffers() {
  for (halo_map_iterator i = halos_.begin(); i != halos_.end(); ++i) {
    std::sort(i->second.begin(), i->second.end());

    int total_size = 0;
    for (halo_vector_const_iterator j = i->second.begin(); j != i->second.end();
         ++j)
      total_size += j->box().extents().prod();

    buffers_[i->first].resize(total_size);
  }

  for (halo_map_iterator i = halos_.begin(); i != halos_.end(); ++i) {
    int offset = 0;
    for (halo_vector_iterator j = i->second.begin(); j != i->second.end();
         ++j) {
      j->set_data(thrust::raw_pointer_cast(&buffers_[i->first][offset]));
      offset += j->box().extents().prod();
    }
  }
}

template <int D>
typename halo_collection<D>::kernel_vector
halo_collection<D>::kernel_halo_containers() const {
  typedef std::map<ivector, halo_vector, extents_compare> halo_extents_map;
  typedef typename halo_extents_map::iterator halo_extents_map_iterator;

  halo_extents_map extents_map;
  for (halo_map_const_iterator i = halos_.begin(); i != halos_.end(); ++i) {
    for (halo_vector_const_iterator j = i->second.begin(); j != i->second.end();
         ++j)
      extents_map[j->box().extents()].push_back(*j);
  }

  kernel_vector v;
  for (halo_extents_map_iterator i = extents_map.begin();
       i != extents_map.end(); ++i) {
    kernel::halo_container<D> container;
    int container_index = 0;
    for (halo_vector_const_iterator j = i->second.begin(); j != i->second.end();
         ++j) {
      assert(container_index < kernel::halo_container<D>::max_size);
      container.halos[container_index++] = j->kernel_halo();
    }
    container.size = container_index;
    v.push_back(container);
  }
  return v;
}

template <int D>
void halo_collection<D>::send_remote_buffers(MPI_Comm comm) {
  assert(reqs_.empty());
  reqs_.reserve(buffers_.size());
  for (buffer_map_iterator i = buffers_.begin(); i != buffers_.end(); ++i) {
    if (i->first != local_rank_) {
      reqs_.push_back(MPI_Request());
      MPI_Isend(thrust::raw_pointer_cast(i->second.data()), i->second.size(),
                mpi_type<real>::type(), i->first, 456, comm, &reqs_.back());
    }
  }
}

template <int D>
void halo_collection<D>::recv_remote_buffers(MPI_Comm comm) {
  assert(reqs_.empty());
  const int rank = mpi::setup::global_rank();
  reqs_.reserve(buffers_.size());
  for (buffer_map_iterator i = buffers_.begin(); i != buffers_.end(); ++i) {
    if (i->first != local_rank_) {
      reqs_.push_back(MPI_Request());
      MPI_Irecv(thrust::raw_pointer_cast(i->second.data()), i->second.size(),
                mpi_type<real>::type(), i->first, 456, comm, &reqs_.back());
    }
  }
}

template <int D>
void halo_collection<D>::wait() {
  if (reqs_.empty()) return;
  MPI_Waitall(reqs_.size(), &reqs_[0], MPI_STATUSES_IGNORE);
  reqs_.clear();
}

template <int D>
void halo_collection<D>::copy_local_buffers(const halo_collection<D>& src,
                                            const stream& s) {
  for (buffer_map_iterator i = buffers_.begin(); i != buffers_.end(); ++i) {
    if (i->first == local_rank_) {
      for (buffer_map_const_iterator j = src.buffers_.begin();
           j != src.buffers_.end(); ++j) {
        if (j->first == local_rank_) {
          assert(i->second.size() == j->second.size());
          LABICEX_CUDA_CHECK(cudaMemcpyAsync(
              thrust::raw_pointer_cast(i->second.data()),
              thrust::raw_pointer_cast(j->second.data()),
              sizeof(real) * i->second.size(), cudaMemcpyDeviceToDevice, s));
          return;
        }
      }
    }
  }
}

}  // detail

template <class Lattice>
class halo_exchanger {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  static const int d = lattice::d;

  halo_exchanger(const distributed_grid<lattice>& grid);

  template <class PopulationStorage>
  void start_exchange(PopulationStorage& f);
  template <class PopulationStorage, class PrimitiveVariablesStorage>
  void start_exchange(PopulationStorage& f, PrimitiveVariablesStorage& pv);
  template <class PopulationStorage>
  void finish_exchange(PopulationStorage& f);
  template <class PopulationStorage, class PrimitiveVariablesStorage>
  void finish_exchange(PopulationStorage& f, PrimitiveVariablesStorage& pv);

  const distributed_grid<lattice>& grid() const { return grid_; }

 private:
  typedef typename detail::halo_collection<d>::kernel_vector kernel_vector;
  typedef typename kernel_vector::const_iterator kernel_vector_const_iterator;

  static int direction_index(const ivector& direction) {
    int index = direction(0) + 1, stride = 3;
    for (int i = 1; i < direction.size(); ++i) {
      index += stride * (direction(i) + 1);
      stride *= 3;
    }
    assert(index >= 0 && index < stride);
    return index;
  }

  void synchronize() const {
    for (std::vector<stream>::const_iterator i = pack_streams_.begin();
         i < pack_streams_.end(); ++i)
      i->synchronize();
  }

  static ivector unitize(const ivector& v) {
    ivector u;
    for (int i = 0; i < d; ++i) u(i) = v(i) < 0 ? -1 : v(i) > 0 ? 1 : 0;
    return u;
  }

  halo_exchanger(const halo_exchanger&);
  halo_exchanger& operator=(const halo_exchanger&);

  const distributed_grid<lattice>& grid_;
  std::vector<stream> pack_streams_;

  // population data
  detail::halo_collection<d> send_halos_, recv_halos_;
  kernel_vector send_kernel_data_, recv_kernel_data_;

  // primitive variables data
  detail::halo_collection<d> pv_send_halos_, pv_recv_halos_;
  kernel_vector pv_send_kernel_data_, pv_recv_kernel_data_;
};

template <class Lattice>
halo_exchanger<Lattice>::halo_exchanger(const distributed_grid<lattice>& grid)
    : grid_(grid),
      pack_streams_(5),
      send_halos_(grid.processes().local_process().rank()),
      recv_halos_(grid.processes().local_process().rank()),
      pv_send_halos_(grid.processes().local_process().rank()),
      pv_recv_halos_(grid.processes().local_process().rank()) {
  typedef
      typename process_grid<d>::neighbors_container::const_iterator nb_iterator;
  const typename process_grid<d>::neighbors_container& neighbors =
      grid_.processes().neighbors();

  // get population halo domains
  for (int i = 0; i < lattice::q; ++i) {
    const ivector ci = lattice::ci(i);
    for (nb_iterator nb = neighbors.begin(); nb < neighbors.end(); ++nb) {
      // send halo
      if (unitize(ci).matrix().dot(nb->direction().matrix()) > 0) {
        ivector halo_min, halo_max;

        for (int j = 0; j < d; ++j) {
          if (nb->direction()(j) == 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          } else if (nb->direction()(j) < 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) = grid_.local().nohalo_domain().min()(j) + abs(ci(j));
          } else {
            halo_min(j) = grid_.local().nohalo_domain().max()(j) - abs(ci(j));
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          }
        }

        if ((halo_max - halo_min).prod() > 0) {
          const int id = i + direction_index(nb->direction()) * lattice::q;
          send_halos_.add_halo(detail::halo<d>(
              nb->rank(), id, i, domain::box<d>(halo_min, halo_max)));
        }
      }

      // recv halo
      if (unitize(ci).matrix().dot(nb->direction().matrix()) < 0) {
        ivector halo_min, halo_max;

        for (int j = 0; j < d; ++j) {
          if (nb->direction()(j) == 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          } else if (nb->direction()(j) < 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j) - abs(ci(j));
            halo_max(j) = grid_.local().nohalo_domain().min()(j);
          } else {
            halo_min(j) = grid_.local().nohalo_domain().max()(j);
            halo_max(j) = grid_.local().nohalo_domain().max()(j) + abs(ci(j));
          }
        }

        if ((halo_max - halo_min).prod() > 0) {
          const int id = i + direction_index(-nb->direction()) * lattice::q;
          recv_halos_.add_halo(detail::halo<d>(
              nb->rank(), id, i, domain::box<d>(halo_min, halo_max)));
        }
      }
    }
  }

  send_halos_.init_buffers();
  recv_halos_.init_buffers();
  send_kernel_data_ = send_halos_.kernel_halo_containers();
  recv_kernel_data_ = recv_halos_.kernel_halo_containers();

  // get primitive variables halo domains
  for (int k = 0; k < lattice::primitive_variables::count; ++k) {
    for (nb_iterator nb = neighbors.begin(); nb < neighbors.end(); ++nb) {
      // send halo
      {
        ivector halo_min, halo_max;
        for (int j = 0; j < d; ++j) {
          if (nb->direction()(j) == 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          } else if (nb->direction()(j) < 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) =
                grid_.local().nohalo_domain().min()(j) + grid_.halo_width();
          } else {
            halo_min(j) =
                grid_.local().nohalo_domain().max()(j) - grid_.halo_width();
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          }
        }

        if ((halo_max - halo_min).prod() > 0) {
          const int id = k +
                         direction_index(nb->direction()) *
                             lattice::primitive_variables::count;
          pv_send_halos_.add_halo(detail::halo<d>(
              nb->rank(), id, k, domain::box<d>(halo_min, halo_max)));
        }
      }

      // recv halo
      {
        ivector halo_min, halo_max;
        for (int j = 0; j < d; ++j) {
          if (nb->direction()(j) == 0) {
            halo_min(j) = grid_.local().nohalo_domain().min()(j);
            halo_max(j) = grid_.local().nohalo_domain().max()(j);
          } else if (nb->direction()(j) < 0) {
            halo_min(j) =
                grid_.local().nohalo_domain().min()(j) - grid_.halo_width();
            halo_max(j) = grid_.local().nohalo_domain().min()(j);
          } else {
            halo_min(j) = grid_.local().nohalo_domain().max()(j);
            halo_max(j) =
                grid_.local().nohalo_domain().max()(j) + grid_.halo_width();
          }
        }

        if ((halo_max - halo_min).prod() > 0) {
          const int id = k +
                         direction_index(-nb->direction()) *
                             lattice::primitive_variables::count;
          pv_recv_halos_.add_halo(detail::halo<d>(
              nb->rank(), id, k, domain::box<d>(halo_min, halo_max)));
        }
      }
    }
  }

  pv_send_halos_.init_buffers();
  pv_recv_halos_.init_buffers();
  pv_send_kernel_data_ = pv_send_halos_.kernel_halo_containers();
  pv_recv_kernel_data_ = pv_recv_halos_.kernel_halo_containers();
}

template <class Lattice>
template <class PopulationStorage>
void halo_exchanger<Lattice>::start_exchange(PopulationStorage& f) {
  send_halos_.wait();
  synchronize();

  int stream_index = 0;
  // pack population halos
  for (kernel_vector_const_iterator i = send_kernel_data_.begin();
       i != send_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::
        pack_halos<<<grid_size, block_size, 0, pack_streams_[stream_index]>>>(
            *i, f.caccessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }

  synchronize();

  send_halos_.send_remote_buffers(grid_.processes().comm());
  recv_halos_.recv_remote_buffers(grid_.processes().comm());
  recv_halos_.copy_local_buffers(send_halos_, pack_streams_[0]);
}

template <class Lattice>
template <class PopulationStorage, class PrimitiveVariablesStorage>
void halo_exchanger<Lattice>::start_exchange(PopulationStorage& f,
                                             PrimitiveVariablesStorage& pv) {
  send_halos_.wait();
  pv_send_halos_.wait();
  synchronize();

  int stream_index = 0;
  // pack population halos
  for (kernel_vector_const_iterator i = send_kernel_data_.begin();
       i != send_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::
        pack_halos<<<grid_size, block_size, 0, pack_streams_[stream_index]>>>(
            *i, f.caccessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }

  // pack primitive variables halos
  for (kernel_vector_const_iterator i = pv_send_kernel_data_.begin();
       i != pv_send_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::
        pack_halos<<<grid_size, block_size, 0, pack_streams_[stream_index]>>>(
            *i, pv.caccessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }
  synchronize();

  send_halos_.send_remote_buffers(grid_.processes().comm());
  recv_halos_.recv_remote_buffers(grid_.processes().comm());
  pv_send_halos_.send_remote_buffers(grid_.processes().comm());
  pv_recv_halos_.recv_remote_buffers(grid_.processes().comm());
  recv_halos_.copy_local_buffers(send_halos_, pack_streams_[0]);
  pv_recv_halos_.copy_local_buffers(pv_send_halos_, pack_streams_[1]);
}

template <class Lattice>
template <class PopulationStorage>
void halo_exchanger<Lattice>::finish_exchange(PopulationStorage& f) {
  synchronize();
  recv_halos_.wait();

  int stream_index = 0;
  // unpack population halos
  for (kernel_vector_const_iterator i = recv_kernel_data_.begin();
       i != recv_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::unpack_halos<<<grid_size, block_size, 0,
                           pack_streams_[stream_index]>>>(*i, f.accessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }

  synchronize();
}

template <class Lattice>
template <class PopulationStorage, class PrimitiveVariablesStorage>
void halo_exchanger<Lattice>::finish_exchange(PopulationStorage& f,
                                              PrimitiveVariablesStorage& pv) {
  synchronize();
  recv_halos_.wait();
  pv_recv_halos_.wait();

  int stream_index = 0;
  // unpack population halos
  for (kernel_vector_const_iterator i = recv_kernel_data_.begin();
       i != recv_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::unpack_halos<<<grid_size, block_size, 0,
                           pack_streams_[stream_index]>>>(*i, f.accessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }

  // unpack primitive variables halos
  for (kernel_vector_const_iterator i = pv_recv_kernel_data_.begin();
       i != pv_recv_kernel_data_.end(); ++i) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(i->halos[0].box.extents(), grid_size,
                                     block_size);
    kernel::unpack_halos<<<grid_size, block_size, 0,
                           pack_streams_[stream_index]>>>(*i, pv.accessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    stream_index = (stream_index + 1) % pack_streams_.size();
  }

  synchronize();
}

}  // mpi

}  // labicex

#endif  // LABICEX_MPI_HALO_EXCHANGER_H

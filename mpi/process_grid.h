#ifndef LABICEX_MPI_PROCESS_GRID_H
#define LABICEX_MPI_PROCESS_GRID_H

#include <vector>

#include <mpi.h>

#include "base/common.h"
#include "base/domain.h"

namespace labicex {

namespace mpi {

template <int D>
class neighbor_process {
 public:
  static const int d = D;
  typedef typename vector_types<d>::ivector ivector;

  const ivector& location() const { return location_; }
  const ivector& direction() const { return direction_; }
  int rank() const { return rank_; }

 private:
  template <int>
  friend class process_grid;

  neighbor_process(const ivector& location, const ivector& direction,
                   int rank = -1)
      : location_(location), direction_(direction), rank_(rank) {}
  neighbor_process() {}

  ivector location_, direction_;
  int rank_;
};

template <int D>
class process_grid {
 public:
  static const int d = D;
  typedef typename vector_types<d>::ivector ivector;
  typedef typename vector_types<d>::bvector bvector;
  typedef neighbor_process<d> neighbor;
  typedef std::vector<neighbor_process<d> > neighbors_container;

  process_grid(const ivector& extents, const bvector& periodic,
               MPI_Comm parent_comm = MPI_COMM_WORLD);
  ~process_grid();

  const ivector& extents() const { return extents_; }
  const bvector& periodic() const { return periodic_; }
  const neighbor_process<d>& local_process() const { return local_process_; }
  const neighbors_container& neighbors() const { return neighbors_; }
  int size() const { return extents_.prod(); }
  const neighbor_process<d>* neighbor_with_direction(
      const ivector& direction) const {
    for (std::size_t i = 0; i < neighbors_.size(); ++i) {
      if ((neighbors_[i].direction() == direction).all()) return &neighbors_[i];
    }
    return NULL;
  }
  MPI_Comm comm() const { return comm_; }

 private:
  process_grid(const process_grid&);
  process_grid& operator=(const process_grid&);

  ivector strides() const {
    ivector s;
    s(0) = 1;
    for (int i = 1; i < d; ++i) s(i) = s(i - 1) * extents_(i - 1);
    return s;
  }

  ivector location_from_rank(int rank) const {
    assert(rank >= 0 && rank < extents_.prod());
    const ivector s = strides();
    ivector l;
    for (int i = d - 1; i >= 0; --i) {
      l(i) = rank / s(i);
      assert(l(i) >= 0 && l(i) < extents_(i));
      rank -= l(i) * s(i);
    }
    return l;
  }

  int rank_from_location(const ivector& location) const {
    assert((location >= 0).all() && (location < extents_).all());
    return (location * strides()).sum();
  }

  ivector extents_;
  bvector periodic_;
  neighbor_process<d> local_process_;
  neighbors_container neighbors_;
  MPI_Comm comm_;
  MPI_Group neighbor_group_;
};

template <int D>
process_grid<D>::process_grid(const ivector& extents, const bvector& periodic,
                              MPI_Comm parent_comm)
    : extents_(extents), periodic_(periodic) {
  int size, rank;
  MPI_Comm_size(parent_comm, &size);
  MPI_Comm_rank(parent_comm, &rank);

  assert(extents.prod() == size);
  local_process_ =
      neighbor_process<d>(location_from_rank(rank), ivector::Zero(), rank);

  const domain::box<d> direction_box(ivector::Constant(-1),
                                     ivector::Constant(2));
  std::vector<int> nb_ranks, nb_weights;
  for (typename domain::box<d>::iterator nb_direction = direction_box.begin();
       nb_direction != direction_box.end(); ++nb_direction) {
    if ((*nb_direction != 0).any()) {
      ivector nb_location = local_process_.location() + *nb_direction;

      bool nb_valid = true;
      for (int i = 0; i < d; ++i)
        if (nb_location(i) < 0 || nb_location(i) >= extents_(i)) {
          if (periodic_(i))
            nb_location(i) = nb_location(i) < 0 ? extents_(i) - 1 : 0;
          else
            nb_valid = false;
        }

      if (nb_valid) {
        neighbors_.push_back(neighbor_process<d>(nb_location, *nb_direction));
        nb_ranks.push_back(rank_from_location(nb_location));
        nb_weights.push_back(d + 1 - nb_direction->abs().sum());
      }
    }
  }

  MPI_Dist_graph_create_adjacent(parent_comm, neighbors_.size(), &nb_ranks[0],
                                 &nb_weights[0], neighbors_.size(),
                                 &nb_ranks[0], &nb_weights[0], MPI_INFO_NULL,
                                 true, &comm_);

  std::vector<int> in_ranks(neighbors_.size()), in_weights(neighbors_.size()),
      out_ranks(neighbors_.size()), out_weights(neighbors_.size());
  MPI_Dist_graph_neighbors(comm_, neighbors_.size(), &in_ranks[0],
                           &in_weights[0], neighbors_.size(), &out_ranks[0],
                           &out_weights[0]);
  assert(in_ranks == out_ranks && in_weights == out_weights);

  for (std::size_t i = 0; i < neighbors_.size(); ++i)
    neighbors_[i].rank_ = in_ranks[i];

  std::sort(in_ranks.begin(), in_ranks.end());
  std::vector<int> unique_ranks;
  unique_ranks.reserve(in_ranks.size());
  std::unique_copy(in_ranks.begin(), in_ranks.end(),
                   std::back_inserter(unique_ranks));
  MPI_Group comm_group;
  MPI_Comm_group(comm_, &comm_group);
  MPI_Group_incl(comm_group, unique_ranks.size(), &unique_ranks[0],
                 &neighbor_group_);
  MPI_Group_free(&comm_group);
}

template <int D>
process_grid<D>::~process_grid() {
  MPI_Group_free(&neighbor_group_);
  MPI_Comm_free(&comm_);
}

}  // mpi

}  // labicex

#endif  // LABICEX_MPI_PROCESS_GRID_H

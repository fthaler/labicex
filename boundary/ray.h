#ifndef LABICEX_BOUNDARY_RAY_H
#define LABICEX_BOUNDARY_RAY_H

#include "base/common.h"

namespace labicex {

namespace boundary {

template <int D>
class ray {
 public:
  static const int d = D;
  typedef typename vector_types<d>::rvector rvector;

  LABICEX_COMMON_INLINE ray(const rvector& origin, const rvector& direction,
                            real max_distance)
      : origin_(origin),
        direction_(direction.matrix().normalized().array()),
        max_distance_(max_distance) {
    assert((direction != rvector::Zero()).any());
  }

  LABICEX_COMMON_INLINE const rvector& origin() const { return origin_; }
  LABICEX_COMMON_INLINE const rvector& direction() const { return direction_; }
  LABICEX_COMMON_INLINE real max_distance() const { return max_distance_; }

 private:
  rvector origin_, direction_;
  real max_distance_;
};

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_RAY_H

#ifndef LABICEX_BOUNDARY_STANDARD_LATTICES_H
#define LABICEX_BOUNDARY_STANDARD_LATTICES_H

#include "base/common.h"
#include "base/inline_math.h"
#include "base/kernel_space.h"
#include "lattice/tags.h"

namespace labicex {

namespace boundary {

template <class Lattice, class BoundaryMapAccessor>
class noslip_updater {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::fvector fvector;
  static const bool uses_primitive_variables = true;

  noslip_updater(const BoundaryMapAccessor& bma) : bma_(bma) {}

  template <class PopulationAccessor, class PrimitiveVariablesAccessor>
  LABICEX_COMMON_INLINE void update(const kernel_space<Lattice::d>& space,
                                    const ivector& x,
                                    const PopulationAccessor& population,
                                    const PrimitiveVariablesAccessor& pva,
                                    real beta, fvector& f) const {
    update_impl(space, x, population, pva, beta, f,
                typename Lattice::lattice_category());
  }

 private:
  template <class PopulationAccessor, class PrimitiveVariablesAccessor>
  LABICEX_COMMON_INLINE void update_impl(const kernel_space<Lattice::d>& space,
                                         const ivector& x,
                                         const PopulationAccessor&,
                                         const PrimitiveVariablesAccessor& pva,
                                         real beta, fvector& f,
                                         lattice::standard_tag) const;

  template <class PopulationAccessor, class PrimitiveVariablesAccessor>
  LABICEX_COMMON_INLINE void update_impl(const kernel_space<Lattice::d>& space,
                                         const ivector& x,
                                         const PopulationAccessor&,
                                         const PrimitiveVariablesAccessor& pva,
                                         real beta, fvector& f,
                                         lattice::thermal_tag) const;

  LABICEX_COMMON_INLINE static real p1(int a, int b, real rho, const rvector& u,
                                       real t, const rtensor& du, real beta,
                                       real cv) {
    real sum = 0;
    if (a == b) {
      LABICEX_UNROLL
      for (int c = 0; c < Lattice::d; ++c) sum += du(c, c);
      sum /= cv;
    }
    return (-1 / (2 * beta)) * rho * t * (du(a, b) + du(b, a) - sum);
  }

  LABICEX_COMMON_INLINE static real q1(int a, int b, int c, real rho,
                                       const rvector& u, real t,
                                       const rtensor& du, const rvector& dt,
                                       real beta, real cv) {
    return (-1 / (2 * beta)) * rho * t *
               (dt(a) * delta(b, c) + dt(b) * delta(a, c) +
                dt(c) * delta(a, b)) +
           u(a) * p1(b, c, rho, u, t, du, beta, cv) +
           u(b) * p1(a, c, rho, u, t, du, beta, cv) +
           u(c) * p1(a, b, rho, u, t, du, beta, cv);
  }

  BoundaryMapAccessor bma_;
};

template <class Lattice, class BoundaryMapAccessor>
template <class PopulationAccessor, class PrimitiveVariablesAccessor>
LABICEX_DEVICE void noslip_updater<Lattice, BoundaryMapAccessor>::update_impl(
    const kernel_space<Lattice::d>& space, const ivector& x,
    const PopulationAccessor&, const PrimitiveVariablesAccessor& pva, real beta,
    fvector& f, lattice::standard_tag) const {
  typedef typename Lattice::primitive_variables primitive_variables;

  if (!bma_.boundary_node(x)) return;

  real tgt_density;
  rvector tgt_velocity;

  if (bma_.far_boundary_node(x)) {
    primitive_variables tgt_pv;
    pva.load(x, tgt_pv);
    tgt_density = tgt_pv.density();
    tgt_velocity = tgt_pv.velocity();
  } else {
    tgt_density = 0;
    tgt_velocity = rvector::Zero();
    real n_missing = 0;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      if (bma_.missing(i, x)) {
        tgt_density += f(Lattice::reverse_index(i));

        const real distance = bma_.reverse_distance(i, x);

        if (distance * distance <= Lattice::d) {
          primitive_variables pvf;
          pva.load(x + Lattice::ci(i), pvf);
          const rvector velocityf = pvf.velocity();
          const rvector velocityw =
              bma_.wall_primitive_variables(space, i, x).velocity();

          const real factorf = distance / Lattice::cf_magnitude(i);
          tgt_velocity += (factorf * velocityf + velocityw) / (factorf + 1);

          ++n_missing;
        }
      } else {
        tgt_density += f(i);
      }
    }
    tgt_velocity /= n_missing;
    assert(tgt_density > 0);
    assert(n_missing > 0);
  }

  rtensor tgt_velocity_derivatives;
  LABICEX_UNROLL
  for (int j = 0; j < Lattice::d; ++j) {
    real lower_distance = bma_.reverse_distance(Lattice::unit_index(j), x);
    rvector lower_velocity;
    if (lower_distance <= 1) {
      lower_velocity =
          bma_.wall_primitive_variables(space, Lattice::unit_index(j), x)
              .velocity();
    } else {
      ivector lower_x = x;
      lower_x(j) -= 1;
      primitive_variables lower_pv;
      pva.load(lower_x, lower_pv);
      lower_velocity = lower_pv.velocity();
      lower_distance = 1;
    }

    real upper_distance = bma_.distance(Lattice::unit_index(j), x);
    rvector upper_velocity;
    if (upper_distance <= 1) {
      upper_velocity =
          bma_.wall_primitive_variables(
                  space, Lattice::reverse_index(Lattice::unit_index(j)), x)
              .velocity();
    } else {
      ivector upper_x = x;
      upper_x(j) += 1;
      primitive_variables upper_pv;
      pva.load(upper_x, upper_pv);
      upper_velocity = upper_pv.velocity();
      upper_distance = 1;
    }

    assert(upper_distance + lower_distance > 0);
    tgt_velocity_derivatives.col(j) =
        (upper_velocity - lower_velocity) / (upper_distance + lower_distance);
  }

  const real cs2 = Lattice::reference_temperature();
  const real p_factor = tgt_density * cs2 / (2 * beta);
  const real pc_factor = 1.0 / (2 * cs2 * cs2);
  const real j_factor = 1.0 / cs2;
  LABICEX_UNROLL
  for (int i = 0; i < Lattice::q; ++i) {
    if (bma_.missing(i, x)) {
      real fi = tgt_density;
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        fi += j_factor * tgt_density * tgt_velocity(a) * Lattice::cf(i)(a);
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          const real p_part = tgt_density * tgt_velocity(a) * tgt_velocity(b) -
                              p_factor * (tgt_velocity_derivatives(a, b) +
                                          tgt_velocity_derivatives(b, a));
          const real c_part =
              Lattice::cf(i)(a) * Lattice::cf(i)(b) - cs2 * delta(a, b);
          fi += pc_factor * p_part * c_part;
        }
      }
      f(i) = Lattice::w(i) * fi;
    }
  }
  /*const fvector feq =
      Lattice::equilibrium::get(primitive_variables(tgt_density, tgt_velocity));

  const real p1fac = 1.0 / (2 * Lattice::reference_temperature() *
                            Lattice::reference_temperature());
  const real q1fac = 1.0 / (6 * Lattice::reference_temperature() *
                            Lattice::reference_temperature() *
                            Lattice::reference_temperature());
  LABICEX_UNROLL
  for (int i = 0; i < Lattice::q; ++i) {
    if (bma_.missing(i, x)) {
      real f1i = 0;
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          f1i += p1(a, b, tgt_density, tgt_velocity,
                    Lattice::reference_temperature(), tgt_velocity_derivatives,
                    beta, Lattice::d / 2.0) *
                 (Lattice::ci(i)(a) * Lattice::ci(i)(b) -
                  Lattice::reference_temperature() * delta(a, b)) *
                 p1fac;
          LABICEX_UNROLL
          for (int c = 0; c < Lattice::d; ++c) {
            f1i +=
                q1(a, b, c, tgt_density, tgt_velocity,
                   Lattice::reference_temperature(), tgt_velocity_derivatives,
                   rvector::Zero(), beta, Lattice::d / 2.0) *
                (Lattice::ci(i)(a) * Lattice::ci(i)(b) * Lattice::ci(i)(c) -
                 3 * Lattice::ci(i)(c) * delta(a, b)) *
                q1fac;
          }
        }
      }
      f(i) = feq(i) + Lattice::w(i) * f1i;
    }
  }*/
  assert(f.sum() > 0);
}

template <class Lattice, class BoundaryMapAccessor>
template <class PopulationAccessor, class PrimitiveVariablesAccessor>
LABICEX_DEVICE void noslip_updater<Lattice, BoundaryMapAccessor>::update_impl(
    const kernel_space<Lattice::d>& space, const ivector& x,
    const PopulationAccessor&, const PrimitiveVariablesAccessor& pva, real beta,
    fvector& f, lattice::thermal_tag) const {
  typedef typename Lattice::primitive_variables primitive_variables;

  if (!bma_.boundary_node(x)) return;

  real tgt_density;
  rvector tgt_velocity;
  real tgt_temperature;

  if (bma_.far_boundary_node(x)) {
    primitive_variables tgt_pv;
    pva.load(x, tgt_pv);
    tgt_density = tgt_pv.density();
    tgt_velocity = tgt_pv.velocity();
    tgt_temperature = tgt_pv.temperature();
  } else {
    tgt_density = 0;
    tgt_velocity = rvector::Zero();
    tgt_temperature = 0;
    real n_missing = 0;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      if (bma_.missing(i, x)) {
        tgt_density += f(Lattice::reverse_index(i));

        const real distance = bma_.reverse_distance(i, x);

        if (distance * distance <= Lattice::d) {
          primitive_variables pvf;
          pva.load(x + Lattice::ci(i), pvf);
          const rvector velocityf = pvf.velocity();
          const real temperaturef = pvf.temperature();
          const primitive_variables pvw =
              bma_.wall_primitive_variables(space, i, x);
          const rvector velocityw = pvw.velocity();
          const real temperaturew = pvw.temperature();

          const real factorf = distance / Lattice::cf_magnitude(i);
          tgt_velocity += (factorf * velocityf + velocityw) / (factorf + 1);
          tgt_temperature +=
              (factorf * temperaturef + temperaturew) / (factorf + 1);

          ++n_missing;
        }
      } else {
        tgt_density += f(i);
      }
    }
    tgt_velocity /= n_missing;
    tgt_temperature /= n_missing;
    assert(tgt_density > 0);
    assert(n_missing > 0);
  }

  rtensor tgt_velocity_derivatives;
  rvector tgt_temperature_derivatives;
  LABICEX_UNROLL
  for (int j = 0; j < Lattice::d; ++j) {
    real lower_distance = bma_.reverse_distance(Lattice::unit_index(j), x);
    primitive_variables lower_pv;
    if (lower_distance <= 1) {
      lower_pv =
          bma_.wall_primitive_variables(space, Lattice::unit_index(j), x);
    } else {
      ivector lower_x = x;
      lower_x(j) -= 1;
      pva.load(lower_x, lower_pv);
      lower_distance = 1;
    }

    real upper_distance = bma_.distance(Lattice::unit_index(j), x);
    primitive_variables upper_pv;
    if (upper_distance <= 1) {
      upper_pv = bma_.wall_primitive_variables(
          space, Lattice::reverse_index(Lattice::unit_index(j)), x);
    } else {
      ivector upper_x = x;
      upper_x(j) += 1;
      pva.load(upper_x, upper_pv);
      upper_distance = 1;
    }

    assert(upper_distance + lower_distance > 0);
    tgt_velocity_derivatives.col(j) =
        (upper_pv.velocity() - lower_pv.velocity()) /
        (upper_distance + lower_distance);
    tgt_temperature_derivatives(j) =
        (upper_pv.temperature() - lower_pv.temperature()) /
        (upper_distance + lower_distance);
  }

  const fvector feq = Lattice::equilibrium::get(
      primitive_variables(tgt_density, tgt_velocity, tgt_temperature));

  const real p1fac = 1.0 / (2 * tgt_temperature * tgt_temperature);
  const real q1fac =
      1.0 / (6 * tgt_temperature * tgt_temperature * tgt_temperature);
  LABICEX_UNROLL
  for (int i = 0; i < Lattice::q; ++i) {
    if (bma_.missing(i, x)) {
      real f1i = 0;
      LABICEX_UNROLL
      for (int a = 0; a < Lattice::d; ++a) {
        LABICEX_UNROLL
        for (int b = 0; b < Lattice::d; ++b) {
          f1i += p1(a, b, tgt_density, tgt_velocity, tgt_temperature,
                    tgt_velocity_derivatives, beta, Lattice::d / 2.0) *
                 (Lattice::ci(i)(a) * Lattice::ci(i)(b) -
                  tgt_temperature * delta(a, b)) *
                 p1fac;
          LABICEX_UNROLL
          for (int c = 0; c < Lattice::d; ++c) {
            f1i += q1(a, b, c, tgt_density, tgt_velocity, tgt_temperature,
                      tgt_velocity_derivatives, tgt_temperature_derivatives,
                      beta, Lattice::d / 2.0) *
                   (Lattice::ci(i)(a) * Lattice::ci(i)(b) * Lattice::ci(i)(c) -
                    3 * Lattice::ci(i)(c) * delta(a, b)) *
                   q1fac;
          }
        }
      }
      f(i) = feq(i) + Lattice::w(i, Lattice::reference_temperature()) * f1i;
    }
  }
  assert(f.sum() > 0);
}

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_STANDARD_LATTICES_H

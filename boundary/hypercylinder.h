#ifndef LABICEX_BOUNDARY_CYLINDER_H
#define LABICEX_BOUNDARY_CYLINDER_H

#include <cfloat>

#include "boundary/hypersphere.h"

namespace labicex {

namespace boundary {

template <int D, int Axis>
class hypercylinder : public hypersphere<(Axis < D ? D - 1 : D)> {
 public:
  static const int d = D;
  typedef typename vector_types<d>::rvector rvector;
  typedef hypersphere<(Axis < D ? D - 1 : D)> base;
  typedef typename base::rvector rvectorp;

  hypercylinder(const rvector& center, real radius)
      : base(project(center), radius) {}

  LABICEX_COMMON_INLINE bool intersect(const ray<d>& r, real& distance) const {
    const ray<base::d> rp(project(r.origin()), project(r.direction()),
                          r.max_distance());
    return base::intersect(rp, distance);
  }

  LABICEX_COMMON_INLINE bool inside(const rvector& x) const {
    return base::inside(project(x));
  }

  LABICEX_COMMON_INLINE real approximate_distance(const rvector& x) const {
    return base::approximate_distance(project(x));
  }

 private:
  LABICEX_COMMON_INLINE rvectorp project(const rvector& x) const {
    rvectorp y;
    LABICEX_UNROLL
    for (int i = 0; i < base::d; ++i) {
      if (i < Axis)
        y(i) = x(i);
      else
        y(i) = x(i + 1);
    }
    return y;
  }
};

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_CYLINDER_H

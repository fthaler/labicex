#ifndef LABICEX_BOUNDARY_DUMMY_UPDATER_H
#define LABICEX_BOUNDARY_DUMMY_UPDATER_H

#include "base/common.h"
#include "base/kernel_space.h"

namespace labicex {

namespace boundary {

template <class Lattice>
class dummy_updater {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  static const bool uses_primitive_variables = false;

  template <class PopulationAccessor, class PrimitiveVariablesAccessor>
  LABICEX_COMMON_INLINE void update(const kernel_space<Lattice::d>&,
                                    const ivector&, const PopulationAccessor&,
                                    const PrimitiveVariablesAccessor&,
                                    fvector& f) const {}

  template <class PopulationAccessorA, class PopulationAccessorB,
            class PrimitiveVariablesAccessorA,
            class PrimitiveVariablesAccessorB>
  LABICEX_COMMON_INLINE void update(const kernel_space<Lattice::d>&,
                                    const ivector&, const PopulationAccessorA&,
                                    const PopulationAccessorB&,
                                    const PrimitiveVariablesAccessorA&,
                                    const PrimitiveVariablesAccessorB&,
                                    fvector& f) const {}
};

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_DUMMY_UPDATER_H

#ifndef LABICEX_BOUNDARY_BOUNDARY_MAP_H
#define LABICEX_BOUNDARY_BOUNDARY_MAP_H

#include <limits>

#include <thrust/copy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include "base/domain.h"
#include "base/indexer.h"
#include "base/kernel_space.h"
#include "boundary/ray.h"

namespace labicex {

namespace boundary {

namespace detail {

template <class Lattice>
class flagged_distances {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::fvector fvector;

  LABICEX_COMMON_INLINE flagged_distances() {}

  flagged_distances(const fvector& distances) : is_near_(false) {
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::q; ++i) {
      distances_[i] = static_cast<float>(distances(i));
      if ((distances(i) * Lattice::cf(i).matrix().normalized())
              .array()
              .abs()
              .maxCoeff() <= 1)
        is_near_ = true;
    }
  }

  LABICEX_COMMON_INLINE real distance(int i) const {
    assert(i >= 0 && i < Lattice::q);
    return distances_[i];
  }
  LABICEX_COMMON_INLINE bool is_near() const { return is_near_; }
  LABICEX_COMMON_INLINE bool is_far() const { return !is_near_; }

 private:
  float distances_[Lattice::q];
  bool is_near_;
};

}  // detail

template <class Lattice, class Indexer, class MapIndex, class WallFunction>
class boundary_map_accessor {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;

  boundary_map_accessor(const MapIndex* indices,
                        const detail::flagged_distances<Lattice>* distances,
                        const Indexer& indexer, const WallFunction& function)
      : indices_(indices),
        distances_(distances),
        indexer_(indexer),
        function_(function) {}

  LABICEX_COMMON_INLINE bool boundary_node(const ivector& x) const {
    const int linear_index = indexer_.linear_index(x);
    return indices_[linear_index + 1] != indices_[linear_index];
  }

  LABICEX_COMMON_INLINE bool near_boundary_node(const ivector& x) const {
    assert(boundary_node(x));
    return distances_[indices_[indexer_.linear_index(x)]].is_near();
  }

  LABICEX_COMMON_INLINE bool far_boundary_node(const ivector& x) const {
    assert(boundary_node(x));
    return distances_[indices_[indexer_.linear_index(x)]].is_far();
  }

  LABICEX_COMMON_INLINE bool missing(int i, const ivector& x) const {
    assert(boundary_node(x));
    return reverse_distance(i, x) <= Lattice::cf_magnitude(i);
  }

  LABICEX_COMMON_INLINE real distance(int i, const ivector& x) const {
    assert(boundary_node(x));
    return distances_[indices_[indexer_.linear_index(x)]].distance(i);
  }

  LABICEX_COMMON_INLINE real reverse_distance(int i, const ivector& x) const {
    assert(boundary_node(x));
    return distance(Lattice::reverse_index(i), x);
  }

  LABICEX_COMMON_INLINE primitive_variables wall_primitive_variables(
      const kernel_space<Lattice::d>& space, int i, const ivector& x) const {
    const rvector xw =
        reverse_distance(i, x) *
        Lattice::cf(Lattice::reverse_index(i)).matrix().normalized().array();
    return function_(space, xw);
  }

 private:
  const MapIndex* __restrict__ indices_;
  const detail::flagged_distances<Lattice>* __restrict__ distances_;
  Indexer indexer_;
  WallFunction function_;
};

template <class Lattice, class Indexer, class MapIndex>
class boundary_map {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::rvector rvector;
  typedef typename lattice::fvector fvector;
  typedef typename lattice::primitive_variables primitive_variables;

  template <class WallFunction>
  struct accessor_type {
    typedef boundary_map_accessor<Lattice, Indexer, MapIndex, WallFunction>
        type;
  };
  static const int d = lattice::d;

  boundary_map(const ivector& extents);

  template <class Boundary>
  void build(const Boundary& boundary);

  template <class WallFunction>
  typename accessor_type<WallFunction>::type caccessor(
      const WallFunction& function) const {
    return typename accessor_type<WallFunction>::type(
        thrust::raw_pointer_cast(indices_.data()),
        thrust::raw_pointer_cast(distances_.data()), indexer_, function);
  }

  template <class WallFunction>
  typename accessor_type<WallFunction>::type accessor(
      const WallFunction& function) const {
    return caccessor(function);
  }

 private:
  thrust::device_vector<MapIndex> indices_;
  thrust::device_vector<detail::flagged_distances<Lattice> > distances_;
  Indexer indexer_;
};

template <class Lattice, class Indexer, class MapIndex>
boundary_map<Lattice, Indexer, MapIndex>::boundary_map(const ivector& extents)
    : indexer_(extents), indices_(extents.prod() + 1) {}

template <class Lattice, class Indexer, class MapIndex>
template <class Boundary>
void boundary_map<Lattice, Indexer, MapIndex>::build(const Boundary& boundary) {
  thrust::host_vector<MapIndex> host_indices(indices_.size());
  thrust::host_vector<detail::flagged_distances<Lattice> > host_distances;

  const real max_distance = sqrt(d) * lattice::max_ci;
  domain::box<d> box(ivector::Zero(), indexer_.extents());
  for (typename domain::box<d>::iterator x = box.begin(); x != box.end(); ++x) {
    if (boundary.approximate_distance(x->template cast<real>()) <=
        max_distance) {
      fvector distances;
      bool any_hit = false;

      for (int i = 0; i < lattice::q; ++i) {
        if ((lattice::ci(i) == ivector::Zero()).all()) {
          distances(i) = std::numeric_limits<real>::max();
        } else {
          ray<d> r(x->template cast<real>(), lattice::cf(i), max_distance);
          real distance;
          if (boundary.intersect(r, distance)) {
            distances(i) = distance;
            any_hit = true;
          } else {
            distances(i) = std::numeric_limits<real>::max();
          }
        }
      }

      host_indices[indexer_.linear_index(*x)] = host_distances.size();
      if (any_hit)
        host_distances.push_back(detail::flagged_distances<Lattice>(distances));
    } else {
      host_indices[indexer_.linear_index(*x)] = host_distances.size();
    }
    assert(host_distances.size() <=
           static_cast<size_t>(std::numeric_limits<MapIndex>::max()));
  }
  host_indices.back() = host_distances.size();

  thrust::copy(host_indices.begin(), host_indices.end(), indices_.begin());
  distances_.resize(host_distances.size());
  thrust::copy(host_distances.begin(), host_distances.end(),
               distances_.begin());
}

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_BOUNDARY_MAP_H

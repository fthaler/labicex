#ifndef LABICEX_BOUNDARY_HYPERSPHERE_H
#define LABICEX_BOUNDARY_HYPERSPHERE_H

#include <cfloat>

#include "base/common.h"
#include "boundary/ray.h"

namespace labicex {

namespace boundary {

template <int D>
class hypersphere {
 public:
  static const int d = D;
  typedef typename vector_types<d>::rvector rvector;

  hypersphere(const rvector& center, real radius)
      : center_(center), radius_(radius) {
    assert(radius_ > 0);
  }

  LABICEX_COMMON bool intersect(const ray<d>& r, real& distance) const {
    const rvector local_origin = r.origin() - center_;

    const real a = (r.direction() * r.direction()).sum();
    const real b = 2 * (local_origin * r.direction()).sum();
    const real c = (local_origin * local_origin).sum() - radius_ * radius_;

    const real d = b * b - 4 * a * c;
    if (d < 0) return false;

    const real t = (-b - sqrt(d)) / (2 * a);
    if (t <= 0 || t > r.max_distance()) return false;

    distance = t;
    assert(!isnan(distance));
    return true;
  }

  LABICEX_COMMON_INLINE bool inside(const rvector& x) const {
    const rvector local_x = x - center_;
    return (local_x * local_x).sum() <= radius_ * radius_;
  }

  LABICEX_COMMON_INLINE real approximate_distance(const rvector& x) const {
    const rvector local_x = x - center_;
    const real r2 = (local_x * local_x).sum();
    if (r2 <= radius_ * radius_) return sizeof(real) == 8 ? DBL_MAX : FLT_MAX;
    return sqrt(r2) - radius_;
  }

  LABICEX_COMMON_INLINE const rvector& center() const { return center_; }
  LABICEX_COMMON_INLINE real radius() const { return radius_; }

 private:
  rvector center_;
  real radius_;
};

}  // boundary

}  // labicex

#endif  // LABICEX_BOUNDARY_HYPERSPHERE_H

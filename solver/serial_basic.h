#ifndef LABICEX_SOLVER_SERIAL_BASIC_H
#define LABICEX_SOLVER_SERIAL_BASIC_H

#include "base/common.h"
#include "base/device_helpers.h"
#include "base/domain.h"
#include "base/stream.h"

namespace labicex {

namespace solver {

namespace kernel {

template <class Domain, class Collision, class Advector,
          class PopulationAccessor0, class PopulationAccessor1>
LABICEX_KERNEL void serial_basic_step(const Domain domain,
                                      const Collision collision,
                                      const Advector advector,
                                      const PopulationAccessor0 population0,
                                      PopulationAccessor1 population1) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = thread_index<Domain::d>();

  if (!domain.inside(x)) return;

  fvector f = advector.load(population0, x);
  collision.collide(f);
  advector.store(population1, x, f);
}

template <class Domain, class Collision, class Advector,
          class PopulationAccessor0, class PopulationAccessor1,
          class PrimitiveVariablesAccessor>
LABICEX_KERNEL void serial_basic_step(const Domain domain,
                                      const Collision collision,
                                      const Advector advector,
                                      const PopulationAccessor0 population0,
                                      PopulationAccessor1 population1,
                                      PrimitiveVariablesAccessor pva) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = thread_index<Domain::d>();

  if (!domain.inside(x)) return;

  fvector f = advector.load(population0, x);
  const primitive_variables pv(f);
  pva.store(x, pv);
  collision.collide(f);
  advector.store(population1, x, f);
}

}  // kernel

template <class Lattice, class Collision, class Advector,
          class PopulationStorage, class PrimitiveVariablesStorage>
class serial_basic {
 public:
  typedef Lattice lattice;
  typedef typename lattice::ivector ivector;
  typedef PopulationStorage population_storage;
  typedef PrimitiveVariablesStorage primitive_variables_storage;

  serial_basic(const ivector& extents, const Collision& collision,
               const Advector& advector)
      : f0_(extents),
        f1_(extents),
        pvs_(extents),
        collision_(collision),
        advector_(advector),
        domain_(extents) {}

  void step(bool update_primitive) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(domain_.extents(), grid_size, block_size);

    if (update_primitive) {
      kernel::
          serial_basic_step<<<grid_size, block_size, 0, compute_stream_>>>(
              domain_, collision_, advector_, f0_.caccessor(), f1_.accessor(),
              pvs_.accessor());
    } else {
      kernel::
          serial_basic_step<<<grid_size, block_size, 0, compute_stream_>>>(
              domain_, collision_, advector_, f0_.caccessor(), f1_.accessor());
    }
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    f0_.swap(f1_);
  }

  const PopulationStorage& f() const { return f0_; }
  PopulationStorage& f() { return f0_; }

  const PrimitiveVariablesStorage& pvs() const { return pvs_; }
  PrimitiveVariablesStorage& pvs() { return pvs_; }

  const stream& compute_stream() const { return compute_stream_; }

 private:
  serial_basic(const serial_basic&);
  serial_basic& operator=(const serial_basic&);

  PopulationStorage f0_, f1_;
  PrimitiveVariablesStorage pvs_;
  Collision collision_;
  Advector advector_;
  domain::box<lattice::d> domain_;
  stream compute_stream_;
};

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_SERIAL_BASIC_H

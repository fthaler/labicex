#ifndef LABICEX_SOLVER_BASE_H
#define LABICEX_SOLVER_BASE_H

#include "advector/onload.h"
#include "base/device_helpers.h"
#include "base/domain.h"
#include "base/kernel_space.h"
#include "base/storage_traits.h"
#include "base/stream.h"
#include "base/timers.h"
#include "base/transfer.h"
#include "io/xdmf.h"
#include "mpi/distributed_grid.h"
#include "mpi/halo_exchanger.h"

namespace labicex {

namespace solver {

template <class Lattice, class Collision, class Forcing>
class raw_base {
  class progress_printer {
   public:
    progress_printer() : percent_(0) {
      std::cout << "progress:" << std::endl;
      timer_.start();
      print_progress_bar();
    }

    ~progress_printer() {
      update_percent(100);
      std::cout << std::endl;
    }

    void update_percent(int new_percent) {
      if (new_percent > percent_) {
        percent_ = new_percent;
        print_progress_bar();
      }
    }

   private:
    void print_progress_bar() {
      std::cout << "\r[";
      for (int i = 0; i < 100; ++i) {
        if (i < percent_)
          std::cout << "=";
        else if (i == percent_)
          std::cout << ">";
        else
          std::cout << "-";
      }
      std::cout << "] " << std::setw(3) << std::right << percent_ << "%";
      if (percent_ > 1) {
        const double elapsed_seconds = timer_.elapsed_seconds();
        const double total_seconds = 100 * elapsed_seconds / percent_;
        double remaining_seconds = total_seconds - elapsed_seconds;
        std::cout << " (estimated remaining computation time: ";
        if (remaining_seconds >= 60) {
          int remaining_minutes = remaining_seconds / 60;
          remaining_seconds -= remaining_minutes * 60;
          if (remaining_minutes >= 60) {
            int remaining_hours = remaining_minutes / 60;
            remaining_minutes -= remaining_hours * 60;
            std::cout << remaining_hours << "h ";
          }
          std::cout << remaining_minutes << "m ";
        }
        std::cout << static_cast<int>(remaining_seconds) << "s)          ";
      }
      std::cout.flush();
    }

    int percent_;
    timer timer_;
  };

 public:
  typedef Lattice lattice;
  static const int d = lattice::d;
  typedef typename lattice::ivector ivector;
  typedef domain::box<d> box;
  typedef domain::box_halo<d> box_halo;
  typedef typename domain::box_halo<d>::box_container box_container;
  typedef labicex::io::xdmf_writer<mpi::distributed_grid<lattice> > xdmf_writer;
  typedef advector::onload<lattice> advector_type;

  raw_base(const mpi::distributed_grid<lattice>& grid,
           const Collision& collision, const Forcing& forcing)
      : grid_(grid),
        outer_compute_domains_(grid.local().outer_domain().sub_boxes()),
        collision_(collision),
        forcing_(forcing) {
    const int reals_per_l2_cache_line = 128 / sizeof(real);
    const box full_domain = grid_.local().full_domain();
    inner_compute_domain_ = grid_.local().inner_domain();
    if (inner_compute_domain_.extents()(0) >= 3 * reals_per_l2_cache_line) {
      inner_compute_domain_.min()(0) =
          max(inner_compute_domain_.min()(0),
              full_domain.min()(0) + reals_per_l2_cache_line);
      inner_compute_domain_.max()(0) =
          min(inner_compute_domain_.max()(0),
              full_domain.max()(0) - reals_per_l2_cache_line);
      const box_halo outer_compute_domain(
          inner_compute_domain_, grid.local().outer_domain().outer_box());
      outer_compute_domains_ = outer_compute_domain.sub_boxes();
    } else {
      inner_compute_domain_.min()(0) = full_domain.min()(0);
      inner_compute_domain_.max()(0) = full_domain.max()(0);
    }
  }

  void step(bool force_pvs_update = false) {
    compute_stream_.synchronize();
    start_halo_exchange(boundary_uses_pvs());
    launch_kernel(get_launch_configuration(grid_.local().inner_domain()),
                  force_pvs_update);
    finish_halo_exchange(boundary_uses_pvs());

    for (typename box_container::const_iterator d =
             outer_compute_domains_.begin();
         d != outer_compute_domains_.end(); ++d)
      launch_kernel(get_launch_configuration(*d), force_pvs_update);

    swap_data();
  }

  void run_simulation(int t) {
    labicex::auto_timer timer("total simulation");
    exchange_halos();
    {
      labicex::mlups_auto_timer timer("computation", grid_, t);
      progress_printer progress;
      for (int ti = 0; ti < t; ++ti) {
        progress.update_percent(ti * 100 / t);
        step(ti == t - 1);
      }
      compute_stream_.synchronize();
    }
  }

  void run_simulation(int t, xdmf_writer& xdmf, int ofreq) {
    labicex::auto_timer timer("total simulation");
    labicex::timer io_timer;
    exchange_halos();

    if (ofreq == 1) {
      progress_printer progress;
      for (int ti = 0; ti < t; ++ti) {
        progress.update_percent(ti * 100 / t);
        step(true);
        compute_stream_.synchronize();
        io_timer.start();
        start_pvs_download(download_stream_);
        xdmf.set_time(ti);
        download_stream_.synchronize();
        write_pvs(xdmf);
        io_timer.stop();
      }
    } else {
      bool output_pending = false;
      {
        labicex::mlups_auto_timer timer("computation", grid_, t);
        progress_printer progress;
        for (int ti = 0; ti < t; ++ti) {
          progress.update_percent(ti * 100 / t);
          if (ti % ofreq == ofreq - 1 || ti == t - 1) {
            if (output_pending) {
              io_timer.start();
              download_stream_.synchronize();
              write_pvs(xdmf);
              output_pending = false;
              io_timer.stop();
            }
            step(true);
          } else {
            if (ti % ofreq == 0) {
              compute_stream_.synchronize();
              io_timer.start();
              start_pvs_download(download_stream_);
              xdmf.set_time(ti);
              output_pending = true;
              io_timer.stop();
            }
            step(false);
          }
        }
        compute_stream_.synchronize();
      }

      if (output_pending) {
        download_stream_.synchronize();
        write_pvs(xdmf);
      }
    }

    io_timer.report("output overhead");
  }

  void run_simulation(int t, const std::string& xdmf_filename, int ofreq) {
    xdmf_writer xdmf(xdmf_filename, grid_, t);
    run_simulation(t, xdmf, ofreq);
  }

  void write_primitive_variables(xdmf_writer& xdmf,
                                 const std::string& info = "") {
    stream s = stream::zero();
    start_pvs_download(s);
    s.synchronize();
    write_pvs(xdmf, info);
  }

  void write_primitive_variables(const std::string& xdmf_filename, int t,
                                 const std::string& info = "") {
    xdmf_writer xdmf(xdmf_filename, grid_, t, t);
    write_primitive_variables(xdmf, info);
  }

  void exchange_halos(bool exchange_pvs = true) {
    start_halo_exchange(exchange_pvs);
    finish_halo_exchange(exchange_pvs);
  }

  const stream& compute_stream() const { return compute_stream_; }
  const mpi::distributed_grid<lattice>& grid() const { return grid_; }

 protected:
  struct launch_configuration {
    box domain;
    dim3 grid_size, block_size;
    kernel_space<d> space;
  };

  virtual void start_halo_exchange(bool exchange_pvs) = 0;
  virtual void finish_halo_exchange(bool exchange_pvs) = 0;
  virtual void launch_kernel(const launch_configuration& lc,
                             bool force_pvs_update) = 0;
  virtual void swap_data() = 0;
  virtual bool boundary_uses_pvs() const = 0;
  virtual void write_pvs(xdmf_writer& xdmf,
                         const std::string& info = "") const = 0;
  virtual void start_pvs_download(const stream& s) = 0;

  const advector_type& advector() const { return advector_; }
  const Collision& collision() const { return collision_; }
  const Forcing& forcing() const { return forcing_; }
  launch_configuration get_launch_configuration(const box& domain) const {
    launch_configuration lc;
    lc.domain = domain;
    const box compute_domain =
        domain == grid_.local().inner_domain() ? inner_compute_domain_ : domain;
    get_default_launch_configuration(compute_domain.extents(), lc.grid_size,
                                     lc.block_size);
    lc.space = kernel_space<d>(grid_, compute_domain.min());
    return lc;
  }

 private:
  raw_base(const raw_base&);
  raw_base& operator=(const raw_base&);

  const mpi::distributed_grid<lattice>& grid_;
  box inner_compute_domain_;
  box_container outer_compute_domains_;
  advector_type advector_;
  Collision collision_;
  Forcing forcing_;
  stream compute_stream_, download_stream_;
};

template <class Lattice, class Collision, class Forcing, class Boundary,
          class PopulationStorage, class PrimitiveVariablesStorage,
          class PrimitiveVariablesHostStorage, int Index = 0>
class storage_base : public virtual raw_base<Lattice, Collision, Forcing> {
  typedef raw_base<Lattice, Collision, Forcing> rbase;

 public:
  typedef typename rbase::xdmf_writer xdmf_writer;

  storage_base(const mpi::distributed_grid<Lattice>& grid,
               const Collision& collision, const Forcing& forcing,
               const Boundary& boundary)
      : rbase(grid, collision, forcing),
        halos_(grid),
        boundary_(boundary),
        f0_(grid.local().full_domain().extents()),
        f1_(grid.local().full_domain().extents()),
        pvs0_(grid.local().full_domain().extents()),
        pvs1_(grid.local().full_domain().extents()),
        pvs_host_(grid.local().full_domain().extents())
#ifdef LABICEX_POPULATION_OUTPUT
        ,
        f_host_(grid.local().full_domain().extents())
#endif
  {
  }

  const PopulationStorage& f() const { return f0_; }
  PopulationStorage& f() { return f0_; }
  const PrimitiveVariablesStorage& pvs() const { return pvs0_; }
  PrimitiveVariablesStorage& pvs() { return pvs0_; }

 protected:
  virtual void swap_data() {
    f0_.swap(f1_);
    pvs0_.swap(pvs1_);
  }

  virtual void start_halo_exchange(bool exchange_pvs) {
    if (exchange_pvs)
      halos_.start_exchange(f0_, pvs0_);
    else
      halos_.start_exchange(f0_);
  }

  virtual void finish_halo_exchange(bool exchange_pvs) {
    if (exchange_pvs)
      halos_.finish_exchange(f0_, pvs0_);
    else
      halos_.finish_exchange(f0_);
  }

  virtual bool boundary_uses_pvs() const {
    return Boundary::uses_primitive_variables;
  }

  virtual void write_pvs(xdmf_writer& xdmf, const std::string& info) const {
    xdmf.write_primitive(pvs_host_, info);
#ifdef LABICEX_POPULATION_OUTPUT
    xdmf.write_population(f_host_, info);
#endif
  }

  virtual void start_pvs_download(const stream& s) {
    storage_copy(pvs0_, pvs_host_, s);
#ifdef LABICEX_POPULATION_OUTPUT
    storage_copy(f0_, f_host_, s);
#endif
  }

  const Boundary& boundary() const { return boundary_; }
  const PopulationStorage& f0() const { return f0_; }
  PopulationStorage& f0() { return f0_; }
  const PopulationStorage& f1() const { return f1_; }
  PopulationStorage& f1() { return f1_; }
  const PrimitiveVariablesStorage& pvs0() const { return pvs0_; }
  PrimitiveVariablesStorage& pvs0() { return pvs0_; }
  const PrimitiveVariablesStorage& pvs1() const { return pvs1_; }
  PrimitiveVariablesStorage& pvs1() { return pvs1_; }

 private:
  storage_base(const storage_base&);
  storage_base& operator=(const storage_base&);

  mpi::halo_exchanger<Lattice> halos_;
  Boundary boundary_;
  PopulationStorage f0_, f1_;
  PrimitiveVariablesStorage pvs0_, pvs1_;
  PrimitiveVariablesHostStorage pvs_host_;
#ifdef LABICEX_POPULATION_OUTPUT
  typename host_storage_traits<Lattice>::population_storage f_host_;
#endif
};

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_BASE_H

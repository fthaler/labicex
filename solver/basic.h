#ifndef LABICEX_SOLVER_BASIC_H
#define LABICEX_SOLVER_BASIC_H

#include "solver/base.h"

namespace labicex {

namespace solver {

namespace kernel {

template <bool PvsUpdate, class Domain, class KernelSpace, class Advector,
          class Collision, class Forcing, class BoundaryUpdater,
          class PopulationAccessor0, class PopulationAccessor1,
          class PrimitiveVariablesAccessor0, class PrimitiveVariablesAccessor1>
LABICEX_KERNEL void basic_step(const Domain domain, const KernelSpace space,
                               const Advector advector,
                               const Collision collision, const Forcing forcing,
                               const BoundaryUpdater boundary,
                               const PopulationAccessor0 population0,
                               PopulationAccessor1 population1,
                               const PrimitiveVariablesAccessor0 pva0,
                               PrimitiveVariablesAccessor1 pva1) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = space.kernel_to_local(thread_index<Domain::d>());

  if (!domain.inside(x)) return;

  fvector f = advector.load(population0, x);
  assert(!isnan(f.sum()) && f.sum() > 0);
  boundary.update(space, x, population0, pva0, f);
  assert(!isnan(f.sum()) && f.sum() > 0);
  collision.collide(f);
  forcing.update(f);
  assert(!isnan(f.sum()) && f.sum() > 0);
  advector.store(population1, x, f);
  if (PvsUpdate) {
    const primitive_variables pv(f);
    pva1.store(x, pv);
  }
}

}  // kernel

template <
    class Lattice, class Collision, class Forcing, class Boundary,
    class PopulationStorage =
        typename device_storage_traits<Lattice>::population_storage,
    class PrimitiveVariablesStorage =
        typename device_storage_traits<Lattice>::primitive_variables_storage,
    class PrimitiveVariablesHostStorage =
        typename host_storage_traits<Lattice>::primitive_variables_storage>
class basic : public storage_base<Lattice, Collision, Forcing, Boundary,
                                  PopulationStorage, PrimitiveVariablesStorage,
                                  PrimitiveVariablesHostStorage> {
  typedef raw_base<Lattice, Collision, Forcing> rbase;
  typedef storage_base<Lattice, Collision, Forcing, Boundary, PopulationStorage,
                       PrimitiveVariablesStorage, PrimitiveVariablesHostStorage>
      sbase;

 public:
  typedef PopulationStorage population_storage;
  typedef PrimitiveVariablesStorage primitive_variables_storage;
  typedef PrimitiveVariablesHostStorage primitive_variables_host_storage;

  basic(const mpi::distributed_grid<Lattice>& grid, const Collision& collision,
        const Forcing& forcing, const Boundary& boundary)
      : sbase(grid, collision, forcing, boundary),
        rbase(grid, collision, forcing) {}

 protected:
  typedef typename rbase::launch_configuration launch_configuration;

  virtual void launch_kernel(const launch_configuration& lc,
                             bool force_pvs_update) {
    if (Boundary::uses_primitive_variables || force_pvs_update) {
      kernel::basic_step<
          true><<<lc.grid_size, lc.block_size, 0, rbase::compute_stream()>>>(
          lc.domain, lc.space, rbase::advector(), rbase::collision(),
          rbase::forcing(), sbase::boundary(), sbase::f0().caccessor(),
          sbase::f1().accessor(), sbase::pvs0().caccessor(),
          sbase::pvs1().accessor());
    } else {
      kernel::basic_step<
          false><<<lc.grid_size, lc.block_size, 0, rbase::compute_stream()>>>(
          lc.domain, lc.space, rbase::advector(), rbase::collision(),
          rbase::forcing(), sbase::boundary(), sbase::f0().caccessor(),
          sbase::f1().accessor(), sbase::pvs0().caccessor(),
          sbase::pvs1().accessor());
    }
  }

 private:
  basic(const basic&);
  basic& operator=(const basic&);
};

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_BASIC_H

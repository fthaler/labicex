#ifndef LABICEX_SOLVER_SERIAL_MIXTURE_H
#define LABICEX_SOLVER_SERIAL_MIXTURE_H

#include "base/common.h"
#include "base/device_helpers.h"
#include "base/domain.h"

namespace labicex {

namespace solver {

namespace kernel {

template <class Domain, class Collision, class Advector,
          class PopulationAccessorA0, class PopulationAccessorA1,
          class PopulationAccessorB0, class PopulationAccessorB1>
LABICEX_KERNEL void serial_basic_step(
    const Domain domain, const Collision collision, const Advector advector,
    const PopulationAccessorA0 a0, PopulationAccessorA1 a1,
    const PopulationAccessorB0 b0, PopulationAccessorB1 b1) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = thread_index<Domain::d>();

  if (!domain.inside(x)) return;

  fvector f_a = advector.load(a0, x);
  fvector f_b = advector.load(b0, x);
  collision.collide(f_a, f_b);
  advector.store(a1, x, f_a);
  advector.store(b1, x, f_b);
}

template <class Domain, class Collision, class Advector,
          class PopulationAccessorA0, class PopulationAccessorA1,
          class PopulationAccessorB0, class PopulationAccessorB1,
          class PrimitiveVariablesAccessorA, class PrimitiveVariablesAccessorB>
LABICEX_KERNEL void serial_basic_step(
    const Domain domain, const Collision collision, const Advector advector,
    const PopulationAccessorA0 a0, PopulationAccessorA1 a1,
    const PopulationAccessorB0 b0, PopulationAccessorB1 b1,
    PrimitiveVariablesAccessorA pva, PrimitiveVariablesAccessorB pvb) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = thread_index<Domain::d>();

  if (!domain.inside(x)) return;

  fvector f_a = advector.load(a0, x);
  fvector f_b = advector.load(b0, x);
  const primitive_variables pv_a(f_a);
  const primitive_variables pv_b(f_b);
  pva.store(x, pv_a);
  pvb.store(x, pv_b);
  collision.collide(f_a, f_b);
  advector.store(a1, x, f_a);
  advector.store(b1, x, f_b);
}

}  // kernel

template <class Population, class Collision, class Advector>
class serial_mixture {
 public:
  typedef typename Population::ivector ivector;
  typedef typename Population::lattice lattice;

  serial_mixture(const ivector& extents, const Collision& collision,
                 const Advector& advector)
      : f0_a_(extents),
        f1_a_(extents),
        f0_b_(extents),
        f1_b_(extents),
        collision_(collision),
        advector_(advector),
        domain_(extents) {}

  void step(cudaStream_t stream = 0) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(domain_.extents(), grid_size, block_size);

    kernel::serial_basic_step<<<grid_size, block_size, 0, stream>>>(
        domain_, collision_, advector_, f0_a_.caccessor(), f1_a_.accessor(),
        f0_b_.caccessor(), f1_b_.accessor());
    LABICEX_CUDA_CHECK(cudaPeekAtLastError());
    f0_a_.swap(f1_a_);
    f0_b_.swap(f1_b_);
  }

  template <class PrimitiveVariablesStorage>
  void step(PrimitiveVariablesStorage& output_a,
            PrimitiveVariablesStorage& output_b, cudaStream_t stream = 0) {
    dim3 grid_size, block_size;
    get_default_launch_configuration(domain_.extents(), grid_size, block_size);

    kernel::serial_basic_step<<<grid_size, block_size, 0, stream>>>(
        domain_, collision_, advector_, f0_a_.caccessor(), f1_a_.accessor(),
        f0_b_.caccessor(), f1_b_.accessor(), output_a.accessor(),
        output_b.accessor());
    f0_a_.swap(f1_a_);
    f0_b_.swap(f1_b_);
  }

  const Population& f_a() const { return f0_a_; }
  Population& f_a() { return f0_a_; }
  const Population& f_b() const { return f0_b_; }
  Population& f_b() { return f0_b_; }

 private:
  serial_mixture(const serial_mixture&);
  serial_mixture& operator=(const serial_mixture&);

  Population f0_a_, f1_a_, f0_b_, f1_b_;
  Collision collision_;
  Advector advector_;
  domain::box<lattice::d> domain_;
};

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_SERIAL_MIXTURE_H

#ifndef LABICEX_SOLVER_BINARY_MIXTURE_H
#define LABICEX_SOLVER_BINARY_MIXTURE_H

#include "solver/base.h"

namespace labicex {

namespace solver {

namespace kernel {

template <bool PvsUpdate, class Domain, class KernelSpace, class Advector,
          class Collision, class Forcing, class BoundaryUpdaterA,
          class BoundaryUpdaterB, class PopulationAccessorA0,
          class PopulationAccessorA1, class PopulationAccessorB0,
          class PopulationAccessorB1, class PrimitiveVariablesAccessorA0,
          class PrimitiveVariablesAccessorA1,
          class PrimitiveVariablesAccessorB0,
          class PrimitiveVariablesAccessorB1>
LABICEX_KERNEL void binary_mixture_step(
    const Domain domain, const KernelSpace space, const Advector advector,
    const Collision collision, const Forcing forcing,
    const BoundaryUpdaterA boundary_a, const BoundaryUpdaterB boundary_b,
    const PopulationAccessorA0 a0, PopulationAccessorA1 a1,
    const PopulationAccessorB0 b0, PopulationAccessorB1 b1,
    const PrimitiveVariablesAccessorA0 pva0, PrimitiveVariablesAccessorA1 pva1,
    const PrimitiveVariablesAccessorB0 pvb0,
    PrimitiveVariablesAccessorB1 pvb1) {
  typedef typename Advector::lattice lattice;
  typedef typename lattice::primitive_variables primitive_variables;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = space.kernel_to_local(thread_index<Domain::d>());

  if (!domain.inside(x)) return;

  register fvector f_a = advector.load(a0, x);
  register fvector f_b = advector.load(b0, x);
  assert(!isnan(f_a.sum()) && f_a.sum() > 0);
  assert(!isnan(f_b.sum()) && f_b.sum() > 0);
  boundary_a.update(space, x, a0, pva0, f_a);
  boundary_b.update(space, x, b0, pvb0, f_b);
  assert(!isnan(f_a.sum()) && f_a.sum() > 0);
  assert(!isnan(f_b.sum()) && f_b.sum() > 0);
  collision.collide(f_a, f_b);
  forcing.update(f_a, f_b);
  assert(!isnan(f_a.sum()) && f_a.sum() > 0);
  assert(!isnan(f_b.sum()) && f_b.sum() > 0);
  advector.store(a1, x, f_a);
  advector.store(b1, x, f_b);
  if (PvsUpdate) {
    const primitive_variables pv_a(f_a);
    const primitive_variables pv_b(f_b);
    pva1.store(x, pv_a);
    pvb1.store(x, pv_b);
  }
}

}  // kernel

template <
    class Lattice, class Collision, class Forcing, class BoundaryA,
    class BoundaryB,
    class PopulationStorage =
        typename device_storage_traits<Lattice>::population_storage,
    class PrimitiveVariablesStorage =
        typename device_storage_traits<Lattice>::primitive_variables_storage,
    class PrimitiveVariablesHostStorage =
        typename host_storage_traits<Lattice>::primitive_variables_storage>
class binary_mixture
    : public storage_base<Lattice, Collision, Forcing, BoundaryA,
                          PopulationStorage, PrimitiveVariablesStorage,
                          PrimitiveVariablesHostStorage, 0>,
      public storage_base<Lattice, Collision, Forcing, BoundaryB,
                          PopulationStorage, PrimitiveVariablesStorage,
                          PrimitiveVariablesHostStorage, 1> {
  typedef raw_base<Lattice, Collision, Forcing> rbase;
  typedef storage_base<Lattice, Collision, Forcing, BoundaryA,
                       PopulationStorage, PrimitiveVariablesStorage,
                       PrimitiveVariablesHostStorage, 0>
      abase;
  typedef storage_base<Lattice, Collision, Forcing, BoundaryB,
                       PopulationStorage, PrimitiveVariablesStorage,
                       PrimitiveVariablesHostStorage, 1>
      bbase;

 public:
  typedef PopulationStorage population_storage;
  typedef PrimitiveVariablesStorage primitive_variables_storage;
  typedef PrimitiveVariablesHostStorage primitive_variables_host_storage;
  typedef typename rbase::xdmf_writer xdmf_writer;

  binary_mixture(const mpi::distributed_grid<Lattice>& grid,
                 const Collision& collision, const Forcing& forcing,
                 const BoundaryA& boundary_a, const BoundaryB& boundary_b)
      : abase(grid, collision, forcing, boundary_a),
        bbase(grid, collision, forcing, boundary_b),
        rbase(grid, collision, forcing) {}

  const PopulationStorage& f_a() const { return abase::f(); }
  PopulationStorage& f_a() { return abase::f(); }
  const PopulationStorage& f_b() const { return bbase::f(); }
  PopulationStorage& f_b() { return bbase::f(); }
  const PrimitiveVariablesStorage& pvs_a() const { return abase::pvs(); }
  PrimitiveVariablesStorage& pvs_a() { return abase::pvs(); }
  const PrimitiveVariablesStorage& pvs_b() const { return bbase::pvs(); }
  PrimitiveVariablesStorage& pvs_b() { return bbase::pvs(); }

 protected:
  typedef typename rbase::launch_configuration launch_configuration;

  virtual void swap_data() {
    abase::swap_data();
    bbase::swap_data();
  }

  virtual void start_halo_exchange(bool exchange_pvs) {
    abase::start_halo_exchange(exchange_pvs);
    bbase::start_halo_exchange(exchange_pvs);
  }

  virtual void finish_halo_exchange(bool exchange_pvs) {
    abase::finish_halo_exchange(exchange_pvs);
    bbase::finish_halo_exchange(exchange_pvs);
  }

  virtual bool boundary_uses_pvs() const {
    return abase::boundary_uses_pvs() || bbase::boundary_uses_pvs();
  }

  virtual void write_pvs(xdmf_writer& xdmf, const std::string& info) const {
    abase::write_pvs(xdmf, info + "_a");
    bbase::write_pvs(xdmf, info + "_b");
  }

  virtual void start_pvs_download(const stream& s) {
    abase::start_pvs_download(s);
    bbase::start_pvs_download(s);
  }

  virtual void launch_kernel(const launch_configuration& lc,
                             bool force_pvs_update) {
    if (boundary_uses_pvs() || force_pvs_update) {
      kernel::binary_mixture_step<
          true><<<lc.grid_size, lc.block_size, 0, rbase::compute_stream()>>>(
          lc.domain, lc.space, rbase::advector(), rbase::collision(),
          rbase::forcing(), abase::boundary(), bbase::boundary(),
          abase::f0().caccessor(), abase::f1().accessor(),
          bbase::f0().caccessor(), bbase::f1().accessor(),
          abase::pvs0().caccessor(), abase::pvs1().accessor(),
          bbase::pvs0().caccessor(), bbase::pvs1().accessor());
    } else {
      kernel::binary_mixture_step<
          false><<<lc.grid_size, lc.block_size, 0, rbase::compute_stream()>>>(
          lc.domain, lc.space, rbase::advector(), rbase::collision(),
          rbase::forcing(), abase::boundary(), bbase::boundary(),
          abase::f0().caccessor(), abase::f1().accessor(),
          bbase::f0().caccessor(), bbase::f1().accessor(),
          abase::pvs0().caccessor(), abase::pvs1().accessor(),
          bbase::pvs0().caccessor(), bbase::pvs1().accessor());
    }
  }

 private:
  binary_mixture(const binary_mixture&);
  binary_mixture& operator=(const binary_mixture&);
};

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_BINARY_MIXTURE_H

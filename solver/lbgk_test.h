#ifndef LABICEX_SOLVER_LBGK_TEST_H
#define LABICEX_SOLVER_LBGK_TEST_H

#include <algorithm>

#include "advector/onload.h"
#include "base/device_helpers.h"
#include "base/transfer.h"
#include "population/soa_contiguous.h"

namespace labicex {

namespace solver {

namespace kernel {

template <class Advector, class PopulationAccessor, class Real>
LABICEX_KERNEL void lbgk_test_step(const Advector advector,
                                   PopulationAccessor population0,
                                   PopulationAccessor population1,
                                   const Real beta) {
  typedef typename PopulationAccessor::lattice lattice;
  typedef typename lattice::equilibrium equilibrium;
  typedef typename lattice::rvector rvector;
  typedef typename lattice::ivector ivector;
  typedef typename lattice::fvector fvector;

  const ivector x = thread_index<lattice::d>();

  if (outside(x, population0.extents())) return;

  fvector f = advector.load(population0, x);

  fvector feq = equilibrium::get(lattice::conserved_moments(f));

  advector.store(population1, x, f + 2 * beta * (feq - f));
}

}  // kernel

template <class Lattice>
class lbgk_test {
 public:
  typedef Lattice lattice;
  typedef typename lattice::rvector rvector;
  typedef typename lattice::ivector ivector;
  typedef population::soa_contiguous<lattice> population_type;

  lbgk_test(const ivector& extents, real re, real uref)
      : f0_(extents),
        f1_(extents),
        advector_(index_mapping::periodic<lattice>(extents)),
        beta_(1 / (6 * uref * extents(0) / re + 1)) {}

  const population_type& f0() const { return f0_; }
  population_type& f0() { return f0_; }
  const population_type& f1() const { return f1_; }
  population_type& f1() { return f1_; }

  void step();

  real nu() const { return (1 - beta_) / (6 * beta_); }
  real beta() const { return beta_; }

 private:
  population::soa_contiguous<lattice> f0_, f1_;
  advector::onload<lattice, index_mapping::periodic<lattice> > advector_;
  real beta_;
};

template <class Lattice>
void lbgk_test<Lattice>::step() {
  dim3 grid_size, block_size;
  get_launch_configuration(f0_.extents(), 32, grid_size, block_size);
  kernel::lbgk_test_step<<<grid_size, block_size>>>(advector_, f0_.accessor(),
                                                      f1_.accessor(), beta_);
  LABICEX_CUDA_CHECK(cudaPeekAtLastError());
  LABICEX_CUDA_CHECK(cudaDeviceSynchronize());

  using std::swap;
  swap(f0_, f1_);
}

}  // solver

}  // labicex

#endif  // LABICEX_SOLVER_LBGK_TEST_H

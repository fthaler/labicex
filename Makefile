.PHONY: clean format wc

debug:
	$(MAKE) -C bin debug

release:
	$(MAKE) -C bin release

clean:
	$(MAKE) -C bin clean

format:
	clang-format -style='{BasedOnStyle: Google, Standard: Cpp03}' -i `find . -regex '.*\.\(cu\|h\)'`
	sed -i 's/\([^>] *\)> > >(/\1>>>(/g' `find . -regex '.*\.\(cu\|h\)'`

wc:
	wc `find . -regex '.*\.\(cu\|h\)'`

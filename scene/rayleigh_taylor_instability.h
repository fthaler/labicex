#ifndef LABICEX_RAYLEIGH_TAYLOR_INSTABILITY_H
#define LABICEX_RAYLEIGH_TAYLOR_INSTABILITY_H

#include <thrust/random.h>

#include "mpi/distributed_grid.h"

namespace labicex {

namespace scene {

namespace detail {

template <class Lattice, class Perturbation, bool FullyThermal>
class rayleigh_taylor_initial_condition {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::primitive_variables primitive_variables;

  rayleigh_taylor_initial_condition(const Perturbation& perturbation,
                                    real split_position, real pe, real m_a,
                                    real m_b, real molar_factor,
                                    real temperature, real x, real smoothing,
                                    real gravity, real mean_density)
      : perturbation_(perturbation),
        split_position_(split_position),
        pe_(pe),
        m_a_(m_a),
        m_b_(m_b),
        molar_factor_(molar_factor),
        temperature_(temperature),
        x_(x),
        smoothing_(smoothing),
        gravity_(gravity),
        mean_density_(mean_density) {}

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const ivector& x) const {
    const ivector x_global = space.local_to_global(x);

    const real lower_n_a = x_ * molar_factor_;
    const real lower_n_b = (1 - x_) * molar_factor_;
    const real lower_rho_ab = lower_n_a * m_a_ + lower_n_b * m_b_;

    const real upper_n_a = (1 - x_) * molar_factor_;
    const real upper_n_b = x_ * molar_factor_;
    const real upper_rho_ab = upper_n_a * m_a_ + upper_n_b * m_b_;

    const real n_ab = molar_factor_;

    const real lower_gravity = (1 - mean_density_ / lower_rho_ab) * gravity_;
    const real upper_gravity = (1 - mean_density_ / upper_rho_ab) * gravity_;
    real p_hydrostatic;
    if (x_global(1) <= split_position_) {
      p_hydrostatic =
          lower_rho_ab * lower_gravity * (x_global(1) - split_position_);
    } else {
      p_hydrostatic =
          upper_rho_ab * upper_gravity * (x_global(1) - split_position_);
    }

    real corrected_temperature = temperature_;
    if (FullyThermal) corrected_temperature -= p_hydrostatic / n_ab;
    assert(corrected_temperature > 0);

    ivector split_offset = ivector::Zero();
    split_offset(1) = split_position_;
    const ivector x_split_global = x_global - split_offset;
    const ivector x_split = space.global_to_local(x_split_global);
    const real erf_arg =
        (x_split_global(1) + perturbation_(space, x_split)) / smoothing_;
    const real x0 = 0.5 + (0.5 - x_) * erf(erf_arg);

    const real fac =
        (1 - 2 * x_) / (sqrt(M_PI) * smoothing_) * exp(-ipow(erf_arg, 2));
    rvector x0_gradient = fac * perturbation_.gradient(space, x_split);
    x0_gradient(1) += fac;
    const real rho_ab = molar_factor_ * (x0 * m_a_ + (1 - x0) * m_b_);
    const rvector rho_ab_gradient = (m_a_ - m_b_) * molar_factor_ * x0_gradient;
    rvector velocity = -rho_ab_gradient / (pe_ * rho_ab);

    velocity += perturbation_.velocity(space, x_split);

    return primitive_variables(x0 * molar_factor_ * m_a_, velocity,
                               corrected_temperature / m_a_);
  }

 private:
  Perturbation perturbation_;
  real split_position_, pe_, m_a_, m_b_, molar_factor_, temperature_, x_,
      smoothing_, gravity_, mean_density_;
};

template <class Lattice>
class rayleigh_taylor_boundary_condition {
 public:
  static const bool uses_primitive_variables = true;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::equilibrium equilibrium;
  typedef typename Lattice::numerical_equilibrium numerical_equilibrium;

  rayleigh_taylor_boundary_condition(const ivector& global_extents,
                                     const primitive_variables& lower_pv,
                                     const primitive_variables& upper_pv)
      : global_extents_(global_extents),
        lower_pv_(lower_pv),
        upper_pv_(upper_pv) {}

  template <class PopulationAccessor, class PrimitiveVariableAccessor>
  LABICEX_DEVICE void update(const kernel_space<Lattice::d>& space,
                             const ivector& x,
                             const PopulationAccessor& population,
                             const PrimitiveVariableAccessor& pva,
                             fvector& f) const {
    const ivector x_global = space.local_to_global(x);
    if (x_global(1) < Lattice::max_ci) {
      // f = equilibrium::get(lower_pv_);
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (Lattice::ci(i)(1) > 0 && x_global(1) < Lattice::ci(i)(1))
          f(i) = f(Lattice::reverse_index(i));
      }
    } else if (x_global(1) >= global_extents_(1) - Lattice::max_ci) {
      // f = equilibrium::get(upper_pv_);
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (Lattice::ci(i)(1) < 0 &&
            x_global(1) >= global_extents_(1) + Lattice::ci(i)(1))
          f(i) = f(Lattice::reverse_index(i));
      }
    }
  }

 private:
  ivector global_extents_;
  primitive_variables lower_pv_, upper_pv_;
};

template <class Lattice>
class rayleigh_taylor_forcing {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::rtensor rtensor;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef typename Lattice::conserved_moments conserved_moments;
  typedef typename Lattice::equilibrium equilibrium;
  typedef typename Lattice::numerical_equilibrium numerical_equilibrium;

  rayleigh_taylor_forcing(real m_a, real m_b, real gravity, real mean_density)
      : m_a_(m_a), m_b_(m_b), gravity_(gravity), mean_density_(mean_density) {}

  LABICEX_DEVICE void update(fvector& f_a, fvector& f_b) const {
    // gravity
    if (gravity_ != 0) {
      const primitive_variables pv_ab((f_a + f_b).eval());

      const real acceleration =
          (1 - mean_density_ / pv_ab.density()) * gravity_;

      const primitive_variables pv_a(f_a.sum(), pv_ab.velocity(),
                                     pv_ab.temperature());
      const primitive_variables pv_b(f_b.sum(), pv_ab.velocity(),
                                     pv_ab.temperature());

      equilibrium::gravity_update(pv_a, acceleration, f_a);
      equilibrium::gravity_update(pv_b, acceleration, f_b);
    }
  }

 private:
  real m_a_, m_b_, gravity_, mean_density_;
};

}  // detail

// initial condition following Taylor's "The Instability of Liquid Surfaces
// when Accelerated in a Direction Perpendicular to their Planes. I"
template <class Lattice>
class rayleigh_taylor_lst_perturbation {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;

  rayleigh_taylor_lst_perturbation(real magnitude, real n, real k)
      : magnitude_(magnitude), n_(n), k_(k) {}

  LABICEX_COMMON_INLINE real operator()(const kernel_space<Lattice::d>& space,
                                        const ivector& x) const {
    const ivector x_global = space.local_to_global(x);
    return magnitude_ * k_ * cos(x_global(0) * k_);
  }

  LABICEX_COMMON_INLINE rvector gradient(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    const ivector x_global = space.local_to_global(x);
    rvector g = rvector::Zero();
    g(0) = -magnitude_ * k_ * k_ * sin(x_global(0) * k_);
    return g;
  }

  LABICEX_COMMON_INLINE rvector velocity(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    const ivector x_global = space.local_to_global(x);
    rvector v = rvector::Zero();

    const real eta = magnitude_ * k_ * cos(x_global(0) * k_);
    v(0) = x_global(1) >= eta
               ? -n_ * k_ * magnitude_ * exp(-x_global(1) * k_) *
                     sin(x_global(0) * k_)
               : n_ * k_ * magnitude_ * exp(x_global(1) * k_) *
                     sin(x_global(0) * k_);
    v(1) = x_global(1) >= eta
               ? -n_ * k_ * magnitude_ * exp(-x_global(1) * k_) *
                     cos(x_global(0) * k_)
               : -n_ * k_ * magnitude_ * exp(x_global(1) * k_) *
                     cos(x_global(0) * k_);
    return v;
  }

 private:
  real magnitude_, n_, k_;
};

template <class Lattice>
class rayleigh_taylor_sine_perturbation {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;

  rayleigh_taylor_sine_perturbation(real magnitude, real scale)
      : magnitude_(magnitude), scale_(scale) {}

  LABICEX_COMMON_INLINE real operator()(const kernel_space<Lattice::d>& space,
                                        const ivector& x) const {
    const ivector x_global = space.local_to_global(x);
    return magnitude_ * sin(x_global(0) * 2 * M_PI * scale_);
  }

  LABICEX_COMMON_INLINE rvector gradient(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    const ivector x_global = space.local_to_global(x);
    rvector g = rvector::Zero();
    g(0) =
        magnitude_ * 2 * M_PI * scale_ * cos(x_global(0) * 2 * M_PI * scale_);
    return g;
  }

  LABICEX_COMMON_INLINE rvector velocity(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    return rvector::Zero();
  }

 private:
  real magnitude_, scale_;
};

template <class Lattice>
class rayleigh_taylor_multiscale_perturbation {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;

  rayleigh_taylor_multiscale_perturbation(real magnitude, real scale, int count)
      : magnitude_(magnitude), scale_(scale), count_(count) {}

  LABICEX_COMMON_INLINE real operator()(const kernel_space<Lattice::d>& space,
                                        const ivector& x) const {
    const ivector x_global = space.local_to_global(x);

    thrust::default_random_engine rng(1034);
    thrust::uniform_real_distribution<real> dist(-1, 1);
    real sum = 0;
    for (int i = 0; i < count_; ++i) {
      const real a = magnitude_ * dist(rng);
      const real b = scale_ * dist(rng);
      const real c = scale_ * dist(rng);
      sum += a * sin(2 * M_PI * (x_global(0) * b + c));
    }
    return sum;
  }

  LABICEX_COMMON_INLINE rvector gradient(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    const ivector x_global = space.local_to_global(x);

    thrust::default_random_engine rng(1034);
    thrust::uniform_real_distribution<real> dist(0, 1);
    rvector g = rvector::Zero();
    for (int i = 0; i < count_; ++i) {
      const real a = magnitude_ * dist(rng);
      const real b = scale_ * dist(rng);
      const real c = scale_ * dist(rng);
      g(0) += 2 * M_PI * a * b * cos(2 * M_PI * (x_global(0) * b + c));
    }
    return g;
  }

  LABICEX_COMMON_INLINE rvector velocity(const kernel_space<Lattice::d>& space,
                                         const ivector& x) const {
    return rvector::Zero();
  }

 private:
  real magnitude_, scale_;
  int count_;
};

template <class Lattice, class Perturbation, bool FullyThermal>
class rayleigh_taylor_instability {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef detail::rayleigh_taylor_initial_condition<Lattice, Perturbation,
                                                    FullyThermal>
      initial_condition;
  typedef detail::rayleigh_taylor_boundary_condition<Lattice>
      boundary_condition;
  typedef detail::rayleigh_taylor_forcing<Lattice> forcing;

  rayleigh_taylor_instability(const mpi::distributed_grid<Lattice>& grid,
                              const Perturbation& perturbation,
                              real split_position, real pe, real m_a, real m_b,
                              real molar_factor, real temperature, real x,
                              real smoothing, real gravity)
      : grid_(grid),
        perturbation_(perturbation),
        split_position_(split_position),
        pe_(pe),
        m_a_(m_a),
        m_b_(m_b),
        molar_factor_(molar_factor),
        temperature_(temperature),
        x_(x),
        smoothing_(smoothing),
        gravity_(gravity),
        mean_density_(molar_factor * (m_a + m_b) / 2),
        lower_temperature_(temperature),
        upper_temperature_(temperature) {
    if (FullyThermal) {
      const real lower_n_a = x_ * molar_factor_;
      const real lower_n_b = (1 - x_) * molar_factor_;
      const real lower_rho_ab = lower_n_a * m_a_ + lower_n_b * m_b_;

      const real upper_n_a = (1 - x_) * molar_factor_;
      const real upper_n_b = x_ * molar_factor_;
      const real upper_rho_ab = upper_n_a * m_a_ + upper_n_b * m_b_;

      const real n_ab = molar_factor_;

      const real lower_gravity = (1 - mean_density_ / lower_rho_ab) * gravity_;
      const real upper_gravity = (1 - mean_density_ / upper_rho_ab) * gravity_;

      const int lower_y = -1;
      const int upper_y = grid_.local().global_domain().extents()(1);

      const real lower_p_hydrostatic =
          lower_rho_ab * lower_gravity * (lower_y - split_position_);
      const real upper_p_hydrostatic =
          upper_rho_ab * upper_gravity * (upper_y - split_position_);

      lower_temperature_ -= lower_p_hydrostatic / n_ab;
      upper_temperature_ -= upper_p_hydrostatic / n_ab;
    }
  }

  initial_condition get_initial_condition_a() const {
    return initial_condition(perturbation_, split_position_, pe_, m_a_, m_b_,
                             molar_factor_, temperature_, x_, smoothing_,
                             gravity_, mean_density_);
  }

  initial_condition get_initial_condition_b() const {
    return initial_condition(perturbation_, split_position_, pe_, m_b_, m_a_,
                             molar_factor_, temperature_, 1 - x_, smoothing_,
                             gravity_, mean_density_);
  }

  boundary_condition get_boundary_condition_a() const {
    return boundary_condition(
        grid_.local().global_domain().extents(),
        primitive_variables(molar_factor_ * x_ * m_a_, rvector::Zero(),
                            lower_temperature_ / m_a_),
        primitive_variables(molar_factor_ * (1 - x_) * m_a_, rvector::Zero(),
                            upper_temperature_ / m_a_));
  }

  boundary_condition get_boundary_condition_b() const {
    return boundary_condition(
        grid_.local().global_domain().extents(),
        primitive_variables(molar_factor_ * (1 - x_) * m_b_, rvector::Zero(),
                            lower_temperature_ / m_b_),
        primitive_variables(molar_factor_ * x_ * m_b_, rvector::Zero(),
                            upper_temperature_ / m_b_));
  }

  forcing get_forcing() const {
    return forcing(m_a_, m_b_, gravity_, mean_density_);
  }

 private:
  const mpi::distributed_grid<Lattice>& grid_;
  Perturbation perturbation_;
  real split_position_, pe_, m_a_, m_b_, molar_factor_, temperature_, x_,
      smoothing_, gravity_, mean_density_, lower_temperature_,
      upper_temperature_;
};

}  // scene

}  // labicex

#endif  // LABICEX_RAYLEIGH_TAYLOR_INSTABILITY_H

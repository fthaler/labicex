#ifndef LABICEX_SCENE_KARMAN_VORTEX_STREET_H
#define LABICEX_SCENE_KARMAN_VORTEX_STREET_H

#include <thrust/random.h>

#include "boundary/boundary_map.h"
#include "boundary/hypercylinder.h"
#include "boundary/noslip_updater.h"
#include "mpi/distributed_grid.h"

namespace labicex {

namespace scene {

namespace detail {

template <class Lattice, class BaseFunction>
class karman_vortex_street_wall_function {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::primitive_variables primitive_variables;

  karman_vortex_street_wall_function(const BaseFunction& base_function)
      : base_function_(base_function) {}

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const rvector& x) const {
    primitive_variables pv = base_function_(space, x.template cast<int>());
    pv.velocity() = rvector::Zero();
    return pv;
  }

 private:
  BaseFunction base_function_;
};

template <class Lattice, class BaseFunction>
class karman_vortex_street_initial_condition {
 public:
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef boundary::hypercylinder<Lattice::d, 2> hypercylinder;

  karman_vortex_street_initial_condition(const ivector& global_extents,
                                         const BaseFunction& base_function,
                                         const hypercylinder& cylinder,
                                         real disturbance)
      : global_extents_(global_extents),
        base_function_(base_function),
        cylinder_(cylinder),
        disturbance_(disturbance) {}

  LABICEX_COMMON_INLINE primitive_variables
  operator()(const kernel_space<Lattice::d>& space, const ivector& x) const {
    primitive_variables pv = base_function_(space, x);

    if (cylinder_.inside(x.template cast<real>())) {
      pv.velocity() = rvector::Zero();
      return pv;
    }

    const ivector x_global = space.local_to_global(x);

    LABICEX_UNROLL
    for (int j = 0; j < Lattice::d; ++j) {
      if (x_global(j) < Lattice::max_ci ||
          x_global(j) >= global_extents_(j) - Lattice::max_ci)
        return pv;
    }

    thrust::default_random_engine rng(hash(x_global));
    thrust::uniform_real_distribution<real> dist(-disturbance_, disturbance_);

    LABICEX_UNROLL
    for (int j = 0; j < Lattice::d; ++j) pv.velocity()(j) += dist(rng);

    return pv;
  }

 private:
  LABICEX_COMMON_INLINE static unsigned hash(const ivector& x_global) {
    const int factors[3] = {1, 100003, 1000003};
    unsigned h = 0;
    LABICEX_UNROLL
    for (int i = 0; i < Lattice::d; ++i)
      h += (factors[i] + hash(x_global(i))) * factors[i];
    return h;
  }

  LABICEX_COMMON_INLINE static unsigned hash(unsigned u) {
    u = (u + 0x7ed55d16) + (u << 12);
    u = (u ^ 0xc761c23c) ^ (u >> 19);
    u = (u + 0x165667b1) + (u << 5);
    u = (u + 0xd3a2646c) ^ (u << 9);
    u = (u + 0xfd7046c5) + (u << 3);
    u = (u ^ 0xb55a4f09) ^ (u >> 16);
    return u;
  }

  ivector global_extents_;
  BaseFunction base_function_;
  hypercylinder cylinder_;
  real disturbance_;
};

template <class Lattice, class BaseFunction>
class karman_vortex_street_boundary_condition {
 public:
  static const bool uses_primitive_variables = true;
  typedef typename Lattice::rvector rvector;
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::fvector fvector;
  typedef typename Lattice::primitive_variables primitive_variables;
  typedef boundary::hypercylinder<Lattice::d, 2> hypercylinder;
  typedef karman_vortex_street_wall_function<Lattice, BaseFunction>
      wall_function;
  typedef boundary::boundary_map<Lattice, indexer<Lattice::d>, int>
      boundary_map;
  typedef typename boundary_map::template accessor_type<wall_function>::type
      boundary_map_accessor;
  typedef boundary::noslip_updater<Lattice, boundary_map_accessor> updater;

  karman_vortex_street_boundary_condition(const boundary_map_accessor& bma,
                                          const ivector& global_extents,
                                          const BaseFunction& base_function,
                                          const hypercylinder& cylinder,
                                          real dynamic_viscosity)
      : noslip_(bma),
        global_extents_(global_extents),
        base_function_(base_function),
        cylinder_(cylinder),
        dynamic_viscosity_(dynamic_viscosity) {}

  template <class PopulationAccessor, class PrimitiveVariablesAccessor>
  LABICEX_COMMON void update(const kernel_space<Lattice::d>& space,
                             const ivector& x,
                             const PopulationAccessor& population,
                             const PrimitiveVariablesAccessor& pva,
                             fvector& f) const {
    if (cylinder_.inside(x.template cast<real>())) {
      primitive_variables pv = base_function_(space, x);
      pv.velocity() = rvector::Zero();
      f = Lattice::equilibrium::get(pv);
      return;
    }

    // TODO: better beta?
    primitive_variables pvf;
    pva.load(x, pvf);
    const real kinematic_viscosity = dynamic_viscosity_ / pvf.density();
    const real beta = 1 / (6 * kinematic_viscosity + 1);
    noslip_.update(space, x, population, pva, beta, f);

    const ivector x_global = space.local_to_global(x);

    // outflow (no boundary)
    if (x_global(0) >= global_extents_(0) - Lattice::max_ci) {
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (x_global(0) >= global_extents_(0) + Lattice::ci(i)(0))
          f(i) = population.load_single(i, x);
      }
      /*const fvector feq = Lattice::equilibrium::get(pvf);
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (x_global(0) >= global_extents_(0) + Lattice::ci(i)(0))
          f(i) = feq(i);
      }*/
    }

    // free slip
    bool corner = x_global(0) >= global_extents_(0) - Lattice::max_ci;
    if (x_global(1) < Lattice::max_ci) {
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (x_global(1) < Lattice::ci(i)(1))
          f(i) = f(Lattice::single_reverse_index(i, 1));
      }
    } else if (x_global(1) >= global_extents_(1) - Lattice::max_ci) {
      LABICEX_UNROLL
      for (int i = 0; i < Lattice::q; ++i) {
        if (x_global(1) >= global_extents_(1) + Lattice::ci(i)(1))
          f(i) = f(Lattice::single_reverse_index(i, 1));
      }
    } else {
      corner = false;
    }

    // inflow (equilibrium) & corners
    if (x_global(0) < Lattice::max_ci || corner)
      f = Lattice::equilibrium::get(base_function_(space, x));
  }

  template <class PopulationAccessorA, class PopulationAccessorB,
            class PrimitiveVariablesAccessorA,
            class PrimitiveVariablesAccessorB>
  LABICEX_COMMON void update(const kernel_space<Lattice::d>& space,
                             const ivector& x,
                             const PopulationAccessorA& population,
                             const PopulationAccessorB&,
                             const PrimitiveVariablesAccessorA& pva,
                             const PrimitiveVariablesAccessorB&,
                             fvector& f) const {
    update(space, x, population, pva, f);
  }

 private:
  updater noslip_;
  ivector global_extents_;
  BaseFunction base_function_;
  hypercylinder cylinder_;
  real dynamic_viscosity_;
};

}  // detail

template <class Lattice, class BaseFunction>
class karman_vortex_street {
 public:
  typedef typename Lattice::ivector ivector;
  typedef typename Lattice::rvector rvector;
  typedef boundary::hypercylinder<Lattice::d, 2> hypercylinder;
  typedef BaseFunction base_function;
  typedef detail::karman_vortex_street_wall_function<Lattice, BaseFunction>
      wall_function;
  typedef detail::karman_vortex_street_initial_condition<Lattice, BaseFunction>
      initial_condition;
  typedef detail::karman_vortex_street_boundary_condition<Lattice, BaseFunction>
      boundary_condition;
  typedef boundary::boundary_map<Lattice, indexer<Lattice::d>, int>
      boundary_map;

  karman_vortex_street(const mpi::distributed_grid<Lattice>& grid,
                       const BaseFunction& base_function,
                       real reference_velocity, real relative_disturbance,
                       real re)
      : grid_(grid),
        base_function_(base_function),
        bm_(grid.local().full_domain().extents()),
        disturbance_(relative_disturbance * reference_velocity),
        dynamic_viscosity_(reference_velocity * d() / re) {
    bm_.build(cylinder());
  }

  real d() const { return grid_.global().global_domain().extents()(1) / 20.0; }

  initial_condition get_initial_condition() const {
    return initial_condition(grid_.global().global_domain().extents(),
                             base_function_, cylinder(), disturbance_);
  }

  boundary_condition get_boundary_condition() const {
    return boundary_condition(bm_.accessor(wall_function(base_function_)),
                              grid_.global().global_domain().extents(),
                              base_function_, cylinder(), dynamic_viscosity_);
  }

  static ivector local_extents_from_d(
      const mpi::process_grid<Lattice::d>& processes, int d) {
    ivector global_extents = ivector::Constant(20 * d);
    global_extents(0) = 80 * d;
    return mpi::distributed_grid<Lattice>::local_extents_from_global(
        processes, global_extents, false);
  }

 private:
  hypercylinder cylinder() const {
    const domain::box<Lattice::d> domain = grid_.local().global_domain();
    rvector center = (domain.min() + domain.max()).template cast<real>() / 2.0;
    center(0) = domain.min()(0) + 10 * d();
    return hypercylinder(center, d() / 2);
  }

  const mpi::distributed_grid<Lattice>& grid_;
  boundary_map bm_;
  BaseFunction base_function_;
  real disturbance_, dynamic_viscosity_;
};

}  // scene

}  // labicex

#endif  // LABICEX_SCENE_KARMAN_VORTEX_STREET_H

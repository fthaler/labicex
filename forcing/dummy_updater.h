#ifndef LABICEX_FORCING_DUMMY_UPDATER_H
#define LABICEX_FORCING_DUMMY_UPDATER_H

namespace labicex {

namespace forcing {

template <class Lattice>
class dummy_updater {
 public:
  typedef typename Lattice::fvector fvector;

  LABICEX_COMMON_INLINE void update(fvector&) const {}
  LABICEX_COMMON_INLINE void update(fvector&, fvector&) const {}
};

}  // forcing

}  // labicex

#endif  // LABICEX_FORCING_DUMMY_UPDATER_H

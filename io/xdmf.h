#ifndef LABICEX_IO_XDMF_H
#define LABICEX_IO_XDMF_H

#include <iomanip>
#include <sstream>
#include <string>

#include <pugixml.hpp>

#include "io/hdf5.h"
#include "mpi/distributed_grid.h"
#include "mpi/setup.h"

namespace labicex {

namespace io {

template <class DistributedGrid>
class xdmf_writer {
 public:
  typedef typename DistributedGrid::lattice lattice;
  typedef typename lattice::ivector ivector;

  xdmf_writer(const std::string filename, const DistributedGrid& grid,
              int max_time, int start_time = 0);
  ~xdmf_writer();

  void set_time(int time);

  template <class T>
  void write_information(const std::string& name, const T& value);

  template <class Storage>
  void write_primitive(const Storage& storage, const std::string& info = "");

  template <class Storage>
  void write_population(const Storage& storage, const std::string& info = "");

  void save();

 private:
  template <class Storage>
  void write_attribute(const Storage& storage, int array_index, hdf5_ptr hf,
                       const std::string& name);

  std::string hdf5_filename(const std::string& kind) const;
  static std::string basename(const std::string& filename);
  static std::string hdf5_dim(const ivector& v);

  const DistributedGrid& grid_;
  pugi::xml_document document_;
  pugi::xml_node lattice_;
  std::string filename_;
  int time_, time_digits_;
};

template <class DistributedGrid>
xdmf_writer<DistributedGrid>::xdmf_writer(const std::string filename,
                                          const DistributedGrid& grid,
                                          int max_time, int start_time)
    : grid_(grid), filename_(filename) {
  document_.append_child(pugi::node_doctype)
      .set_value("Xdmf SYSTEM \"Xdmf.dtd\" []");

  pugi::xml_node xdmf = document_.append_child("Xdmf");
  xdmf.append_attribute("Version").set_value("2.0");

  pugi::xml_node domain = xdmf.append_child("Domain");

  pugi::xml_node lattice_collection = domain.append_child("Grid");
  lattice_collection.append_attribute("Name").set_value("LatticeCollection");
  lattice_collection.append_attribute("GridType").set_value("Collection");
  lattice_collection.append_attribute("CollectionType").set_value("Temporal");

  std::stringstream s;
  s << max_time;
  time_digits_ = s.str().size();

  set_time(start_time);
}

template <class DistributedGrid>
xdmf_writer<DistributedGrid>::~xdmf_writer() {
  save();
}

template <class DistributedGrid>
void xdmf_writer<DistributedGrid>::set_time(int time) {
  time_ = time;

  std::stringstream xpath_stream;
  xpath_stream << "/Xdmf/Domain/Grid/Grid/Time[@Value = \"" << time_ << "\"]";
  pugi::xpath_node xtime =
      document_.select_single_node(xpath_stream.str().c_str());
  if (xtime) {
    lattice_ = xtime.parent();
  } else {
    pugi::xpath_node lattice_collection =
        document_.select_single_node("/Xdmf/Domain/Grid");
    lattice_ = lattice_collection.node().append_child("Grid");
    lattice_.append_attribute("Name").set_value("Lattice");

    pugi::xml_node time = lattice_.append_child("Time");
    time.append_attribute("Value").set_value(time_);

    pugi::xml_node geometry = lattice_.append_child("Geometry");
    assert(DistributedGrid::d == 2 || DistributedGrid::d == 3);
    if (DistributedGrid::d == 2)
      geometry.append_attribute("Type").set_value("Origin_DxDy");
    else
      geometry.append_attribute("Type").set_value("Origin_DxDyDz");

    pugi::xml_node origin = geometry.append_child("DataItem");
    origin.append_attribute("Dimensions").set_value(lattice::d);
    origin.append_attribute("Format").set_value("XML");
    origin.text().set(hdf5_dim(ivector::Zero()).c_str());

    pugi::xml_node dxdydz = geometry.append_child("DataItem");
    dxdydz.append_attribute("Dimensions").set_value(lattice::d);
    dxdydz.append_attribute("Format").set_value("XML");
    dxdydz.text().set(hdf5_dim(ivector::Ones()).c_str());

    pugi::xml_node topology = lattice_.append_child("Topology");
    topology.append_attribute("Dimensions")
        .set_value(hdf5_dim(grid_.global().global_domain().extents()).c_str());
    assert(lattice::d == 2 || lattice::d == 3);
    if (lattice::d == 2)
      topology.append_attribute("Type").set_value("2DCoRectMesh");
    else if (lattice::d == 3)
      topology.append_attribute("Type").set_value("3DCoRectMesh");
  }
}

template <class DistributedGrid>
template <class T>
void xdmf_writer<DistributedGrid>::write_information(const std::string& name,
                                                     const T& value) {
  std::stringstream value_stream;
  value_stream << value;
  pugi::xml_node info = document_.child("Xdmf").append_child("Information");
  info.append_attribute("Name").set_value(name.c_str());
  info.append_attribute("Value").set_value(value_stream.str().c_str());
}

template <class DistributedGrid>
template <class Storage>
void xdmf_writer<DistributedGrid>::write_primitive(const Storage& storage,
                                                   const std::string& info) {
  assert((storage.extents() == grid_.global().full_domain().extents()).all());
  const std::string h5name = hdf5_filename("primitve" + info);
  hdf5_ptr hf = hdf5_parallel_file::create(h5name, grid_.processes().comm());
  write_attribute(storage, 0, hf, "rho" + info);
  write_attribute(storage, 1, hf, "ux" + info);
  if (lattice::d > 1) write_attribute(storage, 2, hf, "uy" + info);
  if (lattice::d > 2) write_attribute(storage, 3, hf, "uz" + info);
  if (lattice::primitive_variables::count > lattice::d + 1)
    write_attribute(storage, lattice::d + 1, hf, "T" + info);

  save();
  if (!storage.check()) {
    std::cerr << "\nXDMF storage check failed -> early exit" << std::endl;
    mpi::setup::global_abort(EXIT_SUCCESS);
  }
}

template <class DistributedGrid>
template <class Storage>
void xdmf_writer<DistributedGrid>::write_population(const Storage& storage,
                                                    const std::string& info) {
  assert((storage.extents() == grid_.global().full_domain().extents()).all());
  const std::string h5name = hdf5_filename("population" + info);
  hdf5_ptr hf = hdf5_parallel_file::create(h5name, grid_.processes().comm());
  for (int i = 0; i < lattice::q; ++i) {
    std::stringstream s;
    s << "f" << i << info;
    write_attribute(storage, i, hf, s.str());
  }

  save();
  if (!storage.check()) {
    std::cerr << "\nXDMF storage check failed -> early exit" << std::endl;
    mpi::setup::global_abort(EXIT_SUCCESS);
  }
}

template <class DistributedGrid>
void xdmf_writer<DistributedGrid>::save() {
  if (grid_.processes().local_process().rank() == 0)
    document_.save_file(filename_.c_str(), "  ");
}

template <class DistributedGrid>
template <class Storage>
void xdmf_writer<DistributedGrid>::write_attribute(const Storage& storage,
                                                   int array_index, hdf5_ptr hf,
                                                   const std::string& name) {
  hf->write_single_haloed(name, storage, array_index, grid_.halo_width(),
                          grid_.global().global_domain().extents(),
                          grid_.global().nohalo_domain().min());
  pugi::xml_node a = lattice_.append_child("Attribute");
  a.append_attribute("Name").set_value(name.c_str());
  pugi::xml_node data = a.append_child("DataItem");
  data.append_attribute("Format").set_value("HDF");
  data.append_attribute("NumberType").set_value("Float");
  data.append_attribute("Precision").set_value(static_cast<int>(sizeof(real)));
  data.append_attribute("Dimensions")
      .set_value(hdf5_dim(grid_.global().global_domain().extents()).c_str());
  data.text().set((hf->filename() + ":/" + name).c_str());
}

template <class DistributedGrid>
std::string xdmf_writer<DistributedGrid>::hdf5_filename(
    const std::string& kind) const {
  std::size_t dotpos = filename_.find_last_of('.');
  std::stringstream s;
  s << filename_.substr(0, dotpos) << "_" << kind << "_" << std::setfill('0')
    << std::setw(time_digits_) << time_ << ".h5";
  return s.str();
}

template <class DistributedGrid>
std::string xdmf_writer<DistributedGrid>::basename(
    const std::string& filename) {
  std::size_t slashpos = filename.find_last_of('/');
  if (slashpos == std::string::npos) return filename;
  return filename.substr(slashpos + 1);
}

template <class DistributedGrid>
std::string xdmf_writer<DistributedGrid>::hdf5_dim(const ivector& iv) {
  std::stringstream s;
  for (int i = iv.size() - 1; i > 0; --i) s << iv(i) << " ";
  s << iv(0);
  return s.str();
}

}  // io

}  // labicex

#endif  //  LABICEX_IO_XDMF_H

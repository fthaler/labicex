#ifndef LABICE_IO_SERIAL_XDMF
#define LABICE_IO_SERIAL_XDMF

#include <iomanip>
#include <sstream>

#include <boost/array.hpp>
#include <pugixml.hpp>

#include "io/hdf5.h"

namespace labicex {

namespace io {

template <class Lattice>
class xdmf_writer {
 public:
  typedef Lattice lattice;
  typedef typename lattice::rvector rvector;
  typedef typename lattice::ivector ivector;

  xdmf_writer(const std::string& filename, const ivector& extents,
              int initial_time = 0);
  ~xdmf_writer();

  void set_time(int time) { time_ = time; }

  template <class Storage>
  void write_primitive(const Storage& storage, const std::string& info = "");

  template <class Storage>
  void write_population(const Storage& storage, const std::string& info = "");

 private:
  pugi::xml_node get_lattice();

  std::string hdf5_filename(const std::string& kind) const;
  static std::string basename(const std::string& filename);
  static std::string ivstr(const ivector& iv);

  std::string filename_;
  ivector extents_;
  pugi::xml_document document_;
  int time_;
};

template <class Lattice>
xdmf_writer<Lattice>::xdmf_writer(const std::string& filename,
                                  const ivector& extents, int initial_time)
    : filename_(filename), extents_(extents), time_(initial_time) {
  document_.append_child(pugi::node_doctype)
      .set_value("Xdmf SYSTEM \"Xdmf.dtd\" []");

  pugi::xml_node xdmf = document_.append_child("Xdmf");
  xdmf.append_attribute("Version").set_value("2.0");

  pugi::xml_node domain = xdmf.append_child("Domain");

  pugi::xml_node lattice_collection = domain.append_child("Grid");
  lattice_collection.append_attribute("Name").set_value("LatticeCollection");
  lattice_collection.append_attribute("GridType").set_value("Collection");
  lattice_collection.append_attribute("CollectionType").set_value("Temporal");
}

template <class Lattice>
xdmf_writer<Lattice>::~xdmf_writer() {
  document_.save_file(filename_.c_str(), "  ");
}

template <class Lattice>
template <class Storage>
void xdmf_writer<Lattice>::write_primitive(const Storage& storage,
                                           const std::string& info) {
  assert((extents_ == storage.extents()).all());
  std::string h5name = hdf5_filename("primitive" + info);
  std::string h5base = basename(h5name) + ":/";
  {
    hdf5_ptr hf = hdf5_file::create(h5name);
    hf->write_single("rho", storage, 0);
    hf->write_single("ux", storage, 1);
    if (Lattice::d > 1) {
      hf->write_single("uy", storage, 2);
      if (Lattice::d > 2) hf->write_single("uz", storage, 3);
    }
    if (Storage::array_count > Lattice::d + 1)
      hf->write_single("T", storage, Lattice::d + 1);
  }

  pugi::xml_node lattice = get_lattice();

  pugi::xml_node rho = lattice.append_child("Attribute");
  rho.append_attribute("Name").set_value(("rho" + info).c_str());
  pugi::xml_node rho_data = rho.append_child("DataItem");
  rho_data.append_attribute("Format").set_value("HDF");
  rho_data.append_attribute("NumberType").set_value("Float");
  rho_data.append_attribute("Precision")
      .set_value(static_cast<int>(sizeof(real)));
  rho_data.append_attribute("Dimensions")
      .set_value(ivstr(extents_.reverse()).c_str());
  rho_data.text().set((h5base + "rho").c_str());

  pugi::xml_node ux = lattice.append_child("Attribute");
  ux.append_attribute("Name").set_value(("ux" + info).c_str());
  pugi::xml_node ux_data = ux.append_child("DataItem");
  ux_data.append_attribute("Format").set_value("HDF");
  ux_data.append_attribute("NumberType").set_value("Float");
  ux_data.append_attribute("Precision")
      .set_value(static_cast<int>(sizeof(real)));
  ux_data.append_attribute("Dimensions")
      .set_value(ivstr(extents_.reverse()).c_str());
  ux_data.text().set((h5base + "ux").c_str());

  if (lattice::d > 1) {
    pugi::xml_node uy = lattice.append_child("Attribute");
    uy.append_attribute("Name").set_value(("uy" + info).c_str());
    pugi::xml_node uy_data = uy.append_child("DataItem");
    uy_data.append_attribute("Format").set_value("HDF");
    uy_data.append_attribute("NumberType").set_value("Float");
    uy_data.append_attribute("Precision")
        .set_value(static_cast<int>(sizeof(real)));
    uy_data.append_attribute("Dimensions")
        .set_value(ivstr(extents_.reverse()).c_str());
    uy_data.text().set((h5base + "uy").c_str());

    if (lattice::d > 2) {
      pugi::xml_node uz = lattice.append_child("Attribute");
      uz.append_attribute("Name").set_value(("uz" + info).c_str());
      pugi::xml_node uz_data = uz.append_child("DataItem");
      uz_data.append_attribute("Format").set_value("HDF");
      uz_data.append_attribute("NumberType").set_value("Float");
      uz_data.append_attribute("Precision")
          .set_value(static_cast<int>(sizeof(real)));
      uz_data.append_attribute("Dimensions")
          .set_value(ivstr(extents_.reverse()).c_str());
      uz_data.text().set((h5base + "uz").c_str());
    }
  }

  if (Storage::array_count > Lattice::d + 1) {
    pugi::xml_node t = lattice.append_child("Attribute");
    t.append_attribute("Name").set_value(("T" + info).c_str());
    pugi::xml_node t_data = t.append_child("DataItem");
    t_data.append_attribute("Format").set_value("HDF");
    t_data.append_attribute("NumberType").set_value("Float");
    t_data.append_attribute("Precision")
        .set_value(static_cast<int>(sizeof(real)));
    t_data.append_attribute("Dimensions")
        .set_value(ivstr(extents_.reverse()).c_str());
    t_data.text().set((h5base + "T").c_str());
  }
}

template <class Lattice>
template <class Storage>
void xdmf_writer<Lattice>::write_population(const Storage& storage,
                                            const std::string& info) {
  assert((extents_ == storage.extents()).all());
  std::string h5name = hdf5_filename("f" + info);
  std::string h5base = basename(h5name) + ":/";
  {
    hdf5_ptr hf = hdf5_file::create(h5name);
    hf->write_all("f" + info, storage);
  }

  pugi::xml_node lattice = get_lattice();

  for (int i = 0; i < lattice::q; ++i) {
    std::stringstream s;
    s << "f" << info << i;
    pugi::xml_node population = lattice.append_child("Attribute");
    population.append_attribute("Name").set_value(s.str().c_str());
    pugi::xml_node population_data = population.append_child("DataItem");
    population_data.append_attribute("Format").set_value("HDF");
    population_data.append_attribute("NumberType").set_value("Float");
    population_data.append_attribute("Precision")
        .set_value(static_cast<int>(sizeof(real)));
    population_data.append_attribute("Dimensions")
        .set_value(ivstr(extents_.reverse()).c_str());
    population_data.text().set((h5base + s.str()).c_str());
  }
}

template <class Lattice>
pugi::xml_node xdmf_writer<Lattice>::get_lattice() {
  std::stringstream xpath_stream;
  xpath_stream << "/Xdmf/Domain/Grid/Grid/Time[@Value = \"" << time_ << "\"]";
  pugi::xpath_node xtime =
      document_.select_single_node(xpath_stream.str().c_str());
  if (xtime) {
    return xtime.parent();
  } else {
    pugi::xpath_node lattice_collection =
        document_.select_single_node("/Xdmf/Domain/Grid");
    pugi::xml_node lattice = lattice_collection.node().append_child("Grid");
    lattice.append_attribute("Name").set_value("Lattice");

    pugi::xml_node time = lattice.append_child("Time");
    time.append_attribute("Value").set_value(time_);

    pugi::xml_node geometry = lattice.append_child("Geometry");
    geometry.append_attribute("Origin").set_value("");
    geometry.append_attribute("Type").set_value("ORIGIN_DXDYDZ");

    pugi::xml_node origin = geometry.append_child("DataItem");
    origin.append_attribute("Dimensions").set_value(lattice::d);
    origin.append_attribute("Format").set_value("XML");
    origin.text().set(ivstr(ivector::Zero()).c_str());

    pugi::xml_node dxdydz = geometry.append_child("DataItem");
    dxdydz.append_attribute("Dimensions").set_value(lattice::d);
    dxdydz.append_attribute("Format").set_value("XML");
    dxdydz.text().set(ivstr(ivector::Ones()).c_str());

    pugi::xml_node topology = lattice.append_child("Topology");
    topology.append_attribute("Dimensions")
        .set_value(ivstr(extents_.reverse()).c_str());
    assert(lattice::d == 2 || lattice::d == 3);
    if (lattice::d == 2)
      topology.append_attribute("Type").set_value("2DCoRectMesh");
    else if (lattice::d == 3)
      topology.append_attribute("Type").set_value("3DCoRectMesh");
    return lattice;
  }
}

template <class Lattice>
std::string xdmf_writer<Lattice>::hdf5_filename(const std::string& kind) const {
  std::size_t dotpos = filename_.find_last_of('.');
  std::stringstream s;
  s << filename_.substr(0, dotpos) << "_" << kind << "_" << std::setfill('0')
    << std::setw(10) << time_ << ".h5";
  return s.str();
}

template <class Lattice>
std::string xdmf_writer<Lattice>::basename(const std::string& filename) {
  std::size_t slashpos = filename.find_last_of('/');
  if (slashpos == std::string::npos) return filename;
  return filename.substr(slashpos + 1);
}

template <class Lattice>
std::string xdmf_writer<Lattice>::ivstr(const ivector& iv) {
  std::stringstream s;
  for (int i = 0; i < iv.size() - 1; ++i) s << iv(i) << " ";
  s << iv[iv.size() - 1];
  return s.str();
}

}  // io

}  // labicex

#endif  // LABICE_IO_SERIAL_XDMF

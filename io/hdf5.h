#ifndef LABICEX_IO_SERIAL_HDF5_H
#define LABICEX_IO_SERIAL_HDF5_H

#include <cstdlib>
#include <sstream>
#include <string>

#include <hdf5.h>
#include <mpi.h>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>

#include "base/soa_storage.h"

#define LABICEX_HDF5_CHECK(x) check((x), __FILE__, __LINE__)

namespace labicex {

namespace io {

template <class T>
struct hdf5_type_traits;

template <>
struct hdf5_type_traits<float> {
  static hid_t native_id() { return H5T_NATIVE_FLOAT; }
};

template <>
struct hdf5_type_traits<double> {
  static hid_t native_id() { return H5T_NATIVE_DOUBLE; }
};

template <>
struct hdf5_type_traits<int> {
  static hid_t native_id() { return H5T_NATIVE_INT; }
};

class hdf5_base {
 public:
  virtual ~hdf5_base() {}

  template <class Ivector, class ValueType>
  void write(const std::string& name, const Ivector& extents,
             const ValueType* data);

  template <class Ivector, class ValueType>
  void write_haloed(const std::string& name, const Ivector& extents,
                    int halo_width, const Ivector& full_extents,
                    const Ivector& offset, const ValueType* data);

  template <class ValueType, class Indexer>
  void write_single(const std::string& name,
                    const pinned_soa_storage<ValueType, Indexer>& storage,
                    int array_index);

  template <class ValueType, class Indexer, class Ivector>
  void write_single_haloed(
      const std::string& name,
      const pinned_soa_storage<ValueType, Indexer>& storage, int array_index,
      int halo_width, const Ivector& full_extents, const Ivector& offset);

  template <class ValueType, class Indexer>
  void write_all(const std::string& basename,
                 const pinned_soa_storage<ValueType, Indexer>& storage);

  template <class ValueType, class Indexer, class Ivector>
  void write_all_haloed(const std::string& basename,
                        const pinned_soa_storage<ValueType, Indexer>& storage,
                        int halo_width, const Ivector& full_extents,
                        const Ivector& offset);

  boost::shared_ptr<hdf5_base> create_group(const std::string& name) const;

  hid_t id() const { return id_; }
  std::string filename() const;

 protected:
  hdf5_base(hid_t id) : id_(id) {}

  template <class T>
  static T check(T ret, const std::string& file, int line) {
    if (ret < 0) {
      std::cerr << "HDF5 error in file " << file << ":" << line << std::endl;
      std::abort();
    }
    return ret;
  }

 private:
  hid_t id_;
};

typedef boost::shared_ptr<hdf5_base> hdf5_ptr;

class hdf5_group : public hdf5_base {
 public:
  hdf5_group(const hdf5_base& parent, const std::string& name);
  ~hdf5_group();
};

class hdf5_file : public hdf5_base {
 public:
  hdf5_file(const std::string& filename);
  ~hdf5_file();

  static hdf5_ptr create(const std::string& filename);
};

class hdf5_parallel_file : public hdf5_base {
 public:
  hdf5_parallel_file(const std::string& filename, MPI_Comm comm);
  ~hdf5_parallel_file();

  static hdf5_ptr create(const std::string& filename, MPI_Comm comm);

 private:
  static hid_t get_id(const std::string& filename, MPI_Comm comm);
};

template <class Ivector, class ValueType>
void hdf5_base::write(const std::string& name, const Ivector& extents,
                      const ValueType* data) {
  typedef typename vector_traits<Ivector>::template changed_type<hsize_t>::type
      hvector;

  hvector hextents = extents.reverse().template cast<hsize_t>();

  hid_t filespace = LABICEX_HDF5_CHECK(
      H5Screate_simple(hextents.size(), hextents.data(), NULL));
  hid_t datatype = hdf5_type_traits<ValueType>::native_id();
  hid_t dataset =
      LABICEX_HDF5_CHECK(H5Dcreate(id(), name.c_str(), datatype, filespace,
                                   H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));

  LABICEX_HDF5_CHECK(
      H5Dwrite(dataset, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, data));

  LABICEX_HDF5_CHECK(H5Dclose(dataset));
  LABICEX_HDF5_CHECK(H5Sclose(filespace));
}

template <class Ivector, class ValueType>
void hdf5_base::write_haloed(const std::string& name, const Ivector& extents,
                             int halo_width, const Ivector& full_extents,
                             const Ivector& offset, const ValueType* data) {
  typedef typename vector_traits<Ivector>::template changed_type<hsize_t>::type
      hvector;

  hvector hfull_extents = full_extents.reverse().template cast<hsize_t>();

  hid_t filespace = LABICEX_HDF5_CHECK(
      H5Screate_simple(hfull_extents.size(), hfull_extents.data(), NULL));
  hid_t datatype = hdf5_type_traits<ValueType>::native_id();
  hid_t dataset =
      LABICEX_HDF5_CHECK(H5Dcreate(id(), name.c_str(), datatype, filespace,
                                   H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));

  hvector hoffset = offset.reverse().template cast<hsize_t>();
  hvector hextents = extents.reverse().template cast<hsize_t>();
  hvector hhalo_offset(halo_width);
  hvector hnohalo_extents = hextents - 2 * halo_width;
  assert((hnohalo_extents > 0).all());
  assert((hoffset + hnohalo_extents <= hfull_extents).all());

  LABICEX_HDF5_CHECK(H5Sselect_hyperslab(filespace, H5S_SELECT_SET,
                                         hoffset.data(), NULL,
                                         hnohalo_extents.data(), NULL));

  hid_t memspace = LABICEX_HDF5_CHECK(
      H5Screate_simple(hextents.size(), hextents.data(), NULL));
  LABICEX_HDF5_CHECK(H5Sselect_hyperslab(memspace, H5S_SELECT_SET,
                                         hhalo_offset.data(), NULL,
                                         hnohalo_extents.data(), NULL));

  hid_t plist = LABICEX_HDF5_CHECK(H5Pcreate(H5P_DATASET_XFER));
  LABICEX_HDF5_CHECK(H5Pset_dxpl_mpio(plist, H5FD_MPIO_COLLECTIVE));
  LABICEX_HDF5_CHECK(
      H5Dwrite(dataset, datatype, memspace, filespace, plist, data));

  LABICEX_HDF5_CHECK(H5Pclose(plist));
  LABICEX_HDF5_CHECK(H5Sclose(memspace));
  LABICEX_HDF5_CHECK(H5Dclose(dataset));
  LABICEX_HDF5_CHECK(H5Sclose(filespace));
}

template <class ValueType, class Indexer>
void hdf5_base::write_single(
    const std::string& name,
    const pinned_soa_storage<ValueType, Indexer>& storage, int array_index) {
  write(name, storage.extents(), storage.array_start(array_index));
}

template <class ValueType, class Indexer, class Ivector>
void hdf5_base::write_single_haloed(
    const std::string& name,
    const pinned_soa_storage<ValueType, Indexer>& storage, int array_index,
    int halo_width, const Ivector& full_extents, const Ivector& offset) {
  write_haloed(name, storage.extents(), halo_width, full_extents, offset,
               storage.array_start(array_index));
}

template <class ValueType, class Indexer>
void hdf5_base::write_all(
    const std::string& basename,
    const pinned_soa_storage<ValueType, Indexer>& storage) {
  for (int i = 0; i < Indexer::array_count; ++i) {
    std::stringstream name;
    name << basename << i;
    write_single(name.str(), storage, i);
  }
}

template <class ValueType, class Indexer, class Ivector>
void hdf5_base::write_all_haloed(
    const std::string& basename,
    const pinned_soa_storage<ValueType, Indexer>& storage, int halo_width,
    const Ivector& full_extents, const Ivector& offset) {
  for (int i = 0; i < Indexer::array_count; ++i) {
    std::stringstream name;
    name << basename << i;
    write_single_haloed(name.str(), storage, i, halo_width, full_extents,
                        offset);
  }
}

}  // io

}  // labicex

#undef LABICEX_HDF5_CHECK

#endif  // LABICEX_IO_SERIAL_HDF5_H

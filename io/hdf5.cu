#include "io/hdf5.h"

#define LABICEX_HDF5_CHECK(x) check((x), __FILE__, __LINE__)

namespace labicex {

namespace io {

hdf5_ptr hdf5_base::create_group(const std::string& name) const {
  return hdf5_ptr(new hdf5_group(*this, name));
}

std::string hdf5_base::filename() const {
  char buffer[100];
  hsize_t size = LABICEX_HDF5_CHECK(H5Fget_name(id(), buffer, 100));
  return std::string(buffer, size);
}

hdf5_group::hdf5_group(const hdf5_base& parent, const std::string& name)
    : hdf5_base(LABICEX_HDF5_CHECK(H5Gcreate(
          parent.id(), name.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT))) {}

hdf5_group::~hdf5_group() { LABICEX_HDF5_CHECK(H5Gclose(id())); }

hdf5_file::hdf5_file(const std::string& filename)
    : hdf5_base(LABICEX_HDF5_CHECK(H5Fcreate(filename.c_str(), H5F_ACC_TRUNC,
                                             H5P_DEFAULT, H5P_DEFAULT))) {}

hdf5_file::~hdf5_file() { LABICEX_HDF5_CHECK(H5Fclose(id())); }

hdf5_ptr hdf5_file::create(const std::string& filename) {
  return hdf5_ptr(new hdf5_file(filename));
}

hdf5_parallel_file::hdf5_parallel_file(const std::string& filename,
                                       MPI_Comm comm)
    : hdf5_base(get_id(filename, comm)) {}

hdf5_parallel_file::~hdf5_parallel_file() {
  LABICEX_HDF5_CHECK(H5Fclose(id()));
}

hdf5_ptr hdf5_parallel_file::create(const std::string& filename,
                                    MPI_Comm comm) {
  return hdf5_ptr(new hdf5_parallel_file(filename, comm));
}

hid_t hdf5_parallel_file::get_id(const std::string& filename, MPI_Comm comm) {
  hid_t plist = LABICEX_HDF5_CHECK(H5Pcreate(H5P_FILE_ACCESS));
  LABICEX_HDF5_CHECK(H5Pset_fapl_mpio(plist, comm, MPI_INFO_NULL));
  hid_t id = LABICEX_HDF5_CHECK(
      H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist));
  LABICEX_HDF5_CHECK(H5Pclose(plist));
  return id;
}

}  // io

}  // labicex

#undef LABICEX_HDF5_CHECK
